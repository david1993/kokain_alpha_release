#-------------------------------------------------
#
# Project created by QtCreator 2014-01-15T00:32:55
#
#-------------------------------------------------

QT += core gui sql network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kokain_alpha_release
TEMPLATE = app


SOURCES += main.cpp\
        main_window.cpp \
    core/attachment.cpp \
    core/comment.cpp \
    core/entity.cpp \
    core/estimation.cpp \
    core/plan.cpp \
    core/project.cpp \
    core/relationship.cpp \
    core/shareable.cpp \
    core/task.cpp \
    core/task_property_definitions.cpp \
    core/team.cpp \
    core/user.cpp \
    gui/action_button_block.cpp \
    gui/activity_board_widget.cpp \
    gui/calendars_general_widget.cpp \
    gui/circle_progress_bar.cpp \
    gui/conversations_general_widget.cpp \
    gui/custom_button.cpp \
    gui/custom_combobox.cpp \
    gui/custom_label.cpp \
    gui/custom_line_edit.cpp \
    gui/custom_menu.cpp \
    gui/custom_widget_style.cpp \
    gui/files_general_widget.cpp \
    gui/flow_layout.cpp \
    gui/icon_button.cpp \
    gui/main_window_central_widget.cpp \
    gui/my_activity_widget.cpp \
    gui/people_general_widget.cpp \
    gui/project_item_widget.cpp \
    gui/project_widget.cpp \
    gui/projects_general_widget.cpp \
    gui/settings_general_widget.cpp \
    gui/tasks_general_widget.cpp \
    gui/terminal_widget.cpp \
    gui/timer_widget.cpp \
    gui/user_activity_widget.cpp \
    gui/user_indicator_widget.cpp \
    gui/window_top_panel.cpp \
    network/connection.cpp \
    network/network_manager.cpp \
    network/package.cpp \
    db/condition.cpp \
    db/db_manager.cpp \
    db/query.cpp \
    network/encryption/aes.cpp \
    network/encryption/crypt.cpp \
    network/encryption/encrypt_manager.cpp \
    db/cache_manager.cpp \
    settings/settings_manager.cpp \
    main_controller.cpp \
    calendar/calendar.cpp \
    calendar/calendar_piece.cpp \
    calendar/current_timezone.cpp \
    calendar/dialog_box.cpp \
    calendar/dragwidget.cpp \
    calendar/event.cpp \
    calendar/event_manager.cpp \
    calendar/event_start_end.cpp \
    calendar/events_list.cpp \
    calendar/flowlayout.cpp \
    calendar/label_edit.cpp \
    calendar/month.cpp \
    calendar/more_events.cpp \
    calendar/more_labels.cpp \
    calendar/multiple_labels.cpp \
    calendar/test.cpp \
    calendar/timezone.cpp \
    calendar/user_data.cpp \
    calendar/user_dialog_box.cpp \
    calendar/year.cpp \
    calendar/year_month.cpp \
    network/connection_manager.cpp \
    network/datagram.cpp \
    network/package_receiver.cpp \
    network/package_sender.cpp \
    network/reliability_manager.cpp \
    db/synchronizer.cpp \
    core/notification.cpp \
    core/notification_manager.cpp \
    gui/notification_list_widget.cpp \
    gui/notification_widget.cpp

HEADERS  += main_window.h \
    core/attachment.h \
    core/comment.h \
    core/entity.h \
    core/estimation.h \
    core/plan.h \
    core/project.h \
    core/relationship.h \
    core/shareable.h \
    core/task.h \
    core/task_property_definitions.h \
    core/team.h \
    core/user.h \
    gui/action_button_block.h \
    gui/activity_board_widget.h \
    gui/calendars_general_widget.h \
    gui/circle_progress_bar.h \
    gui/conversations_general_widget.h \
    gui/custom_button.h \
    gui/custom_combobox.h \
    gui/custom_label.h \
    gui/custom_line_edit.h \
    gui/custom_menu.h \
    gui/custom_widget_style.h \
    gui/files_general_widget.h \
    gui/flow_layout.h \
    gui/icon_button.h \
    gui/main_window_central_widget.h \
    gui/my_activity_widget.h \
    gui/people_general_widget.h \
    gui/project_item_widget.h \
    gui/project_widget.h \
    gui/projects_general_widget.h \
    gui/settings_general_widget.h \
    gui/tasks_general_widget.h \
    gui/terminal_widget.h \
    gui/timer_widget.h \
    gui/user_activity_widget.h \
    gui/user_indicator_widget.h \
    gui/window_top_panel.h \
    network/connection.h \
    network/controller.h \
    network/network_manager.h \
    network/package.h \
    db/condition.h \
    db/db_manager.h \
    db/query.h \
    network/encryption/aes.hpp \
    network/encryption/crypt.h \
    network/encryption/encrypt_manager.h \
    db/cache_manager.h \
    settings/settings_manager.h \
    main_controller.h \
    calendar/calendar.h \
    calendar/calendar_piece.h \
    calendar/current_timezone.h \
    calendar/dialog_box.h \
    calendar/dragwidget.h \
    calendar/event.h \
    calendar/event_manager.h \
    calendar/event_start_end.h \
    calendar/events_list.h \
    calendar/flowlayout.h \
    calendar/label_edit.h \
    calendar/month.h \
    calendar/more_events.h \
    calendar/more_labels.h \
    calendar/multiple_labels.h \
    calendar/test.h \
    calendar/timezone.h \
    calendar/user_data.h \
    calendar/user_dialog_box.h \
    calendar/year.h \
    calendar/year_month.h \
    network/connection_manager.h \
    network/datagram.h \
    network/package_receiver.h \
    network/package_sender.h \
    network/reliability_manager.h \
    db/synchronizer.h \
    core/notification.h \
    core/notification_manager.h \
    gui/notification_list_widget.h \
    gui/notification_widget.h
