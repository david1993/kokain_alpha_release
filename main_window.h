#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>

namespace kokain
{

namespace gui
{

class main_window_central_widget;

}

class main_window : public QMainWindow
{
    Q_OBJECT

public:
    main_window(QWidget *parent = 0);
    ~main_window();

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private slots:
    void handle_maximize_clicks();

private:
    gui::main_window_central_widget* m_central_widget;

private:
    void closeEvent(QCloseEvent* e = 0);

}; // class main_window

} // namespace kokain

#endif // MAIN_WINDOW_H
