#include "notification_list_widget.h"

#include <QCheckBox>
#include <QHBoxLayout>

#include "notification_widget.h"
#include "custom_label.h"
#include "flow_layout.h"
#include "core/notification.h"
#include "core/notification_manager.h"

namespace kokain {

namespace gui {

notification_list_widget::notification_list_widget(QWidget* parent)
    : QWidget(parent)
{
    this->initialize_();
}

notification_list_widget::~notification_list_widget()
{
}

void notification_list_widget::initialize_()
{
    this->create_widgets_();
    this->setup_layout_();
    this->make_connections_();

    m_manager = kokain::core::notification_manager::get_notification_manager_instance();
}

void notification_list_widget::create_widgets_()
{
    this->m_checkbox = new QCheckBox(this);
    this->m_mark_as_read_button = new custom_label("Mark as read", this);
    this->m_mark_as_unread_button = new custom_label("Mark as unread", this);
    this->m_delete_button = new custom_label("Delete", this);
    this->m_more_button = new custom_label("more", this);
}

void notification_list_widget::setup_layout_()
{
    this->m_main_layout = new QHBoxLayout(this);
    flow_layout* panel_layout = new flow_layout();
    panel_layout->addWidget(m_checkbox);
    panel_layout->addWidget(m_mark_as_read_button);
    panel_layout->addWidget(m_mark_as_unread_button);
    panel_layout->addWidget(m_delete_button);
    flow_layout* notifications_layout = new flow_layout();
    m_main_layout->addLayout(panel_layout);
    m_main_layout->addLayout(notifications_layout);
}

void notification_list_widget::make_connections_()
{
    connect(m_checkbox, SIGNAL(clicked()),
            this, SLOT(set_checkboxs_state()));
    connect(m_mark_as_read_button, SIGNAL(clicked()),
            this, SLOT(mark_as_read_selected()));
    connect(m_mark_as_unread_button, SIGNAL(clicked()),
            this, SLOT(mark_as_unread_selected()));
    connect(m_delete_button, SIGNAL(clicked()),
            this, SLOT(delete_selected()));
    connect(m_more_button, SIGNAL(clicked()),
            this, SIGNAL(signal_more_clicked()));
}

void notification_list_widget::add_notification(notification_widget* the_notification)
{
    Q_ASSERT(NULL == the_notification);
    connect(the_notification, SIGNAL(signal_sender_clicked(QString)),
            this, SIGNAL(signal_sender_clicked(QString)));
    connect(the_notification, SIGNAL(signal_text_clicked(QString)),
            this, SIGNAL(signal_text_clicked(QString)));
    connect(the_notification, SIGNAL(signal_entity_clicked(QString)),
            this, SIGNAL(signal_entity_clicked(QString)));
    m_notification_list.push_front(the_notification);
}

void notification_list_widget::set_checkboxs_state()
{
    Qt::CheckState state;
    if(m_checkbox->isChecked()){
        state = Qt::Checked;
    } else {
        state = Qt::Unchecked;
    }
    notification_widget* tmp_pnot;
    foreach (tmp_pnot, m_notification_list) {
        tmp_pnot->set_check_state(state);
    }
}

void notification_list_widget::mark_as_read_selected()
{
    notification_widget* tmp_pnot;
    foreach (tmp_pnot, m_notification_list) {
        if(tmp_pnot->is_checked()){
            tmp_pnot->set_notification_status(notification_widget::READ);
            QString tmp_id = tmp_pnot->get_notification_id();
            this->get_notification_manager_instance()->set_as_read_notification(tmp_id);
        }
    }
}

void notification_list_widget::mark_as_unread_selected()
{
    notification_widget* tmp_pnot;
    foreach (tmp_pnot, m_notification_list) {
        if(tmp_pnot->is_checked()){
            tmp_pnot->set_notification_status(notification_widget::UNREAD);
            QString tmp_id = tmp_pnot->get_notification_id();
            this->get_notification_manager_instance()->set_as_unread_notification(tmp_id);
        }
    }
}

void notification_list_widget::delete_selected()
{
    qint32 num_of_del_not = 0;
    notification_widget* tmp_pnot;
    foreach (tmp_pnot, m_notification_list) {
        if(tmp_pnot->is_checked()){
            QString tmp_id = tmp_pnot->get_notification_id();
            this->get_notification_manager_instance()->remove_notification(tmp_id);
            // delete widget
            ++num_of_del_not;
        }
    }
    if(num_of_del_not){
        this->get_notification_manager_instance()->get_notifications_from_db(num_of_del_not);
    }
}

core::notification_manager* notification_list_widget::get_notification_manager_instance()
{
    Q_ASSERT(NULL == m_manager);
    return m_manager;
}

} //gui
} //kokain
