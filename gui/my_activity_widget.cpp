#include "my_activity_widget.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

#include "timer_widget.h"
#include "custom_button.h"

namespace kokain
{

namespace gui
{

const qint32 my_activity_widget::k_fixed_width = 300;
const qint32 my_activity_widget::k_fixed_height = 120;
const qint32 my_activity_widget::k_button_fixed_size = 65;

my_activity_widget::my_activity_widget(QWidget *parent) :
    QWidget(parent),
    m_my_activity_lbl(NULL),
    m_working_on_wgt(NULL),
    m_total_working_wgt(NULL),
    m_pause_btn(NULL),
    m_complete_btn(NULL),
    m_main_layout(NULL),
    m_paused(true)
{
    initialize_();
}

void my_activity_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();

    m_working_on_wgt->pause_timer();
    m_total_working_wgt->pause_timer();
    setFixedSize(k_fixed_width, k_fixed_height);
    setAutoFillBackground(true);
    setPalette(QPalette(QColor(82, 110, 138)));
}

void my_activity_widget::create_widgets_()
{
    m_my_activity_lbl = new QLabel(tr("MY ACTIVITY TITLE"), this);
    m_working_on_wgt = new timer_widget(this);
    m_total_working_wgt = new timer_widget(this);
    Q_ASSERT(0 != m_my_activity_lbl);
    Q_ASSERT(0 != m_working_on_wgt);
    Q_ASSERT(0 != m_total_working_wgt);
    m_my_activity_lbl->setStyleSheet("color: white;");
    m_working_on_wgt->setStyleSheet("color: white;");
    m_total_working_wgt->setStyleSheet("color: white;");
    m_working_on_wgt->set_progress(true);
    m_working_on_wgt->set_title(tr("working on"));
    m_total_working_wgt->set_title(tr("total working hours"));

    create_buttons_();
}

void my_activity_widget::create_buttons_()
{
    m_pause_btn = new custom_button(this);
    m_complete_btn = new custom_button(this);
    Q_ASSERT(0 != m_pause_btn);
    Q_ASSERT(0 != m_complete_btn);
    m_pause_btn->setText("START");
    m_pause_btn->set_category(custom_button_category_values::INVERSE);
    m_pause_btn->set_rounded(0);
    m_pause_btn->setFixedSize(k_button_fixed_size, k_button_fixed_size);
    m_complete_btn->setText("DONE");
    m_complete_btn->set_category(custom_button_category_values::INVERSE);
    m_complete_btn->set_rounded(0);
    m_complete_btn->setFixedSize(k_button_fixed_size, k_button_fixed_size);
}

void my_activity_widget::setup_layout_()
{
    m_main_layout = new QHBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);

    QHBoxLayout* timers_layout = new QHBoxLayout();
    Q_ASSERT(0 != timers_layout);
    timers_layout->addWidget(m_working_on_wgt);
    timers_layout->addWidget(m_total_working_wgt);

    QVBoxLayout* labels_layout = new QVBoxLayout();
    Q_ASSERT(0 != labels_layout);
    labels_layout->addWidget(m_my_activity_lbl);
    labels_layout->addLayout(timers_layout);

    QVBoxLayout* buttons_layout = new QVBoxLayout();
    Q_ASSERT(0 != buttons_layout);
    buttons_layout->setContentsMargins(0, 0, 0, 0);
    buttons_layout->addWidget(m_pause_btn);
    buttons_layout->addWidget(m_complete_btn);

    m_main_layout->addSpacing(5);
    m_main_layout->addLayout(labels_layout);
    m_main_layout->addLayout(buttons_layout);
}

void my_activity_widget::make_connections_()
{
    connect(m_pause_btn, SIGNAL(clicked()), this, SLOT(on_pause_button_clicked()));
    connect(m_complete_btn, SIGNAL(clicked()), this, SLOT(on_complete_button_clicked()));
}

void my_activity_widget::on_pause_button_clicked()
{
    if (m_paused) {
        m_pause_btn->setText(tr("RESUME"));
        m_working_on_wgt->resume_timer();
        m_total_working_wgt->resume_timer();
    } else {
        m_pause_btn->setText(tr("PAUSE"));
        m_working_on_wgt->pause_timer();
        m_total_working_wgt->pause_timer();
    }
    m_paused = !m_paused;
}

void my_activity_widget::on_complete_button_clicked()
{
    m_working_on_wgt->stop_timer();
    m_total_working_wgt->pause_timer();
}

} // namespace gui

} // namespace kokain
