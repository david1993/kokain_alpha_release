#include "terminal_widget.h"

#include <QHBoxLayout>
#include <QComboBox>
#include <QDebug>

#include "custom_button.h"
#include "custom_line_edit.h"
#include "icon_button.h"

namespace kokain
{

namespace gui
{

const qint32 terminal_widget::k_fixed_height = 40;
const qint32 terminal_widget::k_fixed_width = 60;
const qint32 terminal_widget::k_spacing_default_value = -1;

terminal_widget::terminal_widget(QWidget *parent) :
    QWidget(parent),
    m_editable(NULL),
    m_execute_btn(NULL)
{
    initialize_();
}

void terminal_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void terminal_widget::create_widgets_()
{
    create_editable_widget_();

    m_execute_btn = new custom_button(tr("GO!"), this);
    Q_ASSERT(0 != m_execute_btn);
    m_execute_btn->set_category(custom_button_category_values::SUCCESS);
    m_execute_btn->set_rounded(0);
    m_execute_btn->setFixedHeight(k_fixed_height);
    m_execute_btn->setFixedWidth(k_fixed_width);
}

void terminal_widget::create_editable_widget_()
{
    m_editable = new custom_line_edit(this);
    Q_ASSERT(0 != m_editable);
    m_editable->setText("type command or search for something...");
    m_editable->setFixedHeight(k_fixed_height);
}

void terminal_widget::setup_layout_()
{
    m_main_layout = new QHBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);

    Q_ASSERT(0 != m_editable);
    Q_ASSERT(0 != m_execute_btn);
    QLabel* indicator = new QLabel(this);
    indicator->setStyleSheet("QLabel{background-color: #2ecc71;}");
    indicator->setFixedSize(5, m_editable->height());
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->setSpacing(0);
    m_main_layout->addWidget(indicator);
    m_main_layout->addWidget(m_editable);
    m_main_layout->addSpacing(k_spacing_default_value);
    m_main_layout->addWidget(m_execute_btn);
}

void terminal_widget::make_connections_()
{
    connect(m_execute_btn, SIGNAL(clicked()), m_editable, SLOT(on_return_pressed()));
}

} // namespace gui

} // namespace kokain
