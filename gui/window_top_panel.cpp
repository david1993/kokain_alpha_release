#include "window_top_panel.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QMouseEvent>
#include <QWidgetAction>
#include <QLineEdit>

#include "icon_button.h"
#include "custom_menu.h"
#include "user_indicator_widget.h"

namespace kokain
{

namespace gui
{

const qint32 window_top_panel::k_window_top_panel_fixed_height = 100;
const qint32 window_top_panel::k_button_max_width = 100;
const qint32 window_top_panel::k_buttons_layout_stretch_value = 5;
const qint32 window_top_panel::k_button_fixed_height = 58;
const qint32 window_top_panel::k_user_name_fixed_height = 20;
const qint32 window_top_panel::k_top_panel_fixed_height = 60;
const qint32 window_top_panel::k_spacing_default_value = 1;
const qint32 window_top_panel::k_user_avatar_size = 40;
const qint32 window_top_panel::k_button_indicator_width = 25;

const QString window_top_panel::k_logo_path = "./logo.jpg";
const QString window_top_panel::k_close_btn_icon_path = "./window_panel_buttons/close_window.png";
const QString window_top_panel::k_close_btn_pressed_icon_path = "./window_panel_buttons/close_window_pressed.png";
const QString window_top_panel::k_maximize_btn_icon_path = "./window_panel_buttons/maximize.png";
const QString window_top_panel::k_maximize_btn_pressed_icon_path = "./window_panel_buttons/maximize_pressed.png";
const QString window_top_panel::k_minimize_btn_icon_path = "./window_panel_buttons/minimize.png";
const QString window_top_panel::k_user_avatar_path = "./user/avatar.jpg";

//const QColor window_top_panel::k_top_panel_bg_color = QColor(220, 221, 216);
const QColor window_top_panel::k_top_panel_bg_color = QColor(53,75,94);

window_top_panel::window_top_panel(QWidget *parent)
    : QWidget(parent),
      m_active_view(PROJECTS),
      m_manage_generic_btn(NULL),
      m_main_layout(NULL),
      m_buttons_layout(NULL),
      m_logo_lbl(NULL),
      m_projects_manage_done_btn(NULL),
      m_projects_edit_btn(NULL),
      m_projects_remove_btn(NULL),
      m_projects_share_btn(NULL),
      m_tasks_manage_done_btn(NULL),
      m_tasks_edit_btn(NULL),
      m_tasks_share_btn(NULL),
      m_tasks_more_btn(NULL),
      m_tasks_more_actions_menu(NULL),
      m_tasks_move_action(NULL),
      m_tasks_copy_action(NULL),
      m_tasks_merge_action(NULL),
      m_tasks_archive_action(NULL),
      m_tasks_remove_action(NULL),
      m_calendars_btn(NULL),
      m_calendars_menu(NULL),
      m_calendars_menu_btn(NULL),
      m_my_calendar_action(NULL),
      m_team_calendar_action(NULL),
      m_project_calendar_action(NULL),
      m_back_calendar_btn(NULL),
      m_jump_to_date_btn(NULL),
      m_calendar_views_menu(NULL),
      m_calendar_views_menu_btn(NULL),
      m_month_view_action(NULL),
      m_week_view_action(NULL),
      m_day_view_action(NULL),
      m_calendars_more_menu(NULL),
      m_calendars_more_menu_btn(NULL),
      m_calendars_edit_action(NULL),
      m_calendars_share_action(NULL),
      m_calendars_remove_action(NULL),
      m_events_btn(NULL),
      m_events_more_menu_btn(NULL),
      m_events_more_menu(NULL),
      m_events_edit_action(NULL),
      m_events_share_action(NULL),
      m_events_remove_action(NULL),
      m_people_manage_done_btn(NULL),
      m_people_edit_btn(NULL),
      m_people_remove_btn(NULL),
      m_people_more_btn(NULL),
      m_people_more_menu(NULL),
      m_people_share_action(NULL),
      m_people_merge_action(NULL),
      m_people_copy_action(NULL),
      m_conversations_manage_done_btn(NULL),
      m_conversations_share_btn(NULL),
      m_conversations_remove_btn(NULL),
      m_settings_general_btn(NULL),
      m_settings_projects_btn(NULL),
      m_settings_tasks_btn(NULL),
      m_settings_calendar_btn(NULL),
      m_settings_people_btn(NULL),
      m_settings_conversations_btn(NULL),
      m_settings_files_btn(NULL),
      m_close_btn(NULL),
      m_maximize_btn(NULL),
      m_minimize_btn(NULL),
      m_user_avatar(NULL),
      m_user_name_lbl(NULL),
      m_user_and_functional_btn_layout(NULL),
      m_user_indicator_wgt(NULL),
      m_moving(false)
{
    initialize_();
}

void window_top_panel::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();

    setFixedHeight(k_window_top_panel_fixed_height);
    setAutoFillBackground(true);
    setPalette(QPalette(k_top_panel_bg_color));
    setFixedHeight(k_top_panel_fixed_height);
}

void window_top_panel::create_widgets_()
{
    m_manage_generic_btn = new custom_button(tr("MANAGE"), this);
    m_manage_generic_btn->set_category(custom_button_category_values::DANGER);
    m_manage_generic_btn->setFixedSize(k_button_max_width, k_button_fixed_height);
    m_manage_generic_btn->set_rounded(0);

    create_logo_widget_();
    //create_user_and_window_functional_buttons_();
    create_user_indicator_widget_();
    create_projects_management_buttons_();
    create_tasks_management_buttons_();
    create_calendar_management_buttons_();
    create_people_management_buttons_();
    create_conversations_management_buttons_();
    create_files_management_buttons_();
    create_settings_management_buttons_();
    hide_fuckin_buttons_all_();
}

void window_top_panel::create_user_indicator_widget_()
{
    m_user_indicator_wgt = new user_indicator_widget(this);
    Q_ASSERT(0 != m_user_indicator_wgt);
}

void window_top_panel::hide_fuckin_buttons_all_()
{
    m_manage_generic_btn->hide();
    m_projects_manage_done_btn->hide();
    m_projects_edit_btn->hide();
    m_projects_share_btn->hide();
    m_projects_remove_btn->hide();

    m_tasks_manage_done_btn->hide();
    m_tasks_edit_btn->hide();
    m_tasks_share_btn->hide();
    m_tasks_more_btn->hide();

    m_calendars_btn->hide();
    m_calendars_menu_btn->hide();
    m_back_calendar_btn->hide();
    m_jump_to_date_btn->hide();
    m_calendar_views_menu_btn->hide();
    m_calendars_more_menu_btn->hide();
    m_events_btn->hide();
    m_events_more_menu_btn->hide();

    m_people_manage_done_btn->hide();
    m_people_edit_btn->hide();
    m_people_remove_btn->hide();
    m_people_more_btn->hide();

    m_conversations_manage_done_btn->hide();
    m_conversations_share_btn->hide();
    m_conversations_remove_btn->hide();

    m_files_manage_done_btn->hide();
    m_files_download_btn->hide();
    m_files_edit_btn->hide();
    m_files_share_btn->hide();
    m_files_remove_btn->hide();

    m_settings_general_btn->hide();
    m_settings_projects_btn->hide();
    m_settings_tasks_btn->hide();
    m_settings_calendar_btn->hide();
    m_settings_people_btn->hide();
    m_settings_conversations_btn->hide();
    m_settings_files_btn->hide();
}

void window_top_panel::create_logo_widget_()
{
    m_logo_lbl = new icon_button(this);
    Q_ASSERT(0 != m_logo_lbl);
    m_logo_lbl->set_normal_state_pixmap(QPixmap(k_logo_path));
    m_logo_lbl->setFixedSize(k_top_panel_fixed_height, k_top_panel_fixed_height);
}

void window_top_panel::setup_layout_()
{
    m_main_layout = new QHBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->setSpacing(k_spacing_default_value);

    Q_ASSERT(0 != m_logo_lbl);
    m_main_layout->addWidget(m_logo_lbl);

    setup_buttons_layout_();
    m_main_layout->addLayout(m_buttons_layout);

    //setup_user_and_functional_buttons_layout_();
    //m_main_layout->addLayout(m_user_and_functional_btn_layout);
    m_main_layout->addWidget(m_user_indicator_wgt);
    m_main_layout->addSpacing(k_spacing_default_value);
}

void window_top_panel::setup_buttons_layout_()
{
    m_buttons_layout = new QHBoxLayout();
    Q_ASSERT(0 != m_buttons_layout);
    Q_ASSERT(0 != m_manage_generic_btn);
    m_buttons_layout->addWidget(m_manage_generic_btn, 0, Qt::AlignLeft);
    m_manage_generic_btn->show();
}

void window_top_panel::clear_buttons_layout_()
{
    Q_ASSERT(0 != m_buttons_layout);
    while (!m_buttons_layout->isEmpty()) {
        m_buttons_layout->removeItem(m_buttons_layout->itemAt(0));
    }
    hide_fuckin_buttons_all_();
}

void window_top_panel::make_connections_()
{
    connect(m_manage_generic_btn, SIGNAL(clicked()), this, SLOT(on_active_view_change()));

    connect(m_projects_manage_done_btn, SIGNAL(clicked()), this, SLOT(on_projects_manage_done_clicked()));
    connect(m_projects_edit_btn, SIGNAL(clicked()), this, SIGNAL(projects_edit_button_clicked()));
    connect(m_projects_remove_btn, SIGNAL(clicked()), this, SIGNAL(projects_remove_button_clicked()));
    connect(m_projects_share_btn, SIGNAL(clicked()), this, SIGNAL(projects_share_button_clicked()));

    make_tasks_connections_();
    make_calendar_connections_();
    make_people_connections_();
    make_conversations_connections_();
    make_files_connections_();
    make_settings_connections_();
}

void window_top_panel::make_tasks_connections_()
{
    connect(m_tasks_manage_done_btn, SIGNAL(clicked()), this, SLOT(on_tasks_manage_done_clicked()));
    connect(m_tasks_edit_btn, SIGNAL(clicked()), this, SIGNAL(tasks_edit_button_clicked()));
    connect(m_tasks_share_btn, SIGNAL(clicked()), this, SIGNAL(tasks_share_button_clicked()));
    connect(m_tasks_move_action, SIGNAL(triggered()), this, SIGNAL(tasks_move_button_clicked()));
    connect(m_tasks_copy_action, SIGNAL(triggered()), this, SIGNAL(tasks_copy_button_clicked()));
    connect(m_tasks_merge_action, SIGNAL(triggered()), this, SIGNAL(tasks_merge_button_clicked()));
    connect(m_tasks_archive_action, SIGNAL(triggered()), this, SIGNAL(tasks_archive_button_clicked()));
    connect(m_tasks_remove_action, SIGNAL(triggered()), this, SIGNAL(tasks_remove_button_clicked()));
}

void window_top_panel::make_calendar_connections_()
{
    connect(m_calendars_btn, SIGNAL(clicked()), this, SIGNAL(calendars_button_clicked()));
    connect(m_calendars_btn, SIGNAL(clicked()), this, SLOT(on_calendars_button_clicked()));
    connect(m_my_calendar_action, SIGNAL(triggered()), this, SIGNAL(my_calendar_button_clicked()));
    connect(m_team_calendar_action, SIGNAL(triggered()), this, SIGNAL(team_calendar_button_clicked()));
    connect(m_project_calendar_action, SIGNAL(triggered()), this, SIGNAL(team_calendar_button_clicked()));
    connect(m_back_calendar_btn, SIGNAL(clicked()), this, SIGNAL(calendar_back_button_clicked()));
    connect(m_back_calendar_btn, SIGNAL(clicked()), this, SLOT(on_calendar_back_button_clicked()));
    connect(m_jump_to_date_btn, SIGNAL(returnPressed()), this, SLOT(on_calendar_jump_to_date_clicked()));
    connect(m_day_view_action, SIGNAL(triggered()), this, SIGNAL(calendar_day_view_button_clicked()));
    connect(m_week_view_action, SIGNAL(triggered()), this, SIGNAL(calendar_week_view_button_clicked()));
    connect(m_month_view_action, SIGNAL(triggered()), this, SIGNAL(calendar_month_view_button_clicked()));
    connect(m_calendars_edit_action, SIGNAL(triggered()), this, SIGNAL(calendars_edit_button_clicked()));
    connect(m_calendars_share_action, SIGNAL(triggered()), this, SIGNAL(calendars_share_button_clicked()));
    connect(m_calendars_remove_action, SIGNAL(triggered()), this, SIGNAL(calendars_remove_button_clicked()));

    connect(this, SIGNAL(calendar_month_view_button_clicked()), this, SLOT(on_calendar_month_view_clicked_()));
    connect(this, SIGNAL(calendar_week_view_button_clicked()), this, SLOT(on_calendar_week_view_clicked_()));
    connect(this, SIGNAL(calendar_day_view_button_clicked()), this, SLOT(on_calendar_day_view_clicked_()));

    connect(m_events_btn, SIGNAL(clicked()), this, SIGNAL(events_button_clicked()));
    connect(m_events_btn, SIGNAL(clicked()), this, SLOT(on_events_button_clicked()));
    connect(m_events_edit_action, SIGNAL(triggered()), this, SIGNAL(events_edit_button_clicked()));
    connect(m_events_share_action, SIGNAL(triggered()), this, SIGNAL(events_share_button_clicked()));
    connect(m_events_remove_action, SIGNAL(triggered()), this, SIGNAL(events_remove_button_clicked()));
}

void window_top_panel::make_people_connections_()
{
    connect(m_people_manage_done_btn, SIGNAL(clicked()), this, SIGNAL(people_manage_done_button_clicked()));
    connect(m_people_manage_done_btn, SIGNAL(clicked()), this, SLOT(on_people_manage_done_clicked()));
    connect(m_people_edit_btn, SIGNAL(clicked()), this, SIGNAL(people_edit_button_clicked()));
    connect(m_people_remove_btn, SIGNAL(clicked()), this, SIGNAL(people_edit_button_clicked()));
    connect(m_people_share_action, SIGNAL(triggered()), this, SIGNAL(people_share_button_clicked()));
    connect(m_people_merge_action, SIGNAL(triggered()), this, SIGNAL(people_merge_button_clicked()));
    connect(m_people_copy_action, SIGNAL(triggered()), this, SIGNAL(people_copy_button_clicked()));
}

void window_top_panel::make_conversations_connections_()
{
    connect(m_conversations_manage_done_btn, SIGNAL(clicked()), this, SIGNAL(conversations_manage_done_button_clicked()));
    connect(m_conversations_manage_done_btn, SIGNAL(clicked()), this, SLOT(on_conversations_manage_done_clicked()));
    connect(m_conversations_share_btn, SIGNAL(clicked()), this, SIGNAL(conversations_share_button_clicked()));
    connect(m_conversations_remove_btn, SIGNAL(clicked()), this, SIGNAL(conversations_remove_button_clicked()));
}

void window_top_panel::make_files_connections_()
{
    connect(m_files_manage_done_btn, SIGNAL(clicked()), this, SIGNAL(files_manage_done_button_clicked()));
    connect(m_files_manage_done_btn, SIGNAL(clicked()), this, SLOT(on_files_manage_done_clicked()));
    connect(m_files_download_btn, SIGNAL(clicked()), this, SIGNAL(files_download_button_clicked()));
    connect(m_files_edit_btn, SIGNAL(clicked()), this, SIGNAL(files_edit_button_clicked()));
    connect(m_files_share_btn, SIGNAL(clicked()), this, SIGNAL(files_share_button_clicked()));
    connect(m_files_remove_btn, SIGNAL(clicked()), this, SIGNAL(files_remove_button_clicked()));
}

void window_top_panel::make_settings_connections_()
{
    connect(m_settings_general_btn, SIGNAL(clicked()), this, SIGNAL(settings_general_button_clicked()));
    connect(m_settings_general_btn, SIGNAL(clicked()), this, SLOT(on_settings_general_button_clicked()));
    connect(m_settings_projects_btn, SIGNAL(clicked()), this, SIGNAL(settings_projects_button_clicked()));
    connect(m_settings_projects_btn, SIGNAL(clicked()), this, SLOT(on_settings_projects_button_clicked()));
    connect(m_settings_tasks_btn, SIGNAL(clicked()), this, SIGNAL(settings_tasks_button_clicked()));
    connect(m_settings_tasks_btn, SIGNAL(clicked()), this, SLOT(on_settings_tasks_button_clicked()));
    connect(m_settings_calendar_btn, SIGNAL(clicked()), this, SIGNAL(settings_calendar_buton_clicked()));
    connect(m_settings_calendar_btn, SIGNAL(clicked()), this, SLOT(on_settings_calendar_button_clicked()));
    connect(m_settings_people_btn, SIGNAL(clicked()), this, SIGNAL(settings_people_button_clicked()));
    connect(m_settings_people_btn, SIGNAL(clicked()), this, SLOT(on_settings_people_button_clicked()));
    connect(m_settings_conversations_btn, SIGNAL(clicked()), this, SIGNAL(settings_conversations_button_clicked()));
    connect(m_settings_conversations_btn, SIGNAL(clicked()), this, SLOT(on_settings_conversations_button_clicked()));
    connect(m_settings_files_btn, SIGNAL(clicked()), this, SIGNAL(settings_files_button_clicked()));
    connect(m_settings_files_btn, SIGNAL(clicked()), this, SLOT(on_settings_files_button_clicked()));
}

void window_top_panel::create_projects_management_buttons_()
{
    m_projects_manage_done_btn = new custom_button(tr("DONE"), custom_button_type_values::DEFAULT,
                                                   custom_button_category_values::SUCCESS, this);
    m_projects_edit_btn = new custom_button(tr("EDIT"), custom_button_type_values::LARGE,
                                            custom_button_category_values::WARNING, this);
    m_projects_remove_btn = new custom_button(tr("REMOVE"), custom_button_type_values::LARGE,
                                              custom_button_category_values::DANGER, this);
    m_projects_share_btn = new custom_button(tr("SHARE"), custom_button_type_values::LARGE,
                                             custom_button_category_values::HIGHLIGHT, this);
    Q_ASSERT(0 != m_projects_manage_done_btn);
    Q_ASSERT(0 != m_projects_edit_btn);
    Q_ASSERT(0 != m_projects_remove_btn);
    Q_ASSERT(0 != m_projects_share_btn);
    apply_default_settings_to_button_(m_projects_manage_done_btn);
    apply_default_settings_to_button_(m_projects_edit_btn);
    apply_default_settings_to_button_(m_projects_remove_btn);
    apply_default_settings_to_button_(m_projects_share_btn);
}

void window_top_panel::create_tasks_management_buttons_()
{
    m_tasks_manage_done_btn = new custom_button(tr("DONE"), custom_button_type_values::LARGE,
                                                custom_button_category_values::SUCCESS, this);
    m_tasks_edit_btn = new custom_button(tr("EDIT"), custom_button_type_values::LARGE,
                                         custom_button_category_values::WARNING, this);
    m_tasks_share_btn = new custom_button(tr("SHARE"), custom_button_type_values::LARGE,
                                          custom_button_category_values::HIGHLIGHT, this);
    m_tasks_more_btn = new custom_button(tr("MORE"), custom_button_type_values::LARGE,
                                         custom_button_category_values::INVERSE, this);

    create_tasks_more_actions_menu_();

    Q_ASSERT(0 != m_tasks_manage_done_btn);
    Q_ASSERT(0 != m_tasks_edit_btn);
    Q_ASSERT(0 != m_tasks_share_btn);
    Q_ASSERT(0 != m_tasks_more_btn);
    apply_default_settings_to_button_(m_tasks_manage_done_btn);
    apply_default_settings_to_button_(m_tasks_edit_btn);
    apply_default_settings_to_button_(m_tasks_share_btn);
    apply_default_settings_to_button_(m_tasks_more_btn);
    m_tasks_more_btn->set_category(custom_button_category_values::DEFAULT);
    m_tasks_more_btn->setMenu(m_tasks_more_actions_menu);
}

void window_top_panel::create_tasks_more_actions_menu_()
{
    m_tasks_more_actions_menu = new custom_menu(this);
    m_tasks_move_action = new QAction(tr("MOVE"), this);
    m_tasks_copy_action = new QAction(tr("COPY"), this);
    m_tasks_merge_action = new QAction(tr("MERGE"), this);
    m_tasks_archive_action = new QAction(tr("ARCHIVE"), this);
    m_tasks_remove_action = new QAction(tr("REMOVE"), this);
    Q_ASSERT(0 != m_tasks_more_actions_menu);
    Q_ASSERT(0 != m_tasks_move_action);
    Q_ASSERT(0 != m_tasks_copy_action);
    Q_ASSERT(0 != m_tasks_merge_action);
    Q_ASSERT(0 != m_tasks_archive_action);
    Q_ASSERT(0 != m_tasks_remove_action);
    m_tasks_more_actions_menu->addAction(m_tasks_move_action);
    m_tasks_more_actions_menu->addAction(m_tasks_copy_action);
    m_tasks_more_actions_menu->addAction(m_tasks_merge_action);
    m_tasks_more_actions_menu->addSeparator();
    m_tasks_more_actions_menu->addAction(m_tasks_archive_action);
    m_tasks_more_actions_menu->addAction(m_tasks_remove_action);

}

void window_top_panel::create_calendar_management_buttons_()
{
    m_calendars_btn = new custom_button(tr("CALENDARS"), custom_button_type_values::LARGE,
                                        custom_button_category_values::PRIMARY, this);
    m_calendars_menu = new custom_menu(this);
    m_calendars_menu_btn = new custom_button(" ", this);
    m_my_calendar_action = new QAction(tr("MY CALENDAR"), this);
    m_team_calendar_action = new QAction(tr("TEAM CALENDAR"), this);
    m_project_calendar_action = new QAction(tr("PROJECT CALENDAR"), this);
    m_back_calendar_btn = new custom_button(tr("<- BACK"), custom_button_type_values::LARGE,
                                            custom_button_category_values::PRIMARY, this);
    m_jump_to_date_btn = new QLineEdit("type date", this);
    m_calendar_views_menu = new custom_menu(this);
    m_calendar_views_menu_btn = new custom_button(tr("MONTH VIEW"), custom_button_type_values::LARGE,
                                                  custom_button_category_values::INVERSE, this);
    m_month_view_action = new QAction(tr("MONTH VIEW"), this);
    m_week_view_action = new QAction(tr("WEEK VIEW"), this);
    m_day_view_action = new QAction(tr("DAY VIEW"), this);
    m_calendars_more_menu = new custom_menu(this);
    m_calendars_more_menu_btn = new custom_button(tr("MORE"), custom_button_type_values::LARGE,
                                                  custom_button_category_values::INVERSE, this);
    m_calendars_edit_action = new QAction(tr("EDIT"), this);
    m_calendars_share_action = new QAction(tr("SHARE"), this);
    m_calendars_remove_action = new QAction(tr("REMOVE"), this);

    create_events_management_buttons_();
    apply_calendar_management_buttons_settings_();
}

void window_top_panel::create_events_management_buttons_()
{
    m_events_btn = new custom_button(tr("EVENTS"), custom_button_type_values::LARGE,
                                     custom_button_category_values::WARNING, this);
    m_events_more_menu_btn = new custom_button(" ", this);
    m_events_more_menu = new custom_menu(this);
    m_events_edit_action = new QAction(tr("EDIT"), this);
    m_events_share_action = new QAction(tr("SHARE"), this);
    m_events_remove_action = new QAction(tr("REMOVE"), this);
}

void window_top_panel::apply_calendar_management_buttons_settings_()
{
    _debug_check_cal_buttons_();
    apply_default_settings_to_button_(m_calendars_btn);
    m_calendars_btn->set_category(custom_button_category_values::DANGER);
    m_calendars_menu_btn->setFixedSize(k_button_indicator_width, k_button_fixed_height);
    m_calendars_menu_btn->set_rounded(0);
    m_calendars_menu_btn->set_category(custom_button_category_values::DANGER);
    m_calendars_menu_btn->setMenu(m_calendars_menu);
    QFontMetrics fm(m_project_calendar_action->font());
    m_calendars_menu->set_item_width(fm.width(m_project_calendar_action->text()));
    m_calendars_menu->set_color(custom_color_values::POMEGRANATE);
    m_calendars_menu->addAction(m_my_calendar_action);
    m_calendars_menu->addAction(m_team_calendar_action);
    m_calendars_menu->addAction(m_project_calendar_action);

    m_back_calendar_btn->setFixedSize(k_top_panel_fixed_height, k_button_fixed_height);
    m_back_calendar_btn->set_rounded(0);
    m_jump_to_date_btn->setFixedSize(k_button_max_width, k_button_fixed_height);

    m_calendar_views_menu_btn->setFixedSize(k_button_max_width + k_button_max_width / 2, k_button_fixed_height);
    m_calendar_views_menu_btn->set_rounded(0);
    m_calendar_views_menu_btn->setMenu(m_calendar_views_menu);
    m_calendar_views_menu_btn->set_category(custom_button_category_values::WARNING_LIGHT);
    m_calendar_views_menu->set_item_width(k_button_max_width);
    m_calendar_views_menu->set_color(custom_color_values::ORANGE);
    m_calendar_views_menu->addAction(m_month_view_action);
    m_calendar_views_menu->addAction(m_week_view_action);
    m_calendar_views_menu->addAction(m_day_view_action);

    apply_default_settings_to_button_(m_calendars_more_menu_btn);
    m_calendars_more_menu_btn->set_category(custom_button_category_values::DEFAULT);
    m_calendars_more_menu_btn->setMenu(m_calendars_more_menu);
    m_calendars_more_menu->addAction(m_calendars_edit_action);
    m_calendars_more_menu->addAction(m_calendars_share_action);
    m_calendars_more_menu->addAction(m_calendars_remove_action);

    apply_default_settings_to_button_(m_events_btn);
    m_events_more_menu_btn->setFixedSize(k_button_indicator_width, k_button_fixed_height);
    m_events_more_menu_btn->set_category(custom_button_category_values::WARNING);
    m_events_more_menu_btn->set_rounded(0);
    m_events_more_menu_btn->setMenu(m_events_more_menu);
    m_events_more_menu->set_item_width(k_button_max_width);
    m_events_more_menu->set_color(custom_color_values::PUMPKIN);
    m_events_more_menu->addAction(m_events_edit_action);
    m_events_more_menu->addAction(m_events_share_action);
    m_events_more_menu->addAction(m_events_remove_action);
}

void window_top_panel::_debug_check_cal_buttons_()
{
    Q_ASSERT(0 != m_calendars_btn);
    Q_ASSERT(0 != m_calendars_menu);
    Q_ASSERT(0 != m_calendars_menu_btn);
    Q_ASSERT(0 != m_my_calendar_action);
    Q_ASSERT(0 != m_team_calendar_action);
    Q_ASSERT(0 != m_project_calendar_action);
    Q_ASSERT(0 != m_back_calendar_btn);
    Q_ASSERT(0 != m_jump_to_date_btn);
    Q_ASSERT(0 != m_calendar_views_menu);
    Q_ASSERT(0 != m_calendar_views_menu_btn);
    Q_ASSERT(0 != m_month_view_action);
    Q_ASSERT(0 != m_week_view_action);
    Q_ASSERT(0 != m_day_view_action);
    Q_ASSERT(0 != m_calendars_more_menu);
    Q_ASSERT(0 != m_calendars_more_menu_btn);
    Q_ASSERT(0 != m_calendars_edit_action);
    Q_ASSERT(0 != m_calendars_share_action);
    Q_ASSERT(0 != m_calendars_remove_action);

    Q_ASSERT(0 != m_events_btn);
    Q_ASSERT(0 != m_events_more_menu);
    Q_ASSERT(0 != m_events_more_menu_btn);
    Q_ASSERT(0 != m_events_edit_action);
    Q_ASSERT(0 != m_events_share_action);
    Q_ASSERT(0 != m_events_remove_action);
}

void window_top_panel::create_people_management_buttons_()
{
    m_people_manage_done_btn = new custom_button(tr("DONE"), custom_button_type_values::LARGE,
                                                 custom_button_category_values::SUCCESS, this);
    m_people_edit_btn = new custom_button(tr("EDIT"), custom_button_type_values::LARGE,
                                          custom_button_category_values::WARNING, this);
    m_people_remove_btn = new custom_button(tr("REMOVE"), custom_button_type_values::LARGE,
                                            custom_button_category_values::DANGER, this);
    m_people_more_btn = new custom_button(tr("MORE"), custom_button_type_values::LARGE,
                                          custom_button_category_values::INVERSE, this);
    m_people_more_menu = new custom_menu(this);
    m_people_share_action = new QAction(tr("SHARE"), this);
    m_people_merge_action = new QAction(tr("MERGE"), this);
    m_people_copy_action = new QAction(tr("COPY"), this);

    Q_ASSERT(0 != m_people_manage_done_btn);
    Q_ASSERT(0 != m_people_edit_btn);
    Q_ASSERT(0 != m_people_remove_btn);
    Q_ASSERT(0 != m_people_more_btn);
    Q_ASSERT(0 != m_people_more_menu);
    Q_ASSERT(0 != m_people_share_action);
    Q_ASSERT(0 != m_people_merge_action);
    Q_ASSERT(0 != m_people_copy_action);

    apply_people_button_settings_();
}

void window_top_panel::apply_people_button_settings_()
{
    apply_default_settings_to_button_(m_people_manage_done_btn);
    apply_default_settings_to_button_(m_people_edit_btn);
    apply_default_settings_to_button_(m_people_remove_btn);
    apply_default_settings_to_button_(m_people_more_btn);
    m_people_more_btn->set_category(custom_button_category_values::DEFAULT);
    m_people_more_btn->setMenu(m_people_more_menu);
    m_people_more_menu->addAction(m_people_share_action);
    m_people_more_menu->addAction(m_people_merge_action);
    m_people_more_menu->addAction(m_people_copy_action);
}

void window_top_panel::create_conversations_management_buttons_()
{
    m_conversations_manage_done_btn = new custom_button(tr("DONE"), custom_button_type_values::LARGE,
                                                        custom_button_category_values::SUCCESS, this);
    m_conversations_share_btn = new custom_button(tr("SHARE"), custom_button_type_values::LARGE,
                                                  custom_button_category_values::HIGHLIGHT, this);
    m_conversations_remove_btn = new custom_button(tr("REMOVE"), custom_button_type_values::LARGE,
                                                   custom_button_category_values::DANGER, this);
    Q_ASSERT(0 != m_conversations_manage_done_btn);
    Q_ASSERT(0 != m_conversations_share_btn);
    Q_ASSERT(0 != m_conversations_remove_btn);
    apply_default_settings_to_button_(m_conversations_manage_done_btn);
    apply_default_settings_to_button_(m_conversations_share_btn);
    apply_default_settings_to_button_(m_conversations_remove_btn);
}

void window_top_panel::create_files_management_buttons_()
{
    m_files_manage_done_btn = new custom_button(tr("DONE"), custom_button_type_values::LARGE,
                                                custom_button_category_values::SUCCESS, this);
    m_files_download_btn = new custom_button(tr("DOWNLOAD"), custom_button_type_values::LARGE,
                                             custom_button_category_values::INFO, this);
    m_files_edit_btn = new custom_button(tr("EDIT"), custom_button_type_values::LARGE,
                                         custom_button_category_values::WARNING, this);
    m_files_share_btn = new custom_button(tr("SHARE"), custom_button_type_values::LARGE,
                                          custom_button_category_values::HIGHLIGHT, this);
    m_files_remove_btn = new custom_button(tr("REMOVE"), custom_button_type_values::LARGE,
                                           custom_button_category_values::DANGER, this);
    Q_ASSERT(0 != m_files_manage_done_btn);
    Q_ASSERT(0 != m_files_download_btn);
    Q_ASSERT(0 != m_files_edit_btn);
    Q_ASSERT(0 != m_files_share_btn);
    Q_ASSERT(0 != m_files_remove_btn);
    apply_default_settings_to_button_(m_files_manage_done_btn);
    apply_default_settings_to_button_(m_files_download_btn);
    apply_default_settings_to_button_(m_files_edit_btn);
    apply_default_settings_to_button_(m_files_share_btn);
    apply_default_settings_to_button_(m_files_remove_btn);
}

void window_top_panel::create_settings_management_buttons_()
{
    m_settings_general_btn = new custom_button(tr("GENERAL"), custom_button_type_values::LARGE,
                                               custom_button_category_values::PRIMARY, this);
    m_settings_projects_btn = new custom_button(tr("PROJECTS"), custom_button_type_values::LARGE,
                                                custom_button_category_values::INFO, this);
    m_settings_tasks_btn = new custom_button(tr("TASKS"), custom_button_type_values::LARGE,
                                             custom_button_category_values::WARNING, this);
    m_settings_calendar_btn = new custom_button(tr("CALENDAR"), custom_button_type_values::LARGE,
                                                custom_button_category_values::WARNING_LIGHT, this);
    m_settings_people_btn = new custom_button(tr("PEOPLE"), custom_button_type_values::LARGE,
                                              custom_button_category_values::HIGHLIGHT, this);
    m_settings_conversations_btn = new custom_button(tr("CONVERSATIONS"), custom_button_type_values::LARGE,
                                                     custom_button_category_values::SUCCESS, this);
    m_settings_files_btn = new custom_button(tr("FILES"), custom_button_type_values::LARGE,
                                             custom_button_category_values::DANGER, this);
    Q_ASSERT(0 != m_settings_general_btn);
    Q_ASSERT(0 != m_settings_projects_btn);
    Q_ASSERT(0 != m_settings_tasks_btn);
    Q_ASSERT(0 != m_settings_calendar_btn);
    Q_ASSERT(0 != m_settings_people_btn);
    Q_ASSERT(0 != m_settings_conversations_btn);
    Q_ASSERT(0 != m_settings_files_btn);
    apply_default_settings_to_button_(m_settings_general_btn);
    apply_default_settings_to_button_(m_settings_projects_btn);
    apply_default_settings_to_button_(m_settings_tasks_btn);
    apply_default_settings_to_button_(m_settings_calendar_btn);
    apply_default_settings_to_button_(m_settings_people_btn);
    apply_default_settings_to_button_(m_settings_conversations_btn);
    apply_default_settings_to_button_(m_settings_files_btn);
}

void window_top_panel::apply_default_settings_to_button_(custom_button *b)
{
    Q_ASSERT(0 != b);
    b->setFixedSize(k_button_max_width, k_button_fixed_height);
    b->set_rounded(0);
}

void window_top_panel::on_calendar_back_button_clicked()
{
    load_calendar_button_set_();
}

void window_top_panel::on_calendar_jump_to_date_clicked()
{
    emit calendar_jump_to_date_clicked(QDate::currentDate());
}

void window_top_panel::on_calendars_button_clicked()
{
    load_calendars_chosen_button_set_();
}

void window_top_panel::on_calendar_month_view_clicked_()
{
    Q_ASSERT(0 != m_month_view_action);
    Q_ASSERT(0 != m_calendar_views_menu_btn);
    m_calendar_views_menu_btn->setText(m_month_view_action->text());
}

void window_top_panel::on_calendar_week_view_clicked_()
{
    Q_ASSERT(0 != m_week_view_action);
    Q_ASSERT(0 != m_calendar_views_menu_btn);
    m_calendar_views_menu_btn->setText(m_week_view_action->text());
}

void window_top_panel::on_calendar_day_view_clicked_()
{
    Q_ASSERT(0 != m_day_view_action);
    Q_ASSERT(0 != m_calendar_views_menu_btn);
    m_calendar_views_menu_btn->setText(m_day_view_action->text());
}

void window_top_panel::on_events_button_clicked()
{
    load_events_chosen_button_set_();
}

void window_top_panel::on_tasks_manage_done_clicked()
{
    load_default_button_set_();
}

void window_top_panel::on_people_manage_done_clicked()
{
    load_default_button_set_();
}

void window_top_panel::on_conversations_manage_done_clicked()
{
    load_default_button_set_();
}

void window_top_panel::on_files_manage_done_clicked()
{
    load_default_button_set_();
}

void window_top_panel::set_active_view(active_view bs)
{
    m_active_view = bs;
    load_default_button_set_();
}

window_top_panel::active_view window_top_panel::get_active_view() const
{
    return m_active_view;
}

void window_top_panel::on_active_view_change()
{
    switch (m_active_view)
    {
    case PROJECTS:
        load_projects_button_set_();
        break;
    case TASKS:
        load_tasks_button_set_();
        break;
    case CALENDAR:
        load_calendar_button_set_();
        break;
    case PEOPLE:
        load_people_button_set_();
        break;
    case ACTIVITY_BOARD:
        break;
    case CONVERSATIONS:
        load_conversations_button_set_();
        break;
    case TERMINAL:
        break;
    case FILES:
        load_files_button_set_();
        break;
    case SETTINGS:
        load_settings_button_set_();
        break;
    default:
        break;
    }
}

void window_top_panel::load_default_button_set_()
{
    clear_buttons_layout_();

    Q_ASSERT(0 != m_manage_generic_btn);
    m_buttons_layout->addWidget(m_manage_generic_btn, k_buttons_layout_stretch_value, Qt::AlignLeft);
    m_manage_generic_btn->show();
}

void window_top_panel::on_projects_manage_done_clicked()
{
    load_default_button_set_();
}

void window_top_panel::load_projects_button_set_()
{
    clear_buttons_layout_();

    Q_ASSERT(0 != m_projects_manage_done_btn);
    Q_ASSERT(0 != m_projects_edit_btn);
    Q_ASSERT(0 != m_projects_remove_btn);
    Q_ASSERT(0 != m_projects_share_btn);
    m_buttons_layout->setSpacing(k_spacing_default_value);
    m_buttons_layout->addWidget(m_projects_manage_done_btn);
    m_buttons_layout->addWidget(m_projects_edit_btn);
    m_buttons_layout->addWidget(m_projects_share_btn);
    m_buttons_layout->addWidget(m_projects_remove_btn, 1, Qt::AlignLeft);

    m_projects_manage_done_btn->show();
    m_projects_edit_btn->show();
    m_projects_remove_btn->show();
    m_projects_share_btn->show();
}

void window_top_panel::load_tasks_button_set_()
{
    clear_buttons_layout_();

    Q_ASSERT(0 != m_tasks_manage_done_btn);
    Q_ASSERT(0 != m_tasks_edit_btn);
    Q_ASSERT(0 != m_tasks_share_btn);
    Q_ASSERT(0 != m_tasks_more_btn);
    m_buttons_layout->addWidget(m_tasks_manage_done_btn);
    m_buttons_layout->addWidget(m_tasks_edit_btn);
    m_buttons_layout->addWidget(m_tasks_share_btn);
    m_buttons_layout->addWidget(m_tasks_more_btn, 1, Qt::AlignLeft);

    m_tasks_manage_done_btn->show();
    m_tasks_edit_btn->show();
    m_tasks_share_btn->show();
    m_tasks_more_btn->show();
}

void window_top_panel::load_calendar_button_set_()
{
    clear_buttons_layout_();
    _debug_check_cal_buttons_();
    m_buttons_layout->addWidget(m_calendars_btn);
    m_buttons_layout->addWidget(m_calendars_menu_btn);
    m_buttons_layout->addWidget(m_events_btn);
    m_buttons_layout->addWidget(m_events_more_menu_btn, 1, Qt::AlignLeft);

    m_calendars_menu_btn->set_category(custom_button_category_values::DANGER);
    m_calendars_menu->set_color(custom_color_values::POMEGRANATE);
    m_calendars_btn->show();
    m_calendars_menu_btn->show();
    m_events_btn->show();
    m_events_more_menu_btn->show();
}

void window_top_panel::load_calendars_chosen_button_set_()
{
    clear_buttons_layout_();
    _debug_check_cal_buttons_();
    m_buttons_layout->addWidget(m_back_calendar_btn);
    m_buttons_layout->addWidget(m_calendars_menu_btn);
    m_calendars_menu_btn->set_category(custom_button_category_values::PRIMARY);
    m_calendars_menu->set_color(custom_color_values::GREEN_SEA);
    m_buttons_layout->addWidget(m_jump_to_date_btn);
    m_buttons_layout->addWidget(m_calendar_views_menu_btn);
    m_buttons_layout->addWidget(m_calendars_more_menu_btn, 1, Qt::AlignLeft);

    m_back_calendar_btn->show();
    m_calendars_menu_btn->show();
    m_jump_to_date_btn->show();
    m_calendar_views_menu_btn->show();
    m_calendars_more_menu_btn->show();
}

void window_top_panel::load_events_chosen_button_set_()
{
    /// @deprecated
}

void window_top_panel::load_people_button_set_()
{
    clear_buttons_layout_();
    m_buttons_layout->addWidget(m_people_manage_done_btn);
    m_buttons_layout->addWidget(m_people_edit_btn);
    m_buttons_layout->addWidget(m_people_remove_btn);
    m_buttons_layout->addWidget(m_people_more_btn, 1, Qt::AlignLeft);

    m_people_manage_done_btn->show();
    m_people_edit_btn->show();
    m_people_remove_btn->show();
    m_people_more_btn->show();
}

void window_top_panel::load_conversations_button_set_()
{
    clear_buttons_layout_();
    m_buttons_layout->addWidget(m_conversations_manage_done_btn);
    m_buttons_layout->addWidget(m_conversations_share_btn);
    m_buttons_layout->addWidget(m_conversations_remove_btn, 1, Qt::AlignLeft);

    m_conversations_manage_done_btn->show();
    m_conversations_share_btn->show();
    m_conversations_remove_btn->show();
}

void window_top_panel::load_files_button_set_()
{
    clear_buttons_layout_();
    m_buttons_layout->addWidget(m_files_manage_done_btn);
    m_buttons_layout->addWidget(m_files_download_btn);
    m_buttons_layout->addWidget(m_files_edit_btn);
    m_buttons_layout->addWidget(m_files_share_btn);
    m_buttons_layout->addWidget(m_files_remove_btn, 1, Qt::AlignLeft);

    m_files_manage_done_btn->show();
    m_files_download_btn->show();
    m_files_edit_btn->show();
    m_files_share_btn->show();
    m_files_remove_btn->show();
}

void window_top_panel::load_settings_button_set_()
{
    clear_buttons_layout_();
    m_settings_general_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_projects_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_tasks_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_calendar_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_people_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_conversations_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_files_btn->set_category(custom_button_category_values::DEFAULT);

    m_buttons_layout->addWidget(m_settings_general_btn);
    m_buttons_layout->addWidget(m_settings_projects_btn);
    m_buttons_layout->addWidget(m_settings_tasks_btn);
    m_buttons_layout->addWidget(m_settings_calendar_btn);
    m_buttons_layout->addWidget(m_settings_people_btn);
    m_buttons_layout->addWidget(m_settings_conversations_btn);
    m_buttons_layout->addWidget(m_settings_files_btn, 1, Qt::AlignLeft);

    m_settings_general_btn->show();
    m_settings_projects_btn->show();
    m_settings_tasks_btn->show();
    m_settings_calendar_btn->show();
    m_settings_people_btn->show();
    m_settings_conversations_btn->show();
    m_settings_files_btn->show();
}

void window_top_panel::on_settings_general_button_clicked()
{
    load_settings_button_set_();
    m_settings_general_btn->set_category(custom_button_category_values::PRIMARY);
}

void window_top_panel::on_settings_projects_button_clicked()
{
    load_settings_button_set_();
    m_settings_projects_btn->set_category(custom_button_category_values::INFO);
}

void window_top_panel::on_settings_tasks_button_clicked()
{
    load_settings_button_set_();
    m_settings_tasks_btn->set_category(custom_button_category_values::WARNING);
}

void window_top_panel::on_settings_calendar_button_clicked()
{
    load_settings_button_set_();
    m_settings_calendar_btn->set_category(custom_button_category_values::WARNING_LIGHT);
}

void window_top_panel::on_settings_people_button_clicked()
{
    load_settings_button_set_();
    m_settings_people_btn->set_category(custom_button_category_values::HIGHLIGHT);
}

void window_top_panel::on_settings_conversations_button_clicked()
{
    load_settings_button_set_();
    m_settings_conversations_btn->set_category(custom_button_category_values::SUCCESS);
}

void window_top_panel::on_settings_files_button_clicked()
{
    load_settings_button_set_();
    m_settings_files_btn->set_category(custom_button_category_values::DANGER);
}

void window_top_panel::create_user_and_window_functional_buttons_()
{
    m_close_btn = new icon_button(this);
    Q_ASSERT(0 != m_close_btn);
    m_close_btn->setText("X");

    m_maximize_btn = new icon_button(this);
    Q_ASSERT(0 != m_maximize_btn);
    m_maximize_btn->setText("\u25A1");

    m_minimize_btn = new icon_button(this);
    Q_ASSERT(0 != m_minimize_btn);
    m_minimize_btn->setText("__");

    m_user_avatar = new icon_button(this);
    Q_ASSERT(0 != m_user_avatar);
    m_user_avatar->setFixedSize(k_user_avatar_size, k_user_avatar_size);
    m_user_avatar->setPixmap(QPixmap(k_user_avatar_path));

    m_user_name_lbl = new icon_button(this);
    Q_ASSERT(0 != m_user_name_lbl);
    m_user_name_lbl->setFixedHeight(k_user_name_fixed_height);
    // TODO: the text should be changed to combo
    m_user_name_lbl->setText(tr("MY ACCOUNT"));
    m_user_name_lbl->setFixedWidth(m_user_name_lbl->fontMetrics().width(m_user_name_lbl->text()));
}

void window_top_panel::setup_user_and_functional_buttons_layout_()
{
    QHBoxLayout* func_btns = new QHBoxLayout();
    Q_ASSERT(0 != func_btns);
    Q_ASSERT(0 != m_close_btn);
    Q_ASSERT(0 != m_maximize_btn);
    Q_ASSERT(0 != m_minimize_btn);
    func_btns->setContentsMargins(0, 0, 0, 0);
    func_btns->addWidget(m_minimize_btn);
    func_btns->addWidget(m_maximize_btn);
    func_btns->addWidget(m_close_btn);

    QVBoxLayout* btns_and_username = new QVBoxLayout();
    Q_ASSERT(0 != btns_and_username);
    Q_ASSERT(0 != m_user_name_lbl);
    btns_and_username->addStretch(1);
    btns_and_username->addLayout(func_btns);
    btns_and_username->addWidget(m_user_name_lbl);
    btns_and_username->addStretch(1);

    m_user_and_functional_btn_layout = new QHBoxLayout();
    Q_ASSERT(0 != m_user_and_functional_btn_layout);
    Q_ASSERT(0 != m_user_avatar);
    m_user_and_functional_btn_layout->addWidget(m_user_avatar);
    m_user_and_functional_btn_layout->addSpacing(10);
    m_user_and_functional_btn_layout->addLayout(btns_and_username);
}

void window_top_panel::on_app_close()
{
    /// @todo On close activity
    QApplication::quit();
}

void window_top_panel::on_app_maximize()
{
    emit top_panel_maximize_clicked();
}

void window_top_panel::on_app_minimize()
{
    emit top_panel_minimize_clicked();
}

void window_top_panel::mousePressEvent(QMouseEvent *ev)
{
    QWidget::mousePressEvent(ev);
    if (Qt::LeftButton == ev->button()) {
        m_moving = true;
        m_move_offset = ev->pos();
    }
}

void window_top_panel::mouseReleaseEvent(QMouseEvent *ev)
{
    QWidget::mouseReleaseEvent(ev);
    if (Qt::LeftButton == ev->button()) {
        m_moving = false;
    }
}

void window_top_panel::mouseMoveEvent(QMouseEvent *ev)
{
    QWidget::mouseMoveEvent(ev);
    if (m_moving) {
        emit window_moving(ev->globalPos() - m_move_offset);
    }
}

} // namespace gui

} // namespace kokain
