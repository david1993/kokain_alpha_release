#ifndef CUSTOM_WIDGET_STYLE_H
#define CUSTOM_WIDGET_STYLE_H

#include <map>
#include <vector>

#include <QString>

namespace kokain
{

namespace gui
{

/// @struct Style Sheet Components
struct style_sheet_component
{
private:
    static QString get_property_with_value_(const QString& prop, const QString& val,
                                            const QString& append = "")
    {
        return prop + val + append + style_sheet_component::semicolon();
    }

public:
    /// @brief Fonts
    static QString font_size(const QString& v = "")
    {
        if (v.isEmpty()) { return "font-size:"; }
        return get_property_with_value_(font_size(), v, style_sheet_component::pixel());
    }
    static QString font_style(const QString& v = "")
    {
        if (v.isEmpty()) { return "font-style:"; }
        return get_property_with_value_(font_style(), v);
    }

    /// @breif Text decoration
    static QString text_decoration(const QString& v = "")
    {
        if (v.isEmpty()) { return "text-decoration:"; }
        return get_property_with_value_(text_decoration(), v);
    }

    /// @brief Coloring
    static QString color(const QString& c = "")
    {
        if (c.isEmpty()) { return "color:"; }
        return get_property_with_value_(color(), c);
    }

    static QString background_color(const QString& bc = "")
    {
        if (bc.isEmpty()) { return "background-color:"; }
        return get_property_with_value_(background_color(), bc);
    }

    /// @brief Border
    /// @section1
    /// @{

    static QString border(const QString& b = "")
    {
        if (b.isEmpty()) { return "border:"; }
        return get_property_with_value_(border(), b);
    }

    static QString border_top(const QString& bt = "")
    {
        if (bt.isEmpty()) { return "border-top:"; }
        return get_property_with_value_(border_top(), bt);
    }

    static QString border_bottom(const QString& bb = "")
    {
        if (bb.isEmpty()) { return "border-bottom:"; }
        return get_property_with_value_(border_bottom(), bb);
    }

    static QString border_left(const QString& bl = "")
    {
        if (bl.isEmpty()) { return "border-left:"; }
        return get_property_with_value_(border_left(), bl);
    }

    static QString border_right(const QString& br = "")
    {
        if (br.isEmpty()) { return "border-right:"; }
        return get_property_with_value_(border_right(), br);
    }

    static QString border_color(const QString& bc = "")
    {
        if (bc.isEmpty()) { return "border-color:"; }
        return get_property_with_value_(border_color(), bc);
    }

    static QString border_left_color(const QString& blc = "")
    {
        if (blc.isEmpty()) { return "border-left-color:"; }
        return get_property_with_value_(border_left_color(), blc);
    }

    static QString border_right_color(const QString& brc = "")
    {
        if (brc.isEmpty()) { return "border-right-color:"; }
        return get_property_with_value_(border_right_color(), brc);
    }

    static QString border_top_color(const QString& btc = "")
    {
        if (btc.isEmpty()) { return "border-top-color:"; }
        return get_property_with_value_(border_top_color(), btc);
    }

    static QString border_bottom_color(const QString& bbc = "")
    {
        if (bbc.isEmpty()) { return "border-bottom-color:"; }
        return get_property_with_value_(border_bottom_color(), bbc);
    }

    static QString border_radius(const QString& br = "")
    {
        if (br.isEmpty()) { return "border-radius:"; }
        return get_property_with_value_(border_radius(), br, pixel());
    }

    static QString border_top_left_radius(const QString& btlr = "")
    {
        if (btlr.isEmpty()) { return "border-top-left-radius:"; }
        return get_property_with_value_(border_top_left_radius(), btlr, pixel());
    }

    static QString border_top_right_radius(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-top-right-radius:"; }
        return get_property_with_value_(border_top_right_radius(), v, pixel());
    }

    static QString border_bottom_left_radius(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-bottom-left-radius:"; }
        return get_property_with_value_(border_bottom_left_radius(), v, pixel());
    }

    static QString border_bottom_right_radius(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-bottom-right-radius:"; }
        return get_property_with_value_(border_bottom_right_radius(), v, pixel());
    }

    static QString border_width(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-width:"; }
        return get_property_with_value_(border_width(), v, pixel());
    }

    static QString border_top_width(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-top-width:"; }
        return get_property_with_value_(border_top_width(), v, pixel());
    }

    static QString border_bottom_width(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-bottom-width:"; }
        return get_property_with_value_(border_bottom_width(), v, pixel());
    }

    static QString border_left_width(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-left-width:"; }
        return get_property_with_value_(border_left_width(), v, pixel());
    }

    static QString border_right_width(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-right-width:"; }
        return get_property_with_value_(border_right_width(), v, pixel());
    }

    static QString border_style(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-style:"; }
        return get_property_with_value_(border_style(), v);
    }

    static QString border_top_style(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-top-style:"; }
        return get_property_with_value_(border_top_style(), v);
    }

    static QString border_bottom_style(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-bottom-style:"; }
        return get_property_with_value_(border_bottom_style(), v);
    }

    static QString border_left_style(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-left-style:"; }
        return get_property_with_value_(border_left_style(), v);
    }

    static QString border_right_style(const QString& v = "")
    {
        if (v.isEmpty()) { return "border-right-style:"; }
        return get_property_with_value_(border_right_style(), v);
    }

    /// @}
    /// @endsection1

    /// @brief Padding and margin
    /// @section2
    /// @{

    static QString padding(const QString& v = "")
    {
        if (v.isEmpty()) { return "padding:"; }
        return get_property_with_value_(padding(), v, pixel());
    }

    static QString padding_left(const QString& v = "")
    {
        if (v.isEmpty()) { return "padding-left:"; }
        return get_property_with_value_(padding_left(), v, pixel());
    }

    static QString padding_right(const QString& v = "")
    {
        if (v.isEmpty()) { return "padding-right:"; }
        return get_property_with_value_(padding_right(), v, pixel());
    }

    static QString padding_top(const QString& v = "")
    {
        if (v.isEmpty()) { return "padding-top:"; }
        return get_property_with_value_(padding_top(), v, pixel());
    }

    static QString padding_bottom(const QString& v = "")
    {
        if (v.isEmpty()) { return "padding-bottom:"; }
        return get_property_with_value_(padding_bottom(), v, pixel());
    }

    static QString margin(const QString& v = "")
    {
        if (v.isEmpty()) { return "margin:"; }
        return get_property_with_value_(margin(), v, pixel());
    }

    static QString margin_left(const QString& v = "")
    {
        if (v.isEmpty()) { return "margin-left:"; }
        return get_property_with_value_(margin_left(), v, pixel());
    }

    static QString margin_right(const QString& v = "")
    {
        if (v.isEmpty()) { return "margin-right:"; }
        return get_property_with_value_(margin_right(), v, pixel());
    }

    static QString margin_top(const QString& v = "")
    {
        if (v.isEmpty()) { return "margin-top:"; }
        return get_property_with_value_(margin_top(), v, pixel());
    }

    static QString margin_bottom(const QString& v = "")
    {
        if (v.isEmpty()) { return "margin-bottom:"; }
        return get_property_with_value_(margin_bottom(), v, pixel());
    }

    /// @}
    /// @endsection2

    /// @brief States
    static QString hover_state() { return ":hover"; }
    static QString hover_state_not() { return ":!hover"; }
    static QString pressed_state() { return ":pressed"; }
    static QString pressed_state_not() { return ":!pressed"; }

    /// @brief Various components and symbols
    static QString pixel() { return "px"; }
    static QString colon() { return ":"; }
    static QString semicolon() { return ";"; }
    static QString dash() { return "-"; }
    static QString open_brace() { return "{"; }
    static QString close_brace() { return "}"; }

    /// @brief More functionality
    static QString brace_this(const QString& t)
    {
        return open_brace() + t + close_brace();
    }

    static QString unbrace_this(const QString& t)
    {
        //TODO find and remove leading braces
        return t;
    }

    /// @brief Reflection :D
    static QString _style_() { return "style"; }
    static QString _color_() { return "color"; }
    static QString _width_() { return "width"; }
    static QString _height_() { return "height"; }
    static QString _radius_() { return "radius"; }

    static QString _left_() { return "left"; }
    static QString _right_() { return "right"; }
    static QString _top_() { return "top"; }
    static QString _bottom_() { return "bottom"; }

}; // struct style_sheet_component

/// @section1 Custom Colors
/// @{

namespace custom_color_values
{
    enum custom_color
    {
        DEFAULT = 1,
        CLOUDS,
        SILVER,
        CONCRETE,
        ASBESTOS,
        ALIZARIN,
        POMEGRANATE,
        PETER_RIVER,
        BELIZE_HOLE,
        WET_ASPHALT,
        MIDNIGHT_BLUE,
        SUN_FLOWER,
        ORANGE,
        CARROT,
        PUMPKIN,
        TURQUOISE,
        GREEN_SEA,
        EMERALD,
        NEPHRITIS,
        AMETHYST,
        WISTERIA,
        WHITE,
        custom_color_max
    }; // enum custom_color

} // namespace custom_colors

/// @struct custom_color_mapper
/// @brief Holds static maps, mapping color codes to code names
struct custom_color_mapper
{
public:
    static void initialize_color_maps();

private:
    static bool s_maps_initialized;

private:
    static std::map<custom_color_values::custom_color, QString> s_color_code_map;

public:
    static QString get_color_code_by_enum(custom_color_values::custom_color e);
    static QString get_color_code_by_name(const QString& n);

public:
    static const QString k_color_code_default;
    static const QString k_color_code_clouds;
    static const QString k_color_code_silver;
    static const QString k_color_code_concrete;
    static const QString k_color_code_asbestos;
    static const QString k_color_code_alizarin;
    static const QString k_color_code_pomegranate;
    static const QString k_color_code_peter_river;
    static const QString k_color_code_belize_hole;
    static const QString k_color_code_wet_asphalt;
    static const QString k_color_code_midnight_blue;
    static const QString k_color_code_sun_flower;
    static const QString k_color_code_orange;
    static const QString k_color_code_carrot;
    static const QString k_color_code_pumpkin;
    static const QString k_color_code_turquoise;
    static const QString k_color_code_green_sea;
    static const QString k_color_code_emerald;
    static const QString k_color_code_nephritis;
    static const QString k_color_code_amethyst;
    static const QString k_color_code_wisteria;
    static const QString k_color_code_white;


}; // struct custom_color_mapper

/// @}
/// @endsection1

/// @section2 Border
/// @{

namespace border_style_values
{
    enum border_style
    {
        NONE = 1,
        DASHED,
        DOT_DASH,
        DOT_DOT_DASH,
        DOTTED,
        DOUBLE,
        GROOVE,
        INSET,
        OUTSET,
        RIDGE,
        SOLID,
        border_style_max
    }; // enum border_style

} // namespace border_style_values

/// @struct border_style_mapper
/// @brief Maps border styles to string representations, like SOLID to "solid"
struct border_style_mapper
{
public:
    static void initialize_border_styles();
private:
    static std::map<border_style_values::border_style, QString> s_border_style_map;

public:
    static QString get_border_style_by_enum(border_style_values::border_style e);

private:
    static const QString k_border_style_none;
    static const QString k_border_style_dashed;
    static const QString k_border_style_dot_dash;
    static const QString k_border_style_dot_dot_dash;
    static const QString k_border_style_dotted;
    static const QString k_border_style_double;
    static const QString k_border_style_groove;
    static const QString k_border_style_inset;
    static const QString k_border_style_outset;
    static const QString k_border_style_ridge;
    static const QString k_border_style_solid;

}; // struct border_style_mapper

// TODO: discuss with the team namespace/enum declaration
namespace border_position_values
{
    enum border_position
    {
        NONE = 0,
        LEFT,
        RIGHT,
        TOP,
        BOTTOM,
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        border_position_max
    };

} // namespace border_position_values

struct custom_border_style
{
public:
    custom_border_style();
    explicit custom_border_style(border_style_values::border_style s);
    explicit custom_border_style(qint32 w);
    explicit custom_border_style(custom_color_values::custom_color c);
    custom_border_style(border_style_values::border_style s, qint32 w,
            custom_color_values::custom_color c);
    custom_border_style(const custom_border_style& o);
    custom_border_style& operator=(const custom_border_style& o);
    ~custom_border_style();

private:
    typedef std::vector<std::pair<QString, QString> > cmap_value_t;

private:
    void initialize_();
    cmap_value_t generate_a_ready_to_go_vector_() const;
    void set_whole_style_(border_style_values::border_style s);
    void set_whole_color_(custom_color_values::custom_color c);
    void set_whole_color_(const QString& c);
    void set_whole_width_(qint32 w);
    void set_whole_radius_(qint32 r);

private:
    QString get_value_for_key_(const std::vector<std::pair<QString, QString> >& vec, const QString& val) const;
    void set_value_for_key_(std::vector<std::pair<QString, QString> >& in_vec,
            const QString& key, const QString& value);

public:
    typedef border_position_values::border_position border_pos_t;

    /// @brief Border Color
public:
    void set_color(const QString& c, border_pos_t p = border_position_values::NONE);
    void set_color(custom_color_values::custom_color c,
            border_pos_t p = border_position_values::NONE);
    QString color(border_pos_t p = border_position_values::NONE);

    /// @brief Border Style
public:
    void set_style(border_style_values::border_style s,
            border_pos_t p = border_position_values::NONE);
    QString style(border_pos_t p = border_position_values::NONE);

    /// @brief Border Width
public:
    void set_width(qint32 w, border_pos_t p = border_position_values::NONE);
    qint32 width(border_pos_t p = border_position_values::NONE);

    /// @brief Border radius
public:
    void set_radius(qint32 r, border_pos_t p = border_position_values::NONE);
    qint32 radius(border_pos_t p = border_position_values::NONE);

private:
    typedef std::map<border_position_values::border_position,
            std::vector<std::pair<QString, QString> > > component_map_t;

    component_map_t m_border_components;

    /// @brief Get the border style code
public:
    QString get_style_code();

private:
    QString generate_border_left_style_code_();
    QString generate_border_right_style_code_();
    QString generate_border_top_style_code_();
    QString generate_border_bottom_style_code_();

}; // struct custom_border_style

/// @}
/// @endsection2


/// @section3 Fonts and text decoration
/// @{

namespace font_style_values
{
    enum font_style
    {
        NORMAL = 1,
        BOLD,
        ITALIC,
        OBLIQUE,
        font_style_max
    }; // enum font_style

} // namespace font_style_values

namespace text_decoration_values
{
    enum text_decoration
    {
        NONE = 1,
        UNDERLINE,
        OVERLINE,
        LINE_THROUGH,
        text_decoration_max
    }; // enum text_decoration

} // namespace text_decoration_values

// TODO: font family
struct custom_font_style
{
public:
    custom_font_style();
    custom_font_style(font_style_values::font_style e, qint32 s, text_decoration_values::text_decoration d);
    custom_font_style(const custom_font_style&);
    custom_font_style& operator=(const custom_font_style&);

public:
    QString style() const;
    void set_style(font_style_values::font_style e);

    static QString get_font_style_by_enum(font_style_values::font_style e);

private:
    QString m_style;

public:
    qint32 size() const;
    void set_size(qint32 s);

private:
    qint32 m_size;

public:
    QString text_decoration() const;
    void set_text_decoration(text_decoration_values::text_decoration e);

    static QString get_text_decoration_by_enum(text_decoration_values::text_decoration e);

private:
    QString m_text_decoration;

public:
    QString get_style_code() const;

}; // struct font

/// @}
/// @endsection3


/// @brief Widget states
namespace widget_state_values
{
    enum widget_state
    {
        NONE = 1,
        PRESSED,
        HOVER,
        widget_state_max
    };
}

class custom_widget_style
{
    /// @brief Special member functions
public:
    custom_widget_style();
    explicit custom_widget_style(widget_state_values::widget_state s);
    explicit custom_widget_style(const QString& str);
    custom_widget_style(const custom_widget_style& o);
    custom_widget_style& operator=(const custom_widget_style& o);
    virtual ~custom_widget_style();

    /// @brief Style string
public:
    void set_style_string(const QString& s);
    QString style_string() const;
    void append_style_string(const QString& s);
    QString appended_style_string() const;

private:
    // called by to_string()
    virtual QString generate_whole_style_code_();

private:
    QString m_style_string;
    QString m_appended_style_string;

    /// @brief Generates and returns whole style string
public:
    QString to_string();
    QString to_string(const QString& suffix);

private:
    QString to_string_private_();

    /// @brief Coloring
public:
    void set_color(const QString& c); //rarely used
    void set_color(custom_color_values::custom_color c); // Flat style colors
    QString color() const;
    void set_background_color(const QString& c);
    void set_background_color(custom_color_values::custom_color c);
    QString background_color() const;

private:
    QString generate_color_style_code_() const;

private:
    QString m_color;
    QString m_background_color;

    /// @brief Border
public:
    void set_border_style(const custom_border_style& b);
    custom_border_style border_style() const;

private:
    QString generate_border_style_code_();

private:
    custom_border_style m_border_style;

    /// Padding and margin
public:
    void set_padding(qint32 p);
    void set_padding(qint32 left, qint32 right, qint32 top, qint32 bottom);
    // TODO padding_left(), padding_right(), margin_left(), etc
    qint32 padding() const;
    void set_margin(qint32 m);
    void set_margin(qint32 left, qint32 right, qint32 top, qint32 bottom);
    qint32 margin() const;

private:
    QString generate_padding_and_margin_style_code_() const;

private:
    qint32 m_padding;
    qint32 m_padding_left;
    qint32 m_padding_right;
    qint32 m_padding_top;
    qint32 m_padding_bottom;
    qint32 m_margin;
    qint32 m_margin_left;
    qint32 m_margin_right;
    qint32 m_margin_top;
    qint32 m_margin_bottom;

    /// @brief Fonts and text decoration
public:
    void set_font_style(const custom_font_style& f);
    custom_font_style font_style() const;

private:
    QString generate_font_style_code_() const;

private:
    custom_font_style m_font_style;

    /// @brief Hover and pressed states
public:
    void set_hover_state_style(custom_widget_style* hover);
    custom_widget_style* hover_state_style();
    void set_pressed_state_style(custom_widget_style* pressed);
    custom_widget_style* pressed_state_style();
    void set_widget_state(widget_state_values::widget_state s);
    widget_state_values::widget_state widget_state() const;

public:
    QString generate_hover_state_style_code_();
    QString generate_pressed_state_style_code_();

private:
    widget_state_values::widget_state m_widget_state;

    custom_widget_style* m_hover_state_style;
    custom_widget_style* m_pressed_state_style;

public:
    void set_widget_name(const QString& wn);
    QString widget_name() const;

private:
    QString m_widget_name;

private:
    qint32 test_and_set_int32_(qint32 v);
    QString test_and_set_color(const QString& v);


}; // class custom_widget_style

} // namespace gui

} // namespace kokain

#endif // CUSTOM_WIDGET_STYLE_H
