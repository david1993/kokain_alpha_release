#include "icon_button.h"

namespace kokain
{

namespace gui
{

const qint32 icon_button::k_fixed_size = 20;

icon_button::icon_button(QWidget *parent, qint32 fw, qint32 fh) :
    QLabel(parent),
    m_pressed(false),
    m_fixed_width(fw),
    m_fixed_height(fh)
{
    initialize_();
}

void icon_button::initialize_()
{
    if (!m_normal_state.isNull()) {
        setPixmap(m_normal_state);
    }
    setScaledContents(true);
    m_fixed_width != -1 ? setFixedWidth(m_fixed_width) : setFixedWidth(k_fixed_size);
    m_fixed_height != -1 ? setFixedHeight(m_fixed_height) : setFixedHeight(k_fixed_size);

    make_connections_();
}

void icon_button::make_connections_()
{
    connect(this, SIGNAL(clicked()), this, SLOT(on_click()));
    connect(this, SIGNAL(pressed()), this, SLOT(on_press()));
}

void icon_button::set_normal_state_pixmap(const QPixmap &px)
{
    m_normal_state = px;
    if (!m_normal_state.isNull()) {
        this->setPixmap(m_normal_state);
    }
}

void icon_button::set_pressed_state_pixmap(const QPixmap &px)
{
    m_pressed_state = px;
}

void icon_button::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    if (!m_pressed_state.isNull()) {
        setPixmap(m_pressed_state);
    } else {
        m_pressed ? setStyleSheet("QLabel{color: white;}") : setStyleSheet("QLabel{color: #f39c12;}");
        m_pressed = !m_pressed;
    }
    emit pressed();
}

void icon_button::mouseReleaseEvent(QMouseEvent *ev)
{
    (void)ev;
    if (!m_normal_state.isNull()) {
        setPixmap(m_normal_state);
    }
    emit clicked();
}

void icon_button::on_click()
{

}

void icon_button::on_press()
{

}

} // namespace gui

} // namespace kokain
