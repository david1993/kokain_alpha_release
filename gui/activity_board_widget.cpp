#include "activity_board_widget.h"

#include <QVBoxLayout>
#include <QLabel>

#include "my_activity_widget.h"
#include "user_activity_widget.h"
#include "../core/task.h"
#include "../core/user.h"

namespace kokain
{

namespace gui
{

activity_board_widget::activity_board_widget(QWidget *parent) :
    QWidget(parent),
    m_my_activity_wgt(NULL),
    m_main_layout(NULL)
{
    initialize_();
}

void activity_board_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void activity_board_widget::create_widgets_()
{
    m_my_activity_wgt = new my_activity_widget(this);
    Q_ASSERT(0 != m_my_activity_wgt);
    create_user_activity_widgets_();
}

void activity_board_widget::create_user_activity_widgets_()
{
    core::task* test_task = new core::task("test task title");
    test_task->add_datetime(new core::task_datetime_operation_t(
                core::task_datetime_operation_t::START, QDateTime::currentDateTime(),
                            new core::user()));
    test_task->add_datetime(new core::task_datetime_operation_t(
                                core::task_datetime_operation_t::END, QDateTime::currentDateTime(),
                            new core::user()));
    core::user* test_user = new core::user();
    test_user->set_online_status(core::user::ONLINE);
    test_user->set_active_task(test_task);
    user_activity_widget* temp_activity = new user_activity_widget(test_user, this);
    m_user_activities.push_back(temp_activity);
    m_user_activities.push_back(new user_activity_widget(test_user, this));
    m_user_activities.push_back(new user_activity_widget(test_user, this));
    m_user_activities.push_back(new user_activity_widget(test_user, this));
}

void activity_board_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->setSpacing(0);
    m_main_layout->addWidget(m_my_activity_wgt);
    for (unsigned int i = 0; i < m_user_activities.size(); ++i) {
        m_main_layout->addWidget(m_user_activities[i]);
    }
    m_main_layout->addWidget(new QLabel("kanec", this), 1, Qt::AlignTop);
}

void activity_board_widget::make_connections_()
{

}

} // namespace gui

} // namespace kokain
