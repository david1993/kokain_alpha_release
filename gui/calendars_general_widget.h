#ifndef CALENDARs_GENERAL_WIDGET_H
#define CALENDARs_GENERAL_WIDGET_H

#include <QWidget>

#include "../calendar/calendar.h"

class QVBoxLayout;
class QLabel;

namespace kokain
{

namespace gui
{

class calendars_general_widget : public QWidget
{
    Q_OBJECT
public:
    explicit calendars_general_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    calendar_namespace::calendar* m_calendar;

private:
    QVBoxLayout* m_main_layout;

signals:

public slots:

}; // class calendars_general_widget

} // namespace gui

} // namespace kokain

#endif // CALENDARs_GENERAL_WIDGET_H
