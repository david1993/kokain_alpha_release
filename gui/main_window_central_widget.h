#ifndef MAIN_WINDOW_CENTRAL_WIDGET_H
#define MAIN_WINDOW_CENTRAL_WIDGET_H

#include <QWidget>

class QVBoxLayout;
class QHBoxLayout;

namespace kokain
{

namespace gui
{

class window_top_panel;
class action_button_block;
class terminal_widget;

class projects_general_widget;
class tasks_general_widget;
class calendars_general_widget;
class people_general_widget;
class conversations_general_widget;
class files_general_widget;
class settings_general_widget;

class activity_board_widget;

/**
 * @brief The main_window_central_widget class
 */
class main_window_central_widget : public QWidget
{
    Q_OBJECT
public:
    explicit main_window_central_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void make_connections_();

private:
    void create_central_widgets_();
    void create_suplementary_widgets_();
    void hide_central_widgets_();
    void clear_central_layout_();
    void construct_and_switch_central_layout_(QWidget* wgt);

private:
    window_top_panel* m_top_panel;
    terminal_widget * m_terminal_wgt;
    action_button_block* m_actions_block;

private:
    projects_general_widget* m_projects_wgt;
    tasks_general_widget* m_tasks_wgt;
    calendars_general_widget* m_calendar_wgt;
    people_general_widget* m_people_wgt;
    conversations_general_widget* m_conversations_wgt;
    files_general_widget* m_files_wgt;
    settings_general_widget* m_settings_wgt;

    activity_board_widget* m_activity_board_wgt;


signals:
    void maximize_clicked();
    void minimize_clicked();

private:
    void setup_layout_();
    void setup_central_layout_();

private:
    QVBoxLayout* m_main_layout;
    QHBoxLayout* m_central_layout;

public slots:
    void switch_projects();
    void switch_tasks();
    void switch_calendar();
    void switch_people();
    void switch_conversations();
    void switch_files();
    void switch_settings();

private slots:
    void on_window_moving(QPoint p);

private:
    const static qint32 k_hardcoded_spacing_value;

private:
    main_window_central_widget(const main_window_central_widget&);
    main_window_central_widget& operator=(const main_window_central_widget&);

}; // class main_window_central_widget

} // namespace gui

} // namespace kokain

#endif // MAIN_WINDOW_CENTRAL_WIDGET_H
