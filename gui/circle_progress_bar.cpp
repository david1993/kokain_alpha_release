#include "circle_progress_bar.h"

#include <QStylePainter>
#include <QDebug>

namespace kokain
{

namespace gui
{

const qreal circle_progress_bar::k_start_angle_default_value = 90.0;
const qint32 circle_progress_bar::k_minimum_size = 400;

circle_progress_bar::circle_progress_bar(QWidget *parent) :
    QProgressBar(parent),
    m_transparent_center(false),
    m_start_angle(k_start_angle_default_value)
{
    setMinimumSize(k_minimum_size, k_minimum_size);
}

void circle_progress_bar::set_transparent_center(bool v)
{
     m_transparent_center = v;
}

void circle_progress_bar::set_start_angle(qreal sa)
{
    m_start_angle = sa;
}

void circle_progress_bar::paintEvent(QPaintEvent * /*event*/)
{
    /// @todo not my code, refactor this
    static double coefOuter = 0.6;
    static double coefInner = 0.4;
    static double penSize = 1.0;
    static QColor borderColor(178, 178, 178);
    static QColor grooveColor(202, 202, 202);
    static QColor chunkColor(0, 211, 40);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QRectF outerRect(rect().x()*coefOuter, rect().y()*coefOuter, rect().width()*coefOuter, rect().height()*coefOuter);
    QRectF innerRect(rect().x()*coefInner, rect().y()*coefInner, rect().width()*coefInner, rect().height()*coefInner);
    outerRect.moveCenter(rect().center());
    innerRect.moveCenter(rect().center());

    if (isTextVisible()) {
        painter.save();
    }

    QPainterPath borderInAndOut;
    borderInAndOut.addEllipse(rect().center(), rect().width()/2*coefOuter, rect().height()/2*coefOuter);
    borderInAndOut.addEllipse(rect().center(), rect().width()/2*coefInner, rect().height()/2*coefInner);

    QPen borderPen(borderColor, penSize);
    painter.setPen(borderPen);
    painter.setBrush(grooveColor);
    painter.drawPath(borderInAndOut);

    if (value() > 0) {
        QPainterPath groovePath(rect().center());
        qreal converterAngle = 3.6*value();
        groovePath.arcTo(outerRect, m_start_angle, -converterAngle);
        groovePath.moveTo(rect().center());
        groovePath.arcTo(innerRect, m_start_angle, -converterAngle);
        groovePath = groovePath.simplified();
        painter.setPen(Qt::NoPen);
        painter.setBrush(chunkColor);
        painter.drawPath(groovePath);
    }

    if (!m_transparent_center) {
        QPainterPath painterPathCenter;
        painterPathCenter.addEllipse(rect().center(), rect().width()/2*coefInner - penSize/2, rect().height()/2*coefInner - penSize/2);
        painter.setPen(Qt::NoPen);
        painter.setBrush(QColor(Qt::white));
        painter.drawPath(painterPathCenter);
    }

    if (isTextVisible()) {
        painter.restore();
        QString val = QString::number(value()).append("%");
        //QStyleOptionProgressBarV2 styleOption;
        //style()->drawControl(QStyle::CE_ProgressBarGroove, &styleOption, &painter, this);
        //style()->drawPrimitive(QStyle::PE_FrameStatusBar, &styleOption, &painter, this);
        style()->drawItemText(&painter, rect(), Qt::AlignCenter, palette(), true, val);
    }
}

} // namespace gui

} // namespace kokain
