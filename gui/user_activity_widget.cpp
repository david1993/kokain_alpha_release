#include "user_activity_widget.h"

#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>

#include "../core/user.h"
#include "../core/task.h"
#include "circle_progress_bar.h"
#include "icon_button.h"

namespace kokain
{

namespace gui
{

const qint32 user_activity_widget::k_avatar_fixed_size = 55;
const qint32 user_activity_widget::k_online_status_height = 5;
const qint32 user_activity_widget::k_activity_widget_width = 300;
const qint32 user_activity_widget::k_activity_widget_height = 65;

user_activity_widget::user_activity_widget(QWidget *parent) :
    QWidget(parent),
    m_user_avatar(NULL),
    m_online_status_lbl(NULL),
    m_task_title_lbl(NULL),
    m_task_start_lbl(NULL),
    m_task_end_lbl(NULL),
    m_task_progress_bar(NULL),
    m_main_layout(NULL),
    m_user(NULL)
{
    initialize_();
}

user_activity_widget::user_activity_widget(core::user *u, QWidget *p)
    : QWidget(p),
      m_user_avatar(NULL),
      m_online_status_lbl(NULL),
      m_task_title_lbl(NULL),
      m_task_start_lbl(NULL),
      m_task_end_lbl(NULL),
      m_task_progress_bar(NULL),
      m_main_layout(NULL),
      m_user(u)
{
    Q_ASSERT(0 != m_user);
    initialize_();
    update_activity();
}

void user_activity_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
    setFixedSize(k_activity_widget_width, k_activity_widget_height);
    setAutoFillBackground(true);
    ///setStyleSheet("border: 1px solid red;");
}

void user_activity_widget::create_widgets_()
{
    m_user_avatar = new icon_button(this);
    m_online_status_lbl = new QLabel(this);
    m_task_title_lbl = new QLabel(this);
    m_task_start_lbl = new QLabel(this);
    m_task_end_lbl = new QLabel(this);
    m_task_progress_bar = new circle_progress_bar(this);
    Q_ASSERT(0 != m_user_avatar);
    Q_ASSERT(0 != m_online_status_lbl);
    Q_ASSERT(0 != m_task_title_lbl);
    Q_ASSERT(0 != m_task_start_lbl);
    Q_ASSERT(0 != m_task_end_lbl);
    Q_ASSERT(0 != m_task_progress_bar);
    m_user_avatar->setFixedSize(k_avatar_fixed_size, k_avatar_fixed_size);
    m_online_status_lbl->setFixedSize(k_avatar_fixed_size, k_online_status_height);
    qint32 sz = 80;
    m_task_progress_bar->setFixedSize(sz, sz);
    m_task_start_lbl->setToolTip(tr("start date"));
    m_task_end_lbl->setToolTip(tr("end date"));
}

void user_activity_widget::setup_layout_()
{
    m_main_layout = new QHBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);

    QVBoxLayout* avatar_layout = new QVBoxLayout();
    Q_ASSERT(0 != avatar_layout);
    avatar_layout->setContentsMargins(0, 0, 0, 0);
    avatar_layout->setSpacing(0);
    avatar_layout->addWidget(m_user_avatar);
    avatar_layout->addWidget(m_online_status_lbl);

    QVBoxLayout* task_layout = new QVBoxLayout();
    Q_ASSERT(0 != task_layout);
    task_layout->setContentsMargins(0, 0, 0, 0);
    task_layout->addWidget(m_task_title_lbl);
    QHBoxLayout* task_time_layout = new QHBoxLayout();
    Q_ASSERT(0 != task_time_layout);
    task_time_layout->setContentsMargins(0, 0, 0, 0);
    task_time_layout->addWidget(m_task_start_lbl);
    task_time_layout->addWidget(m_task_end_lbl);
    task_layout->addLayout(task_time_layout);

    m_main_layout->addLayout(avatar_layout);
    m_main_layout->addLayout(task_layout);
    m_main_layout->addWidget(m_task_progress_bar);
}

void user_activity_widget::make_connections_()
{

}

void user_activity_widget::set_user(core::user *u)
{
    m_user = u;
    Q_ASSERT(0 != m_user);
    update_activity();
}

core::user* user_activity_widget::user()
{
    return m_user;
}

void user_activity_widget::update_activity()
{
    Q_ASSERT(0 != m_user);
    _debug_widgets_are_alive_();
    m_user_avatar->setPixmap(m_user->get_picture());
    update_online_status_();
    update_task_status_();
}

void user_activity_widget::update_online_status_()
{
    Q_ASSERT(0 != m_user);
    switch (m_user->get_online_status())
    {
    case core::user::ONLINE:
        m_online_status_lbl->setStyleSheet("background-color: lime");
        break;
    case core::user::OFFLINE:
        m_online_status_lbl->setStyleSheet("background-color: gray");
        break;
    case core::user::AWAY:
        m_online_status_lbl->setStyleSheet("background-color: orange");
        break;
    case core::user::BUSY:
        m_online_status_lbl->setStyleSheet("background-color: red;");
        break;
    case core::user::INVISIBLE:
        m_online_status_lbl->setStyleSheet("background-color: gray");
        break;
    default:
        m_online_status_lbl->setStyleSheet("background-color: gray");
        break;
    }
}

void user_activity_widget::update_task_status_()
{
    core::task* active_task = m_user->active_task();
    if (0 == active_task) {
        m_task_title_lbl->setText(tr("(no active task)"));
        return;
    }
    m_task_title_lbl->setText(active_task->title());
    m_task_start_lbl->setText(active_task->datetime_for(
                                  core::task_datetime_operation_t::START).toString("dd.MM.yyyy"));
    m_task_end_lbl->setText(active_task->datetime_for(
                                core::task_datetime_operation_t::END).toString("dd.MM.yyyy"));
    m_task_progress_bar->setValue(active_task->progress());
    m_task_progress_bar->setValue(44);  // <--- temp value
}

void user_activity_widget::_debug_widgets_are_alive_()
{
    Q_ASSERT(0 != m_user_avatar);
    Q_ASSERT(0 != m_online_status_lbl);
    Q_ASSERT(0 != m_task_title_lbl);
    Q_ASSERT(0 != m_task_start_lbl);
    Q_ASSERT(0 != m_task_end_lbl);
    Q_ASSERT(0 != m_task_progress_bar);
}

void user_activity_widget::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
}

void user_activity_widget::mouseReleaseEvent(QMouseEvent *ev)
{
    emit clicked();
    QWidget::mouseReleaseEvent(ev);
}

} // namespace gui

} // namespace kokain
