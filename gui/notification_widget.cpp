#include "notification_widget.h"

#include "custom_label.h"
#include "flow_layout.h"
#include "core/notification.h"
#include "core/user.h"

namespace kokain {

namespace gui {

notification_widget::notification_widget(QWidget *parent)
    : QWidget(parent)
{
    this->initialize_();
}

notification_widget::notification_widget(kokain::core::notification *the_notification, QWidget *parent)
    : QWidget(parent)
{
    Q_ASSERT(NULL == the_notification);
    this->initialize_();
    m_sender_name->setText(the_notification->get_sender()->get_name());
    m_notification_text->setText(the_notification->get_notification_text());
    //m_entity_name->setText(the_notification);
}

notification_widget::~notification_widget()
{

}

void notification_widget::initialize_()
{
    this->create_widgets_();
    this->setup_layout_();
    this->make_connections_();
}

void notification_widget::create_widgets_()
{
    m_checkbox = new QCheckBox(this);
    m_sender_name = new custom_label(this);
    m_notification_text = new custom_label(this);
    m_entity_name = new custom_label(this);
}

void notification_widget::setup_layout_()
{
    m_flow_layout = new flow_layout(this);
    m_background_color = new QPalette(QPalette::Background, Qt::lightGray);
    this->setAutoFillBackground(true);
    this->setPalette(*m_background_color);
    m_flow_layout->addWidget(m_checkbox);
    m_flow_layout->addWidget(m_sender_name);
    m_flow_layout->addWidget(m_notification_text);
    m_flow_layout->addWidget(m_entity_name);
}

void notification_widget::make_connections_()
{
    connect(m_checkbox, SIGNAL(clicked()),
            this, SLOT(slot_checkbox_clicked()));
    connect(m_sender_name, SIGNAL(clicked()),
            this, SLOT(slot_sender_clicked()));
    connect(m_notification_text, SIGNAL(clicked()),
            this, SLOT(slot_text_clicked()));
    connect(m_entity_name, SIGNAL(clicked()),
            this, SLOT(slot_entity_clicked()));
}

void notification_widget::set_check_state(Qt::CheckState state)
{
    m_checkbox->setCheckState(state);
}

bool notification_widget::is_checked() const
{
    return m_checkbox->isChecked();
}

void notification_widget::set_sender_name(const QString& name)
{
    Q_ASSERT(name.isEmpty());
    m_sender_name->setText(name);
}

QString notification_widget::get_sender_name() const
{
    Q_ASSERT(NULL == m_sender_name);
    return m_sender_name->text();
}

void notification_widget::set_notification_text(const QString& text)
{
    Q_ASSERT(text.isEmpty());
    m_notification_text->setText(text);
}

QString notification_widget::get_notification_text() const
{
    Q_ASSERT(m_notification_text->text().isEmpty());
    return m_notification_text->text();
}

void notification_widget::set_entity_name(const QString& name)
{
    Q_ASSERT(name.isEmpty());
    m_entity_name->setText(name);
}

QString notification_widget::get_entity_name() const
{
    Q_ASSERT(m_entity_name->text().isEmpty());
    return m_entity_name->text();
}

void notification_widget::set_notification_id(const QString &id)
{
    m_notification_id = id;
}

QString notification_widget::get_notification_id() const
{
    return m_notification_id;
}

void notification_widget::slot_checkbox_clicked()
{
    this->set_color();
    emit signal_checkbox_clicked();
}

void notification_widget::slot_entity_clicked()
{
    this->set_notification_status(READ);
    this->set_color();
    QString tmp_not_id = this->get_notification_id();
    emit signal_entity_clicked(tmp_not_id);
}

void notification_widget::slot_sender_clicked()
{
    this->set_notification_status(READ);
    this->set_color();
    QString tmp_not_id = this->get_notification_id();
    emit signal_sender_clicked(tmp_not_id);
}

void notification_widget::slot_text_clicked()
{
    this->set_notification_status(READ);
    this->set_color();
    QString tmp_not_id = this->get_notification_id();
    emit signal_text_clicked(tmp_not_id);
}

void notification_widget::set_color()
{
    if(m_checkbox->isChecked()){
        this->marked_color();
        return;
    }
    if(this->get_notification_status() == UNREAD){
        this->unread_color();
        return;
    }
    this->default_color();
}

void notification_widget::marked_color()
{
    m_background_color->setColor(QPalette::Background, Qt::yellow);
}

void notification_widget::unread_color()
{
    m_background_color->setColor(QPalette::Background, Qt::darkGray);
}

void notification_widget::default_color()
{
    m_background_color->setColor(QPalette::Background, Qt::lightGray);
}

void notification_widget::set_notification_status(notification_status stat)
{
    m_notification_status = stat;
    this->set_color();
}

notification_widget::notification_status notification_widget::get_notification_status() const
{
    return m_notification_status;
}

} // gui
} // kokain
