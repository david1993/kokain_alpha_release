#ifndef TERMINAL_WIDGET_H
#define TERMINAL_WIDGET_H

#include <QWidget>

class QHBoxLayout;

namespace kokain
{

namespace gui
{

class custom_button;
class custom_line_edit;
class icon_button;

class terminal_widget : public QWidget
{
    Q_OBJECT
public:
    explicit terminal_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    void create_editable_widget_();

private:
    custom_line_edit* m_editable;
    custom_button* m_execute_btn;
    QHBoxLayout* m_main_layout;

signals:

public slots:

private:
    const static qint32 k_fixed_height;
    const static qint32 k_fixed_width;
    const static qint32 k_spacing_default_value;

private:
    terminal_widget(const terminal_widget&);
    terminal_widget& operator=(const terminal_widget&);

}; // class terminal_widget

} // namespace gui

} // namespace kokain

#endif // TERMINAL_WIDGET_H
