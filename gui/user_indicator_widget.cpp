#include "user_indicator_widget.h"

#include <QLabel>
#include <QHBoxLayout>

#include "icon_button.h"
#include "custom_button.h"

namespace kokain
{

namespace gui
{

const qint32 user_indicator_widget::k_fixed_height = 58;
const qint32 user_indicator_widget::k_indicator_fixed_width = 5;

std::map<user_indicator_widget::online_status, QString> user_indicator_widget::s_online_status_map;

const QString user_indicator_widget::k_user_avatar_path = "./user/avatar.jpg";

void user_indicator_widget::_fill_online_status_map_()
{
    s_online_status_map[user_indicator_widget::ONLINE] = tr("online");
    s_online_status_map[user_indicator_widget::OFFLINE] = tr("offline");
    s_online_status_map[user_indicator_widget::AWAY] = tr("away");
    s_online_status_map[user_indicator_widget::BUSY] = tr("busy");
    s_online_status_map[user_indicator_widget::INVISIBLE] = tr("invisible");
}

user_indicator_widget::user_indicator_widget(QWidget *parent) :
    QWidget(parent),
    m_status(ONLINE),
    m_status_indicator_lbl(NULL),
    m_user_avatar(NULL),
    m_status_text_lbl(NULL),
    m_username_lbl(NULL),
    m_notifications_number_lbl(NULL)
{
    initialize_();
}

void user_indicator_widget::initialize_()
{
    _fill_online_status_map_();
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void user_indicator_widget::create_widgets_()
{
    m_status_indicator_lbl = new QLabel(this);
    m_user_avatar = new icon_button(this, k_fixed_height, k_fixed_height);
    m_status_text_lbl = new QLabel(s_online_status_map[m_status], this);
    m_username_lbl = new QLabel(this);
    m_notifications_number_lbl = new custom_button("3", custom_button_type_values::DEFAULT,
                                                   custom_button_category_values::DANGER, this);
    Q_ASSERT(0 != m_status_indicator_lbl);
    Q_ASSERT(0 != m_user_avatar);
    Q_ASSERT(0 != m_status_text_lbl);
    Q_ASSERT(0 != m_username_lbl);
    Q_ASSERT(0 != m_notifications_number_lbl);

    m_status_indicator_lbl->setFixedSize(k_indicator_fixed_width + k_indicator_fixed_width / 2, k_fixed_height);
    m_status_indicator_lbl->setStyleSheet("QLabel{background-color: lime;}");
    m_status_text_lbl->setStyleSheet("QLabel{color:lime;}");
    m_user_avatar->setFixedSize(k_fixed_height, k_fixed_height);
    m_user_avatar->setPixmap(QPixmap(k_user_avatar_path));
    m_username_lbl->setText("USERNAME");
    m_notifications_number_lbl->set_rounded(12);
    m_notifications_number_lbl->setFixedSize(25, 25);
    m_status_text_lbl->hide();
}

void user_indicator_widget::setup_layout_()
{
    m_main_layout = new QHBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->setSpacing(0);
    m_main_layout->addWidget(m_status_indicator_lbl);
    m_main_layout->addWidget(m_user_avatar);

    QHBoxLayout* notiflay = new QHBoxLayout();
    notiflay->setContentsMargins(0, 0, 0, 0);
    Q_ASSERT(0 != notiflay);
    notiflay->addWidget(m_notifications_number_lbl);
    notiflay->addSpacing(k_indicator_fixed_width);
    notiflay->addWidget(m_username_lbl);

    QVBoxLayout* texts_layout = new QVBoxLayout();
    Q_ASSERT(0 != texts_layout);
    texts_layout->setContentsMargins(0, 0, 0, 0);
    texts_layout->addWidget(m_status_text_lbl);
    texts_layout->addSpacing(k_fixed_height - k_indicator_fixed_width);
    texts_layout->addLayout(notiflay);

    m_main_layout->addSpacing(-m_notifications_number_lbl->width() + k_indicator_fixed_width);
    m_main_layout->addLayout(texts_layout);
}

void user_indicator_widget::make_connections_()
{

}

void user_indicator_widget::set_online_status(online_status s)
{
    m_status = s;
    on_online_status_changed();
}

user_indicator_widget::online_status user_indicator_widget::get_online_status() const
{
    return m_status;
}

void user_indicator_widget::on_online_status_changed()
{
    switch (m_status)
    {
    case ONLINE:
        m_status_indicator_lbl->setStyleSheet("QLabel{background-color:lime;}");
        m_status_text_lbl->setText(s_online_status_map[ONLINE]);
        m_status_text_lbl->setStyleSheet("QLabel{color:lime;}");
        break;
    case OFFLINE:
        m_status_indicator_lbl->setStyleSheet("QLabel{background-color:gray;}");
        m_status_text_lbl->setText(s_online_status_map[OFFLINE]);
        m_status_text_lbl->setStyleSheet("QLabel{color:gray;}");
        break;
    case AWAY:
        m_status_indicator_lbl->setStyleSheet("QLabel{background-color:yellow;}");
        m_status_text_lbl->setText(s_online_status_map[AWAY]);
        m_status_text_lbl->setStyleSheet("QLabel{color:yellow;}");
        break;
    case BUSY:
        m_status_indicator_lbl->setStyleSheet("QLabel{background-color:red;}");
        m_status_text_lbl->setText(s_online_status_map[BUSY]);
        m_status_text_lbl->setStyleSheet("QLabel{color:red;}");
        break;
    case INVISIBLE:
        m_status_indicator_lbl->setStyleSheet("QLabel{background-color:silver;}");
        m_status_text_lbl->setStyleSheet("QLabel{color:silver;}");
        m_status_text_lbl->setText(s_online_status_map[INVISIBLE]);
        break;
    default:
        m_status_indicator_lbl->setStyleSheet("QLabel{background-color:lime;}");
        m_status_text_lbl->setText(s_online_status_map[OFFLINE]);
        m_status_text_lbl->setStyleSheet("QLabel{color:lime;}");
        break;
    }
}

} // namespace gui

} // namespace kokain
