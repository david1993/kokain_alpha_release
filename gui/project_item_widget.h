#ifndef PROJECT_ITEM_WIDGET_H
#define PROJECT_ITEM_WIDGET_H

#include <QWidget>

class QLabel;
class QVBoxLayout;

namespace kokain
{

namespace gui
{

class custom_button;
class icon_button;
class circle_progress_bar;

class project_item_widget : public QWidget
{
    Q_OBJECT
public:
    explicit project_item_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    custom_button* m_logo_lbl; // currently just colored label
    icon_button* m_project_name_lbl;
    circle_progress_bar* m_progress_bar;
    QVBoxLayout* m_main_layout;

protected:
    virtual void mousePressEvent(QMouseEvent* ev);
    virtual void mouseReleaseEvent(QMouseEvent* ev);

public:
    virtual QSize sizeHint() const;

signals:
    void project_item_clicked();

public slots:

private:
    const static qint32 k_logo_size;

private:
    project_item_widget(const project_item_widget&);
    project_item_widget& operator=(const project_item_widget&);

}; // class project_item_widget

} // namespace gui

} // namespace kokain

#endif // PROJECT_ITEM_WIDGET_H
