#ifndef WINDOW_TOP_PANEL_H
#define WINDOW_TOP_PANEL_H

#include <QWidget>
#include <QDate>

#include "custom_button.h"

class QHBoxLayout;
class QLabel;
class QAction;
class QLineEdit;

namespace kokain
{

namespace gui
{

class icon_button;
class custom_menu;
class user_indicator_widget;

class window_top_panel : public QWidget
{
    Q_OBJECT

public:
    enum active_view {
        PROJECTS = 1,
        TASKS,
        CALENDAR,
        PEOPLE,
        ACTIVITY_BOARD,
        CONVERSATIONS,
        TERMINAL,
        FILES,
        SETTINGS,
        active_view_max
    };

public:
    explicit window_top_panel(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

    /// @brief Top panel update logic
public:
    void set_active_view(active_view bs);
    active_view get_active_view() const;

public slots:
    void on_active_view_change();

private:
    void setup_buttons_layout_();
    void clear_buttons_layout_();
    void load_default_button_set_();

private:
    active_view m_active_view;
    custom_button* m_manage_generic_btn;
    QHBoxLayout* m_main_layout;
    QHBoxLayout* m_buttons_layout;

private:
    // TODO: kinoi azdecutyunn ancav, kpoxes anun@ :P
    void hide_fuckin_buttons_all_();

private:
    void create_logo_widget_();

private:
    icon_button* m_logo_lbl;

    /// @brief Projects' managements buttons
private:
    void load_projects_button_set_();

signals:
    void projects_edit_button_clicked();
    void projects_remove_button_clicked();
    void projects_share_button_clicked();

private slots:
    void on_projects_manage_done_clicked();

private:
    void create_projects_management_buttons_();

private:
    custom_button* m_projects_manage_done_btn;
    custom_button* m_projects_edit_btn;
    custom_button* m_projects_remove_btn;
    custom_button* m_projects_share_btn;

    /// @brief Tasks' management buttons
private:
    void load_tasks_button_set_();

signals:
    void tasks_edit_button_clicked();
    void tasks_archive_button_clicked();
    void tasks_share_button_clicked();
    void tasks_move_button_clicked();
    void tasks_copy_button_clicked();
    void tasks_merge_button_clicked();
    void tasks_remove_button_clicked();

private slots:
    void on_tasks_manage_done_clicked();

private:
    void create_tasks_management_buttons_();
    void create_tasks_more_actions_menu_();
    void make_tasks_connections_();

private:
    custom_button* m_tasks_manage_done_btn;
    custom_button* m_tasks_edit_btn;
    custom_button* m_tasks_share_btn;
    custom_button* m_tasks_more_btn;
    custom_menu* m_tasks_more_actions_menu;
    QAction* m_tasks_move_action;
    QAction* m_tasks_copy_action;
    QAction* m_tasks_merge_action;
    QAction* m_tasks_archive_action;
    QAction* m_tasks_remove_action;

    /// @brief Calendar management buttons
private:
    void load_calendar_button_set_();
    void load_calendars_chosen_button_set_();
    void load_events_chosen_button_set_();

signals:
    void calendars_button_clicked();
    void my_calendar_button_clicked();
    void team_calendar_button_clicked();
    void project_calendar_button_clicked();
    void calendar_back_button_clicked();
    void calendar_jump_to_date_clicked(QDate);
    void calendar_day_view_button_clicked();
    void calendar_week_view_button_clicked();
    void calendar_month_view_button_clicked();
    void calendars_edit_button_clicked();
    void calendars_share_button_clicked();
    void calendars_remove_button_clicked();

    void events_button_clicked();
    void events_edit_button_clicked();
    void events_share_button_clicked();
    void events_remove_button_clicked();

private slots:
    void on_calendar_back_button_clicked();
    void on_calendar_jump_to_date_clicked();
    void on_calendars_button_clicked();
    void on_events_button_clicked();

    void on_calendar_month_view_clicked_();
    void on_calendar_week_view_clicked_();
    void on_calendar_day_view_clicked_();

private:
    void create_calendar_management_buttons_();
    void create_events_management_buttons_();
    void apply_calendar_management_buttons_settings_();
    void _debug_check_cal_buttons_();
    void make_calendar_connections_();

private:
    custom_button* m_calendars_btn;
    custom_menu* m_calendars_menu;
    /// @todo these fixed should be changed to dynamic actions
    custom_button* m_calendars_menu_btn;
    QAction* m_my_calendar_action;
    QAction* m_team_calendar_action;
    QAction* m_project_calendar_action;
    custom_button* m_back_calendar_btn;
    QLineEdit* m_jump_to_date_btn;
    custom_menu* m_calendar_views_menu;
    custom_button* m_calendar_views_menu_btn;
    QAction* m_month_view_action;
    QAction* m_week_view_action;
    QAction* m_day_view_action;
    custom_menu* m_calendars_more_menu;
    custom_button* m_calendars_more_menu_btn;
    QAction* m_calendars_edit_action;
    QAction* m_calendars_share_action;
    QAction* m_calendars_remove_action;

    custom_button* m_events_btn;
    custom_button* m_events_more_menu_btn;
    custom_menu* m_events_more_menu;
    QAction* m_events_edit_action;
    QAction* m_events_share_action;
    QAction* m_events_remove_action;

    /// @brief People management buttons
private:
    void load_people_button_set_();

signals:
    void people_manage_done_button_clicked();
    void people_edit_button_clicked();
    void people_share_button_clicked();
    void people_remove_button_clicked();
    void people_merge_button_clicked();
    void people_copy_button_clicked();

private slots:
    void on_people_manage_done_clicked();

private:
    void create_people_management_buttons_();
    void apply_people_button_settings_();
    void make_people_connections_();

private:
    custom_button* m_people_manage_done_btn;
    custom_button* m_people_edit_btn;
    custom_button* m_people_remove_btn;
    custom_button* m_people_more_btn;
    custom_menu* m_people_more_menu;
    QAction* m_people_share_action;
    QAction* m_people_merge_action;
    QAction* m_people_copy_action;

    /// @brief Conversations management buttons
private:
    void load_conversations_button_set_();

signals:
    void conversations_manage_done_button_clicked();
    void conversations_share_button_clicked();
    void conversations_remove_button_clicked();

private slots:
    void on_conversations_manage_done_clicked();

private:
    void create_conversations_management_buttons_();
    void make_conversations_connections_();

private:
    custom_button* m_conversations_manage_done_btn;
    custom_button* m_conversations_share_btn;
    custom_button* m_conversations_remove_btn;

    /// @brief Files management buttons
private:
    void load_files_button_set_();

signals:
    void files_manage_done_button_clicked();
    void files_download_button_clicked();
    void files_edit_button_clicked();
    void files_share_button_clicked();
    void files_remove_button_clicked();

private slots:
    void on_files_manage_done_clicked();

private:
    void create_files_management_buttons_();
    void make_files_connections_();

private:
    custom_button* m_files_manage_done_btn;
    custom_button* m_files_download_btn;
    custom_button* m_files_edit_btn;
    custom_button* m_files_share_btn;
    custom_button* m_files_remove_btn;

    /// @brief Settings management buttons
private:
    void load_settings_button_set_();

signals:
    void settings_general_button_clicked();
    void settings_projects_button_clicked();
    void settings_tasks_button_clicked();
    void settings_calendar_buton_clicked();
    void settings_people_button_clicked();
    void settings_conversations_button_clicked();
    void settings_files_button_clicked();

private slots:
    void on_settings_general_button_clicked();
    void on_settings_projects_button_clicked();
    void on_settings_tasks_button_clicked();
    void on_settings_calendar_button_clicked();
    void on_settings_people_button_clicked();
    void on_settings_conversations_button_clicked();
    void on_settings_files_button_clicked();

private:
    void create_settings_management_buttons_();
    void make_settings_connections_();

private:
    custom_button* m_settings_general_btn;
    custom_button* m_settings_projects_btn;
    custom_button* m_settings_tasks_btn;
    custom_button* m_settings_calendar_btn;
    custom_button* m_settings_people_btn;
    custom_button* m_settings_conversations_btn;
    custom_button* m_settings_files_btn;

    // Generic helpers
private:
    void apply_default_settings_to_button_(custom_button* b);

    /// @brief Window buttons (close, maximize, minimize)
private:
    void create_user_and_window_functional_buttons_();
    void setup_user_and_functional_buttons_layout_();

private:
    icon_button* m_close_btn;
    icon_button* m_maximize_btn;
    icon_button* m_minimize_btn;
    icon_button* m_user_avatar;
    icon_button* m_user_name_lbl;

    QHBoxLayout* m_user_and_functional_btn_layout;

private:
    void create_user_indicator_widget_();

private:
    user_indicator_widget* m_user_indicator_wgt;

    /// @brief Generic logic
public slots:
    void on_app_close();
    void on_app_maximize();
    void on_app_minimize();
    \
signals:
    void top_panel_maximize_clicked();
    void top_panel_minimize_clicked();
    void window_moving(QPoint);

protected:
    virtual void mousePressEvent(QMouseEvent* ev);
    virtual void mouseReleaseEvent(QMouseEvent* ev);
    virtual void mouseMoveEvent(QMouseEvent* ev);

private:
    bool m_moving;
    QPoint m_move_offset;

    /// @brief Const static data
private:
    const static qint32 k_window_top_panel_fixed_height;
    const static qint32 k_button_max_width;
    const static qint32 k_button_fixed_height;
    const static qint32 k_buttons_layout_stretch_value;
    const static qint32 k_user_name_fixed_height;
    const static qint32 k_top_panel_fixed_height;
    const static qint32 k_spacing_default_value;
    const static qint32 k_user_avatar_size;
    const static qint32 k_button_indicator_width;

    const static QString k_logo_path;
    const static QString k_close_btn_icon_path;
    const static QString k_close_btn_pressed_icon_path;
    const static QString k_maximize_btn_icon_path;
    const static QString k_maximize_btn_pressed_icon_path;
    const static QString k_minimize_btn_icon_path;
    const static QString k_user_avatar_path;

    const static QColor k_top_panel_bg_color;

private:
    window_top_panel(const window_top_panel&);
    window_top_panel& operator=(const window_top_panel&);

}; // class window_top_panel

} // namespace gui

} // namespace kokain

#endif // WINDOW_TOP_PANEL_H
