#ifndef ACTION_BUTTON_BLOCK_H
#define ACTION_BUTTON_BLOCK_H

#include <QWidget>

class QVBoxLayout;

namespace kokain
{

namespace gui
{

class custom_button;

class action_button_block : public QWidget
{
    Q_OBJECT
public:
    explicit action_button_block(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    custom_button* m_projects_btn;
    custom_button* m_tasks_btn;
    custom_button* m_calendar_btn;
    custom_button* m_people_btn;
    custom_button* m_conversations_btn;
    custom_button* m_files_btn;
    custom_button* m_settings_btn;

private:
    QVBoxLayout* m_main_layout;

signals:
    void projects_clicked();
    void tasks_clicked();
    void calendar_clicked();
    void people_clicked();
    void conversations_clicked();
    void files_clicked();
    void settings_clicked();

public slots:
    void on_projects_clicked();
    void on_tasks_clicked();
    void on_calendar_clicked();
    void on_people_clicked();
    void on_conversations_clicked();
    void on_files_clicked();
    void on_settings_clicked();

    void set_button_categories_to_default();

private:
    const static qint32 k_fixed_button_size;

private:
    action_button_block(const action_button_block&);
    action_button_block& operator=(const action_button_block&);

}; // class action_button_block

} // namespace gui

} // namespace kokain

#endif // ACTION_BUTTON_BLOCK_H
