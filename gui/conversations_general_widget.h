#ifndef CONVERSATIONS_GENERAL_WIDGET_H
#define CONVERSATIONS_GENERAL_WIDGET_H

#include <QWidget>

class QVBoxLayout;
class QLabel;

namespace kokain
{

namespace gui
{

class conversations_general_widget : public QWidget
{
    Q_OBJECT
public:
    explicit conversations_general_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QLabel* m_temp_lbl;

private:
    QVBoxLayout* m_main_layout;

signals:

public slots:

}; // class conversations_general_widget

} // namespace gui

} // namespace kokain

#endif // CONVERSATIONS_GENERAL_WIDGET_H
