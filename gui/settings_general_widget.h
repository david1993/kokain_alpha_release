#ifndef SETTINGS_GENERAL_WIDGET_H
#define SETTINGS_GENERAL_WIDGET_H

#include <QWidget>

class QLabel;
class QVBoxLayout;

namespace kokain
{

namespace gui
{

class settings_general_widget : public QWidget
{
    Q_OBJECT
public:
    explicit settings_general_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QLabel* m_temp_lbl;

private:
    QVBoxLayout* m_main_layout;

signals:

public slots:

}; // class settings_general_widget

} // namespace gui

} // namespace kokain

#endif // SETTINGS_GENERAL_WIDGET_H
