#ifndef NOTIFICATION_LIST_WIDGET_H
#define NOTIFICATION_LIST_WIDGET_H

#include <QWidget>

class QCheckBox;
class QHBoxLayout;

namespace kokain {
namespace core {
    class notification_manager;
}
namespace gui {

class notification_widget;
class custom_label;
class flow_layout;

class notification_list_widget : public QWidget
{
   // Q_OBJECT

public:
    notification_list_widget(QWidget* parent = 0);
    ~notification_list_widget();

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

public:
    bool is_checked();
private:
    QCheckBox* m_checkbox;
    custom_label* m_mark_as_read_button;
    custom_label* m_mark_as_unread_button;
    custom_label* m_delete_button;

public slots:
    void add_notification(notification_widget* the_notification);
private:
    QList<notification_widget*> m_notification_list;

private:
    custom_label* m_more_button;

private:
    QHBoxLayout* m_main_layout;

public:
    kokain::core::notification_manager* get_notification_manager_instance();
private:
    kokain::core::notification_manager* m_manager;

public slots:
    void set_checkboxs_state();
    void mark_as_read_selected();
    void mark_as_unread_selected();
    void delete_selected();

signals:
    void signal_mark_as_read(const QString& id);
    void signal_mark_as_unread(const QString& id);
    void signal_remove_notification(const QString& id);
    void signal_sender_clicked(const QString& notification_id);
    void signal_text_clicked(const QString& notification_id);
    void signal_entity_clicked(const QString& notification_id);
    void signal_more_clicked();

}; // notification_list_widget
} // gui
} // kokain

#endif // NOTIFICATION_LIST_WIDGET_H
