#ifndef CUSTOM_BUTTON_H
#define CUSTOM_BUTTON_H

#include <utility>

#include <QPushButton>

#include "custom_widget_style.h"

namespace kokain
{

namespace gui
{

namespace custom_button_type_values
{
    enum custom_button_type
    {
        DEFAULT = 1,
        LARGE,
        TILE,
        DROPDOWN,
        custom_button_type_e_max
    };

} // namespace custom_button_type_values

namespace custom_button_category_values
{
    enum custom_button_category
    {
        DEFAULT = 1,
        DANGER,
        INFO,
        INVERSE,
        WARNING_LIGHT,
        WARNING,
        PRIMARY,
        SUCCESS,
        HIGHLIGHT,
        DISABLED,
        custom_button_category_max
    };

} // namespace custom_button_category_values

class custom_button : public QPushButton
{
    Q_OBJECT
public:
    custom_button(QWidget* p = 0);
    explicit custom_button(const QString& t, QWidget* p = 0);
    explicit custom_button(custom_button_type_values::custom_button_type tp, QWidget* p = 0);
    explicit custom_button(custom_button_category_values::custom_button_category c, QWidget* p = 0);
    custom_button(const QString& n, custom_button_type_values::custom_button_type tp,
            custom_button_category_values::custom_button_category c,
            QWidget* p = 0);
    virtual ~custom_button();

    /// @brief Style
public:
    void set_custom_style(custom_widget_style* st);
    custom_widget_style* custom_style();

private:
    custom_widget_style* m_custom_style;

private:
    void initialize_();
    void make_connections_();

    // styles
private:
    void update_style_(custom_widget_style* st);
    void apply_button_type_style_();
    void apply_button_category_style_();

    void complete_default_type_style_();
    void complete_large_type_style_();
    void complete_tile_type_style_();
    void complete_dropdown_type_style_();

    std::pair<custom_color_values::custom_color, custom_color_values::custom_color>
        get_colors_by_category_() const;

    void set_button_fixed_size_();

    /// @brief Common settings
public:
    void set_disabled(bool d);
    void set_enabled(bool e);
    bool enabled() const;
    bool disabled() const;
    void set_rounded(bool r);
    void set_rounded(qint32 r);
    bool rounded() const;

private:
    bool m_is_disabled;
    bool m_is_rounded;

    /// @brief Indicator
public:
    bool has_indicator() const;
    void set_indicator();
    void remove_indicator();
    void set_indicator_color(custom_color_values::custom_color c);
    void set_indicator_position(border_position_values::border_position p);

private:
    bool m_has_indicator;
    custom_color_values::custom_color m_indicator_color;
    border_position_values::border_position m_indicator_pos;

    /// @brief Button type
public:
    custom_button_type_values::custom_button_type type() const;
    void set_type(custom_button_type_values::custom_button_type t);

private:
    custom_button_type_values::custom_button_type m_type;

    /// @brief Button category
public:
    void set_category(custom_button_category_values::custom_button_category c);
    custom_button_category_values::custom_button_category category() const;

private:
    custom_button_category_values::custom_button_category m_category;
    custom_button_category_values::custom_button_category m_prev_category;

public:
    void set_hover_color(custom_color_values::custom_color c);

signals:

public slots:
    void on_clicked();
    void on_pressed();
    void on_released();

private:
    const static qint32 k_border_width_minimum;
    const static qint32 k_default_padding_lr;
    const static qint32 k_default_padding_tb;
    const static qint32 k_large_padding_lr;
    const static qint32 k_large_padding_tb;
    const static qint32 k_tile_padding_lr;
    const static qint32 k_tile_padding_tb;
    const static qint32 k_rounded_min_value;

}; // class custom_button

} // namespace gui

} // namespace kokain

#endif // CUSTOM_BUTTON_H
