#include "custom_menu.h"

namespace kokain
{

namespace gui
{

const qint32 custom_menu::k_item_width_default = 50;
const qint32 custom_menu::k_item_height_default = 30;

custom_menu::custom_menu(QWidget *parent) :
    QMenu(parent),
    m_custom_style(NULL),
    m_color(custom_color_values::WET_ASPHALT),
    m_item_width(k_item_width_default),
    m_item_height(k_item_height_default)
{
    initialize_();
}

void custom_menu::initialize_()
{
    setup_style_();
}

void custom_menu::setup_style_()
{
    m_custom_style = new custom_widget_style("QMenu");
    Q_ASSERT(0 != m_custom_style);
    m_custom_style->set_background_color(m_color);
    custom_border_style bs;
    bs.set_radius(0);
    m_custom_style->set_border_style(bs);
    m_custom_style->set_margin(2);

    update_style_();
}

void custom_menu::update_style_()
{
    Q_ASSERT(0 != m_custom_style);
    QString append = QString("QMenu::item{")
            + QString("padding: 2px 25px 2px 20px;border: 1px solid transparent;height:")
            + QString::number(m_item_height) + ";"
            + "width:" + QString::number(m_item_width) + ";}";
    append += "QMenu::item:selected {background: rgba(100, 100, 100, 150);}";
    m_custom_style->append_style_string(append);
    this->setStyleSheet(m_custom_style->to_string());
}

void custom_menu::set_custom_style(custom_widget_style *st)
{
    Q_ASSERT(0 != st);
    m_custom_style = st;
    update_style_();
}

custom_widget_style* custom_menu::custom_style()
{
    return m_custom_style;
}

void custom_menu::set_color(custom_color_values::custom_color c)
{
    m_color = c;
    m_custom_style->set_background_color(m_color);
    update_style_();
}

custom_color_values::custom_color custom_menu::color() const
{
    return m_color;
}

void custom_menu::set_item_height(qint32 h)
{
    m_item_height = (h > 0) ? h : k_item_height_default;
    update_style_();
}

qint32 custom_menu::item_height() const
{
    return m_item_height;
}

void custom_menu::set_item_width(qint32 w)
{
    m_item_width = (w > 0) ? w : k_item_width_default;
    update_style_();
}

qint32 custom_menu::item_width() const
{
    return m_item_width;
}

void custom_menu::set_item_size(qint32 w, qint32 h)
{
    set_item_height(h);
    set_item_width(w);
}

} // namespace gui

} // namespace kokain
