#include "custom_widget_style.h"

#include <QDebug>

namespace kokain
{

namespace gui
{

// { start custom colors

std::map<custom_color_values::custom_color, QString> custom_color_mapper::s_color_code_map;

bool custom_color_mapper::s_maps_initialized = false;

const QString custom_color_mapper::k_color_code_default = "#ECF0F1";
const QString custom_color_mapper::k_color_code_clouds = "#ECF0F1";
const QString custom_color_mapper::k_color_code_silver = "#BDC3C7";
const QString custom_color_mapper::k_color_code_concrete = "#95A5A6";
const QString custom_color_mapper::k_color_code_asbestos = "#7F8C8D";
const QString custom_color_mapper::k_color_code_alizarin = "#E74C3C";
const QString custom_color_mapper::k_color_code_pomegranate = "#C0392B";
const QString custom_color_mapper::k_color_code_peter_river = "#3498DB";
const QString custom_color_mapper::k_color_code_belize_hole = "#2980B9";
const QString custom_color_mapper::k_color_code_wet_asphalt = "#34495E";
const QString custom_color_mapper::k_color_code_midnight_blue = "#2C3E50";
const QString custom_color_mapper::k_color_code_sun_flower = "#F1C40F";
const QString custom_color_mapper::k_color_code_orange = "#F39C12";
const QString custom_color_mapper::k_color_code_carrot = "#E67E22";
const QString custom_color_mapper::k_color_code_pumpkin = "#D35400";
const QString custom_color_mapper::k_color_code_turquoise = "#1ABC9C";
const QString custom_color_mapper::k_color_code_green_sea = "#16A085";
const QString custom_color_mapper::k_color_code_emerald = "#2ECC71";
const QString custom_color_mapper::k_color_code_nephritis = "#27AE60";
const QString custom_color_mapper::k_color_code_amethyst = "#9B59B6";
const QString custom_color_mapper::k_color_code_wisteria = "#8E44AD";
const QString custom_color_mapper::k_color_code_white = "#FFFFFF";

void custom_color_mapper::initialize_color_maps()
{
    s_color_code_map[custom_color_values::DEFAULT] = k_color_code_default;
    s_color_code_map[custom_color_values::CLOUDS] = k_color_code_clouds;
    s_color_code_map[custom_color_values::SILVER] = k_color_code_silver;
    s_color_code_map[custom_color_values::CONCRETE] = k_color_code_concrete;
    s_color_code_map[custom_color_values::ASBESTOS] = k_color_code_asbestos;
    s_color_code_map[custom_color_values::ALIZARIN] = k_color_code_alizarin;
    s_color_code_map[custom_color_values::POMEGRANATE] = k_color_code_pomegranate;
    s_color_code_map[custom_color_values::PETER_RIVER] = k_color_code_peter_river;
    s_color_code_map[custom_color_values::BELIZE_HOLE] = k_color_code_belize_hole;
    s_color_code_map[custom_color_values::WET_ASPHALT] = k_color_code_wet_asphalt;
    s_color_code_map[custom_color_values::MIDNIGHT_BLUE] = k_color_code_midnight_blue;
    s_color_code_map[custom_color_values::SUN_FLOWER] = k_color_code_sun_flower;
    s_color_code_map[custom_color_values::ORANGE] = k_color_code_orange;
    s_color_code_map[custom_color_values::CARROT] = k_color_code_carrot;
    s_color_code_map[custom_color_values::PUMPKIN] = k_color_code_pumpkin;
    s_color_code_map[custom_color_values::TURQUOISE] = k_color_code_turquoise;
    s_color_code_map[custom_color_values::GREEN_SEA] = k_color_code_green_sea;
    s_color_code_map[custom_color_values::EMERALD] = k_color_code_emerald;
    s_color_code_map[custom_color_values::NEPHRITIS] = k_color_code_nephritis;
    s_color_code_map[custom_color_values::AMETHYST] = k_color_code_amethyst;
    s_color_code_map[custom_color_values::WISTERIA] = k_color_code_wisteria;
}

QString custom_color_mapper::get_color_code_by_enum(custom_color_values::custom_color e)
{
    if (!s_maps_initialized) {
        initialize_color_maps();
        s_maps_initialized = true;
    }
    return s_color_code_map[e];
}

QString custom_color_mapper::get_color_code_by_name(const QString &n)
{
    return n;
    // TODO: this nice-to-do feature should be implemented later
}

// } end custom colors

// { start border

std::map<border_style_values::border_style, QString> border_style_mapper::s_border_style_map;

const QString border_style_mapper::k_border_style_none = "none";
const QString border_style_mapper::k_border_style_dashed = "dashed";
const QString border_style_mapper::k_border_style_dot_dash ="dot-dash";
const QString border_style_mapper::k_border_style_dot_dot_dash = "dot-dot-dash";
const QString border_style_mapper::k_border_style_dotted = "dotted";
const QString border_style_mapper::k_border_style_double = "double";
const QString border_style_mapper::k_border_style_groove = "groove";
const QString border_style_mapper::k_border_style_inset = "inset";
const QString border_style_mapper::k_border_style_outset = "outset";
const QString border_style_mapper::k_border_style_ridge = "ridge";
const QString border_style_mapper::k_border_style_solid = "solid";

void border_style_mapper::initialize_border_styles()
{
    s_border_style_map[border_style_values::NONE] = k_border_style_none;
    s_border_style_map[border_style_values::DASHED] = k_border_style_dashed;
    s_border_style_map[border_style_values::DOT_DASH] = k_border_style_dot_dash;
    s_border_style_map[border_style_values::DOT_DOT_DASH] = k_border_style_dot_dot_dash;
    s_border_style_map[border_style_values::DOTTED] = k_border_style_dotted;
    s_border_style_map[border_style_values::DOUBLE] = k_border_style_double;
    s_border_style_map[border_style_values::GROOVE] = k_border_style_groove;
    s_border_style_map[border_style_values::INSET] = k_border_style_inset;
    s_border_style_map[border_style_values::OUTSET] = k_border_style_outset;
    s_border_style_map[border_style_values::RIDGE] = k_border_style_ridge;
    s_border_style_map[border_style_values::SOLID] = k_border_style_solid;
}

custom_border_style::custom_border_style()
{
    initialize_();
}

custom_border_style::custom_border_style(border_style_values::border_style s)
{
    initialize_();
    set_whole_style_(s);
}

custom_border_style::custom_border_style(qint32 w)
{
    initialize_();
    set_whole_width_(w);
}

custom_border_style::custom_border_style(custom_color_values::custom_color c)
{
    initialize_();
    set_whole_color_(c);
}

custom_border_style::custom_border_style(border_style_values::border_style s, qint32 w,
        custom_color_values::custom_color c)
{
    initialize_();
    set_whole_style_(s);
    set_whole_width_(w);
    set_whole_color_(c);
}

void custom_border_style::set_whole_style_(border_style_values::border_style s)
{
    set_style(s, border_position_values::LEFT);
    set_style(s, border_position_values::RIGHT);
    set_style(s, border_position_values::TOP);
    set_style(s, border_position_values::BOTTOM);
}

void custom_border_style::set_whole_color_(custom_color_values::custom_color c)
{
    set_color(c, border_position_values::LEFT);
    set_color(c, border_position_values::RIGHT);
    set_color(c, border_position_values::TOP);
    set_color(c, border_position_values::BOTTOM);
}

void custom_border_style::set_whole_color_(const QString& c)
{
    set_color(c, border_position_values::LEFT);
    set_color(c, border_position_values::RIGHT);
    set_color(c, border_position_values::TOP);
    set_color(c, border_position_values::BOTTOM);
}

void custom_border_style::set_whole_width_(qint32 w)
{
    set_width(w, border_position_values::LEFT);
    set_width(w, border_position_values::RIGHT);
    set_width(w, border_position_values::TOP);
    set_width(w, border_position_values::BOTTOM);
}

void custom_border_style::set_whole_radius_(qint32 r)
{
    set_radius(r, border_position_values::TOP_LEFT);
    set_radius(r, border_position_values::TOP_RIGHT);
    set_radius(r, border_position_values::BOTTOM_LEFT);
    set_radius(r, border_position_values::BOTTOM_RIGHT);
}

custom_border_style::custom_border_style(const custom_border_style& o)
{
    m_border_components = o.m_border_components;
}

custom_border_style& custom_border_style::operator =(const custom_border_style& o)
{
    if (this != &o)
    {
        m_border_components = o.m_border_components;
    }
    return *this;
}

custom_border_style::~custom_border_style()
{
    m_border_components.clear();
}

void custom_border_style::initialize_()
{
    m_border_components[border_position_values::LEFT] = generate_a_ready_to_go_vector_();
    m_border_components[border_position_values::RIGHT] = generate_a_ready_to_go_vector_();
    m_border_components[border_position_values::TOP] = generate_a_ready_to_go_vector_();
    m_border_components[border_position_values::BOTTOM] = generate_a_ready_to_go_vector_();
}

std::vector<std::pair<QString, QString> >
custom_border_style::generate_a_ready_to_go_vector_() const
{
    cmap_value_t vec;
    vec.push_back(std::make_pair(style_sheet_component::_style_(), ""));
    vec.push_back(std::make_pair(style_sheet_component::_color_(), ""));
    vec.push_back(std::make_pair(style_sheet_component::_width_(), ""));
    vec.push_back(std::make_pair(style_sheet_component::_radius_(), ""));

    return vec;
}

void custom_border_style::set_color(const QString& c, border_pos_t p)
{
    if (border_position_values::NONE == p) {
        set_whole_color_(c);
        return;
    }
    set_value_for_key_(m_border_components[p], style_sheet_component::_color_(), c);
}

void custom_border_style::set_color(custom_color_values::custom_color c,
        border_pos_t p)
{
    set_color(custom_color_mapper::get_color_code_by_enum(c), p);
}

QString custom_border_style::color(border_pos_t p)
{
    if (border_position_values::NONE == p) {
        p = border_position_values::LEFT;
    }
    return get_value_for_key_(m_border_components[p], style_sheet_component::_color_());
}

QString custom_border_style::get_value_for_key_(
        const std::vector<std::pair<QString, QString> >& vec, const QString& val) const
{
    for (std::vector<std::pair<QString, QString> >::const_iterator it = vec.begin();
            it != vec.end(); ++it)
    {
        if (val == it->first) {
            return it->second;
        }
    }
    return "";
}

void custom_border_style::set_value_for_key_(
        std::vector<std::pair<QString, QString> >& in_vec,
        const QString& key, const QString& value)
{
    for (std::vector<std::pair<QString, QString> >::iterator it = in_vec.begin();
            it != in_vec.end(); ++it)
    {
        if (key == it->first) {
            it->second = value;
            break;
        }
    }
}

void custom_border_style::set_style(border_style_values::border_style s, border_pos_t p)
{
    if (border_position_values::NONE == p) {
        set_whole_style_(s);
        return;
    }
    set_value_for_key_(m_border_components[p], style_sheet_component::_style_(),
            border_style_mapper::get_border_style_by_enum(s));
}

QString custom_border_style::style(border_pos_t p)
{
    if (border_position_values::NONE == p) {
        p = border_position_values::LEFT;
    }
    return get_value_for_key_(m_border_components[p], style_sheet_component::_style_());
}

void custom_border_style::set_width(qint32 w, border_pos_t p)
{
    if (border_position_values::NONE == p) {
        set_whole_width_(w);
        return;
    }
    set_value_for_key_(m_border_components[p], style_sheet_component::_width_(),
            QString::number(w));
}

qint32 custom_border_style::width(border_pos_t p)
{
    if (border_position_values::NONE == p) {
        p = border_position_values::LEFT;
    }
    QString s =  get_value_for_key_(m_border_components[p],
            style_sheet_component::_width_());
    return s.toInt();
}

void custom_border_style::set_radius(qint32 r, border_pos_t p)
{
    if (border_position_values::NONE == p) {
        set_whole_radius_(r);
        return;
    }
    switch (p)
    {
    case border_position_values::TOP_LEFT:
        set_value_for_key_(m_border_components[border_position_values::LEFT],
                style_sheet_component::_radius_(), QString::number(r));
        break;
    case border_position_values::TOP_RIGHT:
        set_value_for_key_(m_border_components[border_position_values::TOP],
                style_sheet_component::_radius_(), QString::number(r));
        break;
    case border_position_values::BOTTOM_RIGHT:
        set_value_for_key_(m_border_components[border_position_values::RIGHT],
                style_sheet_component::_radius_(), QString::number(r));
        break;
    case border_position_values::BOTTOM_LEFT:
        set_value_for_key_(m_border_components[border_position_values::BOTTOM],
                style_sheet_component::_radius_(), QString::number(r));
        break;
    default:
        set_whole_radius_(r);
        break;
    }
}

qint32 custom_border_style::radius(border_pos_t p)
{
    if (border_position_values::NONE == p) {
        p = border_position_values::TOP_LEFT;
    }
    QString val;
    switch (p)
    {
    case border_position_values::TOP_LEFT:
        val = get_value_for_key_(m_border_components[border_position_values::LEFT],
                style_sheet_component::_radius_());
        break;
    case border_position_values::TOP_RIGHT:
        val = get_value_for_key_(m_border_components[border_position_values::TOP],
                style_sheet_component::_radius_());
        break;
    case border_position_values::BOTTOM_RIGHT:
        val = get_value_for_key_(m_border_components[border_position_values::RIGHT],
                style_sheet_component::_radius_());
        break;
    case border_position_values::BOTTOM_LEFT:
        val = get_value_for_key_(m_border_components[border_position_values::BOTTOM],
                style_sheet_component::_radius_());
        break;
    default:
        val = get_value_for_key_(m_border_components[border_position_values::LEFT],
                        style_sheet_component::_radius_());
        break;
    }
    return val.toInt();
}

// TODO: later modify style_sheet_component to not hardcode all left, right, etc.
QString custom_border_style::generate_border_left_style_code_()
{
    cmap_value_t vec = m_border_components[border_position_values::LEFT];
    QString left_style = "";
    QString tmp = get_value_for_key_(vec, style_sheet_component::_width_());
    if (!tmp.isEmpty()) {
        left_style += style_sheet_component::border_left_width(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_style_());
    if (!tmp.isEmpty()) {
        left_style += style_sheet_component::border_left_style(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_color_());
    if (!tmp.isEmpty()) {
        left_style += style_sheet_component::border_left_color(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_radius_());
    if (!tmp.isEmpty()) {
        left_style += style_sheet_component::border_top_left_radius(tmp);
    }

    return left_style;
}

QString custom_border_style::generate_border_right_style_code_()
{
    cmap_value_t vec = m_border_components[border_position_values::RIGHT];
    QString right_style = "";
    QString tmp = get_value_for_key_(vec, style_sheet_component::_width_());
    if (!tmp.isEmpty()) {
        right_style += style_sheet_component::border_right_width(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_style_());
    if (!tmp.isEmpty()) {
        right_style += style_sheet_component::border_right_style(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_color_());
    if (!tmp.isEmpty()) {
        right_style += style_sheet_component::border_right_color(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_radius_());
    if (!tmp.isEmpty()) {
        right_style += style_sheet_component::border_bottom_right_radius(tmp);
    }

    return right_style;
}

QString custom_border_style::generate_border_top_style_code_()
{
    cmap_value_t vec = m_border_components[border_position_values::TOP];
    QString top_style = "";
    QString tmp = get_value_for_key_(vec, style_sheet_component::_width_());
    if (!tmp.isEmpty()) {
        top_style += style_sheet_component::border_top_width(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_style_());
    if (!tmp.isEmpty()) {
        top_style += style_sheet_component::border_top_style(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_color_());
    if (!tmp.isEmpty()) {
        top_style += style_sheet_component::border_top_color(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_radius_());
    if (!tmp.isEmpty()) {
        top_style += style_sheet_component::border_top_right_radius(tmp);
    }

    return top_style;
}

QString custom_border_style::generate_border_bottom_style_code_()
{
    cmap_value_t vec = m_border_components[border_position_values::BOTTOM];
    QString bottom_style = "";
    QString tmp = get_value_for_key_(vec, style_sheet_component::_width_());
    if (!tmp.isEmpty()) {
        bottom_style += style_sheet_component::border_bottom_width(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_style_());
    if (!tmp.isEmpty()) {
        bottom_style += style_sheet_component::border_bottom_style(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_color_());
    if (!tmp.isEmpty()) {
        bottom_style += style_sheet_component::border_bottom_color(tmp);
    }
    tmp = get_value_for_key_(vec, style_sheet_component::_radius_());
    if (!tmp.isEmpty()) {
        bottom_style += style_sheet_component::border_bottom_left_radius(tmp);
    }

    return bottom_style;
}

QString custom_border_style::get_style_code()
{
    // TODO: all these calls need optimization!!
    QString whole_style = "";
    QString side_style = generate_border_left_style_code_();
    if (!side_style.isEmpty()) {
        whole_style += side_style;
    }
    side_style = generate_border_right_style_code_();
    if (!side_style.isEmpty()) {
        whole_style += side_style;
    }
    side_style = generate_border_top_style_code_();
    if (!side_style.isEmpty()) {
        whole_style += side_style;
    }
    side_style = generate_border_bottom_style_code_();
    if (!side_style.isEmpty()) {
        whole_style += side_style;
    }

    return whole_style;
}

// } end border

// { start font

QString custom_font_style::get_font_style_by_enum(font_style_values::font_style e)
{
    switch (e)
    {
    case font_style_values::BOLD:
        return "bold";
        break;
    case font_style_values::ITALIC:
        return "italic";
        break;
    case font_style_values::OBLIQUE:
        return "oblique";
        break;
    default:
        return "normal";
        break;
    }
    return "normal";
}

QString custom_font_style::get_text_decoration_by_enum(text_decoration_values::text_decoration e)
{
    switch (e)
    {
    case text_decoration_values::UNDERLINE:
        return "underline";
        break;
    case text_decoration_values::OVERLINE:
        return "overline";
        break;
    case text_decoration_values::LINE_THROUGH:
        return "line-through";
        break;
    default:
        return "none";
        break;
    }
    return "none";
}

custom_font_style::custom_font_style()
{
    m_style = get_font_style_by_enum(font_style_values::NORMAL);
    m_size = 12;
    m_text_decoration = get_text_decoration_by_enum(text_decoration_values::NONE);
}

custom_font_style::custom_font_style(font_style_values::font_style e,
                                     qint32 s, text_decoration_values::text_decoration d)
{
    m_style = get_font_style_by_enum(e);
    m_size = s;
    m_text_decoration = get_text_decoration_by_enum(d);
}

custom_font_style::custom_font_style(const custom_font_style &o)
{
    m_style = o.m_style;
    m_size = o.m_size;
    m_text_decoration = o.m_text_decoration;
}

custom_font_style& custom_font_style::operator =(const custom_font_style& o)
{
    if (this != &o)
    {
        m_style = o.m_style;
        m_size = o.m_size;
        m_text_decoration = o.m_text_decoration;
    }
    return *this;
}

QString custom_font_style::style() const
{
    return m_style;
}

void custom_font_style::set_style(font_style_values::font_style e)
{
    m_style = get_font_style_by_enum(e);
}

qint32 custom_font_style::size() const
{
    return m_size;
}

void custom_font_style::set_size(qint32 s)
{
    m_size = s;
}

QString custom_font_style::text_decoration() const
{
    return m_text_decoration;
}

void custom_font_style::set_text_decoration(text_decoration_values::text_decoration e)
{
    m_text_decoration = get_text_decoration_by_enum(e);
}

QString custom_font_style::get_style_code() const
{
    // TODO: later develop for em and other non-px formats
    return ( style_sheet_component::font_size(QString::number(m_size))
            + style_sheet_component::font_style(m_style)
            + style_sheet_component::text_decoration(m_text_decoration) );
}

// } end font

QString border_style_mapper::get_border_style_by_enum(border_style_values::border_style e)
{
    return s_border_style_map[e];
}

custom_widget_style::custom_widget_style() :
    m_appended_style_string(""),
    m_color(custom_color_mapper::get_color_code_by_enum(custom_color_values::DEFAULT)),
    m_background_color(custom_color_mapper::get_color_code_by_enum(custom_color_values::DEFAULT)),
    m_padding(0),
    m_padding_left(0),
    m_padding_right(0),
    m_padding_top(0),
    m_padding_bottom(0),
    m_margin(0),
    m_margin_left(0),
    m_margin_right(0),
    m_margin_top(0),
    m_margin_bottom(0),
    m_widget_state(widget_state_values::NONE),
    m_hover_state_style(NULL),
    m_pressed_state_style(NULL),
    m_widget_name("QWidget")
{
    m_style_string = generate_whole_style_code_();
}

custom_widget_style::custom_widget_style(widget_state_values::widget_state s) :
    m_appended_style_string(""),
    m_color(custom_color_mapper::get_color_code_by_enum(custom_color_values::DEFAULT)),
    m_background_color(custom_color_mapper::get_color_code_by_enum(custom_color_values::DEFAULT)),
    m_padding(0),
    m_padding_left(0),
    m_padding_right(0),
    m_padding_top(0),
    m_padding_bottom(0),
    m_margin(0),
    m_margin_left(0),
    m_margin_right(0),
    m_margin_top(0),
    m_margin_bottom(0),
    m_widget_state(s),
    m_hover_state_style(NULL),
    m_pressed_state_style(NULL),
    m_widget_name("QWidget")
{
    m_style_string = generate_whole_style_code_();
}

custom_widget_style::custom_widget_style(const QString &str) :
    m_appended_style_string(""),
    m_color(custom_color_mapper::get_color_code_by_enum(custom_color_values::DEFAULT)),
    m_background_color(custom_color_mapper::get_color_code_by_enum(custom_color_values::DEFAULT)),
    m_padding(0),
    m_padding_left(0),
    m_padding_right(0),
    m_padding_top(0),
    m_padding_bottom(0),
    m_margin(0),
    m_margin_left(0),
    m_margin_right(0),
    m_margin_top(0),
    m_margin_bottom(0),
    m_widget_state(widget_state_values::NONE),
    m_hover_state_style(NULL),
    m_pressed_state_style(NULL),
    m_widget_name("QWidget")
{
    m_style_string = str;
    if (m_style_string.isEmpty()) {
        m_style_string = generate_whole_style_code_();
    }
}

custom_widget_style::custom_widget_style(const custom_widget_style &o) :
    m_style_string(o.m_style_string),
    m_appended_style_string(o.m_appended_style_string),
    m_color(o.m_color),
    m_background_color(o.m_background_color),
    m_border_style(o.m_border_style),
    m_padding(o.m_padding),
    m_padding_left(o.m_padding_left),
    m_padding_right(o.m_padding_right),
    m_padding_top(o.m_padding_top),
    m_padding_bottom(o.m_padding_bottom),
    m_margin(o.m_margin),
    m_margin_left(o.m_margin_left),
    m_margin_right(o.m_margin_right),
    m_margin_top(o.m_margin_top),
    m_margin_bottom(o.m_margin_bottom),
    m_font_style(o.m_font_style),
    m_widget_state(o.m_widget_state),
    m_widget_name(o.m_widget_name)
{
    m_pressed_state_style = new custom_widget_style(*(o.m_pressed_state_style));
    m_hover_state_style = new custom_widget_style(*(o.m_hover_state_style));
}

custom_widget_style& custom_widget_style::operator =(const custom_widget_style& o)
{
    if (this != &o)
    {
        m_appended_style_string = o.m_appended_style_string;
        m_color = o.m_color;
        m_background_color = o.m_background_color;
        m_border_style = o.m_border_style;
        m_padding = o.m_padding;
        m_padding_left = o.m_padding_left;
        m_padding_right = o.m_padding_right;
        m_padding_top = o.m_padding_top;
        m_padding_bottom = o.m_padding_bottom;
        m_margin = o.m_margin;
        m_margin_left = o.m_margin_left;
        m_margin_top = o.m_margin_top;
        m_margin_bottom = o.m_margin_bottom;
        m_font_style = o.m_font_style;
        m_widget_state = o.m_widget_state;

        delete m_hover_state_style;
        m_hover_state_style = new custom_widget_style(*(o.m_hover_state_style));

        delete m_pressed_state_style;
        m_pressed_state_style = new custom_widget_style(*(o.m_pressed_state_style));

        m_widget_name = o.m_widget_name;
    }
    return *this;
}

custom_widget_style::~custom_widget_style()
{
    delete m_hover_state_style;
    m_hover_state_style = NULL;
    delete m_pressed_state_style;
    m_pressed_state_style = NULL;
}

void custom_widget_style::set_style_string(const QString &s)
{
    m_style_string = s;
    if (m_style_string.isEmpty()) {
        m_style_string = generate_whole_style_code_();
    }
}

QString custom_widget_style::style_string() const
{
    return m_style_string;
}

void custom_widget_style::append_style_string(const QString &s)
{
    m_appended_style_string = s;
}

QString custom_widget_style::appended_style_string() const
{
    return m_appended_style_string;
}

QString custom_widget_style::generate_whole_style_code_()
{
    return ( generate_color_style_code_()
            + generate_border_style_code_()
            + generate_padding_and_margin_style_code_()
            + generate_font_style_code_()
            );
}

QString custom_widget_style::to_string()
{
    m_style_string = widget_name();
    return to_string_private_();
}

QString custom_widget_style::to_string(const QString& suffix)
{
    m_style_string = widget_name();
    if (!suffix.isEmpty()) {
        m_style_string += ":" + suffix;
    }
    return to_string_private_();
}

QString custom_widget_style::to_string_private_()
{
    if (widget_state_values::HOVER == m_widget_state) {
        m_style_string += style_sheet_component::hover_state();
    }
    else if (widget_state_values::PRESSED == m_widget_state) {
        m_style_string += style_sheet_component::pressed_state();
    }

    m_style_string += style_sheet_component::brace_this(generate_whole_style_code_());
    m_style_string += m_appended_style_string;
    return m_style_string;
}

QString custom_widget_style::test_and_set_color(const QString &v)
{
    if (v.isEmpty()) {
        return custom_color_mapper::get_color_code_by_enum(
                    custom_color_values::DEFAULT);
    }
    return v;
}

qint32 custom_widget_style::test_and_set_int32_(qint32 v)
{
    if (v < 0) {
        return 0;
    }
    return v;
}

void custom_widget_style::set_color(const QString &c)
{
    m_color = test_and_set_color(c);
}

void custom_widget_style::set_color(custom_color_values::custom_color c)
{
    m_color = custom_color_mapper::get_color_code_by_enum(c);
}

QString custom_widget_style::color() const
{
    return m_color;
}

void custom_widget_style::set_background_color(const QString &c)
{
    m_background_color = test_and_set_color(c);
}

void custom_widget_style::set_background_color(custom_color_values::custom_color c)
{
    m_background_color = custom_color_mapper::get_color_code_by_enum(c);
}

QString custom_widget_style::background_color() const
{
    return m_background_color;
}

QString custom_widget_style::generate_color_style_code_() const
{
    QString tmp = "";
    if (!m_color.isEmpty()) {
        tmp += style_sheet_component::color(m_color);
    }
    if (!m_background_color.isEmpty()) {
        tmp += style_sheet_component::background_color(m_background_color);
    }
    return tmp;
}

void custom_widget_style::set_border_style(const custom_border_style& b)
{
    m_border_style = b;
}

custom_border_style custom_widget_style::border_style() const
{
    return m_border_style;
}

QString custom_widget_style::generate_border_style_code_()
{
    return m_border_style.get_style_code();
}

void custom_widget_style::set_padding(qint32 p)
{
    m_padding = test_and_set_int32_(p);
}

void custom_widget_style::set_padding(qint32 left, qint32 right, qint32 top, qint32 bottom)
{
    m_padding_bottom = test_and_set_int32_(bottom);
    m_padding_left = test_and_set_int32_(left);
    m_padding_right = test_and_set_int32_(right);
    m_padding_top = test_and_set_int32_(top);
}

qint32 custom_widget_style::padding() const
{
    return m_padding;
}

void custom_widget_style::set_margin(qint32 left, qint32 right, qint32 top, qint32 bottom)
{
    m_margin_bottom = bottom;
    m_margin_left = left;
    m_margin_right = right;
    m_margin_top = top;
}

void custom_widget_style::set_margin(qint32 m)
{
    m_margin = m;
}

qint32 custom_widget_style::margin() const
{
    return m_margin;
}

QString custom_widget_style::generate_padding_and_margin_style_code_() const
{
    QString tmp = "";
    if (0 != m_padding) {
        tmp += style_sheet_component::padding(QString::number(m_padding));
    } else {
        tmp += ( style_sheet_component::padding_left(QString::number(m_padding_left))
                + style_sheet_component::padding_right(QString::number(m_padding_right))
                 + style_sheet_component::padding_top(QString::number(m_padding_top))
                 + style_sheet_component::padding_bottom(QString::number(m_padding_bottom))
                 );
    }
    if (0 != m_margin) {
        tmp += style_sheet_component::margin(QString::number(m_margin));
    } else  {
        tmp += (style_sheet_component::margin_left(QString::number(m_margin_left))
                + style_sheet_component::margin_right(QString::number(m_margin_right))
                + style_sheet_component::margin_bottom(QString::number(m_margin_bottom))
                + style_sheet_component::margin_top(QString::number(m_margin_top))
                );
    }
    return tmp;
}

void custom_widget_style::set_font_style(const custom_font_style &f)
{
    m_font_style = f;
}

custom_font_style custom_widget_style::font_style() const
{
    return m_font_style;
}

QString custom_widget_style::generate_font_style_code_() const
{
    return m_font_style.get_style_code();
}

void custom_widget_style::set_hover_state_style(custom_widget_style *hover)
{
    m_hover_state_style = hover;
}

custom_widget_style* custom_widget_style::hover_state_style()
{
    return m_hover_state_style;
}

void custom_widget_style::set_pressed_state_style(custom_widget_style *pressed)
{
    m_pressed_state_style = pressed;
}

custom_widget_style* custom_widget_style::pressed_state_style()
{
    return m_pressed_state_style;
}

void custom_widget_style::set_widget_state(widget_state_values::widget_state s)
{
    m_widget_state = s;
}

widget_state_values::widget_state custom_widget_style::widget_state() const
{
    return m_widget_state;
}

QString custom_widget_style::generate_hover_state_style_code_()
{
    if (NULL != m_hover_state_style) {
        return m_hover_state_style->to_string();
    }
    return "";
}

QString custom_widget_style::generate_pressed_state_style_code_()
{
    if (NULL != m_pressed_state_style) {
        return m_pressed_state_style->to_string();
    }
    return "";
}

void custom_widget_style::set_widget_name(const QString &wn)
{
    m_widget_name = wn;
    if (m_widget_name.isEmpty()) {
        m_widget_name = "QWidget";
    }
}

QString custom_widget_style::widget_name() const
{
    return m_widget_name;
}


} // namespace gui

} // namespace kokain
