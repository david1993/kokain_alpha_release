#ifndef CUSTOM_LINE_EDIT_H
#define CUSTOM_LINE_EDIT_H

#include <QLineEdit>

#include "custom_widget_style.h"

namespace kokain
{

namespace gui
{

class custom_line_edit : public QLineEdit
{
    Q_OBJECT
public:
    explicit custom_line_edit(QWidget *parent = 0);

private:
    void initialize_();
    void make_connections_();

private:
    void set_style_();
    void refresh_line_edit_();

private:
    custom_widget_style m_custom_style;

protected:
    void focusInEvent(QFocusEvent* ev);

signals:
    void focused();

public slots:
    void on_focus();
    void on_text_changed(const QString& s);
    void on_return_pressed();

private:
    bool m_focused;

private:
    const static qint32 k_font_size;
    const static qint32 k_padding_default_value;

private:
    custom_line_edit(const custom_line_edit&);
    custom_line_edit& operator=(const custom_line_edit&);

}; // class custom_line_edit

} // namespace gui

} // namespace kokain

#endif // CUSTOM_LINE_EDIT_H
