#include "calendars_general_widget.h"

#include <QLabel>
#include <QVBoxLayout>

namespace kokain
{

namespace gui
{

calendars_general_widget::calendars_general_widget(QWidget *parent) :
    QWidget(parent),
    m_calendar(NULL),
    m_main_layout(NULL)
{
    initialize_();
}

void calendars_general_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void calendars_general_widget::create_widgets_()
{
    m_calendar = new calendar_namespace::calendar();
    Q_ASSERT(0 != m_calendar);
}

void calendars_general_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);

    Q_ASSERT(0 != m_calendar);
    m_main_layout->addWidget(m_calendar);
}

void calendars_general_widget::make_connections_()
{

}

} // namespace gui

} // namespace kokain
