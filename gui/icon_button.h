#ifndef ICON_BUTTON_H
#define ICON_BUTTON_H

#include <QLabel>

namespace kokain
{

namespace gui
{

class icon_button : public QLabel
{
    Q_OBJECT
public:
    explicit icon_button(QWidget *parent = 0, qint32  fw = -1, qint32 fh = -1);

private:
    void initialize_();
    void make_connections_();

public:
    void set_normal_state_pixmap(const QPixmap& px);
    void set_pressed_state_pixmap(const QPixmap& px);

private:
    QPixmap m_normal_state;
    QPixmap m_pressed_state;

protected:
    virtual void mousePressEvent(QMouseEvent* ev);
    virtual void mouseReleaseEvent(QMouseEvent *ev);

signals:
    void clicked();
    void pressed();

public slots:
    void on_click();
    void on_press();

private:
    bool m_pressed;
    qint32 m_fixed_width;
    qint32 m_fixed_height;

private:
    const static qint32 k_fixed_size;

private:
    icon_button(const icon_button&);
    icon_button& operator=(const icon_button&);

}; // class icon_button

} // namespace gui

} // namespace kokain

#endif // ICON_BUTTON_H
