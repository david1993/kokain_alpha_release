#ifndef PROJECT_WIDGET_H
#define PROJECT_WIDGET_H

#include <QWidget>

class QLabel;
class QHBoxLayout;
class QVBoxLayout;

namespace kokain
{

namespace core {
    class project;
}

namespace gui
{

class custom_button;
class custom_menu;

class project_widget : public QWidget
{
    Q_OBJECT
public:
    explicit project_widget(QWidget *parent = 0);
    explicit project_widget(core::project* cr, QWidget* p = 0);

    // construction std helpers
private:
    void initilize_();
    void create_widgets_();
    void setup_layout_(); // see section UI
    void make_connections_();

    // updates
private:
    void update_widget_data_();
    void make_sure_widgets_are_alive_();
    void _debug_check_component_buttons_();

    /// @brief project core
public:
    void set_project_core(core::project* p);
    core::project* project_core();

private:
    core::project* m_project_core;

    /// @brief UI
private:
    void setup_styles_();

private:
    void setup_title_layout_();
    void setup_component_buttons_layout_();

private:
    QVBoxLayout* m_title_layout;
    QHBoxLayout* m_component_buttons_layout;
    QVBoxLayout* m_main_layout;

    // labels
private:
    void setup_label_styles_();
    void create_labels_();

private:
    QLabel* m_project_name_lbl;
    QLabel* m_project_description_lbl;

    // Project manage buttons and info buttons
private:
    custom_button* m_edit_project_btn;
    custom_button* m_create_project_btn;
    custom_button* m_current_info_btn;

private slots:
    void on_edit_project_button_clicked();
    void on_create_project_button_clicked();
    void on_current_info_button_clicked();

private:
    void create_manage_buttons_();

    // Components
private:
    void create_component_widgets_();

    // Component buttons
private:
    custom_button* m_tasks_btn;
    custom_button* m_plans_btn;
    custom_button* m_files_btn;
    custom_button* m_calendars_btn;
    custom_button* m_comments_btn;
    custom_button* m_people_btn;
    custom_button* m_more_btn; // below buttons are in more
    custom_menu* m_components_more_menu;
    QAction* m_sharing_btn;
    QAction* m_resources_btn;
    QAction* m_references_btn;
    QAction* m_details_btn;

private:
    void create_component_buttons_();
    void configure_component_buttons_();
    void make_component_buttons_connections_();
    void component_buttons_set_to_defaults_();

private slots:
    void on_tasks_button_clicked();
    void on_plans_button_clicked();
    void on_files_button_clicked();
    void on_calendars_button_clicked();
    void on_comments_button_clicked();
    void on_people_button_clicked();
    void on_more_button_clicked();
    void on_sharing_button_clicked();
    void on_resources_button_clicked();
    void on_references_button_clicked();
    void on_details_button_clicked();

    /// @brief constants
private:
    const static qint32 k_project_description_font_size;
    const static qint32 k_project_name_font_size;
    const static qint32 k_fixed_layout_spacing_value;
    const static qint32 k_manage_button_fixed_width;
    const static qint32 k_manage_button_fixed_height;
    const static qint32 k_component_button_fixed_width;
    const static qint32 k_component_button_fixed_height;

}; // class project_widget

} // namespace gui

} // namespace kokain

#endif // PROJECT_WIDGET_H
