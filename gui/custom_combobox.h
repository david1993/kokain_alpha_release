#ifndef CUSTOM_COMBOBOX_H
#define CUSTOM_COMBOBOX_H

#include <QComboBox>

namespace kokain
{

namespace gui
{

class custom_combobox : public QComboBox
{
    Q_OBJECT
public:
    explicit custom_combobox(QWidget *parent = 0);

signals:

public slots:

private:
    custom_combobox(const custom_combobox&);
    custom_combobox& operator=(const custom_combobox&);

}; // class custom_bombobox

} // namespace gui

} // namespace kokain

#endif // CUSTOM_COMBOBOX_H
