#include "custom_label.h"

namespace kokain
{

namespace gui
{

const qint32 custom_label::k_default_fixed_width = 50;
const qint32 custom_label::k_default_fixed_height = 20;
const qint32 custom_label::k_large_fixed_width = 100;
const qint32 custom_label::k_large_fixed_height = 50;
const qint32 custom_label::k_tile_fixed_width = 200;
const qint32 custom_label::k_tile_fixed_height = 150;
const qint32 custom_label::k_indicator_fixed_width = 5;
const qint32 custom_label::k_indicator_fixed_height = 10;
const qint32 custom_label::k_square_fixed_width = 50;
const qint32 custom_label::k_square_fixed_height = 50;


custom_label::custom_label(QWidget *parent) :
    QLabel(parent),
    m_color(custom_color_values::WET_ASPHALT),
    m_background_color(custom_color_values::CLOUDS),
    m_type(DEFAULT),
    m_custom_style(NULL)
{
    initialize_();
}

custom_label::custom_label(const QString &t, QWidget *p)
    : QLabel(p),
      m_color(custom_color_values::WET_ASPHALT),
      m_background_color(custom_color_values::CLOUDS),
      m_type(DEFAULT),
      m_custom_style(NULL)
{
    setText(t);
    initialize_();
}

custom_label::custom_label(label_types t, custom_color_values::custom_color c,
                           custom_color_values::custom_color bc, QWidget* p)
    : QLabel(p),
      m_color(c),
      m_background_color(bc),
      m_type(t),
      m_custom_style(NULL)
{
    initialize_();
}

custom_label::~custom_label()
{
    delete m_custom_style;
    m_custom_style = NULL;
}

void custom_label::initialize_()
{
    m_custom_style = new custom_widget_style("QLabel");
    Q_ASSERT(0 != m_custom_style);
    update_style_();
    make_connections_();
}

void custom_label::make_connections_()
{
    connect(this, SIGNAL(clicked()), this, SLOT(on_label_clicked()));
}

void custom_label::set_color(custom_color_values::custom_color c)
{
    m_color = c;
    update_style_();
}

custom_color_values::custom_color custom_label::color() const
{
    return m_color;
}

void custom_label::set_background_color(custom_color_values::custom_color c)
{
    m_background_color = c;
    update_style_();
}

custom_color_values::custom_color custom_label::background_color() const
{
    return m_background_color;
}

void custom_label::set_font_style(custom_font_style fs)
{
    m_font_style = fs;
    update_style_();
}

void custom_label::set_font_size(qint32 s)
{
    m_font_style.set_size(s);
    update_style_();
}

void custom_label::set_rounded(qint32 r)
{
    Q_ASSERT(0 != m_custom_style);
    custom_border_style bs;
    bs.set_radius(r);
    m_custom_style->set_border_style(bs);
    update_style_();
}

custom_font_style custom_label::font_style() const
{
    return m_font_style;
}

void custom_label::set_type(label_types t)
{
    m_type = t;
    update_style_();
}

custom_label::label_types custom_label::type() const
{
    return m_type;
}

void custom_label::set_custom_style(custom_widget_style *st)
{
    Q_ASSERT(0 != st);
    m_custom_style = st;
}

custom_widget_style* custom_label::custom_style()
{
    return m_custom_style;
}

void custom_label::update_style_()
{
    Q_ASSERT(0 != m_custom_style);
    m_custom_style->set_color(m_color);
    m_custom_style->set_background_color(m_background_color);
    m_custom_style->set_font_style(m_font_style);
    setStyleSheet(m_custom_style->to_string());

    update_type_();
}

void custom_label::update_type_()
{
    switch (m_type)
    {
    case DEFAULT:
        setFixedSize(k_default_fixed_width, k_default_fixed_height);
        break;
    case LARGE:
        setFixedSize(k_large_fixed_width, k_large_fixed_height);
        break;
    case TILE:
        setFixedSize(k_tile_fixed_width, k_tile_fixed_height);
        break;
    case INDICATOR:
        setFixedSize(k_indicator_fixed_width, k_indicator_fixed_height);
        break;
    case SQUARE:
        setFixedSize(k_square_fixed_width, k_square_fixed_height);
        break;
    default:
        setFixedSize(k_default_fixed_width, k_default_fixed_height);
        break;
    }
}

void custom_label::on_label_clicked()
{

}

void custom_label::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
    emit pressed();
}

void custom_label::mouseReleaseEvent(QMouseEvent *ev)
{
    (void)ev;
    emit clicked();
}

} // namespace gui

} // namespace kokain
