#include "timer_widget.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QTime>

namespace kokain
{

namespace gui
{

const QString timer_widget::k_default_title = tr("working hours");
const qint32 timer_widget::k_timeout_value = 1000;
const qint32 timer_widget::k_seconds_in_minute = 60;

timer_widget::timer_widget(QWidget *parent) :
    QWidget(parent),
    m_main_layout(NULL),
    m_title_string(k_default_title),
    m_time_format(HOURS),
    m_seconds(0),
    m_minutes(0),
    m_hours(0),
    m_progress_on(false),
    m_title_lbl(NULL),
    m_timer_lbl(NULL)
{
    initialize_();
}

void timer_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();

    m_timer.start(k_timeout_value);
}

void timer_widget::create_widgets_()
{
    m_title_lbl = new QLabel(m_title_string, this);
    Q_ASSERT(0 != m_title_lbl);
    m_timer_lbl = new QLabel(this);
    Q_ASSERT(0 != m_timer_lbl);
}

void timer_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->addWidget(m_title_lbl);
    m_main_layout->addWidget(m_timer_lbl);
}

void timer_widget::make_connections_()
{
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(update_timer_label()));
}

void timer_widget::set_title(const QString &t)
{
    m_title_lbl->setText(t);
}

QString timer_widget::title() const
{
    return m_title_lbl->text();
}

void timer_widget::set_time_format(time_format t)
{
    m_time_format = t;
}

timer_widget::time_format timer_widget::get_time_format() const
{
    return m_time_format;
}

void timer_widget::set_seconds(qint32 m)
{
    m_seconds = m;
}

qint32 timer_widget::seconds() const
{
    return m_seconds;
}

void timer_widget::set_minutes(qint32 m)
{
    m_minutes = m;
}

qint32 timer_widget::minutes() const
{
    return m_minutes;
}

void timer_widget::set_hours(qint32 h)
{
    m_hours = h;
}

qint32 timer_widget::hours() const
{
    return m_hours;
}

void timer_widget::set_progress(bool p)
{
    m_progress_on = p;
}

void timer_widget::stop_timer()
{
    m_timer.stop();
    m_seconds = 0;
    emit timer_stopped();
}

void timer_widget::start_timer()
{
    m_seconds = 0;
    m_timer.start(k_timeout_value);
    emit timer_started();
}

void timer_widget::resume_timer()
{
    m_timer.start(k_timeout_value);
    emit timer_resumed();
}

void timer_widget::pause_timer()
{
    m_timer.stop();
    emit timer_paused();
}

void timer_widget::update_timer_label()
{
    ++m_seconds;
    m_hours = m_seconds / (k_seconds_in_minute * k_seconds_in_minute);
    m_minutes = (m_seconds / k_seconds_in_minute) % k_seconds_in_minute;
    m_timer_lbl->setText(QString::number(m_hours) + "h " + QString::number(m_minutes) + "m");
}

} // namespace gui

} // namespace kokain
