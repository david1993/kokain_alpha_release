#ifndef PROJECTS_GENERAL_WIDGET_H
#define PROJECTS_GENERAL_WIDGET_H

#include <vector>

#include <QWidget>

#include "project_widget.h"

class QLabel;
class QVBoxLayout;

namespace kokain
{

namespace gui
{

class flow_layout;
class project_item_widget;
class custom_button;
class custom_combobox;
class custom_label;

class projects_general_widget : public QWidget
{
    Q_OBJECT
public:
    explicit projects_general_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    custom_label* m_title_lbl;
    custom_button* m_new_project_btn;
    std::vector<project_item_widget*> m_project_items;

private:
    QVBoxLayout* m_main_layout;
    flow_layout* m_items_layout;

public slots:
    void on_project_item_clicked();
    void on_new_project_button_clicked();

private:
    void clear_whole_projects_layout_();
    void clear_projects_central_area_();
    void hide_project_item_widgets_();
    void hide_top_elements_();
    void load_edit_project_widget_();
    void hide_edit_project_widget_();
    void load_view_project_widget_();
    void hide_view_project_widget_();

private:
    projects_general_widget(const projects_general_widget&);
    projects_general_widget& operator=(const projects_general_widget&);

}; // class projects_general_widget

} // namespace gui

} // namespace kokain

#endif // PROJECTS_GENERAL_WIDGET_H
