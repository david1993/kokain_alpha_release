#ifndef CUSTOM_LABEL_H
#define CUSTOM_LABEL_H

#include <QLabel>

#include "custom_widget_style.h"

namespace kokain
{

namespace gui
{

class custom_label : public QLabel
{
    Q_OBJECT
public:
    enum label_types
    {
        DEFAULT,
        LARGE,
        TILE,
        INDICATOR,
        SQUARE,
        label_types_max
    };

public:
    explicit custom_label(QWidget *p = 0);
    explicit custom_label(const QString& t, QWidget* p = 0);
    custom_label(label_types t, custom_color_values::custom_color c,
                 custom_color_values::custom_color bc, QWidget *p = 0);
    virtual ~custom_label();

private:
    void initialize_();
    void make_connections_();

public:
    void set_color(custom_color_values::custom_color c);
    custom_color_values::custom_color color() const;
    void set_background_color(custom_color_values::custom_color c);
    custom_color_values::custom_color background_color() const;
    void set_font_style(custom_font_style fs);
    custom_font_style font_style() const;
    void set_font_size(qint32 s);
    void set_rounded(qint32 r);

private:
    custom_color_values::custom_color m_color;
    custom_color_values::custom_color m_background_color;
    custom_font_style m_font_style;

public:
    void set_type(label_types t);
    label_types type() const;

private:
    label_types m_type;

public:
    void set_custom_style(custom_widget_style* st);
    custom_widget_style* custom_style();

private:
    custom_widget_style* m_custom_style;

private:
    void update_style_();
    void update_type_();

signals:
    void pressed();
    void clicked();

public slots:
    void on_label_clicked();

protected:
    virtual void mousePressEvent(QMouseEvent* ev);
    virtual void mouseReleaseEvent(QMouseEvent* ev);

private:
    const static qint32 k_default_fixed_width;
    const static qint32 k_default_fixed_height;
    const static qint32 k_large_fixed_width;
    const static qint32 k_large_fixed_height;
    const static qint32 k_tile_fixed_width;
    const static qint32 k_tile_fixed_height;
    const static qint32 k_indicator_fixed_width;
    const static qint32 k_indicator_fixed_height;
    const static qint32 k_square_fixed_width;
    const static qint32 k_square_fixed_height;

private:
    custom_label(const custom_label&);
    custom_label& operator=(const custom_label&);

}; // class custom_label

} // namespace gui

} // namespace kokain

#endif // CUSTOM_LABEL_H
