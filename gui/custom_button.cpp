#include "custom_button.h"

#include <QDebug>

namespace kokain
{

namespace gui
{

// constants
const qint32 custom_button::k_border_width_minimum = 1;
const qint32 custom_button::k_default_padding_lr = 10;
const qint32 custom_button::k_default_padding_tb = 5;
const qint32 custom_button::k_large_padding_lr = 20;
const qint32 custom_button::k_large_padding_tb = 10;
const qint32 custom_button::k_tile_padding_lr = 100;
const qint32 custom_button::k_tile_padding_tb = 80;
const qint32 custom_button::k_rounded_min_value = 5;


custom_button::custom_button(QWidget* p)
    : QPushButton(p),
      m_type(custom_button_type_values::DEFAULT),
      m_category(custom_button_category_values::DEFAULT),
      m_prev_category(custom_button_category_values::DEFAULT)
{
    initialize_();
}

custom_button::custom_button(const QString &n, QWidget *p)
    : QPushButton(n, p),
      m_type(custom_button_type_values::DEFAULT),
      m_category(custom_button_category_values::DEFAULT),
      m_prev_category(custom_button_category_values::DEFAULT)
{
    initialize_();
}

custom_button::custom_button(custom_button_type_values::custom_button_type t, QWidget *p)
    : QPushButton(p),
      m_type(t),
      m_category(custom_button_category_values::DEFAULT),
      m_prev_category(custom_button_category_values::DEFAULT)
{
    initialize_();
}

custom_button::custom_button(custom_button_category_values::custom_button_category c,
        QWidget* p)
    : QPushButton(p),
      m_type(custom_button_type_values::DEFAULT),
      m_category(c),
      m_prev_category(c)
{
    initialize_();
}

custom_button::custom_button(const QString &n,
        custom_button_type_values::custom_button_type t,
        custom_button_category_values::custom_button_category c, QWidget *p)
    : QPushButton(n, p),
      m_type(t),
      m_category(c),
      m_prev_category(c)
{
    initialize_();
}

custom_button::~custom_button()
{
    delete m_custom_style;
    m_custom_style = NULL;
}

void custom_button::initialize_()
{
    this->setFocusPolicy(Qt::NoFocus);
    m_is_disabled = false;
    m_is_rounded = false;
    m_has_indicator = false;
    m_indicator_color = custom_color_values::CARROT;
    m_indicator_pos = border_position_values::LEFT;
    m_custom_style = new custom_widget_style("QPushButton");
    set_hover_color(custom_color_values::ASBESTOS);
    apply_button_type_style_();
    apply_button_category_style_();
    make_connections_();
}

void custom_button::make_connections_()
{
    connect(this, SIGNAL(clicked()), this, SLOT(on_clicked()));
    connect(this, SIGNAL(pressed()), this, SLOT(on_pressed()));
}

void custom_button::apply_button_type_style_()
{
    Q_ASSERT(0 != m_custom_style);
    m_custom_style->set_widget_name("QPushButton");
    m_custom_style->set_color("white");
    m_custom_style->set_background_color(custom_color_values::TURQUOISE);
    custom_border_style border(border_style_values::SOLID, k_border_width_minimum,
            custom_color_values::TURQUOISE);
    m_custom_style->set_border_style(border);

    custom_widget_style* pressed = m_custom_style->pressed_state_style();
    if (NULL == pressed) {
        pressed = new custom_widget_style(widget_state_values::PRESSED);
    }
    pressed->set_background_color(custom_color_values::GREEN_SEA);
    border.set_color(custom_color_values::GREEN_SEA);
    pressed->set_border_style(border);
    m_custom_style->set_pressed_state_style(pressed);

    switch (m_type)
    {
    case custom_button_type_values::DEFAULT:
        complete_default_type_style_();
        break;
    case custom_button_type_values::LARGE:
        complete_large_type_style_();
        break;
    case custom_button_type_values::TILE:
        complete_tile_type_style_();
        break;
    case custom_button_type_values::DROPDOWN:
        complete_dropdown_type_style_();
        break;
    default:
        complete_default_type_style_();
        break;
    }

    set_button_fixed_size_();
    update_style_(m_custom_style);
}

void custom_button::complete_default_type_style_()
{
    //m_custom_style->set_padding(k_default_padding_lr, k_default_padding_lr,
    //        k_default_padding_tb, k_default_padding_tb);
}

void custom_button::complete_large_type_style_()
{
    //m_custom_style->set_padding(k_large_padding_lr, k_large_padding_lr,
    //        k_large_padding_tb, k_large_padding_tb);
}

void custom_button::complete_tile_type_style_()
{
   // m_custom_style->set_padding(k_tile_padding_lr, k_tile_padding_lr,
    //        k_tile_padding_tb, k_tile_padding_tb);
}

void custom_button::complete_dropdown_type_style_()
{
    // do nothing
}

void custom_button::update_style_(custom_widget_style* st)
{
    Q_ASSERT(0 != st);
    QString sheet = st->to_string();
    custom_widget_style* pressedhover = st->pressed_state_style();
    if (0 != pressedhover) {
        sheet += pressedhover->to_string();
    }
    pressedhover = st->hover_state_style();
    if (0 != pressedhover) {
        sheet += pressedhover->to_string();
    }
    this->setStyleSheet(sheet + st->to_string("flat"));
}

void custom_button::set_hover_color(custom_color_values::custom_color c)
{
    custom_widget_style* hov = m_custom_style->hover_state_style();
    if (0 == hov) {
        hov = new custom_widget_style(widget_state_values::HOVER);
    }
    hov->set_background_color(c);
    m_custom_style->set_hover_state_style(hov);
    update_style_(m_custom_style);
}

void custom_button::set_button_fixed_size_()
{
    switch (m_type)
    {
    case custom_button_type_values::DEFAULT:
        // your code here
        break;
    case custom_button_type_values::LARGE:
        //your code here
        break;
    case custom_button_type_values::TILE:
        // your code here
        break;
    case custom_button_type_values::DROPDOWN:
        // your code here
        break;
    default:
        break;
    }
}

void custom_button::apply_button_category_style_()
{
    Q_ASSERT(0 != m_custom_style);
    std::pair<custom_color_values::custom_color, custom_color_values::custom_color>
        colors = get_colors_by_category_();

    m_custom_style->set_background_color(colors.first);
    custom_widget_style* pressed = m_custom_style->pressed_state_style();
    if (0 != pressed) {
        pressed->set_background_color(colors.second);
    }
    custom_widget_style* hover = m_custom_style->hover_state_style();
    if (colors.first != custom_color_values::CONCRETE) {
        hover->set_background_color(colors.second);
        m_custom_style->set_hover_state_style(hover);
    }

    m_custom_style->set_color("white");
    if (custom_color_values::CLOUDS == colors.first) {
        m_custom_style->set_color(custom_color_values::ASBESTOS);
    }
    update_style_(m_custom_style);
}

std::pair<custom_color_values::custom_color, custom_color_values::custom_color>
 custom_button::get_colors_by_category_() const
{
    std::pair<custom_color_values::custom_color, custom_color_values::custom_color> colors;
    switch (m_category)
    {
    case custom_button_category_values::DEFAULT:
        colors.first = custom_color_values::CONCRETE;
        colors.second = custom_color_values::ASBESTOS;
        break;
    case custom_button_category_values::DANGER:
        colors.first = custom_color_values::ALIZARIN;
        colors.second = custom_color_values::POMEGRANATE;
        break;
    case custom_button_category_values::INFO:
        colors.first = custom_color_values::PETER_RIVER;
        colors.second = custom_color_values::BELIZE_HOLE;
        break;
    case custom_button_category_values::INVERSE:
        colors.first = custom_color_values::WET_ASPHALT;
        colors.second = custom_color_values::MIDNIGHT_BLUE;
        break;
    case custom_button_category_values::WARNING_LIGHT:
        colors.first = custom_color_values::SUN_FLOWER;
        colors.second = custom_color_values::ORANGE;
        break;
    case custom_button_category_values::WARNING:
        colors.first = custom_color_values::CARROT;
        colors.second = custom_color_values::PUMPKIN;
        break;
    case custom_button_category_values::PRIMARY:
        colors.first = custom_color_values::TURQUOISE;
        colors.second = custom_color_values::GREEN_SEA;
        break;
    case custom_button_category_values::SUCCESS:
        colors.first = custom_color_values::EMERALD;
        colors.second = custom_color_values::NEPHRITIS;
        break;
    case custom_button_category_values::HIGHLIGHT:
        colors.first = custom_color_values::AMETHYST;
        colors.second = custom_color_values::WISTERIA;
        break;
    case custom_button_category_values::DISABLED:
        colors.first = custom_color_values::CLOUDS;
        colors.second = custom_color_values::CLOUDS;
        break;
    default:
        colors.first = custom_color_values::CONCRETE;
        colors.second = custom_color_values::ASBESTOS;
        break;
    } // switch (m_category)

    return colors;
}


void custom_button::set_custom_style(custom_widget_style *st)
{
    delete m_custom_style;
    m_custom_style = NULL;
    m_custom_style = st;
    this->setStyleSheet(m_custom_style->to_string()
                        + m_custom_style->generate_pressed_state_style_code_()
                        + m_custom_style->generate_hover_state_style_code_());
}

custom_widget_style* custom_button::custom_style()
{
    return m_custom_style;
}

custom_button_type_values::custom_button_type custom_button::type() const
{
    return m_type;
}

void custom_button::set_type(custom_button_type_values::custom_button_type t)
{
    m_type = t;
    apply_button_type_style_();
}

void custom_button::set_disabled(bool d)
{
    m_is_disabled = d;
    this->setEnabled(!m_is_disabled);
    if (!m_is_disabled) { return; }

    // move this to set_category()
    if (custom_button_category_values::DISABLED != m_category) {
        m_prev_category = m_category;
    }
    set_category(custom_button_category_values::DISABLED);
}

void custom_button::set_enabled(bool e)
{
    m_is_disabled = !e;
    this->setEnabled(e);

    if (m_is_disabled) {
        set_disabled(true);
        return;
    }
    set_category(m_prev_category);
}

bool custom_button::enabled() const
{
    return !m_is_disabled;
}

bool custom_button::disabled() const
{
    return m_is_disabled;
}

void custom_button::set_rounded(bool r)
{
    if (r) {
        set_rounded(k_rounded_min_value);
    } else {
        set_rounded(0);
    }
}

void custom_button::set_rounded(qint32 r)
{
    Q_ASSERT(0 != m_custom_style);
    m_is_rounded = true;
    custom_border_style bst = m_custom_style->border_style();
    bst.set_radius(r);
    m_custom_style->set_border_style(bst);
    update_style_(m_custom_style);
}

bool custom_button::rounded() const
{
    return m_is_rounded;
}

bool custom_button::has_indicator() const
{
    return m_has_indicator;
}

void custom_button::set_indicator()
{
    m_has_indicator = true;
    Q_ASSERT(0 != m_custom_style);
    custom_border_style bs = m_custom_style->border_style();
    bs.set_color(m_indicator_color, m_indicator_pos);
    bs.set_width(5, m_indicator_pos);
    m_custom_style->set_border_style(bs);
    update_style_(m_custom_style);
}

void custom_button::remove_indicator()
{
    m_has_indicator = false;
    Q_ASSERT(0 != m_custom_style);
    custom_border_style bs = m_custom_style->border_style();
    bs.set_color(m_custom_style->background_color(), m_indicator_pos);
    m_custom_style->set_border_style(bs);
    update_style_(m_custom_style);
}

void custom_button::set_indicator_color(custom_color_values::custom_color c)
{
    m_indicator_color = c;
    set_indicator();
}

void custom_button::set_indicator_position(border_position_values::border_position p)
{
    if (border_position_values::NONE == p || p >= border_position_values::TOP_LEFT) {
        p = border_position_values::LEFT;
    }
    m_indicator_pos = p;
}

void custom_button::set_category(custom_button_category_values::custom_button_category c)
{
    m_category = c;
    apply_button_category_style_();
}

custom_button_category_values::custom_button_category custom_button::category() const
{
    return m_category;
}



// slots

void custom_button::on_clicked()
{
    qDebug() << "CLICKED";
}

void custom_button::on_pressed()
{
    qDebug() << "PRESSED";
}

void custom_button::on_released()
{

}


} // namespace gui

} // namespace kokain
