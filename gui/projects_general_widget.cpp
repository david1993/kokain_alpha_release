#include "projects_general_widget.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDebug>

#include "flow_layout.h"
#include "project_item_widget.h"
#include "custom_button.h"
#include "custom_combobox.h"
#include "custom_label.h"
#include "../core/project.h"

namespace kokain
{

namespace gui
{

projects_general_widget::projects_general_widget(QWidget *parent) :
    QWidget(parent),
    m_title_lbl(NULL),
    m_new_project_btn(NULL),
    m_main_layout(NULL),
    m_items_layout(NULL)
{
    initialize_();
}

void projects_general_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void projects_general_widget::create_widgets_()
{
    m_title_lbl = new custom_label(tr("Projects"), this);
    m_new_project_btn = new custom_button(tr("+ NEW"), custom_button_type_values::DEFAULT,
                                          custom_button_category_values::SUCCESS, this);
    Q_ASSERT(0 != m_title_lbl);
    Q_ASSERT(0 != m_new_project_btn);
    m_title_lbl->set_font_size(22);
    m_title_lbl->set_background_color(custom_color_values::WHITE);
    m_title_lbl->set_type(custom_label::LARGE);
    m_new_project_btn->set_rounded(5);
    m_new_project_btn->setFixedSize(m_title_lbl->width(), m_title_lbl->height()/2);
    /// @todo this is a WTF moment
    m_project_items.push_back(new project_item_widget(this));
    m_project_items.push_back(new project_item_widget(this));
    m_project_items.push_back(new project_item_widget(this));
    m_project_items.push_back(new project_item_widget(this));
    m_project_items.push_back(new project_item_widget(this));
    m_project_items.push_back(new project_item_widget(this));
    m_project_items.push_back(new project_item_widget(this));
}

void projects_general_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    QHBoxLayout* top_elems = new QHBoxLayout();
    Q_ASSERT(0 != top_elems);
    top_elems->setContentsMargins(0, 0, 0, 0);
    top_elems->addWidget(m_title_lbl);
    top_elems->addWidget(m_new_project_btn, 1, Qt::AlignLeft);
    m_items_layout = new flow_layout();
    Q_ASSERT(0 != m_items_layout);

    for (unsigned int i = 0; i < m_project_items.size(); ++i) {
        m_items_layout->addWidget(m_project_items[i]);
    }
    m_main_layout->addLayout(top_elems);
    m_main_layout->addLayout(m_items_layout);
}

void projects_general_widget::make_connections_()
{
    connect(m_new_project_btn, SIGNAL(clicked()), this, SLOT(on_new_project_button_clicked()));
    for (unsigned int i = 0; i < m_project_items.size(); ++i) {
        connect(m_project_items[i], SIGNAL(project_item_clicked()), this, SLOT(on_project_item_clicked()));
    }
}

void projects_general_widget::on_project_item_clicked()
{
    clear_whole_projects_layout_();
    load_view_project_widget_();
    /// @todo This is a temporary demonstration
    core::project* test_project = new core::project("", "project chlp");
    project_widget* wgt = new project_widget(test_project, this);
    m_main_layout->addWidget(wgt, 1, Qt::AlignTop);
}

void projects_general_widget::clear_whole_projects_layout_()
{
    Q_ASSERT(0 != m_main_layout);
    while (!m_main_layout->isEmpty()) {
        m_main_layout->removeItem(m_main_layout->itemAt(0));
    }
    hide_top_elements_();
    hide_project_item_widgets_();
}

void projects_general_widget::hide_top_elements_()
{
    Q_ASSERT(0 != m_title_lbl);
    Q_ASSERT(0 != m_new_project_btn);
    m_title_lbl->hide();
    m_new_project_btn->hide();
}

void projects_general_widget::clear_projects_central_area_()
{
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->removeItem(m_main_layout->itemAt(1));
    hide_project_item_widgets_();

}

void projects_general_widget::hide_project_item_widgets_()
{
    for (unsigned int i = 0; i < m_project_items.size(); ++i) {
        m_project_items[i]->hide();
    }
}

void projects_general_widget::on_new_project_button_clicked()
{
    clear_projects_central_area_();
    load_edit_project_widget_();
}

void projects_general_widget::load_edit_project_widget_()
{

}

void projects_general_widget::load_view_project_widget_()
{

}

void projects_general_widget::hide_edit_project_widget_()
{

}

void projects_general_widget::hide_view_project_widget_()
{

}

} // namespace gui

} // namespace kokain
