#ifndef USER_ACTIVITY_WIDGET_H
#define USER_ACTIVITY_WIDGET_H

#include <QWidget>

class QLabel;
class QHBoxLayout;

namespace kokain
{
namespace core
{
class user;
}
}

namespace kokain
{
namespace gui
{
class circle_progress_bar;
class icon_button;

class user_activity_widget : public QWidget
{
    Q_OBJECT
public:
    explicit user_activity_widget(QWidget *parent = 0);
    explicit user_activity_widget(core::user* u, QWidget* p = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    icon_button* m_user_avatar;
    QLabel* m_online_status_lbl;
    QLabel* m_task_title_lbl;
    QLabel* m_task_start_lbl;
    QLabel* m_task_end_lbl;
    circle_progress_bar* m_task_progress_bar;
    QHBoxLayout* m_main_layout;

public:
    void set_user(core::user* u);
    core::user* user();

private:
    core::user* m_user;

signals:
    void clicked();

public slots:
    void update_activity();
    void update_online_status_();
    void update_task_status_();

protected:
    void mousePressEvent(QMouseEvent* ev);
    void mouseReleaseEvent(QMouseEvent* ev);

private:
    void _debug_widgets_are_alive_();

private:
    const static qint32 k_avatar_fixed_size;
    const static qint32 k_online_status_height;
    const static qint32 k_activity_widget_width;
    const static qint32 k_activity_widget_height;

private:
    user_activity_widget(const user_activity_widget&);
    user_activity_widget& operator=(const user_activity_widget&);

}; // class user_activity_widget

} // namespace gui

} // namespace kokain

#endif // USER_ACTIVITY_WIDGET_H
