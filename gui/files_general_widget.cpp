#include "files_general_widget.h"

#include <QLabel>
#include <QVBoxLayout>

namespace kokain
{

namespace gui
{

files_general_widget::files_general_widget(QWidget *parent) :
    QWidget(parent),
    m_temp_lbl(NULL),
    m_main_layout(NULL)
{
    initialize_();
}

void files_general_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void files_general_widget::create_widgets_()
{
    m_temp_lbl = new QLabel(this);
    Q_ASSERT(0 != m_temp_lbl);
    m_temp_lbl->setText("files widget");
}

void files_general_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);

    Q_ASSERT(0 != m_temp_lbl);
    m_main_layout->addWidget(m_temp_lbl);
}


void files_general_widget::make_connections_()
{

}

} // namespace gui

} // namespace kokain
