#ifndef ACTIVITY_BOARD_WIDGET_H
#define ACTIVITY_BOARD_WIDGET_H

#include <vector>

#include <QWidget>

class QVBoxLayout;
class QLabel;


namespace kokain
{

namespace gui
{

class user;
class my_activity_widget;
class user_activity_widget;

class activity_board_widget : public QWidget
{
    Q_OBJECT
public:
    explicit activity_board_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    my_activity_widget* m_my_activity_wgt;
    std::vector<user_activity_widget*> m_user_activities;

private:
    void create_user_activity_widgets_();

private:
    QVBoxLayout* m_main_layout;

signals:

public slots:

private:
    activity_board_widget(const activity_board_widget&);
    activity_board_widget& operator=(const activity_board_widget&);

}; // class activity_board_widget

} // namespace gui

} // namespace kokain

#endif // ACTIVITY_BOARD_WIDGET_H
