#ifndef USER_INDICATOR_WIDGET_H
#define USER_INDICATOR_WIDGET_H

#include <map>

#include <QWidget>
#include <QString>

class QLabel;
class QHBoxLayout;

namespace kokain
{

namespace gui
{

class icon_button;
class custom_button;

class user_indicator_widget : public QWidget
{
    Q_OBJECT
public:
    enum online_status {
        ONLINE = 1,
        OFFLINE,
        AWAY,
        BUSY,
        INVISIBLE,
        online_status_max
    };

public:
    explicit user_indicator_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

public:
    void set_online_status(online_status s);
    online_status get_online_status() const;

private:
    online_status m_status;
    QLabel* m_status_indicator_lbl;
    icon_button* m_user_avatar;
    QLabel* m_status_text_lbl;
    QLabel* m_username_lbl;
    custom_button* m_notifications_number_lbl;
    QHBoxLayout* m_main_layout;

private slots:
    void on_online_status_changed();

private:
    static void _fill_online_status_map_();

private:
    const static qint32 k_fixed_height;
    const static qint32 k_indicator_fixed_width;
    static std::map<online_status, QString> s_online_status_map;
    const static QString k_user_avatar_path;

private:
    user_indicator_widget(const user_indicator_widget&);
    user_indicator_widget& operator=(const user_indicator_widget&);

}; // class user_indicator_widget

} // namespace gui

} // namespace kokain

#endif // USER_INDICATOR_WIDGET_H
