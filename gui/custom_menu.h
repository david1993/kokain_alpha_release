#ifndef CUSTOM_MENU_H
#define CUSTOM_MENU_H

#include <QMenu>

#include "custom_widget_style.h"

namespace kokain
{

namespace gui
{

class custom_menu : public QMenu
{
    Q_OBJECT
public:
    explicit custom_menu(QWidget *parent = 0);

private:
    void initialize_();
    void setup_style_();
    void update_style_();

public:
    void set_custom_style(custom_widget_style* st);
    custom_widget_style* custom_style();

    void set_color(custom_color_values::custom_color c);
    custom_color_values::custom_color color() const;

private:
    custom_widget_style* m_custom_style;
    custom_color_values::custom_color m_color;

public:
    void set_item_size(qint32 w, qint32 h);
    void set_item_width(qint32 w);
    void set_item_height(qint32 h);
    qint32 item_width() const;
    qint32 item_height() const;

private:
    qint32 m_item_width;
    qint32 m_item_height;

signals:

public slots:

private:
    const static qint32 k_item_width_default;
    const static qint32 k_item_height_default;

private:
    custom_menu(const custom_menu&);
    custom_menu& operator=(const custom_menu&);

}; // class custom_menu

} // namespace gui

} // namespace kokain

#endif // CUSTOM_MENU_H
