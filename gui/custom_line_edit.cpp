#include "custom_line_edit.h"

#include <QDebug>

namespace kokain
{

namespace gui
{

const qint32 custom_line_edit::k_font_size = 16;
const qint32 custom_line_edit::k_padding_default_value = 5;

custom_line_edit::custom_line_edit(QWidget *parent) :
    QLineEdit(parent),
    m_focused(false)
{
    initialize_();
}

void custom_line_edit::initialize_()
{
    set_style_();
    make_connections_();
}

void custom_line_edit::make_connections_()
{
    connect(this, SIGNAL(focused()), this, SLOT(on_focus()));
    connect(this, SIGNAL(textChanged(QString)), this, SLOT(on_text_changed(const QString&)));
    connect(this, SIGNAL(returnPressed()), this, SLOT(on_return_pressed()));
}

void custom_line_edit::set_style_()
{
    custom_border_style bs;
    bs.set_width(1);
    bs.set_color(custom_color_values::NEPHRITIS, border_position_values::LEFT);
    bs.set_radius(1);
    m_custom_style.set_border_style(bs);
    m_custom_style.set_font_style(custom_font_style(font_style_values::ITALIC, k_font_size,
                                  text_decoration_values::NONE));
    m_custom_style.set_color(custom_color_values::ASBESTOS);
    m_custom_style.set_padding(k_padding_default_value);
    m_custom_style.set_widget_name("QLineEdit");
    this->setStyleSheet(m_custom_style.to_string());
}

void custom_line_edit::focusInEvent(QFocusEvent *ev)
{
    (void)ev;
    emit focused();
}

void custom_line_edit::on_focus()
{
    if (m_focused) {
        this->selectAll();
    } else {
        m_focused = true;
        this->clear();
    }
    m_custom_style.set_font_style(custom_font_style(font_style_values::NORMAL,
                                           k_font_size, text_decoration_values::NONE));
    m_custom_style.set_color(custom_color_values::MIDNIGHT_BLUE);
    refresh_line_edit_();
}

void custom_line_edit::refresh_line_edit_()
{
    setStyleSheet(m_custom_style.to_string());
}

void custom_line_edit::on_text_changed(const QString &s)
{
    (void)s;
}

void custom_line_edit::on_return_pressed()
{
    qDebug() << "enter, text == " << this->text();
}

} // namespace gui

} // namespace kokain
