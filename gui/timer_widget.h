#ifndef TIMER_WIDGET_H
#define TIMER_WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QString>

class QLabel;
class QVBoxLayout;

namespace kokain
{

namespace gui
{

class timer_widget : public QWidget
{
    Q_OBJECT
public:
    enum time_format
    {
        SECONDS = 1,
        MINUTES,
        HOURS,
        DAYS,
        WEEKS,
        MONTHS,
        YEARS,
        time_format_max
    };

public:
    explicit timer_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QVBoxLayout* m_main_layout;

public:
    void set_title(const QString& t);
    QString title() const;

private:
    QString m_title_string;

public:
    void set_time_format(time_format t);
    time_format get_time_format() const;
    void set_seconds(qint32 m);
    qint32 seconds() const;
    void set_minutes(qint32 m);
    qint32 minutes() const;
    void set_hours(qint32 h);
    qint32 hours() const;
    void set_progress(bool p);

private:
    time_format m_time_format;
    qint32 m_seconds;
    qint32 m_minutes;
    qint32 m_hours;
    bool m_progress_on;

private:
    QLabel* m_title_lbl;
    QLabel* m_timer_lbl;
    QTimer m_timer;

signals:
    void timer_stopped();
    void timer_resumed();
    void timer_started();
    void timer_paused();

public slots:
    void stop_timer();
    void start_timer();
    void resume_timer();
    void pause_timer();

private slots:
    void update_timer_label();

private:
    const static QString k_default_title;
    const static qint32 k_timeout_value;
    const static qint32 k_seconds_in_minute;

private:
    timer_widget(const timer_widget&);
    timer_widget& operator=(const timer_widget&);

}; // class timer_widget

} // namespace gui

} // namespace kokain

#endif // TIMER_WIDGET_H
