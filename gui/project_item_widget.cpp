#include "project_item_widget.h"

#include <QLabel>
#include <QVBoxLayout>

#include "custom_button.h"
#include "icon_button.h"
#include "circle_progress_bar.h"

namespace kokain
{

namespace gui
{

const qint32 project_item_widget::k_logo_size = 60;

project_item_widget::project_item_widget(QWidget *parent) :
    QWidget(parent),
    m_logo_lbl(NULL),
    m_project_name_lbl(NULL),
    m_progress_bar(NULL),
    m_main_layout(NULL)
{
    initialize_();
}

void project_item_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void project_item_widget::create_widgets_()
{
    m_logo_lbl = new custom_button("", custom_button_type_values::DEFAULT,
                                   custom_button_category_values::WARNING, this);
    m_project_name_lbl = new icon_button(this);
    m_progress_bar = new circle_progress_bar(this);
    Q_ASSERT(0 != m_logo_lbl);
    Q_ASSERT(0 != m_project_name_lbl);
    Q_ASSERT(0 != m_progress_bar);

    m_logo_lbl->setFixedSize(k_logo_size, k_logo_size);
    m_logo_lbl->set_rounded(k_logo_size / 2);
    m_project_name_lbl->setText("project name");
    m_project_name_lbl->setMinimumWidth(m_project_name_lbl->fontMetrics().width(m_project_name_lbl->text()));
    m_progress_bar->setFixedSize(k_logo_size*2, k_logo_size*2);
    m_progress_bar->setStyleSheet("QProgressBar{background-color:red;}");
    m_progress_bar->setValue(50);
    m_progress_bar->setTextVisible(false);
    m_progress_bar->hide();
}

void project_item_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->addWidget(m_logo_lbl);
    m_main_layout->addWidget(m_project_name_lbl);
}

void project_item_widget::make_connections_()
{
    connect(m_logo_lbl, SIGNAL(clicked()), this, SIGNAL(project_item_clicked()));
    connect(m_project_name_lbl, SIGNAL(clicked()), this, SIGNAL(project_item_clicked()));
}

void project_item_widget::mousePressEvent(QMouseEvent *ev)
{
    (void)ev;
}

void project_item_widget::mouseReleaseEvent(QMouseEvent *ev)
{
    (void)ev;
    emit project_item_clicked();
}

QSize project_item_widget::sizeHint() const
{
    return QSize(k_logo_size, k_logo_size);
}

} // namespace gui

} // namespace kokain
