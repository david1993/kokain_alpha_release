#ifndef NOTIFICATION_WIDGET_H
#define NOTIFICATION_WIDGET_H

#include <QWidget>
#include <QCheckBox>
#include <QPalette>

namespace kokain {

namespace core {
   class notification;
}
namespace gui {

class custom_label;
class flow_layout;

class notification_widget : public QWidget
{
    Q_OBJECT

public:
    enum notification_status{
        READ = 1,
        UNREAD
    };

public:
    notification_widget(QWidget* parent = 0);
    notification_widget(kokain::core::notification* the_notification, QWidget* parent = 0);
    ~notification_widget();

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

public:
    void set_check_state(Qt::CheckState state);
    bool is_checked() const;
private:
    QCheckBox* m_checkbox;

public:
    void set_sender_name(const QString& name);
    QString get_sender_name() const;
private:
    custom_label* m_sender_name;

public:
    void set_notification_text(const QString& text);
    QString get_notification_text() const;
private:
    custom_label* m_notification_text;

public:
    void set_entity_name(const QString& name);
    QString get_entity_name() const;
private:
    custom_label* m_entity_name;

public:
    void set_notification_id(const QString& id);
    QString get_notification_id() const;
private:
    QString m_notification_id;

public slots:
    void slot_checkbox_clicked();
    void slot_sender_clicked();
    void slot_text_clicked();
    void slot_entity_clicked();
    void set_color();
private:
    void marked_color();
    void unread_color();
    void default_color();
private:
    QPalette* m_background_color;
private:
    flow_layout* m_flow_layout;

public:
    void set_notification_status(notification_status stat);
    notification_status get_notification_status() const;
private:
    notification_status m_notification_status;

signals:
    void signal_checkbox_clicked();
    void signal_sender_clicked(const QString& notification_id);
    void signal_text_clicked(const QString& notification_id);
    void signal_entity_clicked(const QString& notification_id);
};

}// gui
}// kokain

#endif // NOTIFICATION_WIDGET_H
