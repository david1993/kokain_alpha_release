#include "action_button_block.h"

#include <QVBoxLayout>
#include <QDebug>

#include "custom_button.h"

namespace kokain
{

namespace gui
{

const qint32 action_button_block::k_fixed_button_size = 60;

action_button_block::action_button_block(QWidget *parent) :
    QWidget(parent),
    m_projects_btn(NULL),
    m_tasks_btn(NULL),
    m_calendar_btn(NULL),
    m_people_btn(NULL),
    m_conversations_btn(NULL),
    m_files_btn(NULL),
    m_settings_btn(NULL)
{
    initialize_();
}

void action_button_block::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
    on_projects_clicked();

    setFixedWidth(k_fixed_button_size);
    setAutoFillBackground(true);
}

void action_button_block::create_widgets_()
{
    m_projects_btn = new custom_button("PROJECTS", this);
    Q_ASSERT(0 != m_projects_btn);
    m_projects_btn->set_rounded(0);
    m_projects_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_projects_btn->set_hover_color(custom_color_values::PETER_RIVER);

    m_tasks_btn = new custom_button("TASKS", this);
    Q_ASSERT(0 != m_tasks_btn);
    m_tasks_btn->set_rounded(0);
    m_tasks_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_tasks_btn->set_hover_color(custom_color_values::CARROT);

    m_calendar_btn = new custom_button("CALENDAR", this);
    Q_ASSERT(0 != m_calendar_btn);
    m_calendar_btn->set_rounded(0);
    m_calendar_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_calendar_btn->set_hover_color(custom_color_values::SUN_FLOWER);

    m_people_btn = new custom_button("PEOPLE", this);
    Q_ASSERT(0 != m_people_btn);
    m_people_btn->set_rounded(0);
    m_people_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_people_btn->set_hover_color(custom_color_values::AMETHYST);

    m_conversations_btn = new custom_button("CONVERSATIONS", this);
    Q_ASSERT(0 != m_conversations_btn);
    m_conversations_btn->set_rounded(0);
    m_conversations_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_conversations_btn->set_hover_color(custom_color_values::EMERALD);

    m_files_btn = new custom_button("FILES", this);
    Q_ASSERT(0 != m_files_btn);
    m_files_btn->set_rounded(0);
    m_files_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_files_btn->set_hover_color(custom_color_values::ALIZARIN);

    m_settings_btn = new custom_button("SETTINGS", this);
    Q_ASSERT(0 != m_settings_btn);
    m_settings_btn->set_rounded(0);
    m_settings_btn->setFixedSize(k_fixed_button_size, k_fixed_button_size);
    m_settings_btn->set_hover_color(custom_color_values::WET_ASPHALT);

    set_button_categories_to_default();
}

void action_button_block::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->setSpacing(1);
    m_main_layout->setContentsMargins(0, 0, 0, 0);
    m_main_layout->addWidget(m_projects_btn);
    m_main_layout->addWidget(m_tasks_btn);
    m_main_layout->addWidget(m_calendar_btn);
    m_main_layout->addWidget(m_people_btn);
    m_main_layout->addWidget(m_conversations_btn);
    m_main_layout->addWidget(m_files_btn);
    m_main_layout->addWidget(m_settings_btn, 1, Qt::AlignTop);
}

void action_button_block::make_connections_()
{
    connect(m_projects_btn, SIGNAL(clicked()), this, SLOT(on_projects_clicked()));
    connect(m_tasks_btn, SIGNAL(clicked()), this, SLOT(on_tasks_clicked()));
    connect(m_calendar_btn, SIGNAL(clicked()), this, SLOT(on_calendar_clicked()));
    connect(m_people_btn, SIGNAL(clicked()), this, SLOT(on_people_clicked()));
    connect(m_conversations_btn, SIGNAL(clicked()), this, SLOT(on_conversations_clicked()));
    connect(m_files_btn, SIGNAL(clicked()), this, SLOT(on_files_clicked()));
    connect(m_settings_btn, SIGNAL(clicked()), this, SLOT(on_settings_clicked()));
}

void action_button_block::on_projects_clicked()
{
    set_button_categories_to_default();
    m_projects_btn->set_category(custom_button_category_values::INFO);
    emit projects_clicked();
}

void action_button_block::on_tasks_clicked()
{
    set_button_categories_to_default();
    m_tasks_btn->set_category(custom_button_category_values::WARNING);
    emit tasks_clicked();
}

void action_button_block::on_calendar_clicked()
{
    set_button_categories_to_default();
    m_calendar_btn->set_category(custom_button_category_values::WARNING_LIGHT);
    emit calendar_clicked();
}

void action_button_block::on_people_clicked()
{
    set_button_categories_to_default();
    m_people_btn->set_category(custom_button_category_values::HIGHLIGHT);
    emit people_clicked();
}

void action_button_block::on_conversations_clicked()
{
    set_button_categories_to_default();
    m_conversations_btn->set_category(custom_button_category_values::SUCCESS);
    emit conversations_clicked();
}

void action_button_block::on_files_clicked()
{
    set_button_categories_to_default();
    m_files_btn->set_category(custom_button_category_values::DANGER);
    emit files_clicked();
}

void action_button_block::on_settings_clicked()
{
    set_button_categories_to_default();
    m_settings_btn->set_category(custom_button_category_values::INVERSE);
    emit settings_clicked();
}

void action_button_block::set_button_categories_to_default()
{
    m_projects_btn->set_category(custom_button_category_values::DEFAULT);
    m_tasks_btn->set_category(custom_button_category_values::DEFAULT);
    m_calendar_btn->set_category(custom_button_category_values::DEFAULT);
    m_people_btn->set_category(custom_button_category_values::DEFAULT);
    m_conversations_btn->set_category(custom_button_category_values::DEFAULT);
    m_files_btn->set_category(custom_button_category_values::DEFAULT);
    m_settings_btn->set_category(custom_button_category_values::DEFAULT);
}

} // namespace gui

} // namespace kokain
