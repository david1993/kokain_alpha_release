#include "main_window_central_widget.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QMoveEvent>
#include <QScrollArea>

#include "../gui/window_top_panel.h"
#include "../gui/terminal_widget.h"
#include "../gui/action_button_block.h"

#include "../gui/projects_general_widget.h"
#include "../gui/tasks_general_widget.h"
#include "../gui/calendars_general_widget.h"
#include "../gui/people_general_widget.h"
#include "../gui/conversations_general_widget.h"
#include "../gui/files_general_widget.h"
#include "../gui/settings_general_widget.h"

#include "../gui/activity_board_widget.h"

namespace kokain
{

namespace gui
{

/// @todo WTF hardcoded??
const qint32 main_window_central_widget::k_hardcoded_spacing_value = -5;

main_window_central_widget::main_window_central_widget(QWidget *parent)
    : QWidget(parent),
      m_top_panel(NULL),
      m_terminal_wgt(NULL),
      m_actions_block(NULL),
      m_projects_wgt(NULL),
      m_main_layout(NULL),
      m_central_layout(NULL)
{
    initialize_();
}

void main_window_central_widget::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void main_window_central_widget::create_widgets_()
{
    m_top_panel = new window_top_panel(this);
    Q_ASSERT(0 != m_top_panel);
    m_top_panel->set_active_view(window_top_panel::PROJECTS);

    m_terminal_wgt = new terminal_widget(this);
    Q_ASSERT(0 != m_terminal_wgt);

    m_actions_block = new action_button_block(this);
    Q_ASSERT(0 != m_actions_block);
    m_actions_block->setMinimumHeight(this->height());

    create_central_widgets_();
    create_suplementary_widgets_();
}

void main_window_central_widget::create_central_widgets_()
{
    m_projects_wgt = new projects_general_widget(this);
    Q_ASSERT(0 != m_projects_wgt);

    m_tasks_wgt = new tasks_general_widget(this);
    Q_ASSERT(0 != m_tasks_wgt);

    m_calendar_wgt = new calendars_general_widget(this);
    Q_ASSERT(0 != m_calendar_wgt);

    m_people_wgt = new people_general_widget(this);
    Q_ASSERT(0 != m_people_wgt);

    m_conversations_wgt = new conversations_general_widget(this);
    Q_ASSERT(0 != m_conversations_wgt);

    m_files_wgt = new files_general_widget(this);
    Q_ASSERT(0 != m_files_wgt);

    m_settings_wgt = new settings_general_widget(this);
    Q_ASSERT(0 != m_settings_wgt);

    hide_central_widgets_();
}

void main_window_central_widget::create_suplementary_widgets_()
{
    m_activity_board_wgt = new activity_board_widget(this);
    Q_ASSERT(0 != m_activity_board_wgt);
}

void main_window_central_widget::hide_central_widgets_()
{
    m_projects_wgt->hide();
    m_tasks_wgt->hide();
    m_calendar_wgt->hide();
    m_people_wgt->hide();
    m_conversations_wgt->hide();
    m_files_wgt->hide();
    m_settings_wgt->hide();
}

void main_window_central_widget::setup_layout_()
{
    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);

    Q_ASSERT(0 != m_top_panel);
    Q_ASSERT(0 != m_terminal_wgt);
    m_main_layout->setSpacing(0);
    m_main_layout->setContentsMargins(1, 1, 1, 1);
    m_main_layout->addSpacing(k_hardcoded_spacing_value);
    m_main_layout->addWidget(m_top_panel);
    m_main_layout->addSpacing(-k_hardcoded_spacing_value);
    m_main_layout->addWidget(m_terminal_wgt);

    setup_central_layout_();
    Q_ASSERT(0 != m_central_layout);
    m_main_layout->addSpacing(-k_hardcoded_spacing_value);
    m_main_layout->addLayout(m_central_layout);
    m_main_layout->addStretch(1);
}

void main_window_central_widget::setup_central_layout_()
{
    m_central_layout = new QHBoxLayout();
    Q_ASSERT(0 != m_central_layout);
    m_central_layout->setContentsMargins(0, 0, 0, 0);

    Q_ASSERT(0 != m_actions_block);
    Q_ASSERT(0 != m_projects_wgt);
    m_central_layout->addWidget(m_actions_block);
    m_central_layout->addSpacing(-k_hardcoded_spacing_value);

    construct_and_switch_central_layout_(m_projects_wgt);
    /*
    // TODO -----------------------------------------------
    QScrollArea *scrollArea = new QScrollArea();
    //scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    scrollArea->setLayout(m_projects_wgt->layout());
    m_central_layout->addWidget(scrollArea);
    // -----------------------------------------------------

    m_central_layout->addStretch(1);
    m_central_layout->addSpacing(-k_hardcoded_spacing_value);
    m_central_layout->addWidget(m_activity_board_wgt);
    m_projects_wgt->show();
    */
}

void main_window_central_widget::make_connections_()
{
    Q_ASSERT(0 != m_top_panel);
    connect(m_top_panel, SIGNAL(top_panel_maximize_clicked()), this, SIGNAL(maximize_clicked()));
    connect(m_top_panel, SIGNAL(top_panel_minimize_clicked()), this, SIGNAL(minimize_clicked()));
    connect(m_top_panel, SIGNAL(window_moving(QPoint)), this, SLOT(on_window_moving(QPoint)));

    Q_ASSERT(0 != m_actions_block);
    connect(m_actions_block, SIGNAL(projects_clicked()), this, SLOT(switch_projects()));
    connect(m_actions_block, SIGNAL(tasks_clicked()), this, SLOT(switch_tasks()));
    connect(m_actions_block, SIGNAL(calendar_clicked()), this, SLOT(switch_calendar()));
    connect(m_actions_block, SIGNAL(people_clicked()), this, SLOT(switch_people()));
    connect(m_actions_block, SIGNAL(conversations_clicked()), this, SLOT(switch_conversations()));
    connect(m_actions_block, SIGNAL(files_clicked()), this, SLOT(switch_files()));
    connect(m_actions_block, SIGNAL(settings_clicked()), this, SLOT(switch_settings()));
}

void main_window_central_widget::clear_central_layout_()
{
    Q_ASSERT(0 != m_central_layout);
    for (int i = 1; i < m_central_layout->count(); ++i) {
        m_central_layout->removeItem(m_central_layout->itemAt(i));
    }
}

void main_window_central_widget::construct_and_switch_central_layout_(QWidget *wgt)
{
    Q_ASSERT(0 != wgt);
    Q_ASSERT(0 != m_central_layout);
    clear_central_layout_();
    hide_central_widgets_();
    wgt->show();
    m_central_layout->addWidget(wgt);
    m_central_layout->addSpacing(-k_hardcoded_spacing_value);
    Q_ASSERT(0 != m_activity_board_wgt);
    m_central_layout->addWidget(m_activity_board_wgt, 0, Qt::AlignRight);
}

void main_window_central_widget::switch_projects()
{
    m_top_panel->set_active_view(window_top_panel::PROJECTS);
    construct_and_switch_central_layout_(m_projects_wgt);
}

void main_window_central_widget::switch_tasks()
{
    m_top_panel->set_active_view(window_top_panel::TASKS);
    construct_and_switch_central_layout_(m_tasks_wgt);
}

void main_window_central_widget::switch_calendar()
{
    m_top_panel->set_active_view(window_top_panel::CALENDAR);
    m_top_panel->on_active_view_change();
    construct_and_switch_central_layout_(m_calendar_wgt);
}

void main_window_central_widget::switch_people()
{
    m_top_panel->set_active_view(window_top_panel::PEOPLE);
    construct_and_switch_central_layout_(m_people_wgt);
}

void main_window_central_widget::switch_conversations()
{
    m_top_panel->set_active_view(window_top_panel::CONVERSATIONS);
    construct_and_switch_central_layout_(m_conversations_wgt);
}

void main_window_central_widget::switch_files()
{
    m_top_panel->set_active_view(window_top_panel::FILES);
    construct_and_switch_central_layout_(m_files_wgt);
}

void main_window_central_widget::switch_settings()
{
    m_top_panel->set_active_view(window_top_panel::SETTINGS);
    m_top_panel->on_active_view_change();
    construct_and_switch_central_layout_(m_settings_wgt);
}

void main_window_central_widget::on_window_moving(QPoint p)
{
    parentWidget()->move(p);
}

} // namespace gui

} // namespace kokain
