#ifndef MY_ACTIVITY_WIDGET_H
#define MY_ACTIVITY_WIDGET_H

#include <QWidget>

class QLabel;
class QHBoxLayout;

namespace kokain
{

namespace gui
{

class timer_widget;
class custom_button;

class my_activity_widget : public QWidget
{
    Q_OBJECT
public:
    explicit my_activity_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    void create_buttons_();

private:
    QLabel* m_my_activity_lbl;
    timer_widget* m_working_on_wgt;
    timer_widget* m_total_working_wgt;
    custom_button* m_pause_btn;
    custom_button* m_complete_btn;
    QHBoxLayout* m_main_layout;

public slots:
    void on_pause_button_clicked();
    void on_complete_button_clicked();

private:
    bool m_paused;

private:
    const static qint32 k_fixed_width;
    const static qint32 k_fixed_height;
    const static qint32 k_button_fixed_size;

private:
    my_activity_widget(const my_activity_widget&);
    my_activity_widget& operator=(const my_activity_widget&);

}; // class my_activity_widget

} // namespace gui

} // namespace kokain

#endif // MY_ACTIVITY_WIDGET_H
