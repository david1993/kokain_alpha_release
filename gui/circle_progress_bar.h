#ifndef CIRCLE_PROGRESS_BAR_H
#define CIRCLE_PROGRESS_BAR_H

#include <QProgressBar>

namespace kokain
{

namespace gui
{

class circle_progress_bar : public QProgressBar
{
    Q_OBJECT

public:
    circle_progress_bar(QWidget *parent = 0);

private:
    bool m_transparent_center;
    qreal m_start_angle;

public:
    void set_transparent_center(bool v);
    void set_start_angle(qreal sa);

protected:
    void paintEvent(QPaintEvent *event);

private:
    const static qreal k_start_angle_default_value;
    const static qint32 k_minimum_size;

private:
    circle_progress_bar(const circle_progress_bar&);
    circle_progress_bar& operator=(const circle_progress_bar&);

}; // class circle_progress_bar

} // namespace gui

} // namespace kokain

#endif // CIRCLE_PROGRESS_BAR_H
