#ifndef FILES_GENERAL_WIDGET_H
#define FILES_GENERAL_WIDGET_H

#include <QWidget>

class QLabel;
class QVBoxLayout;

namespace kokain
{

namespace gui
{

class files_general_widget : public QWidget
{
    Q_OBJECT
public:
    explicit files_general_widget(QWidget *parent = 0);

private:
    void initialize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QLabel* m_temp_lbl;
    QVBoxLayout* m_main_layout;

signals:

public slots:

}; // class files_general_widget

} // namespace gui

} // namespace kokain

#endif // FILES_GENERAL_WIDGET_H
