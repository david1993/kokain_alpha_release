#include "project_widget.h"

#include <QDebug>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QProgressBar>

#include "../core/project.h"
#include "custom_button.h"
#include "circle_progress_bar.h"
#include "custom_widget_style.h"
#include "custom_menu.h"

namespace kokain
{

namespace gui
{

// static const data initialization
const qint32 project_widget::k_project_description_font_size = 10;
const qint32 project_widget::k_project_name_font_size = 18;
const qint32 project_widget::k_manage_button_fixed_width = 180;
const qint32 project_widget::k_manage_button_fixed_height = 35;
const qint32 project_widget::k_component_button_fixed_width = 120;
const qint32 project_widget::k_component_button_fixed_height = 25;
const qint32 project_widget::k_fixed_layout_spacing_value = 500;

project_widget::project_widget(QWidget *parent) :
    QWidget(parent),
    m_project_core(0)
{
    initilize_();
}

project_widget::project_widget(core::project *cr, QWidget *p)
    : QWidget(p),
      m_project_core(cr)
{
    initilize_();
}

void project_widget::initilize_()
{
    create_widgets_();
    setup_layout_();
    setup_styles_();
    make_connections_();
}

void project_widget::create_component_widgets_()
{

}

void project_widget::create_widgets_()
{
    create_labels_();
    create_manage_buttons_();
    create_component_buttons_();
    create_component_widgets_();
}

void project_widget::create_labels_()
{
    m_project_name_lbl = new QLabel();
    m_project_description_lbl = new QLabel();

    if (0 != m_project_core) {
        m_project_name_lbl->setText(m_project_core->name());
        m_project_description_lbl->setText(m_project_core->description());
    }
}

void project_widget::create_manage_buttons_()
{
    m_edit_project_btn = new custom_button(tr("EDIT PROJECT"), this);
    Q_ASSERT(0 != m_edit_project_btn);
    m_edit_project_btn->set_category(custom_button_category_values::PRIMARY);
    m_edit_project_btn->set_rounded(0);
    m_edit_project_btn->setFixedSize(k_manage_button_fixed_width, k_manage_button_fixed_height);

    // using corresponding constructor, instead of calling set_xxx
    m_create_project_btn = new custom_button(tr("CREATE ANOTHER PROJECT"),
            custom_button_type_values::LARGE, custom_button_category_values::PRIMARY, this);
    Q_ASSERT(0 != m_create_project_btn);
    m_create_project_btn->set_rounded(0);
    m_create_project_btn->setFixedSize(k_manage_button_fixed_width, k_manage_button_fixed_height);

    m_current_info_btn = new custom_button("?", custom_button_type_values::DEFAULT,
            custom_button_category_values::WARNING_LIGHT, this);
    Q_ASSERT(0 != m_current_info_btn);
    m_current_info_btn->setFixedSize(k_component_button_fixed_height, k_component_button_fixed_height);
    m_current_info_btn->set_rounded(m_current_info_btn->width() / 2);
}

void project_widget::create_component_buttons_()
{
    m_tasks_btn = new custom_button(tr("TASKS"), this);
    m_plans_btn = new custom_button(tr("PLANS"), this);
    m_files_btn = new custom_button(tr("FILES"), this);
    m_calendars_btn = new custom_button(tr("CALENDARS"), this);
    m_comments_btn = new custom_button(tr("COMMENTS"), this);
    m_people_btn = new custom_button(tr("PEOPLE"), this);
    m_more_btn = new custom_button(tr("MORE.."), this);
    m_components_more_menu = new custom_menu(this);
    m_sharing_btn = new QAction(tr("SHARING"), this);
    m_resources_btn = new QAction(tr("RESOURCES"), this);
    m_references_btn = new QAction(tr("REFERENCES"), this);
    m_details_btn = new QAction(tr("DETAILS"), this);

    configure_component_buttons_();
}

void project_widget::configure_component_buttons_()
{
    _debug_check_component_buttons_();
    m_tasks_btn->set_rounded(0);
    m_tasks_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_plans_btn->set_rounded(0);
    m_plans_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_files_btn->set_rounded(0);
    m_files_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_calendars_btn->set_rounded(0);
    m_calendars_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_comments_btn->set_rounded(0);
    m_comments_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_people_btn->set_rounded(0);
    m_people_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_more_btn->set_rounded(0);
    m_more_btn->setFixedSize(k_component_button_fixed_width, k_component_button_fixed_height);
    m_more_btn->setMenu(m_components_more_menu);
    m_components_more_menu->addAction(m_sharing_btn);
    m_components_more_menu->addAction(m_resources_btn);
    m_components_more_menu->addAction(m_references_btn);
    m_components_more_menu->addAction(m_details_btn);
    component_buttons_set_to_defaults_();
}

void project_widget::setup_title_layout_()
{
    m_title_layout = new QVBoxLayout();
    QHBoxLayout* upper = new QHBoxLayout();
    Q_ASSERT(0 != m_edit_project_btn);
    Q_ASSERT(0 != m_create_project_btn);
    Q_ASSERT(0 != m_project_name_lbl);
    upper->addWidget(m_project_name_lbl);
    upper->addWidget(m_edit_project_btn, 1, Qt::AlignRight);
    upper->addWidget(m_create_project_btn);
    upper->setAlignment(m_create_project_btn, Qt::AlignRight);
    upper->setAlignment(m_edit_project_btn, Qt::AlignRight);

    QHBoxLayout* lower = new QHBoxLayout();
    Q_ASSERT(0 != m_project_description_lbl);
    Q_ASSERT(0 != m_current_info_btn);
    lower->addWidget(m_project_description_lbl);
    lower->addWidget(m_current_info_btn);
    lower->setAlignment(m_current_info_btn, Qt::AlignRight);

    Q_ASSERT(0 != m_title_layout);
    m_title_layout->addLayout(upper);
    m_title_layout->addLayout(lower);
}

void project_widget::setup_component_buttons_layout_()
{
    make_sure_widgets_are_alive_();
    m_component_buttons_layout = new QHBoxLayout();
    Q_ASSERT(0 != m_component_buttons_layout);
    m_component_buttons_layout->setContentsMargins(0, 0, 0, 0);
    m_component_buttons_layout->setSpacing(0);
    m_component_buttons_layout->addWidget(m_tasks_btn);
    m_component_buttons_layout->addWidget(m_plans_btn);
    m_component_buttons_layout->addWidget(m_files_btn);
    m_component_buttons_layout->addWidget(m_calendars_btn);
    m_component_buttons_layout->addWidget(m_comments_btn);
    m_component_buttons_layout->addWidget(m_people_btn);
    m_component_buttons_layout->addWidget(m_more_btn, 1, Qt::AlignLeft);
}

void project_widget::setup_layout_()
{
    setup_title_layout_();
    setup_component_buttons_layout_();

    m_main_layout = new QVBoxLayout(this);
    Q_ASSERT(0 != m_main_layout);
    m_main_layout->addLayout(m_title_layout);
    m_main_layout->addLayout(m_component_buttons_layout);
}

void project_widget::make_connections_()
{
    connect(m_edit_project_btn, SIGNAL(clicked()), this, SLOT(on_edit_project_button_clicked()));
    connect(m_create_project_btn, SIGNAL(clicked()), this, SLOT(on_create_project_button_clicked()));
    connect(m_current_info_btn, SIGNAL(clicked()), this, SLOT(on_current_info_button_clicked()));

    make_component_buttons_connections_();
}

void project_widget::make_component_buttons_connections_()
{
    connect(m_tasks_btn, SIGNAL(clicked()), this, SLOT(on_tasks_button_clicked()));
    connect(m_plans_btn, SIGNAL(clicked()), this, SLOT(on_plans_button_clicked()));
    connect(m_files_btn, SIGNAL(clicked()), this, SLOT(on_files_button_clicked()));
    connect(m_calendars_btn, SIGNAL(clicked()), this, SLOT(on_calendars_button_clicked()));
    connect(m_comments_btn, SIGNAL(clicked()), this, SLOT(on_comments_button_clicked()));
    connect(m_people_btn, SIGNAL(clicked()), this, SLOT(on_people_button_clicked()));
    connect(m_more_btn, SIGNAL(clicked()), this, SLOT(on_more_button_clicked()));
    connect(m_sharing_btn, SIGNAL(triggered()), this, SLOT(on_sharing_button_clicked()));
    connect(m_resources_btn, SIGNAL(triggered()), this, SLOT(on_resources_button_clicked()));
    connect(m_references_btn, SIGNAL(triggered()), this, SLOT(on_references_button_clicked()));
    connect(m_details_btn, SIGNAL(triggered()), this, SLOT(on_details_button_clicked()));
}

void project_widget::setup_styles_()
{
    setup_label_styles_();
}

// TODO: later move this under RFUF
void project_widget::setup_label_styles_()
{
    QFont f = m_project_name_lbl->font();
    f.setBold(true);
    f.setPointSize(k_project_name_font_size);
    m_project_name_lbl->setFont(f);

    m_project_description_lbl->setStyleSheet("QLabel { color: gray; }");
    f = m_project_description_lbl->font();
    f.setItalic(true);
    f.setPointSize(k_project_description_font_size);
    m_project_description_lbl->setFont(f);
}

void project_widget::update_widget_data_()
{
    make_sure_widgets_are_alive_();
    m_project_name_lbl->setText(m_project_core->name());
    m_project_description_lbl->setText(m_project_core->description());
}

void project_widget::make_sure_widgets_are_alive_()
{
    Q_ASSERT(0 != m_project_core);
    Q_ASSERT(0 != m_project_name_lbl);
    Q_ASSERT(0 != m_project_description_lbl);
    _debug_check_component_buttons_();
}

void project_widget::_debug_check_component_buttons_()
{
    Q_ASSERT(0 != m_tasks_btn);
    Q_ASSERT(0 != m_plans_btn);
    Q_ASSERT(0 != m_files_btn);
    Q_ASSERT(0 != m_calendars_btn);
    Q_ASSERT(0 != m_comments_btn);
    Q_ASSERT(0 != m_people_btn);
    Q_ASSERT(0 != m_more_btn);
    Q_ASSERT(0 != m_components_more_menu);
    Q_ASSERT(0 != m_sharing_btn);
    Q_ASSERT(0 != m_resources_btn);
    Q_ASSERT(0 != m_references_btn);
    Q_ASSERT(0 != m_details_btn);
}

void project_widget::set_project_core(kokain::core::project *p)
{
    Q_ASSERT(p != 0);
    m_project_core = p;
    update_widget_data_();
}

core::project* project_widget::project_core()
{
    return m_project_core;
}

void project_widget::on_edit_project_button_clicked()
{

}

void project_widget::on_create_project_button_clicked()
{

}

void project_widget::on_current_info_button_clicked()
{

}

void project_widget::on_tasks_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_tasks_btn->set_category(custom_button_category_values::WARNING);
}

void project_widget::on_plans_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_plans_btn->set_category(custom_button_category_values::INFO);
}

void project_widget::on_files_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_files_btn->set_category(custom_button_category_values::DANGER);
}

void project_widget::on_calendars_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_calendars_btn->set_category(custom_button_category_values::WARNING_LIGHT);
}

void project_widget::on_comments_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_comments_btn->set_category(custom_button_category_values::PRIMARY);
}

void project_widget::on_people_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_people_btn->set_category(custom_button_category_values::HIGHLIGHT);
}

void project_widget::on_more_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_more_btn->set_category(custom_button_category_values::INVERSE);
}

void project_widget::on_sharing_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_more_btn->set_category(custom_button_category_values::HIGHLIGHT);
}

void project_widget::on_resources_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_more_btn->set_category(custom_button_category_values::SUCCESS);
}

void project_widget::on_references_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_more_btn->set_category(custom_button_category_values::DANGER);
}

void project_widget::on_details_button_clicked()
{
    component_buttons_set_to_defaults_();
    m_more_btn->set_category(custom_button_category_values::INVERSE);
}

void project_widget::component_buttons_set_to_defaults_()
{
    _debug_check_component_buttons_();
    m_tasks_btn->set_category(custom_button_category_values::DEFAULT);
    m_plans_btn->set_category(custom_button_category_values::DEFAULT);
    m_files_btn->set_category(custom_button_category_values::DEFAULT);
    m_calendars_btn->set_category(custom_button_category_values::DEFAULT);
    m_comments_btn->set_category(custom_button_category_values::DEFAULT);
    m_people_btn->set_category(custom_button_category_values::DEFAULT);
}

} // namespace gui

} // namespace kokain
