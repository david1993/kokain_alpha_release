#include "main_controller.h"

#include <QApplication>

#include "db/db_manager.h"
#include "settings/settings_manager.h"
#include "db/cache_manager.h"
#include "network/network_manager.h"
#include "calendar/calendar.h"
#include "main_window.h"

using namespace kokain;

main_controller::main_controller()
{
    qDebug() << "An instance of overall_manager is created.";
}

main_controller::~main_controller()
{
}

main_controller* main_controller::s_instance;

main_controller* main_controller::get_instance()
{
    if(!s_instance)
    {
        main_controller* tmp = new main_controller();
        s_instance = tmp;
    }
    return s_instance;
}

void main_controller::my_entry()
{
    db::db_manager* dbmngr = db::db_manager::get_instance();
    settings::settings_manager* setmngr = settings::settings_manager::get_instance();
    db::cache_manager* cachemngr = db::cache_manager::get_instance();

    kokain::main_window* w = new kokain::main_window();
    w->showMaximized();
}

void main_controller::remove_instance()
{
    qDebug() << "removing main_controller isntance";
    db::cache_manager::remove_instance();
    settings::settings_manager::remove_instance();
    db::db_manager::remove_instance();

    delete s_instance;
    s_instance = 0;
    qDebug() << "main_oontroller removed.";
}

void main_controller::set_current_user_id(qint32 id)
{
    m_cur_user_id = id;
    qDebug() << "Current user's id is set == " << id;
}

qint32 main_controller::current_user_id() const
{
    return m_cur_user_id;
}
