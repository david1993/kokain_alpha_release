#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

class entity;
class package;

class controller : public QObject
{
public:
    controller(QObject* parent = 0);
    ~controller();

public slots:
    void evaluate_child_controller(package* new_package);
    void verify_from_child_controller(package* new_package);

};

#endif // CONTROLLER_H
