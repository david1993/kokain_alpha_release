#ifndef KOKAIN_NETWORK_PACKAGE_RECEIVER_H
#define KOKAIN_NETWORK_PACKAGE_RECEIVER_H

#include <set>
#include <map>

#include <QObject>
#include <QByteArray>
#include <QTimer>
#include <QSharedPointer>

namespace kokain
{

namespace db
{

class db_manager;
class synchronizer;

}

namespace network
{

class datagram;
class package;
class connection;
class network_manager;

/**
 * @class package_receiver
 * @brief Represents receiver logic for both UDP and TCP protocols
 * Receiving implement based on package's protocol() == UDP || TCP.
 *
 */
class package_receiver : public QObject
{
    Q_OBJECT
private:
    explicit package_receiver(network_manager* nm, QObject *parent = 0);

private:
    void initialize_();
    void load_from_db_();
    void make_connections_();
    void start_timers_();

private:
    QSharedPointer<network_manager> m_network_manager;
    db::db_manager* m_db_manager; // wrap with shared pointer later, after db_manager
    QSharedPointer<db::synchronizer> m_synchronizer;
    QTimer* m_package_checker_timer;

public:
    void process_datagram(const QSharedPointer<datagram>& d);

public:
    void process_ready_package(const QSharedPointer<package> &p);

private:
    void reply_for_package_(const QSharedPointer<package> p, const QSharedPointer<package> rep);

private:
    std::map<QByteArray, QSharedPointer<package> > m_packages;
    std::set<QByteArray> m_received_table;

signals:
    void check_packages();
    void package_received(const QByteArray&);

    /// @brief UDP transfer-related signals
signals:
    void udp_require_lost_datagrams(const QByteArray&);
    void udp_final_datagram_accepted(const QSharedPointer<datagram>&);
    void udp_final_datagram_arrived(const QSharedPointer<datagram>&);
    void udp_package_receive_accepted(const QSharedPointer<datagram>&);
    void udp_package_receive_arrived(const QSharedPointer<datagram>&);
    void udp_datagram_required(const QSharedPointer<datagram>&);
    void udp_unknown_datagram_received(const QSharedPointer<datagram>&);

    /// @brief Reliability-related signals
signals:
    void autosave_current_state();

public slots:
    void verify_packages(); // do we need this as public?

private:
    // used by verify_packages
    bool check_if_need_to_panic_(const QSharedPointer<package>& p);

private slots:
    void on_package_received(const QByteArray&);
    void on_autosave_current_state();

    /// @brief TCP-related slots
public slots:
    void on_tcp_package_receive(const QSharedPointer<connection> &c);

    /// @brief UDP-related slots
private slots:
    void on_udp_package_receive_accepted(const QSharedPointer<datagram>&);
    void on_udp_require_lost_datagram(const QByteArray&);
    void on_udp_final_datagram_arrived(const QSharedPointer<datagram>&);

public:
    static QSharedPointer<package_receiver> get_instance(network_manager* nm, QObject* p);

private:
    static QSharedPointer<package_receiver> s_instance;

private:
    const static qint32 k_package_checker_time_interval;
    const static qreal k_lost_signaling_percentage;
    const static qint64 k_lost_signaling_timespan;

private:
    package_receiver(const package_receiver&);
    package_receiver& operator=(const package_receiver&);

}; // class package_receiver

} // namespace network

} // namespace network

#endif // KOKAIN_NETWORK_PACKAGE_RECEIVER_H
