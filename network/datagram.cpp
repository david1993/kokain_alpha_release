#include "datagram.h"

#include <QByteArray>
#include <QDataStream>

#include "package.h"

namespace kokain
{

namespace network
{

const qint32 datagram::k_protocol_id = 0x1124;
const qint32 datagram::k_default_sending_counts = 2;
const qint32 datagram::k_data_size_in_bytes = 226;
const qint32 datagram::k_header_size_in_bytes = 4;
const qint32 datagram::k_fixed_datagram_size = 256;

datagram::datagram(QObject *parent)
    : QObject(parent),
      m_protocol_id(k_protocol_id),
      m_parent_package(NULL),
      m_type(DATA),
      m_sequence_number(0),
      m_status(NOT_SENT),
      m_priority(datagram::MIDDLE_PRIORITY),
      m_target_connection(NULL),
      m_timer(NULL),
      m_sending_count(k_default_sending_counts)
{
}

datagram::datagram(const QSharedPointer<package> &p, QObject *parent)
    : QObject(parent),
      m_protocol_id(k_protocol_id),
      m_parent_package(NULL),
      m_type(DATA),
      m_sequence_number(0),
      m_status(NOT_SENT),
      m_priority(datagram::MIDDLE_PRIORITY),
      m_target_connection(NULL),
      m_timer(NULL),
      m_sending_count(k_default_sending_counts)
{
    initialize_from_parent_package_(p);
}

datagram::datagram(const QSharedPointer<package> &p, datagram_type t, qint32 sn,
                   const QByteArray &d, QObject *parent)
    : QObject(parent),
      m_protocol_id(k_protocol_id),
      m_parent_package(p),
      m_type(t),
      m_sequence_number(sn),
      m_status(NOT_SENT),
      m_data(d),
      m_priority(datagram::MIDDLE_PRIORITY),
      m_target_connection(NULL),
      m_timer(NULL),
      m_sending_count(k_default_sending_counts)
{
    initialize_from_parent_package_(p);
}

void datagram::initialize_from_parent_package_(const QSharedPointer<package> &parent)
{
    Q_ASSERT(0 != parent);
    m_parent_package = parent;

    Q_ASSERT(0 != m_parent_package->target_connection());
    Q_ASSERT(m_data.size() <= k_data_size_in_bytes);
    m_priority = m_parent_package->priority();
    m_target_connection = m_parent_package->target_connection();
    m_checksum = m_parent_package->checksum();
}

qint32 datagram::protocol_id() const
{
    return m_protocol_id;
}

QSharedPointer<package> datagram::parent_package()
{
    return m_parent_package;
}

void datagram::set_parent_package(const QSharedPointer<package>& p)
{
    m_parent_package = p;
}

QByteArray datagram::checksum() const
{
    return m_checksum;
}

void datagram::set_checksum(const QByteArray& ch)
{
    m_checksum = ch;
}

datagram::datagram_type datagram::type() const
{
    return m_type;
}

void datagram::set_type(datagram_type dt)
{
    m_type = dt;
}

qint32 datagram::sequence_number() const
{
    return m_sequence_number;
}

qint32 datagram::number() const
{
    return sequence_number();
}

void datagram::set_sequence_number(qint32 n)
{
    m_sequence_number = n;
}

datagram::datagram_status datagram::status() const
{
    return m_status;
}

void datagram::set_status(datagram_status s)
{
    m_status = s;
}

QByteArray datagram::data() const
{
    return m_data;
}

void datagram::set_data(const QByteArray& d)
{
    Q_ASSERT(d.size() <= k_data_size_in_bytes);
    m_data = d;
}

QByteArray datagram::to_byte_array() const
{
    QByteArray ba;
    ba.reserve(k_fixed_datagram_size);
    QDataStream writer(&ba, QIODevice::WriteOnly);
    writer.setVersion(QDataStream::Qt_5_0);
    writer << k_protocol_id;
    writer << m_checksum;
    writer << static_cast<quint16>(m_type);
    writer << m_sequence_number;
    writer << m_data;

    Q_ASSERT(ba.size() <= k_fixed_datagram_size);
    return ba;
}

void datagram::from_byte_array(const QByteArray &ba)
{
    Q_ASSERT(ba.size() <= k_fixed_datagram_size);
    QByteArray local(ba);
    QDataStream reader(&local, QIODevice::ReadOnly);
    reader.setVersion(QDataStream::Qt_5_0);
    reader >> m_protocol_id;
    // ensure to read exactly 20 bytes
    // local tests showed up with good results(read exactly 20 bytes without any special finduflushka)
    reader >> m_checksum;
    quint16 tp;
    reader >> tp;
    m_type = (datagram_type)tp;
    reader >> m_sequence_number;
    reader >> m_data;
}

datagram::datagram_priority datagram::priority() const
{
    return m_priority;
}

void datagram::set_priority(datagram::datagram_priority p)
{
    m_priority = p;
}

QSharedPointer<connection> datagram::target_connection()
{
    return m_target_connection;
}

void datagram::set_target_connection(const QSharedPointer<connection>& c)
{
    m_target_connection = c;
}

QPointer<QTimer> datagram::timer()
{
    return m_timer;
}

void datagram::set_timer(const QPointer<QTimer>& t)
{
    Q_ASSERT(0 != t);
    m_timer = t;
    connect(m_timer, SIGNAL(timeout()), this, SLOT(on_special_timer_timeout_()));
}

void datagram::on_special_timer_timeout_()
{
    emit send_me(QSharedPointer<datagram>(this));
}

qint32 datagram::sending_count() const
{
    return m_sending_count;
}

void datagram::set_sending_count(qint32 c)
{
    m_sending_count = c;
}

bool datagram::operator <(const datagram& d)
{
    return m_priority < d.priority();
}

bool datagram::operator ==(const datagram& d)
{
    return m_priority == d.priority();
}

bool datagram::is_equal(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != d);
    return (this->checksum() == d->checksum() )
            && ( this->sequence_number() == d->sequence_number() )
            && ( this->type() == d->type() );
}

} // namespace network

} // namespace kokain
