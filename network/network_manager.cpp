#include "network_manager.h"

#include "package_receiver.h"
#include "package_sender.h"
#include "datagram.h"

namespace kokain
{

namespace network
{

QSharedPointer<network_manager> network_manager::s_instance(NULL);

network_manager::network_manager(QObject *p)
    : QObject(p)
{
    initialize_();
}

QSharedPointer<network_manager> network_manager::get_instance(QObject *p)
{
    if (NULL == s_instance) {
        s_instance.reset(new network_manager(p));
    }
    return s_instance;
}

void network_manager::initialize_()
{
    m_connection_manager = connection_manager::get_instance(this);
    Q_ASSERT(0 != m_connection_manager);
    m_package_receiver = package_receiver::get_instance(this, this);
    Q_ASSERT(0 != m_package_receiver);
    m_package_sender = package_sender::get_instance(this);
    m_package_sender->set_connection_manager(m_connection_manager);

    m_tcp_server = new QTcpServer(this);
    Q_ASSERT(0 != m_tcp_server);
    m_tcp_server->listen(QHostAddress::Any); // TODO: fix the port later
    m_udp_socket = new QUdpSocket(this);
    Q_ASSERT(0 != m_udp_socket);
    m_udp_socket->bind(QHostAddress::LocalHost, 4321); // TODO: fix the port later

    make_connections_();
}

void network_manager::make_connections_()
{
    connect(m_tcp_server, SIGNAL(newConnection()), this, SLOT(on_incoming_tcp_connection_()));
    connect(m_udp_socket, SIGNAL(readyRead()), this, SLOT(on_incoming_udp_datagram_()));
}

void network_manager::on_incoming_tcp_connection_()
{
    QPointer<QTcpSocket> sock = m_tcp_server->nextPendingConnection();
    QSharedPointer<connection> new_con(new connection(this));
    new_con->set_tcp_socket(sock);

    Q_ASSERT(0 != m_connection_manager);
    Q_ASSERT(0 != m_package_receiver);
    m_connection_manager->add_connection(new_con);
    m_package_receiver->on_tcp_package_receive(new_con);
}

void network_manager::on_incoming_udp_datagram_()
{
    Q_ASSERT(0 != m_udp_socket);
    while (m_udp_socket->hasPendingDatagrams()) {
        QByteArray dgram;
        dgram.resize(m_udp_socket->pendingDatagramSize());
        m_udp_socket->readDatagram(dgram.data(), dgram.size());
        Q_ASSERT(0 != m_package_receiver);
        QSharedPointer<datagram> received_dgram(new datagram());
        received_dgram->from_byte_array(dgram);
        m_package_receiver->process_datagram(received_dgram);
    }
}

void network_manager::send_data(const QSharedPointer<package> &p)
{
    Q_ASSERT(0 != m_package_sender);
    m_package_sender->send_package(p);
}

void network_manager::send_datagram(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != m_package_sender);
    m_package_sender->send_datagram(d);
}

} // namespace network

} // namespace kokain
