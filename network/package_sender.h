#ifndef KOKAIN_NETWORK_PACKAGE_SENDER_H
#define KOKAIN_NETWORK_PACKAGE_SENDER_H

#include <map>
#include <queue>

#include <QObject>
#include <QTimer>
#include <QUdpSocket>

#include "connection_manager.h"

namespace kokain
{

namespace network
{

class datagram;
class package;

class package_sender : public QObject
{
    Q_OBJECT
private:
    explicit package_sender(QObject *parent = 0);

private:
    void initialize_();
    void load_from_db_();
    void make_connections_();
    void start_timers_();

public:
    void set_connection_manager(const QSharedPointer<connection_manager>& cm);

private:
    QTimer* m_failed_package_checker;
    QTimer* m_autosave_timer;
    QSharedPointer<connection_manager> m_connection_manager;

public:
    void send_package(const QSharedPointer<package>& p);
    void send_datagram(const QSharedPointer<datagram>& d);
    void send_datagram_vector(const std::vector<QSharedPointer<datagram> >& dvec);

private:
    void send_package_tcp_(const QSharedPointer<package>& p);
    void send_package_udp_(const QSharedPointer<package>& p);
    void send_special_datagram_(const QSharedPointer<datagram>& sd);
    void send_datagram_private_(const QSharedPointer<datagram>& d);
    void save_package_to_try_again_(const QByteArray& chk);
    void save_failed_packages_();
    void load_failed_packages_();
    void update_failed_packages_table_();
    void register_datagram_(const QSharedPointer<datagram>& d);
    void stop_and_remove_from_special_dgrams_(const QSharedPointer<datagram>& d);

private:
    std::priority_queue<QSharedPointer<datagram> > m_dgrams_queue;
    std::map<QByteArray, QSharedPointer<package> > m_sending_packages;
    std::vector<QSharedPointer<package> > m_failed_packages;
    std::map<QByteArray, std::vector<QSharedPointer<datagram> > > m_special_dgrams;
    std::map<QByteArray, std::map<qint32, bool> > m_registered_datagrams;

private:
    QPointer<QUdpSocket> m_receiving_udp_socket;
    QPointer<QUdpSocket> m_sending_udp_socket;

signals:
    void failed_packages_try_again();
    void sender_socket_error(QAbstractSocket::SocketError);
    void autosave_current_state();
    void sender_package_not_sent(QSharedPointer<package>);

public slots:
    void on_udp_final_datagram_accepted(const QSharedPointer<datagram>&);
    void on_udp_package_receive_arrived(const QSharedPointer<datagram>&);
    void udp_send_allowed();
    void on_udp_datagram_required(const QSharedPointer<datagram>&);

private slots:
    void on_udp_special_datagram_timeout(const QSharedPointer<datagram> &d);
    void on_failed_packages_try_again();

public:
    static QSharedPointer<package_sender> get_instance(QObject *p = 0);

private:
    static QSharedPointer<package_sender> s_instance;

private:
    const static qint32 k_default_interval_for_failures;
    const static qint32 k_autosave_timer_interval;

private:
    package_sender(const package_sender&);
    package_sender& operator=(const package_sender&);

}; // class package_sender

} // namespace network

} // namespace kokain

#endif // KOKAIN_NETWORK_PACKAGE_SENDER_H
