#ifndef KOKAIN_NETWORK_NETWORK_MANAGER_H
#define KOKAIN_NETWORK_NETWORK_MANAGER_H

#include <QObject>
#include <QSharedPointer>
#include <QTcpServer>
#include <QPointer>
#include <QUdpSocket>

namespace kokain
{

namespace network
{

class package;
class datagram;
class connection_manager;
class package_sender;
class package_receiver;

class network_manager : public QObject
{
    Q_OBJECT
private:
    explicit network_manager(QObject* parent = 0);

private:
    void initialize_();
    void make_connections_();

public:
    void send_data(const QSharedPointer<package>&);
    void send_datagram(const QSharedPointer<datagram>&);

private:
    QSharedPointer<connection_manager> m_connection_manager;
    QSharedPointer<package_sender> m_package_sender;
    QSharedPointer<package_receiver> m_package_receiver;

private:
    QPointer<QTcpServer> m_tcp_server;
    QPointer<QUdpSocket> m_udp_socket;

private slots:
    void on_incoming_tcp_connection_();
    void on_incoming_udp_datagram_();

    /// @brief Instance logic
public:
    static QSharedPointer<network_manager> get_instance(QObject* p = 0);

private:
    static QSharedPointer<network_manager> s_instance;

private:
    network_manager(const network_manager&);
    network_manager& operator=(const network_manager&);

}; // class network_manager

} // namespace network

} // namespace kokain

#endif // KOKAIN_NETWORK_NETWORK_MANAGER_H
