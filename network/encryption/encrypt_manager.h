#ifndef ENCRYPT_MANAGER_H
#define ENCRYPT_MANAGER_H

#include <string>

namespace kokain
{
namespace network
{

class encrypt_manager
{
public:
    static std::string encript_str(const std::string &str);
    static std::string decript_str(const std::string &str);
private:
    encrypt_manager();
};

}//namespace network
}//namespace kokain
#endif // ENCRYPT_MANAGER_H
