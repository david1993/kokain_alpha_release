#ifndef CRYPT_H
#define CRYPT_H

#include <string>

namespace kokain
{
namespace network
{

class crypt
{
public:
    static std::string encrypt(const std::string &str);
    static std::string deccrypt(const std::string &str);
 private:
    crypt();
};

}//namespace network
}//namespace kokain


#endif // CRYPT_H
