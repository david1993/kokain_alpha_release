#include "package_sender.h"

#include "package.h"

namespace kokain
{

namespace network
{

const qint32 package_sender::k_default_interval_for_failures = 3000; // 3 seconds
const qint32 package_sender::k_autosave_timer_interval = 300000; // 300 seconds

QSharedPointer<package_sender> package_sender::s_instance(NULL);

QSharedPointer<package_sender> package_sender::get_instance(QObject* p)
{
    if (NULL != s_instance) {
        s_instance.reset(new package_sender(p));
    }
    return s_instance;
}

package_sender::package_sender(QObject *parent) :
    QObject(parent)
  // get back here after class completion
{
    initialize_();
}

void package_sender::initialize_()
{
    m_failed_package_checker = new QTimer(this);
    m_autosave_timer = new QTimer(this);
    load_from_db_();
    make_connections_();
    start_timers_();
}

void package_sender::load_from_db_()
{
    // if db_manager is OK
}

void package_sender::make_connections_()
{
    connect(m_failed_package_checker, SIGNAL(timeout()), this, SIGNAL(failed_packages_try_again()));
    connect(m_autosave_timer, SIGNAL(timeout()), this, SIGNAL(autosave_current_state()));
}

void package_sender::start_timers_()
{
    Q_ASSERT(0 != m_failed_package_checker);
    Q_ASSERT(0 != m_autosave_timer);
    m_failed_package_checker->setInterval(k_default_interval_for_failures);
    m_autosave_timer->setInterval(k_autosave_timer_interval);
    m_failed_package_checker->start();
    m_autosave_timer->start();
}

void package_sender::set_connection_manager(const QSharedPointer<connection_manager> &cm)
{
    Q_ASSERT(0 != cm);
    m_connection_manager = cm;
}

void package_sender::send_package(const QSharedPointer<package> &p)
{
    Q_ASSERT(0 != p);
    if (p->protocol() & (quint16)package::TCP) {
        send_package_tcp_(p);
    } // don't use else, cause package may be sent via both TCP and UDP
    if (p->protocol() & (quint16)package::UDP) {
        send_package_udp_(p);
    }
    if (p->protocol() & (quint16)package::UNKNOWN_PROTOCOL) {
        // do nothing until a new protocol is not invented by kokain developers :P
    }
}

void package_sender::send_datagram(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != d);
    register_datagram_(d); // do we need this?
    if (datagram::ACK == d->type() || datagram::FIN == d->type()) {
        send_special_datagram_(d);
    } else {
        m_dgrams_queue.push(d);
    }
    d->set_status(datagram::IN_PROGRESS);
}

void package_sender::send_package_tcp_(const QSharedPointer<package> &p)
{
    Q_ASSERT(0 != p);
    QSharedPointer<connection> con = p->target_connection();
    Q_ASSERT(0 != con);
    if (NULL == con->tcp_socket()) {
        // review tcp_socket's parent, currently set this
        con->set_tcp_socket(QPointer<QTcpSocket>(new QTcpSocket(this)));
    }
    QPointer<QTcpSocket> tcp_sock = con->tcp_socket();
    tcp_sock->connectToHost(con->host_address(), con->port_number());
    if (!tcp_sock->waitForConnected(k_default_interval_for_failures)) {
        emit sender_package_not_sent(p);
        return;
    }
    tcp_sock->write(p->to_byte_array());
    if (p->type() != package::REQUEST_PACKAGE || p->type() != package::VERIFY_PACKAGE)
    {
        tcp_sock->disconnectFromHost();
    }
}

void package_sender::send_package_udp_(const QSharedPointer<package> &p)
{
    Q_ASSERT(0 != p);
    m_sending_packages[p->checksum()] = p;
    package::datagrams_map_t dgrams = p->to_datagrams();
    package::datagrams_map_t::iterator it = dgrams.begin();
    for ( ; it != dgrams.end(); ++it) {
        send_datagram(it->second);
    }
}

void package_sender::send_special_datagram_(const QSharedPointer<datagram> &sd)
{
    QPointer<QTimer> tm(new QTimer(sd.data()));
    tm->setInterval(k_default_interval_for_failures);
    sd->set_timer(tm);
    connect(sd.data(), SIGNAL(send_me(QSharedPointer<datagram>)), this,
            SLOT(on_udp_special_datagram_timeout(QSharedPointer<datagram>))) ;
    m_special_dgrams[sd->checksum()].push_back(sd);
    send_datagram_private_(sd);
    sd->timer()->start();
}

void package_sender::send_datagram_private_(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != d);
    QByteArray ba = d->to_byte_array();
    QSharedPointer<connection> con = d->target_connection();
    if (NULL == con) {
        Q_ASSERT(0 != m_connection_manager);
        con = m_connection_manager->get_connection(d->checksum());
    }
    if (NULL == con) {
        con.reset(new connection());
        con->set_broadcast(true);
    }
    Q_ASSERT(0 != m_sending_udp_socket);
    m_sending_udp_socket->writeDatagram(ba, con->host_address(), con->port_number());
    d->set_status(datagram::SENT);
}

void package_sender::register_datagram_(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != d);
    std::map<qint32, bool> tmp = m_registered_datagrams[d->checksum()];
    tmp[d->sequence_number()] = true;
    m_registered_datagrams[d->checksum()] = tmp;
}

void package_sender::save_package_to_try_again_(const QByteArray &chk)
{
    QSharedPointer<package> p = m_sending_packages[chk];
    Q_ASSERT(0 != p);
    m_failed_packages.push_back(p);
    save_failed_packages_();
    m_sending_packages.erase(chk);
}

void package_sender::save_failed_packages_()
{
    // insert into db m_failed_packages
}

void package_sender::on_udp_final_datagram_accepted(const QSharedPointer<datagram>& d)
{
    // TO-CHALLENGE-ACCEPTED: refactor this code
    Q_ASSERT(0 != d);
    QSharedPointer<datagram> fin(
                new datagram(d->parent_package(), datagram::FIN, d->sequence_number(), d->checksum()));
    stop_and_remove_from_special_dgrams_(fin);
    // if not returned (see above)
    QSharedPointer<datagram> accepted(NULL);
    typedef QSharedPointer<package> package_ptr;
    for (std::vector<package_ptr>::iterator it = m_failed_packages.begin(); it != m_failed_packages.end(); ++it)
    {
        package_ptr pack = *it;
        if (pack->checksum() == d->checksum()) {
            accepted = pack->get_datagram_by(d->sequence_number(), datagram::FIN);
            if (accepted != NULL) {
                accepted->timer()->stop();
                accepted->set_status(datagram::SENT);
                if (pack->is_complete()) {
                    m_sending_packages[pack->checksum()] = pack;
                    m_failed_packages.erase(it);
                    break;
                }
            }
        }
    }
}

void package_sender::on_udp_package_receive_arrived(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != d);
    QSharedPointer<datagram> ack(new datagram(m_sending_packages[d->checksum()], datagram::ACK, -1, d->checksum()));
    m_sending_packages.erase(d->checksum());
    m_special_dgrams.erase(d->checksum());
    send_datagram(ack);
}

void package_sender::udp_send_allowed()
{
    QSharedPointer<datagram> dg = m_dgrams_queue.top();
    send_datagram_private_(dg);
    m_dgrams_queue.pop();
}

void package_sender::on_udp_datagram_required(const QSharedPointer<datagram>& d)
{
    Q_ASSERT(0 != d);
    QSharedPointer<package> pack = m_sending_packages[d->checksum()];
    QSharedPointer<datagram> req = pack->get_datagram_by(d->sequence_number(), datagram::DATA);
    if (req != NULL) {
        send_datagram_private_(req);
    }
}

void package_sender::on_udp_special_datagram_timeout(const QSharedPointer<datagram>& d)
{
    Q_ASSERT(0 != d);
    qint32 ts = k_default_interval_for_failures;
    ts *= (ts * ts);
    if (d->timer()->interval() >= ts) {
        stop_and_remove_from_special_dgrams_(d);
        if (d->type() != datagram::ACK && d->number() != -1) {
            save_package_to_try_again_(d->checksum());
        }
    } else {
        d->timer()->setInterval(d->timer()->interval() * k_default_interval_for_failures);
        for (qint32 i = 0; i < d->sending_count(); ++i) {
            send_datagram_private_(d);
        }
    }
}

void package_sender::stop_and_remove_from_special_dgrams_(const QSharedPointer<datagram> &d)
{
    Q_ASSERT(0 != d);
    typedef std::vector<QSharedPointer<datagram> > dgrams_vec_t;
    dgrams_vec_t vec = m_special_dgrams[d->checksum()];
    for (dgrams_vec_t::iterator it = vec.begin(); it != vec.end(); ++it) {
        QSharedPointer<datagram> tmp = *it;
        if (tmp->sequence_number() == d->sequence_number() && tmp->type() == d->type()) {
            QPointer<QTimer> tm = tmp->timer();
            if (tm) { tm->stop(); }
            vec.erase(it);
            m_special_dgrams[d->checksum()] = vec;
            break;
        }
    }
}

void package_sender::send_datagram_vector(const std::vector<QSharedPointer<datagram> > &dvec)
{
    std::vector<QSharedPointer<datagram> >::const_iterator it = dvec.begin();
    for ( ; it != dvec.end(); ++it) {
        send_datagram(*it);
    }
}

void package_sender::on_failed_packages_try_again()
{
    load_failed_packages_();
    std::vector<QSharedPointer<package> >::iterator it = m_failed_packages.begin();
    for ( ; it != m_failed_packages.end(); ++it) {
        QSharedPointer<package> pack = *it;
        m_sending_packages.insert(std::make_pair(pack->checksum(), pack));
        send_datagram_vector(pack->get_datagrams_by_status(datagram::NOT_SENT));
    }
    m_failed_packages.clear();
    update_failed_packages_table_();
}

void package_sender::load_failed_packages_()
{
    // integrate to db after db_manager completed
}

void package_sender::update_failed_packages_table_()
{
    // you know what
}

} // namespace network

} // namespace kokain
