#ifndef KOKAIN_NETWORK_PACKAGE_H
#define KOKAIN_NETWORK_PACKAGE_H

#include <QString>
#include <QSharedPointer>
#include <QDateTime>

#include "../core/user.h"
#include "connection.h"
#include "datagram.h"

namespace kokain
{

namespace network
{

struct entity_type_t
{
    /// @todo Should we move this to core::entity?
    enum type
    {
        PROJECT,
        TASK,
        CALENDAR,
        EVENT,
        USER,
        NOTIFICATION,
        ANALYTICS,
        STATISTICS,
        ACTIVITY,
        OTHER,
        entity_type_max
    };

    static QString value_for_entity_type(type t)
    {
        initialize_entity_type_map_();
        return s_entity_type_to_string[t];
    }

private:
    static void initialize_entity_type_map_()
    {
        if (s_entity_map_initialized) {
            return;
        }
        s_entity_map_initialized = true;
        s_entity_type_to_string[PROJECT] = "project";
        s_entity_type_to_string[TASK] = "task";
        s_entity_type_to_string[CALENDAR] = "calendar";
        s_entity_type_to_string[EVENT] = "event";
        s_entity_type_to_string[USER] = "user";
        s_entity_type_to_string[NOTIFICATION] = "notification";
        s_entity_type_to_string[ANALYTICS] = "analytics";
        s_entity_type_to_string[STATISTICS] = "statistics";
        s_entity_type_to_string[ACTIVITY] = "activity";
        s_entity_type_to_string[OTHER] = "other";
    }

    static std::map<type, QString> s_entity_type_to_string;
    static bool s_entity_map_initialized;
};

/**
 * @class package
 * @brief Represents logical block, required by @ref network_manager to send and receive
 * data on higher level. For UDP connections the package is being dropped into datagrams (@ref datagram),
 * for TCP connections package is being converted into one stream of QByteArray.
 *
 * Structure (significant headers):
 *  - from, to : user* (source and target users, their 4 byte length IDs)
 *  - checksum : QByteArray (SHA-1 unique checksum, based on source and target users' IDs and current
 *               datetime with milliseconds) (20 bytes, fixed)
 *  - type : package_type
 *              (INSERT - by receiving insert packages, client should perform 'insert' operation
 *                        into its local database,
 *               UPDATE, REMOVE - same as insert (these 3 operations are being processed by synchronizer),
 *               REQUEST - requests an entity or its content from current client,
 *               VERIFY - sends entity content to verify (compare) with current client's entity
 *               IGNORE - mostly used for keep-alive packages,
 *               REPLY - reply package for REQUEST or VERIFY,
 *               UNKNOWN - extending transfer options for further releases)
 *  - entity_type : type of the content (either project, task, event, etc. )
 *  - creation_datetime, sent_datetime : for low-level and scheduling purposes
 *  - data : JSON string, containing entity and/or protocol-related info
 */
class package
{
public:
    enum package_type
    {
        INSERT_PACKAGE = 2,
        UPDATE_PACKAGE = 4,
        REMOVE_PACKAGE = 8,
        REQUEST_PACKAGE = 16,
        VERIFY_PACKAGE = 32,
        REPLY_PACKAGE = 64,
        IGNORE_PACKAGE = 128,
        UNKNOWN_PACKAGE = 256,
        package_type_max
    };

    enum transfer_protocol
    {
        UDP = 2,
        TCP = 4,
        UNKNOWN_PROTOCOL = 8,
        transfer_protocol_max
    };

public:
    package();
    explicit package(const QByteArray& fromba);

private:
    void initialize_();

    /// @brief Source and target users (from, to)
public:
    QSharedPointer<core::user> from();
    void set_from(const QSharedPointer<core::user>& u);
    QSharedPointer<core::user> to();
    void set_to(const QSharedPointer<core::user>& u);

private:
    QSharedPointer<core::user> m_from;
    QSharedPointer<core::user> m_to;

    /// @brief Checksum and datetimes
public:
    QByteArray checksum() const;
    void set_checksum(const QByteArray& ch);
    void regenerate_checksum();

    QDateTime creation_datetime() const;
    QDateTime sent_datetime() const;
    void set_sent_datetime(const QDateTime& dt);
    QDateTime received_datetime() const;
    void set_received_datetime(const QDateTime& dt);

private:
    QByteArray m_checksum;
    QDateTime m_creation_datetime;
    QDateTime m_sent_datetime;
    QDateTime m_received_datetime;

    /// @brief Types (package type and entiy type) and priority
public:
    package_type type() const;
    void set_type(package_type t);
    entity_type_t::type entity_type() const;
    void set_entity_type(entity_type_t::type et);
    datagram::datagram_priority priority() const;
    void set_priority(datagram::datagram_priority p);

private:
    package_type m_type;
    entity_type_t::type m_entity_type;
    datagram::datagram_priority m_priority;

    /// @brief Data
public:
    QString data() const;
    void set_data(const QString& d);
    QByteArray data_byte_array() const;

private:
    QString m_data;

    /// @brief Protocol
    /// Could be either UDP or TCP (just in case added UNKNOWN for further possible changes)
    /// Protocol represented as unsigned short to be able to transfer with both UDP and TCP
    /// at the same time (just setting &= UDP, &= TCP)
public:
    quint16 protocol() const;
    void set_protocol(quint16 t);

private:
    quint16 m_protocol;

    /// @brief Target connection (mostly used for TCP connections)
public:
    QSharedPointer<connection> target_connection();
    void set_target_connection(const QSharedPointer<connection>& tc);

private:
    QSharedPointer<connection> m_target_connection;

    /// @brief Datagrams-related stuff
public:
    bool is_complete();
    QDateTime last_datagram_added_datetime() const;

    qint32 datagrams_quantity() const;
    qint32 datagrams_missing_quantity() const;
    qint32 datagrams_max_sequence_number() const;
    qreal datagrams_missing_percentage() const;

    bool is_final_datagram_missing() const;
    QSharedPointer<datagram> final_datagram();

    std::vector<qint32> missing_datagram_numbers();
    std::vector<QSharedPointer<datagram> > missing_datagrams();
    QSharedPointer<datagram> get_datagram_by(qint32 seqnum, datagram::datagram_type t);
    std::vector<QSharedPointer<datagram> > get_datagrams_by_status(datagram::datagram_status s);

    typedef std::map<qint32, QSharedPointer<datagram> > datagrams_map_t;
    datagrams_map_t to_datagrams();

    void add_datagram(const QSharedPointer<datagram>& dg);
    void remove_datagram(const QSharedPointer<datagram>& dg);

private:
    void construct_package_from_datagrams_();

private:
    bool m_is_complete;
    QDateTime m_last_datagram_added;

    datagrams_map_t m_datagrams;
    std::vector<QSharedPointer<datagram> > m_missing_datagrams;
    std::vector<qint32> m_missing_datagram_numbers;
    QSharedPointer<datagram> m_final_datagram;

    /// @brief ByteArray, used for TCP connections (mostly)
    /// represents the whole package in a single byte array.
    /// However, the content is not being parsed (e.g. headers are placed sequentially)
    /// Only the actual data should be parsed, because contains JSON data.
    ///
    /// Each to_byte_array() call processes the whole package, however, every time the result
    /// is being saved, so if no changes made to the package, you should call last_processed_byte_array()
    /// to avoid complex operation.
public:
    QByteArray to_byte_array();
    QByteArray last_processed_byte_array() const;
    void from_byte_array(const QByteArray& ba);

private:
    QByteArray m_last_processed;

    /// @brief Operators
public:
    bool operator<(const package& p);
    bool operator==(const package& p);
    package& operator+=(const QSharedPointer<datagram> &d);

private:
    const static qint32 k_checksum_fixed_length;
    const static QString k_datetime_format_mseconds;

private:
    package(const package&);
    package& operator=(const package&);

}; // class package

} // namespace network

} // namespace kokain



#endif // KOKAIN_NETWORK_PACKAGE_H
