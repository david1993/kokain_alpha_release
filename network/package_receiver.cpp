#include "package_receiver.h"

#include "../db/db_manager.h"
#include "../db/synchronizer.h"
#include "network_manager.h"
#include "datagram.h"
#include "package.h"
#include "connection.h"

namespace kokain
{

namespace network
{

QSharedPointer<package_receiver> package_receiver::s_instance(NULL);
const qint32 package_receiver::k_package_checker_time_interval = 500;
const qreal package_receiver::k_lost_signaling_percentage = 5.0;
const qint64 package_receiver::k_lost_signaling_timespan = 30;

package_receiver::package_receiver(network_manager* nm, QObject *parent) :
    QObject(parent),
    m_network_manager(nm),
    m_db_manager(NULL),
    m_synchronizer(NULL),
    m_package_checker_timer(NULL)
{
    Q_ASSERT(0 != m_network_manager);
    initialize_();
}

void package_receiver::initialize_()
{
    m_package_checker_timer = new QTimer(this);
    m_package_checker_timer->setInterval(k_package_checker_time_interval);
    m_synchronizer = db::synchronizer::get_instance();
    Q_ASSERT(0 != m_synchronizer);
    load_from_db_();
    make_connections_();
    start_timers_();
}

void package_receiver::load_from_db_()
{
    m_db_manager = db::db_manager::get_instance();
    /// @todo loading logic after db_manager completed
}

void package_receiver::make_connections_()
{
    connect(m_package_checker_timer, SIGNAL(timeout()), this, SIGNAL(check_packages()));
    connect(this, SIGNAL(package_received(QByteArray)), this, SLOT(on_package_received(QByteArray)));
    connect(this, SIGNAL(udp_require_lost_datagrams(QByteArray)), this, SLOT(on_udp_require_lost_datagram(QByteArray)));
    connect(this, SIGNAL(udp_final_datagram_arrived(datagram*)), this, SLOT(on_udp_final_datagram_arrived(datagram*)));
    connect(this, SIGNAL(udp_package_receive_accepted(datagram*)), this, SLOT(on_udp_package_receive_accepted(datagram*)));
}

void package_receiver::start_timers_()
{
    Q_ASSERT(0 != m_package_checker_timer);
    m_package_checker_timer->start();
}

void package_receiver::process_datagram(const QSharedPointer<datagram>& d)
{
    Q_ASSERT(0 != d);
    switch (d->type())
    {
    case datagram::ACK:
    {
        if (-1 != d->number()) {
            emit udp_final_datagram_accepted(d);
        } else {
            emit udp_package_receive_accepted(d);
        }
        break;
    }
    case datagram::FIN:
    {
        if (-1 != d->number()) {
            emit udp_final_datagram_arrived(d);
        } else {
            emit udp_package_receive_arrived(d);
        }
        break;
    }
    case datagram::REQ:
    {
        emit udp_datagram_required(d);
        break;
    }
    case datagram::DATA:
    {
        if (!m_packages[d->checksum()]) {
            m_packages[d->checksum()].reset(new package());
            m_packages[d->checksum()]->set_checksum(d->checksum());
        }
        m_packages[d->checksum()]->add_datagram(QSharedPointer<datagram>(d));
        break;
    }
    default:
        emit udp_unknown_datagram_received(d);
    }
}

void package_receiver::process_ready_package(const QSharedPointer<package>& p)
{
    Q_ASSERT(0 != p);
    Q_ASSERT(0 != m_synchronizer);
    QSharedPointer<package> rep = m_synchronizer->process_incoming_package(p);
    if (NULL != rep) {
        rep->set_target_connection(p->target_connection());
        rep->set_protocol(p->protocol());
        reply_for_package_(p, rep);
    }
}

void package_receiver::reply_for_package_(const QSharedPointer<package> p, const QSharedPointer<package> rep)
{
    Q_ASSERT(0 != p);
    Q_ASSERT(0 != rep);
    Q_ASSERT(0 != m_network_manager);
    m_network_manager->send_data(rep);
    /// @todo get back here after network_manager completed
}

// this works only for UDP, however TCP packages are not inserted into m_packages
// so everything is cool, and morally very high
void package_receiver::verify_packages()
{
    std::map<QByteArray, QSharedPointer<package> >::const_iterator cit = m_packages.begin();
    for ( ; cit != m_packages.end(); ++cit) {
        const QSharedPointer<package> p = cit->second;
        if (p->is_complete()) {
            emit package_received(p->checksum());
        } else if (check_if_need_to_panic_(p) ){
            emit udp_require_lost_datagrams(p->checksum());
        }
    }
}

bool package_receiver::check_if_need_to_panic_(const QSharedPointer<package>& p)
{
    Q_ASSERT(0 != p);
    qint64 timediff = p->last_datagram_added_datetime().secsTo(
                QDateTime::currentDateTime());
    return (p->datagrams_missing_percentage() < k_lost_signaling_percentage
            || p->is_final_datagram_missing()
            || (timediff >= k_lost_signaling_timespan) );
}

void package_receiver::on_package_received(const QByteArray& chk)
{
    Q_ASSERT(!chk.isEmpty());
    m_received_table.insert(chk);
    QSharedPointer<package> p = m_packages[chk];
    QSharedPointer<datagram> d(new datagram());
    d->set_checksum(chk);
    d->set_parent_package(p);
    d->set_priority(datagram::HIGH_PRIORITY);
    d->set_sequence_number(-1);
    d->set_target_connection(p->target_connection());
    d->set_type(datagram::FIN);
    m_network_manager->send_datagram(d);

    process_ready_package(p);
    m_packages.erase(chk);
}

void package_receiver::on_autosave_current_state()
{
    // right after db_manager completed
}

void package_receiver::on_tcp_package_receive(const QSharedPointer<connection>& c)
{
    Q_ASSERT(0 != c);
    QPointer<QTcpSocket> tcp_socket = c->tcp_socket();
    QByteArray data;
    QDataStream reader(tcp_socket.data());
    reader >> data;
    QSharedPointer<package> p(new package());
    p->set_target_connection(c);
    p->set_protocol(package::TCP);

    process_ready_package(p);
}

void package_receiver::on_udp_package_receive_accepted(const QSharedPointer<datagram>& d)
{
    m_received_table.erase(d->checksum());
}

void package_receiver::on_udp_require_lost_datagram(const QByteArray& chk)
{
    Q_ASSERT(!chk.isEmpty());
    QSharedPointer<package> p = m_packages[chk];
    if (NULL != p) {
        typedef std::vector<qint32> numvec_t;
        numvec_t missings = p->missing_datagram_numbers();
        numvec_t::iterator it = missings.begin();
        for ( ; it != missings.end(); ++it) {
            // assume network_manager operates with sharedpointers
            datagram* d = new datagram();
            d->set_checksum(chk);
            d->set_type(datagram::REQ);
            d->set_sequence_number(*it);
            m_network_manager->send_datagram(QSharedPointer<datagram>(d));
        }
    }
}

void package_receiver::on_udp_final_datagram_arrived(const QSharedPointer<datagram>& fin)
{
    datagram* ack = new datagram();
    ack->set_type(datagram::ACK);
    ack->set_sequence_number(fin->sequence_number());
    ack->set_checksum(fin->checksum());
    m_network_manager->send_datagram(QSharedPointer<datagram>(ack));
    if (!m_packages[fin->checksum()]) {
        m_packages[fin->checksum()].reset(new package());
        m_packages[fin->checksum()]->set_checksum(fin->checksum());
    }
    m_packages[fin->checksum()]->add_datagram(QSharedPointer<datagram>(fin));
}

QSharedPointer<package_receiver> package_receiver::get_instance(network_manager *nm, QObject *p)
{
    if (NULL == s_instance) {
        s_instance.reset(new package_receiver(nm, p));
    }
    return s_instance;
}

} // namespace network

} // namespace kokain
