#include "connection.h"

namespace kokain
{

namespace network
{

bool operator==(const connection& a, const connection& b)
{
    return (a.host_address() == b.host_address() )
            && ( a.port_number() == b.port_number() )
            && ( a.target_user() == b.target_user() );
}

const quint16 connection::k_connection_default_port = 4321;

connection::connection(QObject *p)
    : QObject(p),
      m_target_user(NULL),
      m_host_address(QHostAddress::LocalHost),
      m_port_number(k_connection_default_port),
      m_tcp_socket(NULL),
      m_is_broadcast(false)
{
    initialize_();
}

void connection::initialize_()
{
    m_tcp_socket = new QTcpSocket(this);
    make_connections_();
}

void connection::make_connections_()
{
    connect(m_tcp_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(on_tcp_socket_error(QAbstractSocket::SocketError)));
    connect(m_tcp_socket, SIGNAL(readyRead()), this, SLOT(on_tcp_socket_ready_read()));
    connect(m_tcp_socket, SIGNAL(disconnected()), m_tcp_socket, SLOT(deleteLater()));
}

void connection::on_tcp_socket_error(QAbstractSocket::SocketError er)
{
    // do some handling if needed
    emit tcp_socket_error(er);
}

void connection::on_tcp_socket_ready_read()
{
    emit tcp_ready_read(QSharedPointer<connection>(this));
}

const QSharedPointer<core::user> connection::target_user() const
{
    return m_target_user;
}

void connection::set_target_user(const QSharedPointer<core::user> &tu)
{
    Q_ASSERT(0 != tu);
    m_target_user = tu;
}

QHostAddress connection::host_address() const
{
    return m_host_address;
}

void connection::set_host_address(const QHostAddress &ha)
{
    m_host_address = ha;
}

quint16 connection::port_number() const
{
    return m_port_number;
}

void connection::set_port_number(quint16 pn)
{
    m_port_number = pn;
}

void connection::set_tcp_socket(QPointer<QTcpSocket> sock)
{
    Q_ASSERT(0 != sock);
    m_tcp_socket = sock;
}

QPointer<QTcpSocket> connection::tcp_socket()
{
    return m_tcp_socket;
}

void connection::set_broadcast(bool b)
{
    m_is_broadcast = b;
    if (m_is_broadcast) {
        m_host_address = QHostAddress::Broadcast;
    }
}

bool connection::is_broadcast() const
{
    return m_is_broadcast;
}

void connection::register_processed_package(const QByteArray &chk)
{
    m_processed_packages.insert(chk);
}

bool connection::is_package_processed(const QByteArray &chk)
{
    return ( m_processed_packages.find(chk) != m_processed_packages.end() );
}

} // namespace network

} // namespace kokain
