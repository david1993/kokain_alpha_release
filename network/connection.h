#ifndef KOKAIN_NETWORK_CONNECTION_H
#define KOKAIN_NETWORK_CONNECTION_H

#include <set>

#include <QObject>
#include <QPointer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDateTime>

namespace kokain
{

namespace core
{

class user;

}

namespace network
{

class connection : public QObject
{
    Q_OBJECT
public:
    explicit connection(QObject* p = 0);

private:
    void initialize_();
    void make_connections_();

public:
    const QSharedPointer<core::user> target_user() const;
    void set_target_user(const QSharedPointer<core::user>& tu);

private:
    QSharedPointer<core::user> m_target_user;

public:
    QHostAddress host_address() const;
    void set_host_address(const QHostAddress& ha);
    quint16 port_number() const;
    void set_port_number(quint16 pn);

private:
    QHostAddress m_host_address;
    quint16 m_port_number;

public:
    void set_tcp_socket(QPointer<QTcpSocket> sock);
    QPointer<QTcpSocket> tcp_socket();

private:
    QPointer<QTcpSocket> m_tcp_socket;

private slots:
    void on_tcp_socket_error(QAbstractSocket::SocketError);
    void on_tcp_socket_ready_read();

signals:
    void tcp_ready_read(const QSharedPointer<connection>&);
    void tcp_socket_error(QAbstractSocket::SocketError);

public:
    void set_broadcast(bool);
    bool is_broadcast() const;

private:
    bool m_is_broadcast;

public:
    bool is_active() const;
    QDateTime last_active() const;
    void set_last_active(const QDateTime& dt);

private:
    QDateTime m_last_active;

public:
    void register_processed_package(const QByteArray&);
    bool is_package_processed(const QByteArray&);

private:
    std::set<QByteArray> m_processed_packages;

private:
    const static quint16 k_connection_default_port;

private:
    connection(const connection&);
    connection& operator=(const connection&);

}; // class connection

} // namespace network

} // namespace kokain

#endif // CONNECTION_H
