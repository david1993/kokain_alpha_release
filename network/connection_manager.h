#ifndef KOKAIN_NETWORK_CONNECTION_MANAGER_H
#define KOKAIN_NETWORK_CONNECTION_MANAGER_H

#include <QObject>

#include "connection.h"

class QTcpServer;

namespace kokain
{

namespace network
{

class connection_manager : public QObject
{
    Q_OBJECT
private:
    explicit connection_manager(QObject *parent = 0);

private:
    void initialize_();
    void make_connections_();

public:
    void add_connection(const QSharedPointer<connection>& con);
    void remove_connection(const QSharedPointer<connection>& con);
    QSharedPointer<connection> get_connection(const QSharedPointer<core::user>& u);
    QSharedPointer<connection> get_connection(const QHostAddress& ad, quint16 p);
    QSharedPointer<connection> get_connection(const QByteArray& ba);
    std::vector<QSharedPointer<connection> > connections();
    void set_connections(const std::vector<QSharedPointer<connection> >& cv);

private:
    std::vector<QSharedPointer<connection> > m_connections;

public slots:
    void on_new_connection(QTcpServer* serv);

public:
    static QSharedPointer<connection_manager> get_instance(QObject *p = 0);

private:
    static QSharedPointer<connection_manager> s_instance;

private:
    connection_manager(const connection_manager&);
    connection_manager& operator=(const connection_manager&);

}; // class connection_manager

} // namespace network

} // namespace kokain

#endif // KOKAIN_NETWORK_CONNECTION_MANAGER_H
