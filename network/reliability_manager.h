#ifndef KOKAIN_NETWORK_RELIABILITY_MANAGER_H
#define KOKAIN_NETWORK_RELIABILITY_MANAGER_H

#include <QObject>
#include <QAbstractSocket>
#include <QSharedPointer>

namespace kokain
{

namespace network
{

class reliability_manager : public QObject
{
    Q_OBJECT
public:
    enum proxy_issue_value
    {
        AUTH_REQUIRED,
        CONNECTION_REFUSED,
        CONNECTION_CLOSED,
        CONNECTION_TIMEOUT,
        NOT_FOUND,
        PROTOCOL_ERROR,
        proxy_issue_value_max
    };

    enum ssl_issue_value
    {
        HANDSHAKE_FAILED,
        INTERNAL_ERROR,
        INVALID_USER_DATA,
        ssl_issue_value_max
    };

    enum socket_error_value
    {
        ACCESS_ERROR,
        RESOURCE_ERROR,
        TIMEOUT_ERROR,
        UNSUPPORTED_OPERATION_ERROR,
        UNFINISHED_OPERATION,
        UNKNOWN_ERROR,
        socker_error_value_max
    };

    enum udp_issue_value
    {
        DATAGRAM_TOO_LARGE,
        ADDRESS_IN_USE,
        ADDRESS_NOT_AVAILABLE,
        udp_issue_value_max
    };

private:
    explicit reliability_manager(QObject *parent = 0);

public slots:
    void on_package_sender_tcp_error(QAbstractSocket::SocketError);
    void on_package_sender_udp_error(QAbstractSocket::SocketError);
    void on_package_receiver_tcp_error(QAbstractSocket::SocketError);
    void on_package_receiver_udp_error(QAbstractSocket::SocketError);

signals:
    void save_sender_packages();
    void save_receiver_packages();
    void no_connection();
    void host_not_found();
    void connection_refused();
    void socket_issue(socket_error_value);
    void proxy_issue(proxy_issue_value);
    void ssl_issue(ssl_issue_value);
    void udp_issue(udp_issue_value);

public:
    static QSharedPointer<reliability_manager> get_instance(QObject* p = 0);

private:
    static QSharedPointer<reliability_manager> s_instance;

private:
    reliability_manager(const reliability_manager&);
    reliability_manager& operator=(const reliability_manager&);

}; // class reliability_manager

} // namespace network

} // namespace kokain

#endif // KOKAIN_NETWORK_RELIABILITY_MANAGER_H
