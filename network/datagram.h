#ifndef KOKAIN_NETWORK_DATAGRAM_H
#define KOKAIN_NETWORK_DATAGRAM_H

#include <QObject>
#include <QSharedPointer>
#include <QTimer>
#include <QPointer>

namespace kokain
{

namespace network
{

class package;
class connection;

/**
 * @class datagram
 * @brief Represents the minumum portion of transferable data (via UDP)
 * Used by UDP data receiver and sender (@ref package_sender, @ref package_receiver)
 * Maximum size of a datagram = 256 bytes
 * Structure:
 *  - protocol_id (4 bytes) : qint32
 *  - checksum (20 bytes) : QByteArray SHA-1
 *  - type (2 bytes) : quint16
 *  - sequence_number (4 bytes) : qint32
 *  - data (226 bytes) : QByteArray
 *
 * Operators: <, ==
 * Signals: send_me(QByteArray)
 */
class datagram : public QObject
{
    Q_OBJECT
public:
    enum datagram_type
    {
        DATA = 2,
        ACK = 4,
        REQ = 8,
        FIN = 16,
        datagram_type_max
    };

    enum datagram_status
    {
        NOT_SENT,
        IN_PROGRESS,
        SENT,
        datagram_status_max
    };

    enum datagram_priority
    {
        LOWEST_PRIORITY,
        LOW_PRIORITY,
        MIDDLE_PRIORITY,
        HIGH_PRIORITY,
        HIGHEST_PRIORITY,
        package_priority_max
    };

public:
    explicit datagram(QObject *parent = 0);
    explicit datagram(const QSharedPointer<package>& p, QObject* parent = 0);
    datagram(const QSharedPointer<package>& p, datagram_type t, qint32 sn, const QByteArray& d,
             QObject* parent = 0);

private:
    void initialize_from_parent_package_(const QSharedPointer<package>& parent);

public:
    qint32 protocol_id() const;
    QSharedPointer<package> parent_package();
    void set_parent_package(const QSharedPointer<package> &p);

private:
    qint32 m_protocol_id;
    QSharedPointer<package> m_parent_package;

    /// @brief Inherited (the value) by parent @ref package class.
    /// Uniquely identifes a datagram in package context by combining
    /// checksum with datagram sequence number.
public:
    QByteArray checksum() const;
    void set_checksum(const QByteArray&);

private:
    QByteArray m_checksum;

    /// @brief Datagram type and sequence number
    /// DATA = plain data,
    /// ACK = acknowledgement for received data (ACK with SEQUENCE NUMBER >= 0 means acknowledgement
    /// for received final (FIN, N) datagram, ACK with SEQUENCE NUMBER == -1 means acceptance for received
    /// complete package.
    /// FIN = finish (if SEQUENCE NUMBER >= 0, FIN identifies final datagram for the sent package,
    ///     if SEQUENCE NUMBER == -1, FIN means "package received successfully").
    /// REQ = require, client may require any datagram by sending REQ datagrams.
public:
    datagram_type type() const;
    void set_type(datagram_type dt);
    qint32 sequence_number() const;
    qint32 number() const;
    void set_sequence_number(qint32);
    void set_status(datagram_status s);
    datagram_status status() const;

private:
    datagram_type m_type;
    qint32 m_sequence_number;
    datagram_status m_status;

    /// @note It's package's responsibility to set data no more than 226 bytes.
    /// set_data trunkates extra data
public:
    QByteArray data() const;
    void set_data(const QByteArray&);

    /// @brief to_byte_array() returns the whole datagram in QByteArray
    /// whole datagram == headers(protocol_id, checksum, type, sequence_number) and data.
    QByteArray to_byte_array() const;

    /// @brief from_byte_array() asserts on input byte array's size to be 256 bytes.
    void from_byte_array(const QByteArray& ba);

private:
    QByteArray m_data;

    /// @brief Priority used in @ref package_sender priority_queue (for sendable datagrams)
    /// By default the value is inherited from parent package (however could be changed manually).
    /// Target connection is also inherited from parent package, identifies receiver for this datagram.
public:
    datagram_priority priority() const;
    void set_priority(datagram_priority p);
    QSharedPointer<connection> target_connection();
    void set_target_connection(const QSharedPointer<connection>& c);

private:
    datagram_priority m_priority;
    QSharedPointer<connection> m_target_connection;

    /// @brief Special case, timer, sending_count and send_me signal are being used
    /// only when the datagram is marked as special, e.g. when its type is either ACK or FIN
    /// (these kind of datagrams should be treated as special to increase transfer reliability)
public:
    QPointer<QTimer> timer();
    void set_timer(const QPointer<QTimer>& t);
    qint32 sending_count() const;
    void set_sending_count(qint32 c);

private:
    QPointer<QTimer> m_timer;
    qint32 m_sending_count;

signals:
    void send_me(QSharedPointer<datagram>);

private slots:
    void on_special_timer_timeout_();

    /// @brief Operators (currently no need to make them global)
    /// These operators need for priority_queue (package_sender), if you need to
    /// compare datagrams, you should call is_equal(datagram*) function.
public:
    bool operator<(const datagram& d);
    bool operator==(const datagram& d);
    bool is_equal(const QSharedPointer<datagram>& d);

public:
    const static qint32 k_data_size_in_bytes;

private:
    const static qint32 k_protocol_id;
    const static qint32 k_default_sending_counts;
    const static qint32 k_header_size_in_bytes;
    const static qint32 k_fixed_datagram_size;

private:
    datagram(const datagram&);
    datagram& operator=(const datagram&);

}; // class datagram

} // namespace network

} // namespace kokain

#endif // KOKAIN_NETWORK_DATAGRAM_H
