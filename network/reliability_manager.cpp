#include "reliability_manager.h"

namespace kokain
{

namespace network
{

QSharedPointer<reliability_manager> reliability_manager::s_instance(NULL);

reliability_manager::reliability_manager(QObject *parent) :
    QObject(parent)
{
}

QSharedPointer<reliability_manager> reliability_manager::get_instance(QObject* p)
{
    if (s_instance != NULL) {
        s_instance.reset(new reliability_manager(p));
    }
    return s_instance;
}

void reliability_manager::on_package_sender_tcp_error(QAbstractSocket::SocketError er)
{
    switch (er) {
    case QAbstractSocket::ConnectionRefusedError:
        emit connection_refused();
        break;
    case QAbstractSocket::RemoteHostClosedError:
        emit no_connection();
        break;
    case QAbstractSocket::HostNotFoundError:
        emit host_not_found();
        break;
    case QAbstractSocket::SocketAccessError:
        emit socket_issue(ACCESS_ERROR);
        break;
    case QAbstractSocket::SocketResourceError:
        emit socket_issue(RESOURCE_ERROR);
        break;
    case QAbstractSocket::SocketTimeoutError:
        emit socket_issue(TIMEOUT_ERROR);
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        emit socket_issue(UNSUPPORTED_OPERATION_ERROR);
        break;
    case QAbstractSocket::UnfinishedSocketOperationError:
        emit socket_issue(UNFINISHED_OPERATION);
        break;
    case QAbstractSocket::UnknownSocketError:
        emit socket_issue(UNKNOWN_ERROR);
        break;
    case QAbstractSocket::NetworkError:
        emit no_connection();
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        emit proxy_issue(AUTH_REQUIRED);
        break;
    case QAbstractSocket::ProxyConnectionClosedError:
        emit proxy_issue(CONNECTION_CLOSED);
        break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        emit proxy_issue(CONNECTION_REFUSED);
        break;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        emit proxy_issue_value(TIMEOUT_ERROR);
        break;
    case QAbstractSocket::ProxyNotFoundError:
        emit proxy_issue_value(NOT_FOUND);
        break;
    case QAbstractSocket::ProxyProtocolError:
        emit proxy_issue(PROTOCOL_ERROR);
        break;
    case QAbstractSocket::OperationError:
    case QAbstractSocket::TemporaryError:
        emit no_connection();
        break;
    case QAbstractSocket::SslInternalError:
        emit ssl_issue(INTERNAL_ERROR);
        break;
    case QAbstractSocket::SslHandshakeFailedError:
        emit ssl_issue(HANDSHAKE_FAILED);
        break;
    case QAbstractSocket::SslInvalidUserDataError:
        emit ssl_issue(INVALID_USER_DATA);
        break;
    default:
        break;
    }
}

void reliability_manager::on_package_sender_udp_error(QAbstractSocket::SocketError er)
{
    // don't know if proxy errors may occure for UDP sockets
    switch (er)
    {
    case QAbstractSocket::DatagramTooLargeError:
        emit udp_issue(DATAGRAM_TOO_LARGE);
        break;
    case QAbstractSocket::AddressInUseError:
        emit udp_issue(ADDRESS_IN_USE);
        break;
    case QAbstractSocket::SocketAddressNotAvailableError:
        emit udp_issue(ADDRESS_NOT_AVAILABLE);
        break;
    case QAbstractSocket::ConnectionRefusedError:
        emit connection_refused();
        break;
    case QAbstractSocket::RemoteHostClosedError:
        emit no_connection();
        break;
    case QAbstractSocket::HostNotFoundError:
        emit host_not_found();
        break;
    case QAbstractSocket::SocketAccessError:
        emit socket_issue(ACCESS_ERROR);
        break;
    case QAbstractSocket::SocketResourceError:
        emit socket_issue(RESOURCE_ERROR);
        break;
    case QAbstractSocket::SocketTimeoutError:
        emit socket_issue(TIMEOUT_ERROR);
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        emit socket_issue(UNSUPPORTED_OPERATION_ERROR);
        break;
    case QAbstractSocket::UnknownSocketError:
        emit socket_issue(UNKNOWN_ERROR);
        break;
    case QAbstractSocket::NetworkError:
        emit no_connection();
        break;
    case QAbstractSocket::TemporaryError:
        emit no_connection();
        break;
    default:
        break;
    }
}

void reliability_manager::on_package_receiver_tcp_error(QAbstractSocket::SocketError)
{

}

void reliability_manager::on_package_receiver_udp_error(QAbstractSocket::SocketError)
{

}

} // namespace network

} // namespace kokain
