#include "connection_manager.h"

#include <QTcpServer>

namespace kokain
{

namespace network
{

QSharedPointer<connection_manager> connection_manager::s_instance(NULL);

connection_manager::connection_manager(QObject *parent) :
    QObject(parent)
{
    initialize_();
}

QSharedPointer<connection_manager> connection_manager::get_instance(QObject* p)
{
    if (NULL == s_instance) {
        s_instance.reset(new connection_manager(p));
    }
    return s_instance;
}

QSharedPointer<connection> connection_manager::get_connection(const QByteArray &chk)
{
    std::vector<QSharedPointer<connection> >::iterator it = m_connections.begin();
    for ( ; it != m_connections.end(); ++it) {
        QSharedPointer<connection> tmp = *it;
        if (tmp->is_package_processed(chk)) {
            return tmp;
        }
    }
    return QSharedPointer<connection>(NULL);
}

void connection_manager::initialize_()
{
    make_connections_();
}

void connection_manager::make_connections_()
{

}

void connection_manager::add_connection(const QSharedPointer<connection> &con)
{
    Q_ASSERT(0 != con);
    m_connections.push_back(con);
}

void connection_manager::remove_connection(const QSharedPointer<connection> &con)
{
    Q_ASSERT(0 != con);
    m_connections.erase(remove(m_connections.begin(), m_connections.end(), con),
                        m_connections.end());
}

QSharedPointer<connection> connection_manager::get_connection(const QSharedPointer<core::user> &u)
{
    Q_ASSERT(0 != u);
    std::vector<QSharedPointer<connection> >::iterator it = m_connections.begin();
    for ( ; it != m_connections.end(); ++it) {
        QSharedPointer<connection> tmp = *it;
        if (tmp->target_user() == u) {
            return tmp;
        }
    }
    return QSharedPointer<connection>(NULL);
}

QSharedPointer<connection> connection_manager::get_connection(const QHostAddress &ad, quint16 p)
{
    std::vector<QSharedPointer<connection> >::iterator it = m_connections.begin();
    for ( ; it != m_connections.end(); ++it) {
        QSharedPointer<connection> tmp = *it;
        if (tmp->host_address() == ad && tmp->port_number() == p) {
            return tmp;
        }
    }
    return QSharedPointer<connection>(NULL);
}

std::vector<QSharedPointer<connection> > connection_manager::connections()
{
    return m_connections;
}

void connection_manager::set_connections(const std::vector<QSharedPointer<connection> > &cv)
{
    Q_ASSERT(!cv.empty());
    m_connections = cv;
}

void connection_manager::on_new_connection(QTcpServer *serv)
{
    Q_ASSERT(0 != serv);
    // TODO after network_manager
}

} // namespace network

} // namespace kokain
