#include "package.h"

#include <QCryptographicHash>
#include <QDataStream>

#include "datagram.h"

namespace kokain
{

namespace network
{

// entity_type_t initialization

std::map<entity_type_t::type, QString> entity_type_t::s_entity_type_to_string;
bool entity_type_t::s_entity_map_initialized = false;

// package class

const qint32 package::k_checksum_fixed_length = 20;
const QString package::k_datetime_format_mseconds = "hh:mm:ss.zzz";

package::package()
    : m_from(NULL),
      m_to(NULL),
      m_creation_datetime(QDateTime::currentDateTime()),
      m_type(UNKNOWN_PACKAGE),
      m_entity_type(entity_type_t::OTHER),
      m_priority(datagram::MIDDLE_PRIORITY),
      m_protocol(UDP),
      m_target_connection(NULL),
      m_is_complete(false),
      m_final_datagram(NULL)
{
    initialize_();
}

package::package(const QByteArray &fromba)
    : m_from(NULL),
      m_to(NULL),
      m_creation_datetime(QDateTime::currentDateTime()),
      m_type(UNKNOWN_PACKAGE),
      m_entity_type(entity_type_t::OTHER),
      m_priority(datagram::MIDDLE_PRIORITY),
      m_protocol(UDP),
      m_target_connection(NULL),
      m_is_complete(false),
      m_final_datagram(NULL)
{
    initialize_();
    from_byte_array(fromba);
}

void package::initialize_()
{
    regenerate_checksum();
}

QSharedPointer<core::user> package::from()
{
    return m_from;
}

void package::set_from(const QSharedPointer<core::user> &u)
{
    Q_ASSERT(0 != u);
    m_from = u;
}

QSharedPointer<core::user> package::to()
{
    return m_to;
}

void package::set_to(const QSharedPointer<core::user> &u)
{
    Q_ASSERT(0 != u);
    m_to = u;
}

QByteArray package::checksum() const
{
    return m_checksum;
}

void package::set_checksum(const QByteArray &ch)
{
    Q_ASSERT(ch.size() == k_checksum_fixed_length);
    m_checksum = ch;
}

void package::regenerate_checksum()
{
    Q_ASSERT(0 != m_from);
    Q_ASSERT(m_creation_datetime.isValid());
    QString source = QString::number(m_from->id())
            + m_creation_datetime.toString(k_datetime_format_mseconds)
            + QString::number(qrand());
    m_checksum = QCryptographicHash::hash(source.toLatin1(), QCryptographicHash::Sha1);
}

QDateTime package::creation_datetime() const
{
    return m_creation_datetime;
}

QDateTime package::sent_datetime() const
{
    return m_sent_datetime;
}

void package::set_sent_datetime(const QDateTime &dt)
{
    Q_ASSERT(dt.isValid());
    m_sent_datetime = dt;
}

QDateTime package::received_datetime() const
{
    return m_received_datetime;
}

void package::set_received_datetime(const QDateTime &dt)
{
    m_received_datetime = dt;
}

package::package_type package::type() const
{
    return m_type;
}

void package::set_type(package_type t)
{
    m_type = t;
}

entity_type_t::type package::entity_type() const
{
    return m_entity_type;
}

void package::set_entity_type(entity_type_t::type et)
{
    m_entity_type = et;
}

datagram::datagram_priority package::priority() const
{
    return m_priority;
}

void package::set_priority(datagram::datagram_priority p)
{
    m_priority = p;
}

QString package::data() const
{
    return m_data;
}

void package::set_data(const QString &d)
{
    Q_ASSERT(!d.isEmpty());
    m_data = d;
}

QByteArray package::data_byte_array() const
{
    return m_data.toUtf8();
}

quint16 package::protocol() const
{
    return m_protocol;
}

void package::set_protocol(quint16 t)
{
    m_protocol = t;
}

QSharedPointer<connection> package::target_connection()
{
    return m_target_connection;
}

void package::set_target_connection(const QSharedPointer<connection> &tc)
{
    Q_ASSERT(0 != tc);
    m_target_connection = tc;
}

bool package::is_complete()
{
    if (m_final_datagram == NULL) {
        m_is_complete = false;
    } else {
        m_is_complete = (m_datagrams.size() == (m_final_datagram->sequence_number() + 1));
    }
    return m_is_complete;
}

QDateTime package::last_datagram_added_datetime() const
{
    return m_last_datagram_added;
}

qint32 package::datagrams_quantity() const
{
    return m_datagrams.size();
}

qint32 package::datagrams_missing_quantity() const
{
    return m_missing_datagrams.size();
}

bool package::is_final_datagram_missing() const
{
    return (NULL == m_final_datagram);
}

QSharedPointer<datagram> package::final_datagram()
{
    return m_final_datagram;
}

std::vector<QSharedPointer<datagram> > package::missing_datagrams()
{
    qint32 maxnum = datagrams_max_sequence_number();
    std::map<qint32, bool> dgrams_map;
    for (qint32 i = 0; i <= maxnum; ++i) {
        dgrams_map[i] = false;
    }
    datagrams_map_t::const_iterator it = m_datagrams.begin();
    for ( ; it != m_datagrams.end(); ++it) {
        dgrams_map[it->first] = true;
    }
    m_missing_datagrams.clear();
    for (std::map<qint32, bool>::const_iterator it = dgrams_map.begin();
         it != dgrams_map.end(); ++it)
    {
        if (it->second == false) {
            QSharedPointer<datagram> dg(new datagram());
            dg->set_checksum(this->checksum());
            dg->set_sequence_number(it->first);
            dg->set_priority(this->priority());
            dg->set_parent_package(QSharedPointer<package>(this));
            dg->set_target_connection(this->target_connection());
            m_missing_datagrams.push_back(dg);
        }
    }
    return m_missing_datagrams;
}

std::vector<qint32> package::missing_datagram_numbers()
{
    // it's better to call missing_datagrams() before this call
    for (std::vector<QSharedPointer<datagram> >::const_iterator cit = m_missing_datagrams.begin();
         cit != m_missing_datagrams.end(); ++cit)
    {
        const QSharedPointer<datagram> d = *cit;
        m_missing_datagram_numbers.push_back(d->sequence_number());
    }
    return m_missing_datagram_numbers;
}


qint32 package::datagrams_max_sequence_number() const
{
    datagrams_map_t::const_reverse_iterator rit = m_datagrams.rbegin();
    return rit->first;
}

qreal package::datagrams_missing_percentage() const
{
    qreal max = (qreal)(datagrams_max_sequence_number() + 1);
    qreal miss = (qreal)datagrams_missing_quantity();
    return (100 * miss) / max;
}

package::datagrams_map_t package::to_datagrams()
{
    QByteArray pba = to_byte_array();
    QDataStream reader(&pba, QIODevice::ReadOnly);
    reader.setVersion(QDataStream::Qt_5_0);
    qint32 seqnum = 0;
    while (true)
    {
        QByteArray temp = reader.device()->read(datagram::k_data_size_in_bytes);
        if (temp.isEmpty()) {
            break;
        }
        QSharedPointer<datagram> dg(new datagram());
        dg->set_parent_package(QSharedPointer<package>(this));
        dg->set_priority(this->priority());
        dg->set_checksum(this->checksum());
        dg->set_data(temp);
        dg->set_sequence_number(seqnum);
        dg->set_type(datagram::DATA);
        dg->set_target_connection(this->target_connection());

        m_datagrams[seqnum] = dg;

        ++seqnum;
    }
    m_final_datagram = m_datagrams[seqnum - 1];
    m_final_datagram->set_type(datagram::FIN);

    return m_datagrams;
}

void package::add_datagram(const QSharedPointer<datagram> &dg)
{
    m_last_datagram_added = QDateTime::currentDateTime();
    m_datagrams[dg->sequence_number()] = dg;
    if (is_complete()) {
        construct_package_from_datagrams_();
    }
}

void package::remove_datagram(const QSharedPointer<datagram> &dg)
{
    m_datagrams[dg->sequence_number()].clear();
}

QSharedPointer<datagram> package::get_datagram_by(qint32 seqnum, datagram::datagram_type t)
{
    QSharedPointer<datagram> result = m_datagrams[seqnum];
    if (result != NULL && result->type() == t) {
        return result;
    }
    return QSharedPointer<datagram>(NULL);
}

std::vector<QSharedPointer<datagram> > package::get_datagrams_by_status(datagram::datagram_status status)
{
    std::vector<QSharedPointer<datagram> > vec;
    datagrams_map_t::iterator it = m_datagrams.begin();
    for ( ; it != m_datagrams.end(); ++it) {
        QSharedPointer<datagram> d = it->second;
        if (d->status() == status) {
            vec.push_back(d);
        }
    }
    return vec;
}

void package::construct_package_from_datagrams_()
{
    m_last_processed.clear();
    QDataStream writer(&m_last_processed, QIODevice::WriteOnly);
    writer.setVersion(QDataStream::Qt_5_0);

    datagrams_map_t::const_iterator cit = m_datagrams.begin();
    for ( ; cit != m_datagrams.end(); ++cit) {
        QSharedPointer<datagram> dg = cit->second;
        writer << dg->data();
    }

    this->from_byte_array(m_last_processed);
    this->set_priority(m_datagrams[0]->priority());
}

QByteArray package::to_byte_array()
{
    m_last_processed.clear();
    QDataStream writer(&m_last_processed, QIODevice::WriteOnly);
    writer.setVersion(QDataStream::Qt_5_0);

    writer << this->from()->id();
    writer << this->to()->id();
    writer << this->checksum();
    writer << (quint16)this->type();
    writer << (quint16)this->entity_type();
    writer << m_creation_datetime.toString();
    writer << m_sent_datetime.toString();
    writer << m_data;

    return m_last_processed;
}

QByteArray package::last_processed_byte_array() const
{
    return m_last_processed;
}

void package::from_byte_array(const QByteArray &ba)
{
    Q_ASSERT(!ba.isEmpty());
    QByteArray local(ba);
    QDataStream reader(&local, QIODevice::ReadOnly);
    reader.setVersion(QDataStream::Qt_5_0);
    // reading from (user id)
    qint32 userid = 0;
    reader >> userid;
    if (0 == this->from()) {
        this->set_from(QSharedPointer<core::user>(new core::user()));
    }
    this->from()->set_id(userid);
    // reading to (user id)
    reader >> userid;
    if (0 == this->to()) {
        this->set_to(QSharedPointer<core::user>(new core::user()));
    }
    this->to()->set_id(userid);

    // reading checksum
    reader >> m_checksum;

    // reading type
    quint16 tp = 0;
    reader >> tp;
    m_type = (package_type)tp;

    // reading entity_type
    reader >> tp;
    m_entity_type = (entity_type_t::type)tp;

    // reading creation and sent datetimes
    QString datetime;
    reader >> datetime;
    m_creation_datetime.fromString(datetime); // check format moments later

    reader >> datetime;
    m_sent_datetime.fromString(datetime);

    // reading data
    reader >> m_data; // TODO: check for '\0's etc

    // end reading
}

bool package::operator <(const package& p)
{
    (void)p;
    return false; // how to define who is lesser?
}

bool package::operator ==(const package& p)
{
    (void)p;
    return false; // the same WTF here
    // and make these operators global
}

package& package::operator +=(const QSharedPointer<datagram>& d)
{
    this->add_datagram(d);
    return *this;
}

} // namespace network

} // namespace kokain
