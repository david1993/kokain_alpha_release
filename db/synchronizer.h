#ifndef SYNCHRONIZER_H
#define SYNCHRONIZER_H

#include <QObject>
#include <QSharedPointer>

namespace kokain
{

namespace network
{

class package;

}

namespace db
{

class synchronizer : public QObject
{
    Q_OBJECT
private:
    explicit synchronizer(QObject *parent = 0);

public:
    QSharedPointer<network::package> process_incoming_package(const QSharedPointer<network::package>& p);

public:
    static QSharedPointer<synchronizer> get_instance();

private:
    static QSharedPointer<synchronizer> s_instance;

private:
    synchronizer(const synchronizer&);
    synchronizer& operator=(const synchronizer&);

}; // class synchronizer

} // namespace db

} // namespace kokain

#endif // SYNCHRONIZER_H
