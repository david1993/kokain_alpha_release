#include "condition.h"

namespace kokain
{
    namespace db
    {
        condition::condition()
            :m_value("")
        {}

        condition::condition(const QString& column, const QString& my_operator, const QString& value)
            :m_value("")
        {
            m_value += column + " " + my_operator + " '" + value + "' ";
        }

        condition::condition(const QString& column, const QString& my_operator, double value)
            :m_value("")
        {
            QString svalue = QString::number(value);
            m_value += column + " " + my_operator + " " + svalue + " ";
        }

        QString condition::get_value() const
        {
            return m_value;
        }

        void condition::set_value(const QString& value)
        {
            m_value = value;
        }

/*        condition& condition::operator+=(const condition& rhs)
        {
            this->m_value = "(" + this->m_value +"OR " + rhs.m_value + ")";
            return (*this);
        }

        condition condition::operator+(const condition& rhs)
        {
            condition tmp;
            tmp.set_value("(" + this->m_value +"OR " + rhs.m_value + ")");
            return tmp;
        }

        condition& condition::operator*=(const condition& rhs)
        {
            this->m_value = "(" +this->m_value + "AND " + rhs.m_value + ")";
            return (*this);
        }

        condition condition::operator*(const condition& rhs)
        {
            condition tmp;
            tmp.set_value("(" + this->m_value +"AND " + rhs.m_value + ")");
            return tmp;
        }
*/
    }//namespace db
}//namespace kokain
