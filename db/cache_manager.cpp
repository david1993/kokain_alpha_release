#include "cache_manager.h"

#include <QDebug>

#include "../core/entity.h"
#include "../core/project.h"
#include "../core/task.h"
#include "../core/comment.h"
#include "../core/user.h"
#include "../calendar/event.h"

using namespace kokain::db;

//cache
QString cache::get_value()
{
    QJsonObject j_obj;

    QJsonArray projects_arr;
    for(QVector<QString>::iterator it = project_ids.begin(); it != project_ids.end(); ++it)
    {
        projects_arr.push_back((*it));
    }
    j_obj.insert("projects", projects_arr);

    QJsonArray tasks_arr;
    for(QVector<QString>::iterator it = task_ids.begin(); it != task_ids.end(); ++it)
    {
        tasks_arr.push_back((*it));
    }
    j_obj.insert("tasks", tasks_arr);

    QJsonArray comments_arr;
    for(QVector<QString>::iterator it = comment_ids.begin(); it != comment_ids.end(); ++it)
    {
        comments_arr.push_back((*it));
    }
    j_obj.insert("comments", comments_arr);

    QJsonArray events_arr;
    for(QVector<QString>::iterator it = event_ids.begin(); it != event_ids.end(); ++it)
    {
        events_arr.push_back((*it));
    }
    j_obj.insert("events", events_arr);

    QJsonArray teams_arr;
    for(QVector<QString>::iterator it = team_ids.begin(); it != team_ids.end(); ++it)
    {
        teams_arr.push_back((*it));
    }
    j_obj.insert("teams", teams_arr);
    //here>>>>>>>>>>>>>>

    QJsonArray users_arr;
    for(QVector<int>::iterator it = user_ids.begin(); it != user_ids.end(); ++it)
    {
        users_arr.push_back((*it));
    }
    j_obj.insert("users", users_arr);

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void cache::init_with_value(const QString& val)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject j_obj = j_doc.object();

    QJsonArray projects_arr = j_obj.value("projects").toArray();
    for(QJsonArray::iterator it = projects_arr.begin(); it != projects_arr.end(); ++it)
    {
        project_ids.push_back((*it).toString());
    }

    QJsonArray tasks_arr = j_obj.value("tasks").toArray();
    for(QJsonArray::iterator it = tasks_arr.begin(); it != tasks_arr.end(); ++it)
    {
        task_ids.push_back((*it).toString());
    }

    QJsonArray comments_arr = j_obj.value("comments").toArray();
    for(QJsonArray::iterator it = comments_arr.begin(); it != comments_arr.end(); ++it)
    {
        comment_ids.push_back((*it).toString());
    }

    QJsonArray events_arr = j_obj.value("events").toArray();
    for(QJsonArray::iterator it = events_arr.begin(); it != events_arr.end(); ++it)
    {
        event_ids.push_back((*it).toString());
    }

    QJsonArray teams_arr = j_obj.value("teams").toArray();
    for(QJsonArray::iterator it = teams_arr.begin(); it != teams_arr.end(); ++it)
    {
        team_ids.push_back((*it).toString());
    }
    //here>>>>>>>>>>>>>>

    QJsonArray users_arr = j_obj.value("users").toArray();
    for(QJsonArray::iterator it = users_arr.begin(); it != users_arr.end(); ++it)
    {
        user_ids.push_back((*it).toInt());
    }
}
//

//cache_manager

cache_manager* cache_manager::s_instance;
cache cache_manager::m_cache;
QList<kokain::core::entity*> cache_manager::m_old_entities;
QList<kokain::core::entity*> cache_manager::m_new_entities;
QList<kokain::core::user*> cache_manager::m_old_users;
QList<kokain::core::user*> cache_manager::m_new_users;

cache_manager::cache_manager()
{
    qDebug() << "An instance of cache_manager is going to be created.";

    query q(query::SELECT, "cache");
    QSqlQuery qr = db_manager::get_instance()->execute(q);
    if(qr.next())
    {
        QString result = qr.value(0).toString();
        qDebug() << " == cache == ";
        qDebug() << result;
        m_cache.init_with_value(result);
    }
    else
    {
        qDebug() << "first time, no cache!";
        query q(query::INSERT, "cache");
        q.set_value(m_cache.get_value());
        db_manager::get_instance()->execute(q);
    }

    load_cache_from_db_();
    oldenize_();

    qDebug() << "An instance of cache_manager is created.";
}

cache_manager::~cache_manager()
{}

cache_manager* cache_manager::get_instance()
{
    if(!s_instance)
    {
        s_instance = new cache_manager();
    }
    return s_instance;
}

void cache_manager::remove_instance()
{
    qDebug() << "cache_manager is going to be removed. Some data is to be saved!";
    save_current_cache();
    query q(query::UPDATE, "cache");
    condition cond("content", "MATCH", "projects");
    q.set_condition(cond);
    q.set_value(m_cache.get_value());
    db_manager::get_instance()->execute(q);
    qDebug() << "cache_manager is removed.";
}

void cache_manager::load_cache_from_db_()
{
    for(QVector<QString>::iterator it = m_cache.project_ids.begin(); it != m_cache.project_ids.end(); ++it)
    {
        load_from_db("project", (*it));
    }
    for(QVector<QString>::iterator it = m_cache.task_ids.begin(); it != m_cache.task_ids.end(); ++it)
    {
        load_from_db("task", (*it));
    }
    for(QVector<QString>::iterator it = m_cache.comment_ids.begin(); it != m_cache.comment_ids.end(); ++it)
    {
        load_from_db("comment", (*it));
    }
    for(QVector<QString>::iterator it = m_cache.event_ids.begin(); it != m_cache.event_ids.end(); ++it)
    {
        load_from_db("cal_event", (*it));
    }
    for(QVector<QString>::iterator it = m_cache.team_ids.begin(); it != m_cache.team_ids.end(); ++it)
    {
        load_from_db("team", (*it));
    }
    //here>>>>>>>>>>>>>>
    for(QVector<qint32>::iterator it = m_cache.user_ids.begin(); it != m_cache.user_ids.end(); ++it)
    {
        load_user_from_db((*it));
    }
}

void cache_manager::oldenize_()
{
    for(QList<core::entity*>::iterator it = m_new_entities.begin(); it != m_new_entities.end(); ++it)
    {
        m_old_entities.push_back((*it));
    }
    m_new_entities.clear();

    for(QList<kokain::core::user*>::iterator it = m_new_users.begin(); it != m_new_users.end(); ++it)
    {
        m_old_users.push_back((*it));
    }
    m_new_users.clear();
}

void cache_manager::save_current_cache()
{
    //cleaning old data
    m_cache.project_ids.clear();
    m_cache.task_ids.clear();
    m_cache.comment_ids.clear();
    m_cache.event_ids.clear();
    m_cache.team_ids.clear();
    //here>>>>>>>>>>>>>>
    m_cache.user_ids.clear();

    //filling data
    for(QList<core::entity*>::iterator it = m_new_entities.begin(); it != m_new_entities.end(); ++it)
    {
        core::entity* tmp = (*it);
        if(tmp->object_type() == "project")
            m_cache.project_ids.push_back(tmp->id());
        if(tmp->object_type() == "task")
            m_cache.task_ids.push_back(tmp->id());
        if(tmp->object_type() == "comment")
            m_cache.comment_ids.push_back(tmp->id());
        if(tmp->object_type() == "cale_event")
            m_cache.event_ids.push_back(tmp->id());
        if(tmp->object_type() == "team")
            m_cache.team_ids.push_back(tmp->id());
        //here>>>>>>>>>>>>>>
    }
    for(QList<kokain::core::user*>::iterator it = m_new_users.begin(); it != m_new_users.end(); ++it)
    {
        kokain::core::user* tmp = (*it);
        m_cache.user_ids.push_back(tmp->id());
    }

}

kokain::core::entity* cache_manager::entity_with_id(const QString &obj_type, const QString &id)
{
    Q_ASSERT(!obj_type.isEmpty());
    Q_ASSERT(!id.isEmpty());
    qDebug() << "trying to find a " << obj_type << " object in cache with id " << id;
    kokain::core::entity* ent_to_return = 0;

    for(QList<core::entity*>::iterator it = m_old_entities.begin(); it != m_old_entities.end(); ++it)
    {
        qDebug() << "looking for in olds";
        core::entity* tmp = (*it);
        if(id == tmp->id())
        {
            qDebug() << "found in olds";
            ent_to_return = tmp;
            m_old_entities.erase(it);
            m_new_entities.push_back(tmp);
            return ent_to_return;
        }
    }

    for(QList<core::entity*>::iterator it = m_new_entities.begin(); it != m_new_entities.end(); ++it)
    {
        qDebug() << "looking for in news";
        core::entity* tmp = (*it);
        if(id == tmp->id())
        {
            qDebug() << "found in news";
            ent_to_return = tmp;
            return ent_to_return;
        }
    }

    ent_to_return = load_from_db(obj_type, id);
    if(ent_to_return)
    {
        qDebug() << "found in db";
        return ent_to_return;
    }
    else
    {
        qDebug() << "could not find in db";
        return 0;
    }
}

kokain::core::user *cache_manager::user_with_id(qint32 id)
{
    Q_ASSERT(id >= 0);
    qDebug() << "trying to find user with id " << id;
    kokain::core::user* user_to_return = 0;

    for(QList<kokain::core::user*>::iterator it = m_old_users.begin(); it != m_old_users.end(); ++it)
    {
        qDebug() << "looking for in olds";
        kokain::core::user* tmp = (*it);
        if(id == tmp->id())
        {
            qDebug() << "found in olds";
            user_to_return = tmp;
            m_old_users.erase(it);
            m_new_users.push_back(tmp);
            return user_to_return;
        }
    }

    for(QList<kokain::core::user*>::iterator it = m_new_users.begin(); it != m_new_users.end(); ++it)
    {
        qDebug() << "looking for in news";
        kokain::core::user* tmp = (*it);
        if(id == tmp->id())
        {
            qDebug() << "found in news";
            user_to_return = tmp;
            return user_to_return;
        }
    }

    user_to_return = load_user_from_db(id);
    if(user_to_return)
    {
        qDebug() << "found in db";
        return user_to_return;
    }
    else
    {
        qDebug() << "could not find in db";
        return 0;
    }
}

kokain::core::entity* cache_manager::load_from_db(const QString& obj_type, const QString& id)
{
    Q_ASSERT(!obj_type.isEmpty());
    Q_ASSERT(!id.isEmpty());
    if(obj_type == "project")
    {
        query q(query::SELECT, "projects");
        condition cond("content", "MATCH", "\"id\": \"" + id + "\"");
        q.set_condition(cond);
        QSqlQuery qr = db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            core::entity* ent_to_return = new core::project();
            ent_to_return->deserialize_from_json(qr.value(0).toString());
            return ent_to_return;
        }
        else
        {
            return 0;
        }
    }
    if(obj_type == "task")
    {
        query q(query::SELECT, "tasks");
        condition cond("content", "MATCH", "\"id\": \"" + id + "\"");
        q.set_condition(cond);
        QSqlQuery qr = db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            core::entity* ent_to_return = new core::task();
            ent_to_return->deserialize_from_json(qr.value(0).toString());
            return ent_to_return;
        }
        else
        {
            return 0;
        }
    }
    if(obj_type == "comment")
    {
        query q(query::SELECT, "comments");
        condition cond("content", "MATCH", "\"id\": \"" + id + "\"");
        q.set_condition(cond);
        QSqlQuery qr = db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            core::entity* ent_to_return = new core::comment();
            ent_to_return->deserialize_from_json(qr.value(0).toString());
            return ent_to_return;
        }
        else
        {
            return 0;
        }
    }
    if(obj_type == "cal_event")
    {
        query q(query::SELECT, "calendar_events");
        condition cond("content", "MATCH", "\"id\": \"" + id + "\"");
        q.set_condition(cond);
        QSqlQuery qr = db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            core::entity* ent_to_return = new calendar_namespace::cal_event();
            ent_to_return->deserialize_from_json(qr.value(0).toString());
            return ent_to_return;
        }
        else
        {
            return 0;
        }
    }
    if(obj_type == "team")
    {
        query q(query::SELECT, "teams");
        condition cond("content", "MATCH", "\"id\": \"" + id + "\"");
        q.set_condition(cond);
        QSqlQuery qr = db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            core::entity* ent_to_return = new calendar_namespace::cal_event();
            ent_to_return->deserialize_from_json(qr.value(0).toString());
            return ent_to_return;
        }
        else
        {
            return 0;
        }
    }
    //here>>>>>>>>>>>>>>
    return 0;
}

kokain::core::user* cache_manager::load_user_from_db(qint32 id)
{
    Q_ASSERT(id >= 0);
    kokain::core::user* usr_to_return = new kokain::core::user;
    query q(query::SELECT, "users");
    condition cond("content", "MATCH", "\"id\": " + QString::number(id));
    q.set_condition(cond);
    QSqlQuery qr = db_manager::get_instance()->execute(q);
    if(qr.next())
    {
        usr_to_return->deserialize_from_json(qr.value(0).toString());
        return usr_to_return;
    }
    else
    {
        return 0;
    }
}
//

