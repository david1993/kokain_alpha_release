#include "query.h"
#include <iostream>

namespace kokain
{
namespace db
{
QString query::get_result()
{
    m_result == "";

    if (m_type == query::INSERT)
    {
        m_result = "INSERT INTO " + m_table_name + "(content)";

        m_result += (" VALUES('" + m_value + "'" + "); ");

        return m_result;
    }

    else if (m_type == query::SELECT)
    {
        m_result = "SELECT * ";
        m_result += "FROM " + m_table_name;
        if(!m_cond.get_value().isEmpty())
        {
            m_result += " WHERE " + m_cond.get_value() + ";";
        }
        return m_result;
    }

    else if (m_type == query::REMOVE)
    {
        m_result = "DELETE FROM " + m_table_name;
        if(!m_cond.get_value().isEmpty())
        {
            m_result += " WHERE " + m_cond.get_value() + ";";
        }
        return m_result;
    }

    else if (m_type == query::UPDATE)
    {
        m_result = "UPDATE " + m_table_name + " SET ";
        m_result += ("content = '" + m_value+ "' ");
        if(!m_cond.get_value().isEmpty())
        {
            m_result += ("WHERE " + m_cond.get_value() + ";");
        }
        return m_result;
    }

    else if (m_type == query::CREATE)
    {
        m_result = "CREATE VIRTUAL TABLE " +
                m_table_name + " USING fts3(content);";
        return m_result;
    }

    else if (m_type == query::DROP)
    {
        m_result = "DROP TABLE " + m_table_name + ";";
        return m_result;
    }

    else
        return "";
}

query::query(query_type type, const QString& tbl_name)
    : m_type(type),
      m_table_name(tbl_name),
      m_result("")
{}

void query::set_value(const QString& val)
{
    this->m_value = val;
}

QString query::get_value()
{
    return this->m_value;
}

void query::set_condition(const condition &cond)
{
    this->m_cond = cond;
}

condition query::get_condition()
{
    return this->m_cond;
}

query::query_type query::get_query_type()
{
    return this->m_type;
}

QString query::get_table_name()
{
    return this->m_table_name;
}

void query::set_table_name(const QString& tbl_name)
{
    m_table_name = tbl_name;
}

}//namespace db
}//namespace kokain
