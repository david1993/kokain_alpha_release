#ifndef DB_MANAGER_H
#define DB_MANAGER_H

#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QString>
#include <QList>
#include <QDebug>

#include "query.h"
#include "../core/entity.h"
#include "../core/user.h"

namespace kokain
{
namespace db
{
class db_manager
{
private:
    db_manager();
    db_manager(const db_manager& obj);
    db_manager& operator=(const db_manager& obj);

    ~db_manager();
    QSqlDatabase m_sdb;
    static db_manager* s_instance;
    int counter;
    static const QString k_file_name;
public:
    QSqlQuery execute(query& q);
    const QSqlDatabase getDb();

    static db_manager* get_instance()
    {
        if (NULL == s_instance)
        {
            s_instance = new db_manager();
        }
        return s_instance;
    }
    static void remove_instance()
    {
        qDebug() << "removing db_manager instance";
        delete s_instance;
        s_instance = NULL;
        qDebug() << "db_manager removed";
    }

private:
    void create_tables_();

public:
    core::entity* give_instance_with_id(const QString& obj_type, const QString& id);
    core::user* give_user_with_id(qint32 id);
};
}//namespace db
}//namespace kokain
#endif // DB_MANAGER_H
