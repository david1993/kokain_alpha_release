#ifndef CONDITION_H
#define CONDITION_H
#include <QString>

namespace kokain
{
namespace db
{
class condition
{
public:
    condition();
    condition(const QString& column,const QString& my_operator,const QString& value);
    condition(const QString& column,const QString& my_operator,double value);

    condition operator+(const condition& rhs);
    condition& operator+=(const condition& rhs);
    condition operator*(const condition& rhs);
    condition& operator*=(const condition& rhs);

    //setters and getters
    QString get_value() const;
    void set_value(const QString& value);
private:
    QString m_value;
};//class condition
}//namespace db
}//namespace kokain

#endif // CONDITION_H
