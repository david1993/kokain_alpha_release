#include "db_manager.h"

#include <iostream>

#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QVariant>
#include <QtSql/QSqlError>
#include <QFile>
#include <QDir>

#include "query.h"
#include "cache_manager.h"

namespace kokain
{
namespace db
{

const QString db_manager::k_file_name = "kokain_db.sqlite";

db_manager* db_manager::s_instance;

db_manager::db_manager()
{
    qDebug() << "An instance of db_manager is being created.";
    QFile db_file(QDir::currentPath() + "/" + k_file_name);
    if(!db_file.exists())
    {
        qDebug() << "Database does not exist";
        qDebug() << "Creating new database";
        db_file.open(QIODevice::ReadWrite);
        qDebug() << "new database created at " + QDir::currentPath() + "/" + k_file_name;
        this->m_sdb = QSqlDatabase::addDatabase("QSQLITE");
        this->m_sdb.setDatabaseName(QDir::currentPath() + "/" + k_file_name);
        this->m_sdb.open();
        counter = 0;
        create_tables_();
    }
    else
    {
        qDebug() << "Database already exists";
        this->m_sdb = QSqlDatabase::addDatabase("QSQLITE");
        this->m_sdb.setDatabaseName(QDir::currentPath() + "/" + k_file_name);
        this->m_sdb.open();
        counter = 0;
    }
    qDebug() << "An instance of db_manager is created.";
}

void db_manager::create_tables_()
{
    qDebug() << "creating tables";

    query qr(query::CREATE, "projects");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("resources");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("plans");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("repositories");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("project_filters");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("tasks");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("task_attachments");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("task_filters");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("calendar_events");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("event_filters");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("users");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("contacts");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("user_activity");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("user_gamification_info");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("teams");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("badges");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("user_filters");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("commands");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("actions");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("search_metadata");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("activity_board");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("notifications");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("analytics");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("analytics_filters");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("sync_data");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("db_log");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("db_stats");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("packages");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("connections");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("network_settings");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("triggers");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("settings");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("permissions");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("auto_recovery_data");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("cache");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";

    qr.set_table_name("task_datetime_estimations_t");
    execute(qr);
    qDebug() << qr.get_table_name() << " table created";
}

const QSqlDatabase db_manager::getDb()
{
    return this->m_sdb;
}

QSqlQuery db_manager::execute(query& q)
{
    QSqlQuery sqlquery(q.get_result(), m_sdb);
    qDebug() << "query == " << q.get_result();
    return sqlquery;
}

db_manager::~db_manager()
{
    /*
    delete this->m_sdb;
    this->m_sdb = NULL;
    */
}

core::entity* db_manager::give_instance_with_id(const QString& obj_type, const QString& id)
{
    Q_ASSERT(!obj_type.isEmpty());
    Q_ASSERT(!id.isEmpty());
    qDebug() << "an instance of " << obj_type << " with id " << id << " is required!";
    core::entity* tmp = 0;
    //try to find in cache manager
    tmp = cache_manager::get_instance()->entity_with_id(obj_type, id);
    if(tmp)
    {
        return tmp;
    }
    else
    {
        //ask sinchronizer
        //TODO
        //SPECIAL FOR VARDAN
    }
    return 0;
}

core::user* db_manager::give_user_with_id(qint32 id)
{
    Q_ASSERT(id < 0);
    qDebug() << "An user with id " << id << " is required!";
    core::user* tmp = new core::user();
    //try to find in cache
    tmp = cache_manager::get_instance()->user_with_id(id);
    if(tmp)
    {
        return tmp;
    }
    else
    {
        //ask sinchronizer
        //TODO
        //SPECIAL FOR VARDAN
    }
    return 0;
}

}//namespace db
}//namespace kokain
