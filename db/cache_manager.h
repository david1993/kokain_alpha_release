#ifndef CACHE_MANAGER_H
#define CACHE_MANAGER_H

#include <QString>
#include <QVariant>

#include "db_manager.h"

namespace kokain
{
namespace core
{
class entity;
class user;
}
}

namespace kokain
{
namespace db
{

struct cache
{
    QString get_value();
    void init_with_value(const QString& val);

    QVector<QString> project_ids;
    QVector<QString> task_ids;
    QVector<QString> comment_ids;
    QVector<QString> event_ids;
    QVector<QString> team_ids;
    //...and others
    QVector<int> user_ids;
};

class cache_manager
{
private:
    cache_manager();
    cache_manager(const cache_manager&);
    cache_manager& operator=(const cache_manager*);
    ~cache_manager();

    //singleton
public:
    static cache_manager* s_instance;
    static cache_manager* get_instance();
    static void remove_instance();

private:
    void load_cache_from_db_();
    void oldenize_();
    static void save_current_cache();

    //cache
private:
    static cache m_cache;
    static QList<core::entity*> m_old_entities;
    static QList<core::entity*> m_new_entities;
    static QList<kokain::core::user*> m_old_users;
    static QList<kokain::core::user*> m_new_users;

public:
    core::entity* entity_with_id(const QString& obj_type, const QString& id);
    kokain::core::user* user_with_id(qint32 id);
private:
    core::entity* load_from_db(const QString& obj_type, const QString& id);
    kokain::core::user* load_user_from_db(qint32 id);
};

}//namespace db
}//namespace kokain

#endif // CACHE_MANAGER_H
