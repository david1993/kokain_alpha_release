#ifndef QUERY_H
#define QUERY_H

#include <QVector>
#include <QString>

#include "condition.h"

namespace kokain
{
namespace db
{

class query
{
public:
    enum query_type
    {
        CREATE,
        INSERT,
        SELECT,
        REMOVE,
        UPDATE,
        DROP
    };

public:
    query(query_type type, const QString& tbl_name);

    QString get_result();

    void set_value(const QString& val);
    QString get_value();

    void set_condition(const condition& cond);
    condition get_condition();

    query_type get_query_type();
    QString get_table_name();
    void set_table_name(const QString& tbl_name);

private:
    query(const query& obj);
    query& operator=(const query& obj);

private:
    QString m_table_name;
    int m_id;

    QString m_value;
    condition m_cond;
    query_type m_type;
    QString m_result;
};//class query
}//namespace db
}//namespace kokain
#endif // QUERY_H
