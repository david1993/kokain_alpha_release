#include "synchronizer.h"

#include "../network/package.h"

namespace kokain
{

namespace db
{

QSharedPointer<synchronizer> synchronizer::s_instance(NULL);

synchronizer::synchronizer(QObject *parent) :
    QObject(parent)
{
}

QSharedPointer<synchronizer> synchronizer::get_instance()
{
    if (NULL == s_instance) {
        s_instance.reset(new synchronizer());
    }
    return s_instance;
}

QSharedPointer<network::package> synchronizer::process_incoming_package(const QSharedPointer<network::package> &p)
{
    Q_ASSERT(0 != p);
    switch (p->type())
    {
    // START HERE
    }
    return QSharedPointer<network::package>(NULL);
}

} // namespace db

} // namespace kokain
