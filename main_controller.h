#ifndef OVERALL_MANAGER_H
#define OVERALL_MANAGER_H

#include <QtGlobal>

namespace kokain
{

class main_controller
{
private:
    main_controller();
    main_controller(const main_controller&);
    main_controller& operator=(const main_controller&);
    ~main_controller();

    //singleton
public:
    static main_controller* s_instance;
    static main_controller* get_instance();
    static void remove_instance();

    //entry
public:
    void my_entry();

    //current user's id
public:
    void set_current_user_id(qint32 id);
    qint32 current_user_id() const;
private:
    qint32 m_cur_user_id;
};

}//namespace kokain

#endif // OVERALL_MANAGER_H
