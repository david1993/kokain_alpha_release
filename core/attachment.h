#ifndef ATTACHMENT_H
#define ATTACHMENT_H

#include <QString>

#include "entity.h"

namespace kokain
{

namespace core
{

/// @brief Replace with Vanand's implementation, this is just a shell-class
class attachment : public entity
{
public:
    explicit attachment(const QString& t = k_attachment_default_title);

public:
    void set_title(const QString& t);
    QString title() const;

private:
    QString m_title;

private:
    static const QString k_attachment_default_title;

private:
    attachment(const attachment&);
    attachment& operator=(const attachment&);

}; // class attachment

} // namespace core

} // namespace kokain

#endif // ATTACHMENT_H
