#include "user.h"

#include "entity.h"
#include "../db/db_manager.h"
#include "../db/query.h"
#include "../db/condition.h"
#include "../calendar/calendar.h"
#include "../calendar/user_data.h"

using namespace kokain::core;

user::user(QWidget* widget)
    : QWidget(widget)
{
    initilize_();
}

void user::initilize_()
{
    create_widgets_();
    setup_layout_();
}

void user::create_widgets_()
{
    m_user_calendar = new calendar_namespace::calendar();
    m_user_calendar->close();
    QPixmap* pix = new QPixmap("C:/Users/David/user_avatar_list_widget/images/Smile.png");
    m_picture = new QLabel("", this);
    m_picture->setPixmap(pix->scaled(30,40));

    m_name = new QLabel(this);
    m_name->setStyleSheet("qproperty-alignment: AlignCenter;");

    m_picture->setScaledContents(true);
    m_picture->setGeometry(100,100,100,100);

    m_indicator = QString("on_workers");
    m_data = new calendar_namespace::user_data();
    m_data->add_data(QString("Email"), false);
    m_data->add_data(QString("Phones"), false);
    m_data->add_data(QString("Websites"), true);
    m_data->add_data(QString("IMs"), false);
    m_data->add_data(QString("Skills"), false);
    m_data->add_data(QString("Social"), true);
    m_data->add_data(QString("Experience"), false);
    m_data->add_data(QString("Education"), false);
    m_data->add_data(QString("About"), false);
    m_data->add_data(QString("Notes on contact"), false);
    m_data->add_data(QString("Activity"), false);
    connect(m_data, SIGNAL(on_data_changed()), this, SLOT(user_data_changed()));
    connect(m_data, SIGNAL(on_calendar_showed()), this, SLOT(calendar_showed()));
    installEventFilter(this);
    setMouseTracking(true);
}

void user::setup_layout_()
{
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_picture);
    layout->addWidget(m_name);
    setLayout(layout);
}

void user::set_name(QString name)
{
    m_name->setText(name);
    QFont font = m_name->font();
    font.setPointSize(7);
    m_name->setFont(font);
}

void user::set_picture(QPixmap picture_url)
{
    m_picture->setPixmap(picture_url.scaled(40,40));
}

QString user::get_name() const
{
    return m_name->text();
}

QPixmap user::get_picture() const
{
    return *m_picture->pixmap();
}

void user::set_pos(QPoint pos)
{
    m_ptDragPos = pos;
}

QPoint user::get_pos()
{
    return m_ptDragPos;
}

QString user::get_indicator()
{
    return m_indicator;
}

void user::set_indicator(QString ind)
{
    m_indicator = ind;
}



void user::startDrag()
{
    QPoint hotSpot = m_ptDragPos;
    QByteArray itemData;



    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    QString str = get_name();
    QPixmap pic = get_picture();
    QString ind = get_indicator();

    dataStream << id() << QPoint(hotSpot);
    QMimeData* pMimeData = new QMimeData;
    pMimeData->setData("application/x-fridgemagnet", itemData);

    QDrag* pDrag = new QDrag(this);
    pDrag->setMimeData(pMimeData);
    pDrag->setPixmap(pic.scaled(40,40));
    if (pDrag->exec(Qt::MoveAction)){
        this->close();
    } else {
        set_name(str);
        set_picture(pic);
    }
}





bool user::eventFilter(QObject *target, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress){
        QMouseEvent *ev = static_cast<QMouseEvent*>(event);
        if (ev->button() == Qt::LeftButton) {
            m_ptDragPos = ev->pos();
        }
        return true;
    }

    if(event->type() == QEvent::MouseMove){
        QMouseEvent *pe = static_cast<QMouseEvent*>(event);
        if (pe->buttons() & Qt::LeftButton) {
            QPoint dist = pe->pos() - m_ptDragPos;
            int distance = dist.manhattanLength();
            if (distance > QApplication::startDragDistance()) {
            startDrag();
            }
        }
        return true;
    }

    if(event->type() == QEvent::MouseButtonRelease){
        QMouseEvent *pe = static_cast<QMouseEvent*>(event);
        m_data->show();
            emit data_showed();
        return true;
    }
    return false;
}

kokain::calendar_namespace::user_data* user::get_m_data()
{
    return m_data;
}

void user::set_id(qint32 id)
{
    m_id = id;
}

qint32 user::id() const
{
    return m_id;
}

void user::set_online_status(online_status os)
{
    m_online_status = os;
}

user::online_status user::get_online_status() const
{
    return m_online_status;
}

QString user::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("id", id());
    j_obj.insert("name", get_name());
    QByteArray user_picture;
    QBuffer buffer( &user_picture );
    buffer.open( QIODevice::WriteOnly );
    get_picture().save( &buffer, "PNG" );
    j_obj.insert("picture", QString(user_picture));

    QJsonArray emails;
    for(int it = 0; it < m_data->get_all_options().value(0)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(0)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("emails", m_data->get_all_options().value(0)->get_all_labels().at(it)->get_label());
            emails.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(0)->get_all_labels().remove(it);
        }
    }
    j_obj.insert("EMAILS", emails);

    QJsonArray phones;
    for(int it = 0; it < m_data->get_all_options().value(1)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(1)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("phones", m_data->get_all_options().value(1)->get_all_labels().at(it)->get_label());
            phones.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(1)->get_all_labels().remove(it);
        }
    }
    j_obj.insert("PHONES", phones);


    QJsonArray websites;
    for(int it = 0; it < m_data->get_all_options().value(2)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(2)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("websites", m_data->get_all_options().value(2)->get_all_labels().at(it)->get_label());
            websites.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(2)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("WEBSITES", websites);

    QJsonArray ims;
    for(int it = 0; it < m_data->get_all_options().value(3)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(3)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("ims", m_data->get_all_options().value(3)->get_all_labels().at(it)->get_label());
            ims.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(3)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("IMS", ims);

    QJsonArray skills;
    for(int it = 0; it < m_data->get_all_options().value(4)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(4)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("skills", m_data->get_all_options().value(4)->get_all_labels().at(it)->get_label());
            skills.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(4)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("SKILLS", skills);


    QJsonArray social;
    for(int it = 0; it < m_data->get_all_options().value(5)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(5)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("social", m_data->get_all_options().value(5)->get_all_labels().at(it)->get_label());
            social.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(5)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("SOCIAL", social);

    QJsonArray experience;
    for(int it = 0; it < m_data->get_all_options().value(6)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(6)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("experience", m_data->get_all_options().value(6)->get_all_labels().at(it)->get_label());
            experience.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(6)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("EXPERIENCE", experience);

    QJsonArray education;
    for(int it = 0; it < m_data->get_all_options().value(7)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(7)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("education", m_data->get_all_options().value(7)->get_all_labels().at(it)->get_label());
            education.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(7)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("EDUCATION", education);

    QJsonArray about;
    for(int it = 0; it < m_data->get_all_options().value(8)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(8)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("about", m_data->get_all_options().value(8)->get_all_labels().at(it)->get_label());
            about.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(8)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("ABOUT", about);

    QJsonArray notes_on_contact;
    for(int it = 0; it < m_data->get_all_options().value(9)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(9)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("notes on contact", m_data->get_all_options().value(9)->get_all_labels().at(it)->get_label());
            notes_on_contact.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(9)->get_all_labels().remove(it);
        };
    }

    j_obj.insert("NOTES ON CONTACT", notes_on_contact);

    QJsonArray activity;
    for(int it = 0; it < m_data->get_all_options().value(10)->get_all_labels().size(); ++it)
    {
        QJsonObject tmp_obj;
        if(!m_data->get_all_options().value(10)->get_all_labels().at(it)->isHidden()){
            tmp_obj.insert("activity", m_data->get_all_options().value(10)->get_all_labels().at(it)->get_label());
            activity.push_back(tmp_obj);
        }
        else{
            m_data->get_all_options().value(10)->get_all_labels().remove(it);
        }
    }

    j_obj.insert("ACTIVITY", activity);

    j_obj.insert("online_status", m_online_status);


    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void user::deserialize_from_json(const QString& serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();
    set_id(qint64(j_obj.value("id").toInt()));
    set_name(j_obj.value("name").toString());
    QPixmap picture;
    if(picture.loadFromData(j_obj.value("picture").toVariant().toByteArray(), "PNG"))
            set_picture(picture);

    QJsonArray emails_arr;
    emails_arr = j_obj.value("EMAILS").toArray();

    QVector<QString> email;
    QJsonObject tmp;

    foreach (QJsonValue tmp_obj, emails_arr) {
         email.push_back(tmp_obj.toObject().value("emails").toString());
         qDebug() << tmp_obj.toObject().value("emails").toString();
    }

    QJsonArray phones_arr;
    phones_arr = j_obj.value("PHONES").toArray();

    QVector<QString> phones;

    foreach (QJsonValue tmp_obj, phones_arr) {
        phones.push_back(tmp_obj.toObject().value("phones").toString());
    }

    QJsonArray websites_arr;
    websites_arr = j_obj.value("WEBSITES").toArray();

    QVector<QString> websites;

    foreach (QJsonValue tmp_obj, websites_arr) {
        websites.push_back(tmp_obj.toObject().value("websites").toString());
    }

    QJsonArray ims_arr;
    ims_arr = j_obj.value("IMS").toArray();

    QVector<QString> ims;

    foreach (QJsonValue tmp_obj, ims_arr) {
        ims.push_back(tmp_obj.toObject().value("ims").toString());

    }

    QJsonArray skills_arr;
    skills_arr = j_obj.value("SKILLS").toArray();

    QVector<QString> skills;

    foreach (QJsonValue tmp_obj, skills_arr) {
        skills.push_back(tmp_obj.toObject().value("skills").toString());
    }

    QJsonArray social_arr;
    social_arr = j_obj.value("SOCIAL").toArray();

    QVector<QString> social;

    foreach (QJsonValue tmp_obj, social_arr) {
        social.push_back(tmp_obj.toObject().value("social").toString());
    }

    QJsonArray experience_arr;
    experience_arr = j_obj.value("EXPERIENCE").toArray();

    QVector<QString> experience;

    foreach (QJsonValue tmp_obj, experience_arr) {
        experience.push_back(tmp_obj.toObject().value("experience").toString());
    }


    QJsonArray education_arr;
    education_arr = j_obj.value("EDUCATION").toArray();

    QVector<QString> education;

    foreach (QJsonValue tmp_obj, education_arr) {
        education.push_back(tmp_obj.toObject().value("education").toString());
    }

    QJsonArray about_arr;
    about_arr = j_obj.value("ABOUT").toArray();

    QVector<QString> about;

    foreach (QJsonValue tmp_obj, about_arr) {
        about.push_back(tmp_obj.toObject().value("about").toString());
    }

    QJsonArray notes_on_contact_arr;
    notes_on_contact_arr = j_obj.value("NOTES ON CONTACT").toArray();

    QVector<QString> notes_on_contact;

    foreach (QJsonValue tmp_obj, notes_on_contact_arr) {
        notes_on_contact.push_back(tmp_obj.toObject().value("notes on contact").toString());
    }

    QJsonArray activity_arr;
    activity_arr = j_obj.value("ACTIVITY").toArray();

    QVector<QString> activity;

    foreach (QJsonValue tmp_obj, activity_arr) {
        activity.push_back(tmp_obj.toObject().value("activity").toString());
    }

    QVector<QVector<QString> > all_data;
    all_data.push_back(email);
    all_data.push_back(phones);
    all_data.push_back(websites);
    all_data.push_back(ims);
    all_data.push_back(skills);
    all_data.push_back(social);
    all_data.push_back(experience);
    all_data.push_back(education);
    all_data.push_back(about);
    all_data.push_back(notes_on_contact);
    all_data.push_back(activity);


    m_data->set_data(all_data);

    m_online_status = online_status(j_obj.value("online_status").toInt());

    return;
}

void user::user_data_changed()
{
    kokain::db::query* q = new kokain::db::query(kokain::db::query::REMOVE, "users");
    kokain::db::condition* cond = new kokain::db::condition("content", "MATCH", "\"id\": " + QString::number(id()));
    q->set_condition(*cond);
    QSqlQuery res2;
    res2 = kokain::db::db_manager::get_instance()->execute(*q);



    q = new kokain::db::query(kokain::db::query::INSERT, "users");
    q->set_value(serialize_to_json());
    QSqlQuery res = kokain::db::db_manager::get_instance()->execute(*q);
}

QString user::object_type() const
{
    return "user";
}

void user::calendar_showed()
{
    m_user_calendar->show();
}


void user::set_permission_for_entity(entity *e, permission_t::permission_type pt)
{
    Q_ASSERT(0 != e);
    permission_t perm =  m_user_permissions[e->id()];
    perm.type = static_cast<permission_t::permission_type>(perm.type | pt);
    m_user_permissions[e->id()] = perm;
}

void user::unset_permission_for_entity(entity *e, permission_t::permission_type pt)
{
    Q_ASSERT(0 != e);
    permission_t perm = m_user_permissions[e->id()];
    perm.type = static_cast<permission_t::permission_type>(perm.type & ~pt);
    m_user_permissions[e->id()] = perm;
}

bool user::has_permission_for_entity(entity *e, permission_t::permission_type pt)
{
    Q_ASSERT(0 != e);
    permission_t perm = m_user_permissions[e->id()];
    return ( (perm.type & pt) != 0 );
}

void user::set_permission_object_for_entity(entity *e, const permission_t &po)
{
    Q_ASSERT(0 != e);
    m_user_permissions[e->id()] = po;
}

void user::remove_permission_object_for_entity(entity *e)
{
    Q_ASSERT(0 != e);
    m_user_permissions.erase(e->id());
}

permission_t user::permission_object_for_entity(entity *e)
{
    Q_ASSERT(0 != e);
    return m_user_permissions[e->id()];
}

void user::set_entity_permission_map(const std::map<QString, permission_t> &m)
{
    if (!m.empty()) {
        m_user_permissions.clear(); // why?
        m_user_permissions = m;
    }
}

std::map<QString, permission_t> user::entity_permission_map() const
{
    return m_user_permissions;
}

void user::set_active_task(task *t)
{
    m_active_task = t;
    Q_ASSERT(0 != m_active_task);
}

task* user::active_task()
{
    return m_active_task;
}

void user::set_tasks(const std::vector<task *> &tv)
{
    if (!tv.empty()) {
        m_tasks.clear();
        m_tasks.assign(tv.begin(), tv.end());
    }
}

void user::add_task(task *t)
{
    Q_ASSERT(0 != t);
    m_tasks.push_back(t);
}

std::vector<task*> user::tasks()
{
    return m_tasks;
}
