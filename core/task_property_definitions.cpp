#include "task_property_definitions.h"

#include "../core/user.h"
#include "../db/db_manager.h"

namespace kokain
{

namespace core
{

bool operator==(const work_period& a, const work_period& b)
{
    return a.id() == b.id();
}


// priority

bool task_priority_t::s_initialized = false;
std::map<task_priority_t::priority, QString> task_priority_t::s_priority_enum_map;

void task_priority_t::initialize_enum_map_()
{
    if (s_initialized) { return; }
    s_initialized = true;
    // TODO: later hardcoded values should be replaced with read from DB
    s_priority_enum_map[task_priority_t::LOW] = QObject::tr("low");
    s_priority_enum_map[task_priority_t::MIDDLE] = QObject::tr("middle");
    s_priority_enum_map[task_priority_t::HIGH] = QObject::tr("high");
    s_priority_enum_map[task_priority_t::CRITICAL] = QObject::tr("critical");
    s_priority_enum_map[task_priority_t::BLOCKING] = QObject::tr("blocking");
}

QString task_priority_t::get_priority_name_by_enum(priority p)
{
    initialize_enum_map_();
    switch (p)
    {
    case task_priority_t::LOW:
    case task_priority_t::MIDDLE:
    case task_priority_t::HIGH:
    case task_priority_t::CRITICAL:
    case task_priority_t::BLOCKING:
        return s_priority_enum_map[p];
        break;
    default:
        return s_priority_enum_map[task_priority_t::MIDDLE]; // as default value
        break;
    }
}

task_priority_t::task_priority_t(priority p)
    : m_value(p)
{
}

void task_priority_t::set_value(priority p)
{
    m_value = p;
}

task_priority_t::priority task_priority_t::value() const
{
    return m_value;
}

// status
bool task_status_t::s_initialized = false;
std::map<task_status_t::status, QString> task_status_t::s_status_enum_map;

void task_status_t::initialize_enum_map_()
{
    if (s_initialized) { return; }
    s_initialized = true;
    s_status_enum_map[task_status_t::NOT_STARTED] = QObject::tr("not started");
    s_status_enum_map[task_status_t::IN_PROGRESS] = QObject::tr("in progress");
    s_status_enum_map[task_status_t::CLOSED] = QObject::tr("closed");
    s_status_enum_map[task_status_t::REOPENED] = QObject::tr("reopened");
    s_status_enum_map[task_status_t::WONT_FIX] = QObject::tr("won't fix");
    s_status_enum_map[task_status_t::NOT_REPRO] = QObject::tr("not reproducible");
    s_status_enum_map[task_status_t::COMPLETED] = QObject::tr("completed");
}

QString task_status_t::get_status_name_by_enum(status p)
{
    initialize_enum_map_();
    switch (p)
    {
    case task_status_t::NOT_STARTED:
    case task_status_t::IN_PROGRESS:
    case task_status_t::CLOSED:
    case task_status_t::REOPENED:
    case task_status_t::WONT_FIX:
    case task_status_t::NOT_REPRO:
    case task_status_t::COMPLETED:
        return s_status_enum_map[p];
        break;
    default:
        return s_status_enum_map[task_status_t::NOT_STARTED]; // as default
        break;
    }
}

task_status_t::task_status_t(status s)
    : m_value(s)
{

}

void task_status_t::set_value(status s)
{
    m_value = s;
}

task_status_t::status task_status_t::value() const
{
    return m_value;
}

// task type
bool task_type_t::s_initialized = false;
std::map<task_type_t::type, QString> task_type_t::s_type_enum_map;

void task_type_t::initialize_enum_map_()
{
    if (s_initialized) { return; }
    s_initialized = true;
    s_type_enum_map[task_type_t::TASK] = QObject::tr("task");
    s_type_enum_map[task_type_t::BUG] = QObject::tr("bug");
    s_type_enum_map[task_type_t::ENHANCEMENT] = QObject::tr("enhancement");
    s_type_enum_map[task_type_t::FEATURE] = QObject::tr("feature");
    s_type_enum_map[task_type_t::INQUIRY] = QObject::tr("inquiry");
}

QString task_type_t::get_type_name_by_enum(type t)
{
    initialize_enum_map_();
    switch (t)
    {
    case task_type_t::TASK:
    case task_type_t::BUG:
    case task_type_t::ENHANCEMENT:
    case task_type_t::FEATURE:
    case task_type_t::INQUIRY:
        return s_type_enum_map[t];
        break;
    default:
        return s_type_enum_map[task_type_t::TASK];
        break;
    }
}

task_type_t::task_type_t(type t)
    : m_value(t)
{

}

void task_type_t::set_value(type t)
{
    m_value = t;
}

task_type_t::type task_type_t::value() const
{
    return m_value;
}

// task area
bool task_area_t::s_initialized = false;
std::map<task_area_t::area, QString> task_area_t::s_area_enum_map;

void task_area_t::initialize_enum_map_()
{
    if (s_initialized) { return; }
    s_initialized = true;
    s_area_enum_map[task_area_t::CODE] = QObject::tr("code");
    s_area_enum_map[task_area_t::DOCUMENTATION] = QObject::tr("documentation");
    s_area_enum_map[task_area_t::DATABASE] = QObject::tr("database");
    s_area_enum_map[task_area_t::USER_INTERFACE] = QObject::tr("user interface");
    s_area_enum_map[task_area_t::TEST] = QObject::tr("test");
    s_area_enum_map[task_area_t::MISCELLANEOUS] = QObject::tr("miscellaneous");
}

QString task_area_t::get_area_name_by_enum(area a)
{
    switch (a)
    {
    case task_area_t::CODE:
    case task_area_t::DOCUMENTATION:
    case task_area_t::DATABASE:
    case task_area_t::USER_INTERFACE:
    case task_area_t::TEST:
    case task_area_t::MISCELLANEOUS:
        return s_area_enum_map[a];
        break;
    default:
        return s_area_enum_map[task_area_t::CODE];
        break;
    }
}

task_area_t::task_area_t(area a)
{
    m_value = a;
}

void task_area_t::set_value(area a)
{
    m_value = a;
}

task_area_t::area task_area_t::value() const
{
    return m_value;
}

// task datetime operation

bool operator==(const task_datetime_operation_t& a, const task_datetime_operation_t& b)
{
    return a.id() == b.id();
}

task_datetime_operation_t::task_datetime_operation_t()
    : m_type(NONE),
      m_previous_type(NONE),
      m_datetime(QDateTime::currentDateTime()),
      m_author(NULL)
{
}

task_datetime_operation_t::task_datetime_operation_t(operation o, const QDateTime& dt, user* u)
    : m_type(o),
      m_previous_type(NONE),
      m_datetime(dt),
      m_author(u)
{
    Q_ASSERT(0 != m_author);
}

void task_datetime_operation_t::set_operation_type(operation o)
{
    m_type = o;
}

task_datetime_operation_t::operation task_datetime_operation_t::operation_type() const
{
    return m_type;
}

void task_datetime_operation_t::set_previous_operation_type(operation o)
{
    m_previous_type = o;
}

task_datetime_operation_t::operation task_datetime_operation_t::previous_operation_type() const
{
    return m_previous_type;
}

void task_datetime_operation_t::set_datetime(const QDateTime& dt)
{
    m_datetime = dt;
}

QDateTime task_datetime_operation_t::datetime() const
{
    return m_datetime;
}

void task_datetime_operation_t::set_author(user *u)
{
    Q_ASSERT(0 != u);
    m_author = u;
}

user* task_datetime_operation_t::author()
{
    return m_author;
}

//json-mson functions of work_period
QString work_period::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("start", m_start.toString("dd:MM:yyyy hh:mm:ss"));
    j_obj.insert("pause", m_pause.toString("dd:MM:yyyy hh:mm:ss"));

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void work_period::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    m_start = QDateTime::fromString(j_obj.value("start").toString(), "dd:MM:yyyy hh:mm:ss");
    m_pause = QDateTime::fromString(j_obj.value("pause").toString(), "dd:MM:yyyy hh:mm:ss");
    return;
}

QString work_period::object_type() const
{
    return "work_period";
}

//json-msons of task_datetime_operation_t
QString task_datetime_operation_t::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("type", m_type);
    j_obj.insert("previous_type", m_previous_type);
    j_obj.insert("datetime", m_datetime.toString("dd:MM:yyyy hh:mm:ss"));
    j_obj.insert("author", m_author->id());

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void task_datetime_operation_t::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    this->set_operation_type(task_datetime_operation_t::operation(j_obj.value("type").toInt()));
    this->set_previous_operation_type(task_datetime_operation_t::operation(j_obj.value("previous_type").toInt()));
    this->set_datetime(QDateTime::fromString(j_obj.value("datetime").toString(), "dd:MM:yyyy hh:mm:ss"));

    user* tmp = kokain::db::db_manager::get_instance()->give_user_with_id(j_obj.value("author").toInt());
    this->set_author(tmp);
    return;
}

QString task_datetime_operation_t::object_type() const
{
    return "task_datetime_operation_t";
}

} // namespace core

} // namespace kokain
