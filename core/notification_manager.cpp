#include "notification_manager.h"

#include <QTimer>

#include "notification.h"
#include "user.h"

namespace kokain {

namespace core {

notification_manager* notification_manager::m_notification_manager_instance;

notification_manager::notification_manager()
{
    this->m_db_manager = kokain::db::db_manager::get_instance();
    m_timer = new QTimer(this);
    m_timer->setInterval(10000);
    //m_timer->setSingleShot(false);
    connect(m_timer, SIGNAL(timeout()),
            this, SIGNAL(request_new_notifications()));
    m_timer->start();
}

notification_manager::~notification_manager()
{
}

notification_manager* notification_manager::get_notification_manager_instance()
{
    if(m_notification_manager_instance == NULL){
        m_notification_manager_instance = new notification_manager();
    }
    return m_notification_manager_instance;
}

kokain::db::db_manager* notification_manager::get_db_manager()
{
    return m_db_manager;
}

void notification_manager::make_notification(QSharedPointer<notification> the_notification)
{
    Q_ASSERT(NULL == the_notification);
    the_notification->set_notification_text(this->generate_notification_text(the_notification));

    kokain::db::query query_obj(kokain::db::query::INSERT, "notifications");
    query_obj.set_value(the_notification->serialize_to_json());
    this->get_db_manager()->execute(query_obj);
}

QString notification_manager::generate_notification_text(QSharedPointer<notification> the_notification)
{
    Q_ASSERT(NULL == the_notification);
    QString text;
    notification::notification_type type = the_notification->get_notification_type();
    switch(type)
    {
    case notification::NEW_MEMBER:
        text += " added new member to ";
        break;
    case notification::DEADLINE_CHANGED:
        text += " changed deadline of ";
        break;
    case notification::NAME_CHANGED:
        text += " changed name of ";
        break;
    case notification::DESCRIPTION_CHANGED:
        text += " changed deadline of ";
        break;
    case notification::RESOURCE_CHANGED:
        text += " changed resources of ";
        break;
    case notification::NEW_TASK:
        text += " added new task on ";
        break;
    case notification::MEMBER_REMOVED:
        text += " removed member from ";
        break;
    case notification::PROJECT_REMOVED:
        text += " removed project ";
        break;
    case notification::TASK_REMOVED:
        text += " removed task ";
        break;
    case notification::COMMENT:
        text += " commented on ";
        break;
    };

    return text;
}

void notification_manager::remove_notification(const QString& not_id)
{
    QSharedPointer<notification> tmp_not;
    foreach (tmp_not, m_notifications_list) {
        if(tmp_not->id() == not_id){
            tmp_not->delete_notification();
            //m_notifications_list.removeOne(tmp_not); operator== must be
            return;
        }
    }
}

void notification_manager::set_as_read_notification(const QString& not_id)
{
    QSharedPointer<notification> tmp_not;
    foreach (tmp_not, m_notifications_list) {
        if(tmp_not->id() == not_id){
            tmp_not->set_notification_as_read();
            return;
        }
    }
}

void notification_manager::set_as_unread_notification(const QString& not_id)
{
    QSharedPointer<notification> tmp_not;
    foreach (tmp_not, m_notifications_list) {
        if(tmp_not->id() == not_id){
            tmp_not->set_notification_as_unread();
            return;
        }
    }
}

void notification_manager::change_notification_status(const QString& notification_id,
                                                      notification::notification_status status)
{
    QSharedPointer<notification> pnot;
    foreach (pnot, m_notifications_list) {
        if(pnot->id() == notification_id){
            pnot->set_notification_status(status);
            return;
        }
    }
}

void notification_manager::get_notifications_from_db(qint32 num_of_not_to_get, qint32 current_user_id)
{
   kokain::db::query query_obj(kokain::db::query::SELECT, "notifications"); // needs mysql LIMIT
   kokain::db::condition condition_obj("content", "MATCH", current_user_id); // in targets list
   query_obj.set_condition(condition_obj);
   QSqlQuery result = this->get_db_manager()->execute(query_obj);
   while(result.next()){
       QString json_not = result.value(0).toString();
       QSharedPointer<notification> tmp_notification;
       tmp_notification->deserialize_from_json(json_not);
       m_notifications_list.push_back(tmp_notification);
   }
}

QList<QSharedPointer<notification> > notification_manager::get_notifications_list()
{
    return m_notifications_list;
}

qint32 notification_manager::get_num_of_unread_notifications()
{
    kokain::db::query query_obj(kokain::db::query::SELECT, "notifications"); // need COUNT function
    kokain::db::condition condition_obj("content", "MATCH", "unread"); // notification status is unread
    query_obj.set_condition(condition_obj);
    QSqlQuery result = this->get_db_manager()->execute(query_obj);
    // todo
}

} // core
} // kokain
