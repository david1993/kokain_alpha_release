#ifndef TEAM_H
#define TEAM_H

#include <QString>
#include <QVector>

#include "entity.h"

namespace kokain
{
namespace core
{
class user;

/// @class team - Represents a collection of users
class team : public entity
{
public:
    team();
    explicit team(const QString& t);

private:
    void initialize_();

    /// @brief Title and description
public:
    void set_title(const QString& t);
    QString title() const;
    void set_description(const QString& d);
    QString description() const;

private:
    QString m_title;
    QString m_description;

    /// @brief People
    /// @todo Later should be added 'sharing' properties
public:
    void set_manager(user* u);
    user* manager();

    void set_members(const QVector<user*>& ms);
    void add_member(user* u);
    void remove_member(user* u);
    QVector<user*> members();

private:
    user* m_manager;
    QVector<user*> m_members;

private:
    const static QString k_team_title_default;
    const static QString k_team_description_default;

    //json_mson
public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

private:
    team(const team& );
    team& operator=(const team&);
}; // class team

} // namespace core

} // namespace kokain

#endif // TEAM_H
