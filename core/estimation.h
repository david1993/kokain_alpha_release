#ifndef ESTIMATION_H
#define ESTIMATION_H

#include <QVector>
#include <QtGlobal>
#include <QDateTime>


namespace kokain
{

namespace core
{
class user;
class task;

/// @class estimation - Represents estimation-related information
class estimation
{
public:
    enum estimation_unit
    {
        MINUTE = 1,
        HOUR,
        DAY,
        WEEK,
        MONTH,
        unit_max
    };

    struct estim
    {
        qint32 m_current;
        QDateTime m_datetime;
        estimation_unit m_unit;
        user* m_author;
        task* m_task;
    };

    /// @brief Special member-functions
public:
    estimation();
    estimation(qint32 cur, const QDateTime& dt, estimation_unit unit, user* a, task* t);

    /// @brief Estimations
public:
    void add_estimation(estim obj);
    void add_estimation(qint32 cur, const QDateTime& dt, estimation_unit unit, user* a, task* t);
    void set_estimations(const QVector<estim>& estims);
    estim last_estimation();
    estim i_th_estimation(int i);//from 1 to size
    QVector<estim> all_estimations();
    qint32 count_of_estimations() const;

private:
    QVector<estim> m_estimations;

private:
    estimation(const estimation&);
    estimation& operator=(const estimation&);

}; // class estimation

} // namespace core

} // namespace kokain

#endif // ESTIMATION_H
