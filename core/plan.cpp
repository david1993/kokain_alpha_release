#include "plan.h"

namespace kokain
{

namespace core
{

const QString plan::k_plan_default_title = "Default plan";

plan::plan(const QString& t)
    : m_title(t)
{
    Q_ASSERT(!t.isEmpty());
}

void plan::set_title(const QString &t)
{
    m_title = t;
    Q_ASSERT(!m_title.isEmpty());
}

QString plan::title() const
{
    return m_title;
}

QString plan::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("id", id());
    j_obj.insert("title", title());

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void plan::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    set_id(j_obj.value("id").toString());
    set_title(j_obj.value("title").toString());
    return;
}

QString plan::object_type() const
{
    return "plan";
}

} // namespace core

} // namespace kokain
