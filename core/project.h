#ifndef PROJECT_H
#define PROJECT_H

#include <QString>
#include <QtGlobal>
#include <QVector>
#include <QDateTime>

#include "entity.h"

namespace kokain
{

namespace core
{

class user;
class task;

class project;

bool operator==(const project& a, const project& b);

class project : public entity
{
public:
    project();
//    explicit project(const QString &id);
    explicit project(const QString id, const QString& name = "(untitled project)");
    explicit project(const QString &name);
    ~project();

    // construction helpers
private:
    void initialize_();

    /// @brief Static components, name, description
public:
    QString name() const;
    void set_name(const QString& name);
    QString description() const;
    void set_description(const QString& desc);

private:
    QString m_name;
    QString m_description;

    /// @brief Deadline
    /// @todo This should be modified after plans and milestones
public:
    QDateTime deadline() const;
    void set_deadline(const QDateTime& dt);

private:
    QDateTime m_deadline;

    /// @brief Tasks
public:
    void set_tasks(const QVector<task*>& tv);
    QVector<task*> tasks();
    void add_task(task* t);
    void remove_task(task* t);
    bool has_task(task* t) const;

private:
    QVector<task*> m_tasks;

    /// @brief Overrided from shareable via entity
public:
    virtual entity* current_entitiy();

    //json-mson
public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

    // to copy use clone() member function
private:
    project(const project&);
    project& operator=(const project&);

}; // class project

} // namespace core

} // namespace kokain

#endif // PROJECT_H
