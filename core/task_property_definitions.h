#ifndef TASK_PROPERTY_DEFINITIONS_H
#define TASK_PROPERTY_DEFINITIONS_H

#include <map>

#include <QString>
#include <QDateTime>

#include "entity.h"

namespace kokain
{

namespace core
{
class user;

// TODO: later this kind entities will expand to include analysis and sharing
/// @struct task_priority_t Holds task's priority values
struct task_priority_t
{
    enum priority
    {
        LOW = 1,
        MIDDLE,
        HIGH,
        CRITICAL,
        BLOCKING,
        priority_max
    };

public:
    task_priority_t(priority p = MIDDLE);

public:
    void set_value(priority p);
    priority value() const;

private:
    priority m_value;

public:
    static QString get_priority_name_by_enum(priority p);

private:
    static void initialize_enum_map_();

private:
    static bool s_initialized;
    static std::map<priority, QString> s_priority_enum_map;

}; // struct task_priority_t

/// @struct task_status_t Holds task's status values
struct task_status_t
{
    enum status
    {
        NOT_STARTED = 1,
        IN_PROGRESS,
        CLOSED,
        REOPENED,
        WONT_FIX,
        NOT_REPRO,
        COMPLETED,
        status_max
    };

public:
    task_status_t(status s = NOT_STARTED);

public:
    void set_value(status s);
    status value() const;

private:
    status m_value;

public:
    static QString get_status_name_by_enum(status p);

private:
    static void initialize_enum_map_();

private:
    static bool s_initialized;
    static std::map<status, QString> s_status_enum_map;

}; // struct task_status_t

/// @struct task_type_t Holds task's type values
struct task_type_t
{
    enum type
    {
        TASK = 1,
        BUG,
        ENHANCEMENT,
        FEATURE,
        INQUIRY,
        type_max
    };

    task_type_t(type t = TASK);

public:
    void set_value(type t);
    type value() const;

private:
    type m_value;

public:
    static QString get_type_name_by_enum(type t);

private:
    static void initialize_enum_map_();

private:
    static bool s_initialized;
    static std::map<type, QString> s_type_enum_map;

}; // struct task_type_t

/// @struct task_area_t Holds task's area values
struct task_area_t
{
    enum area
    {
        CODE = 1,
        DOCUMENTATION,
        DATABASE,
        USER_INTERFACE,
        TEST,
        MISCELLANEOUS,
        area_max
    };

    task_area_t(area a);

public:
    void set_value(area a);
    area value() const;

private:
    area m_value;

public:
    static QString get_area_name_by_enum(area a);

private:
    static void initialize_enum_map_();

private:
    static bool s_initialized;
    static std::map<area, QString> s_area_enum_map;

}; // struct task_area_t

// task-related dates and times

struct work_period : public entity
{
    work_period()
    {}

    work_period(const QDateTime& s, const QDateTime& p)
        : m_start(s),
          m_pause(p)
    {
    }

    QDateTime m_start;
    QDateTime m_pause;

    qint64 difference_in_seconds() const
    {
        return m_start.secsTo(m_pause);
    }

    qint64 difference_in_minutes() const
    {
        return difference_in_seconds() / k_seconds;
    }

    qint64 difference_in_hours() const
    {
        return difference_in_minutes() / k_seconds;
    }

    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

    const static qint64 k_seconds = 60;

}; // struct work_period

/**
 * @brief The task_datetime_operation_t struct
 * Named operation because it describes the activity like closing the task,
 * or reopening it etc.
 */
struct task_datetime_operation_t : public entity
{
    /// @todo may be extended
    enum operation
    {
        NONE = 1,
        START,
        END,
        CLOSE,
        OPEN,
        REOPEN,
        operation_max
    };

public:
    task_datetime_operation_t();
    task_datetime_operation_t(operation o, const QDateTime&, user* u);

    /// @brief Operation (start, end, close, open, etc.)
public:
    void set_operation_type(operation o);
    operation operation_type() const;

    void set_previous_operation_type(operation o);
    operation previous_operation_type() const;

private:
    operation m_type;
    operation m_previous_type;

    /// @brief Datetime
public:
    void set_datetime(const QDateTime& dt);
    QDateTime datetime() const;

private:
    QDateTime m_datetime;

    /// @brief Author
public:
    void set_author(user* u);
    user* author();

private:
    user* m_author;

    //json functions
public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

private:
    task_datetime_operation_t(const task_datetime_operation_t&);
    task_datetime_operation_t& operator=(const task_datetime_operation_t&);

}; // struct task_datetime_operation_t


} // namespace core

} // namespace kokain

#endif // TASK_PROPERTY_DEFINITIONS
