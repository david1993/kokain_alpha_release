#include "team.h"

#include <QObject>

#include "../core/user.h"
#include "../db/db_manager.h"

namespace kokain
{

namespace core
{

const QString team::k_team_title_default = QObject::tr("(untitled team)");
const QString team::k_team_description_default = QObject::tr("(no description)");

bool operator==(const team& a, const team& b)
{
    return a.id() == b.id();
}

team::team()
    : m_title(k_team_title_default),
      m_description(k_team_description_default),
      m_manager(NULL)
{
    initialize_();
}

team::team(const QString &t)
    : m_title(t),
      m_description(k_team_description_default),
      m_manager(NULL)
{
    initialize_();
}

void team::initialize_()
{
    if (m_title.isEmpty()) {
        m_title = k_team_title_default;
    }
}

void team::set_title(const QString &t)
{
    m_title = ( t.isEmpty() ) ? k_team_title_default : t;
}

QString team::title() const
{
    return m_title;
}

void team::set_description(const QString &d)
{
    m_description = ( d.isEmpty() ) ? k_team_description_default : d;
}

QString team::description() const
{
    return m_description;
}

void team::set_manager(user *u)
{
    Q_ASSERT(0 != u);
    m_manager = u;
}

user* team::manager()
{
    return m_manager;
}

void team::set_members(const QVector<user *> &ms)
{
    if (!ms.empty()) {
        m_members = ms;
    }
}

void team::add_member(user *u)
{
    Q_ASSERT(0 != u);
    m_members.push_back(u);
}

void team::remove_member(user *u)
{
    for (QVector<user*>::iterator it = m_members.begin();
         it != m_members.end(); ++it)
    {
        const user* tmp = *it;
        if (tmp->id() == u->id()) {
            m_members.erase(it);
            break;
        }
    }
}

QVector<user*> team::members()
{
    return m_members;
}

QString team::serialize_to_json() const
{
    QJsonObject j_obj;

    QJsonArray j_arr_members;
    for(QVector<user*>::const_iterator it = m_members.begin(); it != m_members.end(); ++it)
    {
        user* tmp = *it;
        j_arr_members.push_back(tmp->id());
    }

    j_obj.insert("title", m_title);
    j_obj.insert("description", m_description);
    j_obj.insert("manager", m_manager->id());
    j_obj.insert("members", j_arr_members);

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void team::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    this->set_title(j_obj.value("title").toString());
    this->set_description(j_obj.value("description").toString());

    user* tmp_mng = kokain::db::db_manager::get_instance()->give_user_with_id(j_obj.value("manager").toInt());
    this->set_manager(tmp_mng);

    QJsonArray j_arr_members = j_obj.value("members").toArray();
    for(QJsonArray::iterator it = j_arr_members.begin(); it != j_arr_members.end(); ++it)
    {
        user* tmp = kokain::db::db_manager::get_instance()->give_user_with_id((*it).toInt());
        m_members.push_back(tmp);
    }
    return;
}

QString team::object_type() const
{
    return "team";
}

} // namespace core

} // namespace kokain
