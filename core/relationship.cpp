#include "relationship.h"

#include "task.h"

namespace kokain
{

namespace core
{

bool operator==(task_relationship& a, task_relationship& b)
{
    return (*a.source() == *b.source())
            && (*a.target() == *b.target())
            && (a.type() == b.type());
}

// TODO: see .h's @todo
relationship::relationship()
{
}

task_relationship::task_relationship()
    : m_source(NULL),
      m_target(NULL),
      m_type(task_relationship::UNKNOWN)
{

}

task_relationship::task_relationship(relationship_type t)
    : m_source(NULL),
      m_target(NULL),
      m_type(t)
{

}

task_relationship::task_relationship(task *s, task *t, relationship_type rt)
    : m_source(s),
      m_target(t),
      m_type(rt)
{
}

void task_relationship::set_source(task *s)
{
    Q_ASSERT(0 != s);
    m_source = s;
}

task* task_relationship::source()
{
    return m_source;
}

void task_relationship::set_target(task *t)
{
    Q_ASSERT(0 != t);
    m_target = t;
}

task* task_relationship::target()
{
    return m_target;
}

void task_relationship::set_type(relationship_type e)
{
    m_type = e;
}

task_relationship::relationship_type task_relationship::type() const
{
    return m_type;
}

} // namespace core

} // namespace kokain
