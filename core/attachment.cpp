#include "attachment.h"

namespace kokain
{

namespace core
{

attachment::attachment(const QString& t)
    : m_title(t)
{
    Q_ASSERT(!m_title.isEmpty());
}

void attachment::set_title(const QString &t)
{
    m_title = t;
    Q_ASSERT(!m_title.isEmpty());
}

QString attachment::title() const
{
    return m_title;
}

} // namespace core

} // namespace kokain
