#include "task.h"

#include "entity.h"
#include "project.h"
#include "user.h"
#include "estimation.h"
#include "team.h"
#include "comment.h"
#include "attachment.h"
#include "plan.h"
#include "../db/db_manager.h"

namespace kokain
{

namespace core
{

bool operator==(const task& a, const task& b)
{
    return a.id() == b.id(); // thinking to remove this, as entity::operator== is called
}

const QString task::k_default_task_title = QObject::tr("(untitled task)");
const QString task::k_default_task_description = QObject::tr("(no description available)");

task::task()
    : m_project(NULL),
      m_title(k_default_task_title),
      m_description(k_default_task_description),
      m_priority(task_priority_t::MIDDLE),
      m_status(task_status_t::NOT_STARTED),
      m_area(task_area_t::CODE),
      m_estimation(NULL),
      m_author(NULL),
      m_spent_hours(Q_INT64_C(0)),
      m_progress(0)
{
}

task::task(const QString &n)
    : m_project(NULL),
      m_title(n),
      m_description(k_default_task_description),
      m_priority(task_priority_t::MIDDLE),
      m_status(task_status_t::NOT_STARTED),
      m_area(task_area_t::CODE),
      m_estimation(NULL),
      m_author(NULL),
      m_spent_hours(Q_INT64_C(0)),
      m_progress(0)
{
    initialize_();
}

task::task(project *p)
    : m_project(p),
      m_title(k_default_task_title),
      m_description(k_default_task_description),
      m_priority(task_priority_t::MIDDLE),
      m_status(task_status_t::NOT_STARTED),
      m_area(task_area_t::CODE),
      m_estimation(NULL),
      m_author(NULL),
      m_spent_hours(Q_INT64_C(0)),
      m_progress(0)
{
    initialize_();
}

task::task(project *p, const QString &n)
    : m_project(p),
      m_title(n),
      m_description(k_default_task_description),
      m_priority(task_priority_t::MIDDLE),
      m_status(task_status_t::NOT_STARTED),
      m_area(task_area_t::CODE),
      m_estimation(NULL),
      m_author(NULL),
      m_spent_hours(Q_INT64_C(0)),
      m_progress(0)
{
    initialize_();
}

void task::initialize_()
{
    if (m_title.isEmpty()) {
        m_title = k_default_task_title;
    }
    // TODO: move this to initialization list
    m_spent_hours = Q_INT64_C(0);
}

void task::set_owner_project(project *p)
{
    Q_ASSERT(0 != p);
    m_project = p;
}

project* task::owner_project()
{
    return m_project;
}

void task::set_title(const QString &t)
{
    m_title = (t.isEmpty() ) ? k_default_task_title : t;
}

QString task::title() const
{
    return m_title;
}

void task::set_description(const QString &d)
{
    m_description = (d.isEmpty() ) ? k_default_task_description : d;
}

QString task::description() const
{
    return m_description;
}

void task::set_priority(task_priority_t p)
{
    m_priority = p;
}

task_priority_t task::priority() const
{
    return m_priority;
}

void task::set_status(task_status_t s)
{
    m_status = s;
}

task_status_t task::status() const
{
    return m_status;
}

void task::set_area(task_area_t a)
{
    m_area = a;
}

task_area_t task::area() const
{
    return m_area;
}

void task::set_relationships(const QVector<task_relationship *> &v)
{
    if (!v.empty()) {
        m_relationships.clear();
        m_relationships = v;
    }
}

void task::add_relationship(task_relationship *r)
{
    m_relationships.push_back(r);
}

void task::remove_relationship(task_relationship *r)
{
    for (QVector<task_relationship*>::iterator it = m_relationships.begin();
         it != m_relationships.end(); ++it)
    {
        if (*(*it) == *r) { // WTF? think, calls operator==
            m_relationships.erase(it);
            break;
        }
    }
}

QVector<task_relationship*> task::relationships()
{
    return m_relationships;
}

bool task::is_related_to(task *t)
{
    for (QVector<task_relationship*>::iterator it = m_relationships.begin();
         it != m_relationships.end(); ++it)
    {
        task_relationship* tmp = *it;
        if (*t == *(tmp->target())) {
            return true;
        }
    }
    return false;
}

void task::add_seen_user(user *u)
{
    m_seen_map[u] = true;
}

bool task::has_seen(user *u) const
{
    return ( m_seen_map.find(u) != m_seen_map.end() );
}

void task::set_task_estimation(estimation *e)
{
    Q_ASSERT(0 != e);
    m_estimation = e;
}

estimation* task::task_estimation()
{
    return m_estimation;
}

void task::set_author(user *u)
{
    Q_ASSERT(0 != u);
    m_author = u;
}

user* task::author()
{
    return m_author;
}

void task::set_assignees(QVector<user *> &as)
{
    if (!as.empty()) {
        m_assignees.clear();
        m_assignees = as;
    }
}

void task::add_assignee(user *u)
{
    Q_ASSERT(0 != u);
    m_assignees.push_back(u);
}

void task::remove_assignee(user *u)
{
    for (QVector<user*>::iterator it = m_assignees.begin();
         it != m_assignees.end(); ++it)
    {
        const user* tmp = *it;
        if (tmp->id() == u->id()) {
            m_assignees.erase(it);
            break;
        }
    }
}

QVector<user*> task::assignees()
{
    return m_assignees;
}

void task::set_followers(QVector<user *> &fl)
{
    if (!fl.empty()) {
        m_followers.clear(); // TODO: forgot to do this for the rest, FIX THEM!
        m_followers = fl;
    }
}

void task::add_follower(user *f)
{
    Q_ASSERT(0 != f);
    m_followers.push_back(f);
}

void task::remove_follower(user *f)
{
    for (QVector<user*>::iterator it = m_followers.begin();
         it != m_followers.end(); ++it)
    {
        const user* tmp = *it;
        if (tmp->id() == f->id()) {
            m_followers.erase(it);
            break;
        }
    }
}

QVector<user*> task::followers()
{
    return m_followers;
}

void task::set_teams(const QVector<team *> &tm)
{
    if (!tm.empty()) {
        m_teams.clear();
        m_teams = tm;
    }
}

void task::add_team(team *t)
{
    Q_ASSERT(0 != t);
    m_teams.push_back(t);
}

void task::remove_team(team *t)
{
    for (QVector<team*>::iterator it = m_teams.begin();
         it != m_teams.end(); ++it)
    {
        const team* tmp = *it;
        if (*tmp == *t) {
            m_teams.erase(it);
            break;
        }
    }
}

QVector<team*> task::teams()
{
    return m_teams;
}

void task::set_comments(const QVector<comment *> &cm)
{
    if (!cm.empty()) {
        m_comments.clear();
        m_comments = cm;
    }
}

void task::add_comment(comment *c)
{
    Q_ASSERT(0 != c);
    m_comments.push_back(c);
}

void task::remove_comment(comment *c)
{
    Q_ASSERT(0 != c);
    for (QVector<comment*>::iterator it = m_comments.begin();
         it != m_comments.end(); ++it)
    {
        const comment* tmp = *it;
        if (*tmp == *c) {
            m_comments.erase(it);
            break;
        }
    }
}

QVector<comment*> task::comments()
{
    return m_comments;
}

void task::set_work_periods(const QVector<work_period *> &wp)
{
    if (!wp.empty()) {
        m_work_periods.clear();
        m_work_periods = wp;
    }
}

void task::add_work_period(work_period *wp)
{
    Q_ASSERT(0 != wp);
    m_work_periods.push_back(wp);
    m_spent_hours += wp->difference_in_hours();
}

void task::remove_work_period(work_period *wp)
{
    Q_ASSERT(0 != wp);
    for (QVector<work_period*>::iterator it = m_work_periods.begin();
         it != m_work_periods.end(); ++it)
    {
        const work_period* tmp = *it;
        if (*tmp == *wp) {
            m_work_periods.erase(it);
            break;
        }
    }
}

QVector<work_period*> task::work_periods()
{
    return m_work_periods;
}

qint64 task::spent_hours() const
{
    return m_spent_hours;
}

qint64 task::spent_minutes() const
{
    return ( m_spent_hours * work_period::k_seconds );
}

void task::set_progress(qint32 p)
{
    m_progress = p;
}

qint32 task::progress()
{
    QDateTime start = datetime_for(task_datetime_operation_t::START);
    QDateTime end = datetime_for(task_datetime_operation_t::END);
    work_period tmp(start, end);
    qint64 common = tmp.difference_in_hours();
    m_progress = (qint32) ( (common * m_spent_hours)  / Q_INT64_C(10) );
    return m_progress;
}

void task::set_datetimes(const QVector<task_datetime_operation_t *> &tdt)
{
    if (!tdt.empty()) {
        m_datetimes.clear();
        m_datetimes = tdt;
    }
}

void task::add_datetime(task_datetime_operation_t *dt)
{
    Q_ASSERT(0 != dt);
    m_datetimes.push_back(dt);
}

void task::remove_datetime(task_datetime_operation_t *dt)
{
    Q_ASSERT(0 != dt);
    for (QVector<task_datetime_operation_t*>::iterator it = m_datetimes.begin();
         it != m_datetimes.end(); ++it)
    {
        const task_datetime_operation_t* cur = *it;
        if (*cur == *dt) {
            m_datetimes.erase(it);
            break;
        }
    }
}

QVector<task_datetime_operation_t*> task::datetimes()
{
    return m_datetimes;
}

QDateTime task::datetime_for(task_datetime_operation_t::operation op) const
{
    QVector<task_datetime_operation_t*>::const_iterator cit = m_datetimes.begin();
    for ( ; cit != m_datetimes.end(); ++cit)
    {
        const task_datetime_operation_t* cur = *cit;
        if (cur->operation_type() == op) {
            return cur->datetime();
        }

    }
    return QDateTime();
}

void task::set_attachments(const QVector<attachment *> &at)
{
    if (!at.empty()) {
        m_attachments.clear();
        m_attachments = at;
    }
}

void task::add_attachment(attachment *a)
{
    Q_ASSERT(0 != a);
    m_attachments.push_back(a);
}

void task::remove_attachment(const attachment *a)
{
    Q_ASSERT(0 != a);
    QVector<attachment*>::iterator it = m_attachments.begin();
    for ( ; it != m_attachments.end(); ++it) {
        const attachment* tmp = *it;
        if (*tmp == *a) {
            m_attachments.erase(it);
            break;
        }
    }
}

bool task::has_attachment(const attachment *a) const
{
    Q_ASSERT(0 != a);
    QVector<attachment*>::const_iterator cit = m_attachments.begin();
    for ( ; cit != m_attachments.end(); ++cit) {
        const attachment* tmp = *cit;
        if (*tmp == *a) {
            return true;
        }
    }
    return false;
}

QVector<attachment*> task::attachments()
{
    return m_attachments;
}

void task::set_plans(const QVector<plan *> &pl)
{
    if (!pl.empty()) {
        m_plans.clear();
        m_plans = pl;
    }
}

void task::add_plan(plan *p)
{
    Q_ASSERT(0 != p);
    m_plans.push_back(p);
}

void task::remove_plan(const plan *p)
{
    Q_ASSERT(0 != p);
    QVector<plan*>::iterator it = m_plans.begin();
    for ( ; it != m_plans.end(); ++it) {
        const plan* tmp = *it;
        if (*tmp == *p) {
            m_plans.erase(it);
            break;
        }
    }
}

bool task::has_plan(const plan *p) const
{
    Q_ASSERT(0 != p);
    QVector<plan*>::const_iterator cit = m_plans.begin();
    for ( ; cit != m_plans.end(); ++cit) {
        const plan* tmp = *cit;
        if (*tmp == *p) {
            return true;
        }
    }
    return false;
}

QVector<plan*> task::plans()
{
    return m_plans;
}

user *task::default_author()
{
    return m_author;
}

QString task::serialize_to_json() const
{
    QJsonObject j_obj;

    QJsonArray j_arr_relationships;
    for(QVector<task_relationship*>::const_iterator it = m_relationships.begin(); it != m_relationships.end(); ++it)
    {
        task_relationship* cur = *it;
        QJsonObject tmp_j_obj;
        tmp_j_obj.insert("target", cur->target()->id());
        tmp_j_obj.insert("type", cur->type());
        j_arr_relationships.push_back(tmp_j_obj);
    }

    QJsonArray j_arr_seen_map;
    for(QMap<user*, bool>::const_iterator it = m_seen_map.begin(); it != m_seen_map.end(); ++it)
    {
        QJsonObject tmp_j_obj;
        tmp_j_obj.insert("user", it.key()->id());
        tmp_j_obj.insert("bool", it.value());
        j_arr_seen_map.push_back(tmp_j_obj);
    }

    QJsonArray j_arr_estimations;
    QVector<estimation::estim> tmp = m_estimation->all_estimations();
    for(QVector<estimation::estim>::iterator it = tmp.begin(); it != tmp.end(); ++it)
    {
        estimation::estim tmp_est = *it;
        QJsonObject tmp_obj;
        tmp_obj.insert("current", tmp_est.m_current);
        tmp_obj.insert("date_time", tmp_est.m_datetime.toString("dd:MM:yyyy hh:mm:ss"));
        tmp_obj.insert("unit", tmp_est.m_unit);
        tmp_obj.insert("task", tmp_est.m_task->id());
        tmp_obj.insert("author", tmp_est.m_author->id());
        j_arr_estimations.push_back(tmp_obj);
    }

    QJsonArray j_arr_assignees;
    for(QVector<user*>::const_iterator it = m_assignees.begin(); it != m_assignees.end(); ++it)
    {
        user* tmp_user = *it;
        j_arr_assignees.push_back(tmp_user->id());
    }

    QJsonArray j_arr_followers;
    for(QVector<user*>::const_iterator it = m_followers.begin(); it != m_followers.end(); ++it)
    {
        user* tmp_user = *it;
        j_arr_followers.push_back(tmp_user->id());
    }

    QJsonArray j_arr_teams;
    for(QVector<team*>::const_iterator it = m_teams.begin(); it != m_teams.end(); ++it)
    {
        team* tmp_team = *it;
        j_arr_teams.push_back(tmp_team->id());
    }

    QJsonArray j_arr_comments;
    for(QVector<comment*>::const_iterator it = m_comments.begin(); it != m_comments.end(); ++it)
    {
        comment* tmp_comment = *it;
        j_arr_comments.push_back(tmp_comment->id());
    }

    QJsonArray j_arr_work_periods;
    for(QVector<work_period*>::const_iterator it = m_work_periods.begin(); it != m_work_periods.end(); ++it)
    {
        work_period* tmp_wp = *it;
        j_arr_work_periods.push_back(tmp_wp->serialize_to_json());
    }

    QJsonArray j_arr_tdot;
    for(QVector<task_datetime_operation_t*>::const_iterator it = m_datetimes.begin(); it != m_datetimes.end(); ++it)
    {
        task_datetime_operation_t* tmp_tdot = *it;
        j_arr_tdot.push_back(tmp_tdot->id());
    }

    QJsonArray j_arr_attachments;
    for(QVector<attachment*>::const_iterator it = m_attachments.begin(); it != m_attachments.end(); ++it)
    {
        attachment* tmp_att = *it;
        j_arr_attachments.push_back(tmp_att->id());
    }

    QJsonArray j_arr_plans;
    for(QVector<plan*>::const_iterator it = m_plans.begin(); it != m_plans.end(); ++it)
    {
        plan* tmp_plan = *it;
        j_arr_plans.push_back(tmp_plan->id());
    }

    j_obj.insert("id", this->id());
    j_obj.insert("project", m_project->id());
    j_obj.insert("title", title());
    j_obj.insert("description", description());
    j_obj.insert("priority", priority().value());
    j_obj.insert("status", status().value());
    j_obj.insert("area", area().value());
    j_obj.insert("relationships", j_arr_relationships);
    j_obj.insert("seen_map", j_arr_seen_map);
    j_obj.insert("estimations", j_arr_estimations);
    j_obj.insert("author", m_author->id());
    j_obj.insert("assignees", j_arr_assignees);
    j_obj.insert("followers", j_arr_followers);
    j_obj.insert("teams", j_arr_teams);
    j_obj.insert("comments", j_arr_comments);
    j_obj.insert("work_periods", j_arr_work_periods);
    j_obj.insert("spent_hours", m_spent_hours);
    j_obj.insert("progress", m_progress);
    j_obj.insert("task_datetime_operations_t", j_arr_tdot);
    j_obj.insert("attachments", j_arr_attachments);
    j_obj.insert("plans", j_arr_plans);

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void task::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    this->set_id(j_obj.value("id").toString());
    project* tmp_proj = dynamic_cast<project*>(kokain::db::db_manager::get_instance()->give_instance_with_id("project", j_obj.value("project").toString()));
    this->set_owner_project(tmp_proj);
    this->set_title(j_obj.value("title").toString());
    this->set_description(j_obj.value("description").toString());

    task_priority_t tmp_tpt(task_priority_t::priority(j_obj.value("priority").toInt()));
    this->set_priority(tmp_tpt);

    task_status_t tmp_tst(task_status_t::status(j_obj.value("status").toInt()));
    this->set_status(tmp_tst);

    task_area_t tmp_tat(task_area_t::area(j_obj.value("area").toInt()));
    this->set_area(tmp_tat);

    QJsonArray j_arr_relationships = j_obj.value("relationships").toArray();
    for(QJsonArray::iterator it = j_arr_relationships.begin(); it != j_arr_relationships.end(); ++it)
    {
        QJsonObject tmp_j_obj = (*it).toObject();
        task_relationship* tmp = new task_relationship();
        tmp->set_source(this);
        tmp->set_target(dynamic_cast<task*>(kokain::db::db_manager::get_instance()->give_instance_with_id("task", tmp_j_obj.value("target").toString())));
        tmp->set_type(task_relationship::relationship_type(tmp_j_obj.value("type").toInt()));
        m_relationships.push_back(tmp);
    }

    QJsonArray j_arr_seen_map = j_obj.value("seen_map").toArray();
    for(QJsonArray::iterator it = j_arr_seen_map.begin(); it != j_arr_seen_map.end(); ++it)
    {
        user* tmp_first = kokain::db::db_manager::get_instance()->give_user_with_id((*it).toObject().value("user").toInt());
        bool tmp_second = (*it).toObject().value("bool").toBool();
        m_seen_map[tmp_first] = tmp_second;
    }

    QJsonArray j_arr_estimations;
    for(QJsonArray::iterator it = j_arr_estimations.begin(); it != j_arr_estimations.end(); ++it)
    {
        estimation::estim tmp_est;
        tmp_est.m_current = qint32((*it).toObject().value("current").toInt());
        tmp_est.m_datetime = QDateTime::fromString((*it).toObject().value("date_time").toString(), "dd:MM:yyyy hh:mm:ss");
        tmp_est.m_unit = estimation::estimation_unit((*it).toObject().value("unit").toInt());
        tmp_est.m_task = dynamic_cast<task*>(kokain::db::db_manager::get_instance()->give_instance_with_id("task", (*it).toObject().value("task").toString()));
        tmp_est.m_author = kokain::db::db_manager::get_instance()->give_user_with_id((*it).toObject().value("author").toInt());
        m_estimation->add_estimation(tmp_est);
    }

    set_author(kokain::db::db_manager::get_instance()->give_user_with_id(j_obj.value("author").toInt()));

    QJsonArray j_arr_assignees = j_obj.value("assignees").toArray();
    for(QJsonArray::iterator it = j_arr_assignees.begin(); it != j_arr_assignees.end(); ++it)
    {
        user* tmp = kokain::db::db_manager::get_instance()->give_user_with_id((*it).toInt());
        m_assignees.push_back(tmp);
    }

    QJsonArray j_arr_followers = j_obj.value("followers").toArray();
    for(QJsonArray::iterator it = j_arr_followers.begin(); it != j_arr_followers.end(); ++it)
    {
        user* tmp = kokain::db::db_manager::get_instance()->give_user_with_id((*it).toInt());
        m_followers.push_back(tmp);
    }

    QJsonArray j_arr_teams = j_obj.value("teams").toArray();
    for(QJsonArray::iterator it = j_arr_teams.begin(); it != j_arr_teams.end(); ++it)
    {
        team* tmp = dynamic_cast<team*>(kokain::db::db_manager::get_instance()->give_instance_with_id("team", (*it).toString()));
        m_teams.push_back(tmp);
    }

    QJsonArray j_arr_comments = j_obj.value("comments").toArray();
    for(QJsonArray::iterator it = j_arr_comments.begin(); it != j_arr_comments.end(); ++it)
    {
        comment* tmp = dynamic_cast<comment*>(kokain::db::db_manager::get_instance()->give_instance_with_id("comment", (*it).toString()));
        m_comments.push_back(tmp);
    }

    QJsonArray j_arr_work_periods = j_obj.value("work_periods").toArray();
    for(QJsonArray::iterator it = j_arr_work_periods.begin(); it != j_arr_work_periods.end(); ++it)
    {
        work_period* tmp = new work_period();
        tmp->deserialize_from_json((*it).toString());
        m_work_periods.push_back(tmp);
    }

    this->m_spent_hours = qint64(j_obj.value("spent_hours").toInt());
    this->m_progress = qint32(j_obj.value("progress").toInt());

    QJsonArray j_arr_tdot = j_obj.value("task_datetime_operations_t").toArray();
    for(QJsonArray::iterator it = j_arr_tdot.begin(); it != j_arr_tdot.end(); ++it)
    {
        task_datetime_operation_t* tmp = new task_datetime_operation_t();
        kokain::db::query q(kokain::db::query::SELECT, "task_datetime_estimations_t");
        kokain::db::condition cond("content", "GLOB", "*\"id\": " + (*it).toString() + "*");
        q.set_condition(cond);
        QSqlQuery qr = kokain::db::db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            tmp->deserialize_from_json(qr.value(0).toString());
        }
        else
        {
            qDebug() << "********ERROR********";
            qDebug() << "task_datetime_ooperation_t is not found with this id ---> " << (*it).toString();
        }
        m_datetimes.push_back(tmp);
    }

    QJsonArray j_arr_attachment = j_obj.value("attachments").toArray();
    for(QJsonArray::iterator it = j_arr_attachment.begin(); it != j_arr_attachment.end(); ++it)
    {
        team* tmp = new team();
        kokain::db::query q(kokain::db::query::SELECT, "teams");
        kokain::db::condition cond("content", "GLOB", "*\"id\": " + (*it).toString() + "*");
        q.set_condition(cond);
        QSqlQuery qr = kokain::db::db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            tmp->deserialize_from_json(qr.value(0).toString());
        }
        else
        {
            qDebug() << "********ERROR********";
            qDebug() << "team is not found with this id ---> " << (*it).toString();
        }
        m_teams.push_back(tmp);
    }

    QJsonArray j_arr_plans = j_obj.value("plans").toArray();
    for(QJsonArray::iterator it = j_arr_plans.begin(); it != j_arr_plans.end(); ++it)
    {
        plan* tmp = new plan();
        kokain::db::query q(kokain::db::query::SELECT, "plans");
        kokain::db::condition cond("content", "GLOB", "*\"id\": " + (*it).toString() + "*");
        q.set_condition(cond);
        QSqlQuery qr = kokain::db::db_manager::get_instance()->execute(q);
        if(qr.next())
        {
            tmp->deserialize_from_json(qr.value(0).toString());
        }
        else
        {
            qDebug() << "********ERROR********";
            qDebug() << "plan is not found with this id ---> " << (*it).toString();
        }
        m_plans.push_back(tmp);
    }
    return;
}

QString task::object_type() const
{
    return "task";
}

} // namespace core

} // namespace kokain
