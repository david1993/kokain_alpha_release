#ifndef COMMENT_H
#define COMMENT_H

#include <QDateTime>
#include <QString>

#include "entity.h"
#include "user.h"

namespace kokain
{

namespace core
{

/**
 * @brief The comment class
 * This class doesn't store any information about its owners (like task*, project* etc)
 * It may be fixed later, but currently doesn't show me up as a problem.
 */
class comment : public entity
{
public:
    comment();
    explicit comment(const QString& t);
    comment(const QString& t, user* u, const QDateTime& dt);

    /// @brief All components (text, date-time, user)
public:
    void set_text(const QString& t);
    QString text() const;

    void set_author(user* u);
    const user* author() const;

    void set_datetime(const QDateTime& dt);
    QDateTime datetime() const;

private:
    QString m_text;
    user* m_author;
    QDateTime m_datetime;

    /// @brief Overridden public member-functions
public:
    virtual entity* current_entitiy();

    /// @brief Overridden serializing function
public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

private:
    comment(const comment&);
    comment& operator=(const comment&);

}; // class comment

} // namespace core

} // namespace kokain

#endif // COMMENT_H
