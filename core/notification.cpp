#include "notification.h"

#include "entity.h"

namespace kokain {

namespace core {

notification::notification()
{
}

notification::~notification()
{

}

QString notification::serialize_to_json() const
{
    QJsonObject j_obj;
    QJsonArray j_arr;

    j_obj.insert("id", id());
    j_obj.insert("sender", m_sender->serialize_to_json());
    j_obj.insert("text", get_notification_text());
    j_obj.insert("entity", m_entity->serialize_to_json());
    QList<qint32> targets_id = get_targets_list();
    qint32 target_id;
    foreach (target_id, targets_id) {
        j_arr.push_back(target_id);
    }
    j_obj.insert("targets", j_arr);
    j_obj.insert("status", get_notification_status());
    j_obj.insert("type", get_notification_type());
    j_obj.insert("datetime", get_datetime());

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void notification::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    QSharedPointer<user> tmp_sender;
    tmp_sender->deserialize_from_json(j_obj.value("sender").toString());
    this->set_sender(tmp_sender);
    this->set_notification_text(j_obj.value("text").toString());
    QSharedPointer<entity> tmp_entity;
    tmp_entity->deserialize_from_json(j_obj.value("entity").toString());
    this->set_entity(tmp_entity);
    QJsonArray j_arr_targets = j_obj.value("targets").toArray();
    for(QJsonArray::iterator it = j_arr_targets.begin(); it != j_arr_targets.end(); ++it){
        qint32 tmp_target = (*it).toInt();
        this->add_target(tmp_target);
    }
    notification_status tmp_status = notification_status(j_obj.value("status").toInt());
    this->set_notification_status(tmp_status);
    notification_type tmp_type = notification_type(j_obj.value("type").toInt());
    this->set_notification_type(tmp_type);
    this->set_datetime(j_obj.value("datetime").toString());
}

QString notification::object_type() const
{
    return "notification";
}

/*void notification::set_sender_id(qint32 sender_id)
{
    Q_ASSERT(sender_id < 1);
    m_sender_id = sender_id;
}

qint32 notification::get_sender_id() const
{
    Q_ASSERT(m_sender_id < 1);
    return m_sender_id;
}
*/
void notification::set_sender(QSharedPointer<user> sender)
{
    Q_ASSERT(NULL == sender);
    m_sender = sender;
}

QSharedPointer<user> notification::get_sender()
{
    Q_ASSERT(NULL == m_sender);
    return m_sender;
}

void notification::set_notification_text(const QString& text)
{
    Q_ASSERT(text.isEmpty());
    m_notification_text = text;
}

QString notification::get_notification_text() const
{
    Q_ASSERT(m_notification_text.isEmpty());
    return m_notification_text;
}

void notification::set_entity(QSharedPointer<entity> the_entity)
{
    m_entity = the_entity;
}

QSharedPointer<entity> notification::get_entity()
{
    return m_entity;
}

/*void notification::set_entity_id(const QString& entity_id)
{
    Q_ASSERT(entity_id.isEmpty());
    m_entity_id = entity_id;
}

QString notification::get_entity_id() const
{
    Q_ASSERT(m_entity_id.isEmpty());
    return m_entity_id;
}*/

void notification::set_targets_id(const QList<qint32>& targets_id)
{
    Q_ASSERT(targets_id.isEmpty());
    m_targets_list = targets_id;
}

void notification::add_target(qint32 target_id)
{
    Q_ASSERT(target_id < 1);
    m_targets_list.push_back(target_id);
}

bool notification::remove_target(qint32 target_id)
{
    Q_ASSERT(target_id < 1);
    //return m_targets_list.removeOne(target->id()); // operator== should be
}

QList<qint32> notification::get_targets_list() const
{
    Q_ASSERT(m_targets_list.isEmpty());
    return m_targets_list;
}

/*void notification::set_targets(const QList<QSharedPointer<user> >& targets)
{
    Q_ASSERT(targets.isEmpty());
    m_targets_list = targets;
}

void notification::add_target(QSharedPointer<user> target)
{
    Q_ASSERT(NULL == target);
    m_targets_list.push_back(target);
}

bool notification::remove_target(QSharedPointer<user> target)
{
    Q_ASSERT(NULL == target);
    //return m_targets_list.removeOne(target->id()); // operator== should be
}

QList<QSharedPointer<user> > notification::get_targets_list() const
{
    Q_ASSERT(m_targets_list.isEmpty());
    return m_targets_list;
}*/

void notification::set_notification_id(const QString& notification_id)
{
    Q_ASSERT(notification_id.isEmpty());
    m_notification_id = notification_id;
}

QString notification::get_notification_id() const
{
    Q_ASSERT(m_notification_id.isEmpty());
    return m_notification_id;
}

void notification::set_notification_status(notification_status status)
{
    m_notification_status = status;
}

void notification::set_notification_as_read()
{
    m_notification_status = READ;
}

void notification::set_notification_as_unread()
{
    m_notification_status = UNREAD;
}

void notification::delete_notification()
{
    m_notification_status = REMOVE;
}

notification::notification_status notification::get_notification_status() const
{
    return m_notification_status;
}

void notification::set_notification_type(notification_type type)
{
    m_notification_type = type;
}

notification::notification_type notification::get_notification_type() const
{
   return m_notification_type;
}

void notification::set_datetime(const QString& datetime)
{
    m_datetime = datetime;
}

QString notification::get_datetime() const
{
    return m_datetime;
}

} // core
} // kokain


