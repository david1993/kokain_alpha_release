#include "comment.h"

#include "../db/db_manager.h"

namespace kokain
{

namespace core
{

bool operator==(const comment& a, const comment& b)
{
    return a.id() == b.id();
}

comment::comment()
    : m_author(NULL),
      m_datetime(QDateTime::currentDateTime())
{
}

comment::comment(const QString &t)
    : m_text(t),
      m_author(NULL),
      m_datetime(QDateTime::currentDateTime())
{
}

comment::comment(const QString &t, user *u, const QDateTime &dt)
    : m_text(t),
      m_author(u),
      m_datetime(dt)
{
}

void comment::set_text(const QString &t)
{
    m_text = t;
}

QString comment::text() const
{
    return m_text;
}

void comment::set_author(user *u)
{
    Q_ASSERT(0 != u);
    m_author = u;
}

const user *comment::author() const
{
    return m_author;
}

void comment::set_datetime(const QDateTime &dt)
{
    m_datetime = dt;
}

QDateTime comment::datetime() const
{
    return m_datetime;
}

entity* comment::current_entitiy()
{
    return this;
}

QString comment::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("id", id());
    j_obj.insert("text", text());
    j_obj.insert("author", author()->id());
    j_obj.insert("datetime", datetime().toString("dd:MM:yyyy hh:mm:ss"));

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void comment::deserialize_from_json(const QString& serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    set_id(j_obj.value("id").toString());
    set_text(j_obj.value("text").toString());
    set_author(kokain::db::db_manager::get_instance()->give_user_with_id(qint64(j_obj.value("author").toInt())));
    set_datetime(QDateTime::fromString(j_obj.value("datetime").toString(), "dd:MM:yyyy hh:mm:ss"));
    return;
}

QString comment::object_type() const
{
    return "comment";
}

} // namespace core

} // namespace kokain
