#ifndef ENTITY_H
#define ENTITY_H

#include <vector>

#include <QtGlobal>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QStringList>

#include "shareable.h"

namespace kokain
{
namespace core
{

class entity;

bool operator==(const entity& e1, const entity& e2);

bool operator<(const entity& e1, const entity& e2);

//class tag;
//class reference;

class entity : public shareable
{
public:
    entity();
    virtual ~entity();

    /// @brief ID
public:
    void set_id(const QString& _id); // NOTE: should be used carefully!!
    QString id() const;

    /// @brief Overrided member-functions (from shareable)
public:
    virtual QString entity_id() const;
    virtual entity* current_entity();

protected:
    virtual user* default_author();

private:
    QString m_id;

    /// @brief Pure virtual functions referred to json-like serializations
public:
    virtual QString serialize_to_json() const = 0;
    virtual void deserialize_from_json(const QString& serialized) = 0;
    virtual QString object_type() const = 0;

public:
    static void initialize_id_counter(qint32 id);
    static qint32 get_last_id();

private:
    static qint64 s_id_counter;
    QString user_id;

private:
    entity(const entity&);
    entity& operator=(const entity&);

}; // class entity

} // namespace core

} // namespace kokain

#endif // ENTITY_H
