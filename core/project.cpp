#include "project.h"

#include <QDebug>

#include "task.h"
#include "../db/db_manager.h"

namespace kokain
{

namespace core
{

bool operator==(const project& a, const project& b)
{
    return a.id() == b.id();
}

project::project()
    : /*m_id(Q_INT64_C(0)),*/
      m_name("(untitled project)"),
      m_description("(no description)")
{
    initialize_();
}

//project::project(const QString& id)
//    : /*m_id(id),*/
//      m_name("(untitled project)"),
//      m_description("(no description")
//{
//    this->set_id(id);
//    initialize_();
//}

project::project(const QString id, const QString &name)
    : /*m_id(id),*/
      m_name(name),
      m_description("(no description)")
{
    this->set_id(id);
    initialize_();
}

project::project(const QString &name)
    : /*m_id(Q_INT64_C(0)),*/
      m_name(name),
      m_description("(no description)")
{
    initialize_();
}

project::~project()
{

}

void project::initialize_()
{
    if (m_name.isEmpty()) {
        m_name = "(untitled project)";
    }
}

//quint64 project::id() const
//{
//    return m_id;
//}

//void project::set_id(quint64 id)
//{
//    m_id = id;
//}

QString project::name() const
{
    return m_name;
}

void project::set_name(const QString &name)
{
    m_name = name;
    if (m_name.isEmpty()) {
        m_name = "(untitled project)";
    }
}

QString project::description() const
{
    return m_description;
}

void project::set_description(const QString &desc)
{
    m_description = desc;
    if (m_description.isEmpty()) {
        m_description = "(no description)";
    }
}

QDateTime project::deadline() const
{
    return m_deadline;
}

void project::set_deadline(const QDateTime &dt)
{
    Q_ASSERT(!dt.isValid());
    m_deadline = dt;
}

void project::set_tasks(const QVector<task *> &tv)
{
    if (!tv.empty()) {
        m_tasks.clear();
        m_tasks = tv;
    }
}

QVector<task*> project::tasks()
{
    return m_tasks;
}

void project::add_task(task *t)
{
    Q_ASSERT(0 != t);
    m_tasks.push_back(t);
}

void project::remove_task(task *t)
{
    Q_ASSERT(0 != t);
    for (QVector<task*>::iterator it = m_tasks.begin(); it != m_tasks.end(); ++it)
    {
        const task* tmp = *it;
        if (*tmp == *t) {
            m_tasks.erase(it);
            break;
        }
    }
}

bool project::has_task(task *t) const
{
    QVector<task*>::const_iterator cit = m_tasks.begin();
    for ( ; cit != m_tasks.end(); ++cit) {
        const task* tmp = *cit;
        if (*tmp == *t) {
            return true;
        }
    }
    return false;
}

entity* project::current_entitiy()
{
    return this;
}

QString project::serialize_to_json() const
{
    QJsonObject j_obj;

    QJsonArray j_arr_tasks;
    for (QVector<task*>::const_iterator it = m_tasks.begin(); it != m_tasks.end(); ++it)
    {
        task* tmp = *it;
        j_arr_tasks.push_back(tmp->id());
    }

    j_obj.insert("id", id());
    j_obj.insert("name", name());
    j_obj.insert("description", description());
    j_obj.insert("tasks", j_arr_tasks);

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void project::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    set_id(j_obj.value("id").toString());
    set_name(j_obj.value("name").toString());
    set_description(j_obj.value("description").toString());

    QJsonArray tmp_arr = j_obj.value("tasks").toArray();
    for (QJsonArray::iterator it = tmp_arr.begin(); it != tmp_arr.end(); ++it)
    {
        task* tmp = dynamic_cast<task*>(kokain::db::db_manager::get_instance()->give_instance_with_id("task", (*it).toString()));
        m_tasks.push_back(tmp);
    }
    return;
}

QString project::object_type() const
{
    return "project";
}

} // namespace core

} // namespace kokain
