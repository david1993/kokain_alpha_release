#ifndef USER_H
#define USER_H

#include <vector>

#include <QString>
#include <QtWidgets>

#include "shareable.h"
#include "../calendar/calendar.h"

namespace kokain
{
namespace calendar_namespace
{
class user_data;
}
}

namespace kokain
{

namespace core
{

class entity;
class task;

class user : public QWidget
{
    Q_OBJECT
 public:
     enum online_status
     {
         ONLINE = 1,
         OFFLINE,
         INVISIBLE,
         BUSY,
         AWAY,
         online_status_max
     };

 public:
     explicit user(QWidget* widget = 0);

 private:
     void initilize_();
     void create_widgets_();
     void setup_layout_();


 private:
     QLabel* m_picture;
     QLabel* m_name;
     QPoint m_ptDragPos;
     QString m_indicator;
     calendar_namespace::user_data* m_data;
     calendar_namespace::calendar* m_user_calendar;
     online_status m_online_status;
     qint32 m_id;

 public:
     void set_name(QString);
     void set_picture(QPixmap picture);
     void set_pos(QPoint pos);

     QPoint get_pos();
     QString get_indicator();
     void set_indicator(QString ind);
     QString get_name() const;
     QPixmap get_picture() const;
     calendar_namespace::user_data* get_m_data();
     void set_online_status(online_status os);
     online_status get_online_status() const;
     void set_id(qint32 id);
     qint32 id() const;

 protected:
     void startDrag();
     virtual bool eventFilter(QObject *target, QEvent *event);

     /// @brief Tasks
 public:
     void set_active_task(task* t);
     task* active_task();
     void set_tasks(const std::vector<task*>& tv);
     void add_task(task* t);
     std::vector<task*> tasks();

 private:
     task* m_active_task;
     std::vector<task*> m_tasks;

     /// @brief User permissions
     /// @todo Also implement user roles
 public:
     void set_permission_for_entity(entity* e, permission_t::permission_type pt);
     void unset_permission_for_entity(entity* e, permission_t::permission_type pt);
     bool has_permission_for_entity(entity* e, permission_t::permission_type pt);
     void set_permission_object_for_entity(entity* e, const permission_t& po);
     void remove_permission_object_for_entity(entity* e);
     permission_t permission_object_for_entity(entity* e);

 public:
     void set_entity_permission_map(const std::map<QString, permission_t>& m);
     std::map<QString, permission_t> entity_permission_map() const;

 private:
     std::map<QString, permission_t> m_user_permissions;

 public:
     QString serialize_to_json() const;
     void deserialize_from_json(const QString &serialized);
     QString object_type() const;

 public slots:
     void user_data_changed();
     void calendar_showed();


 signals:
     void data_showed();
     void on_user_data_changed();

}; // class user

} // namespace core

} // namespace kokain

#endif // USER_H
