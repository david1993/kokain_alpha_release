#ifndef PLAN_H
#define PLAN_H

#include <QString>

#include "entity.h"

namespace kokain
{

namespace core
{

/// @class plan - Smoke it :P
/// @todo This class needs more attention, used mostly by @ref project
class plan : public entity
{
public:
    explicit plan(const QString& t = k_plan_default_title);

    /// @brief
public:
    void set_title(const QString& t);
    QString title() const;

private:
    QString m_title;

private:
    static const QString k_plan_default_title;

    //json-mson
public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

private:
    plan(const plan&);
    plan& operator=(const plan&);

}; // class plan

} // namespace core

} // namespace kokain

#endif // PLAN_H
