#include "estimation.h"

#include "task.h"
#include "user.h"

namespace kokain
{

namespace core
{

estimation::estimation()
{
}

estimation::estimation(qint32 cur, const QDateTime &dt, estimation_unit unit, user *a, task *t)
{
    estim tmp;
    tmp.m_author = a;
    tmp.m_task = t;
    tmp.m_datetime = dt;
    tmp.m_current = cur;
    tmp.m_unit = unit;
    m_estimations.push_back(tmp);
}

void estimation::add_estimation(estim obj)
{
    m_estimations.push_back(obj);
}

void estimation::add_estimation(qint32 cur, const QDateTime &dt, estimation_unit unit, user *a, task *t)
{
    estim tmp;
    tmp.m_author = a;
    tmp.m_task = t;
    tmp.m_datetime = dt;
    tmp.m_current = cur;
    tmp.m_unit = unit;
    m_estimations.push_back(tmp);
}

void estimation::set_estimations(const QVector<estim> &estims)
{
    m_estimations = estims;
}

estimation::estim estimation::last_estimation()
{
    return (*m_estimations.end());
}

estimation::estim estimation::i_th_estimation(int i)
{
    return m_estimations[i];
}

QVector<estimation::estim> estimation::all_estimations()
{
    return m_estimations;
}

qint32 estimation::count_of_estimations() const
{
    return m_estimations.size();
}

} // namespace core

} // namespace kokain
