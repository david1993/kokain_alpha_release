#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QObject>
#include <QDateTime>

#include "entity.h"
#include "user.h"

namespace kokain {

namespace core {

class notification : public entity
{
public:
    enum notification_status{
        READ = 1,
        UNREAD,
        REMOVE
    };

    enum notification_type{
        NEW_MEMBER = 1,
        DEADLINE_CHANGED,
        NAME_CHANGED,
        DESCRIPTION_CHANGED,
        RESOURCE_CHANGED,
        NEW_TASK,
        MEMBER_REMOVED,
        PROJECT_REMOVED,
        TASK_REMOVED,
        COMMENT
    };

public:
    notification();
    ~notification();

public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString& serialized);
    virtual QString object_type() const;

/*public:
    void set_sender_id(qint32 sender_id);
    qint32 get_sender_id() const;
private:
    qint32 m_sender_id;
*/
public:
    void set_sender(QSharedPointer<user> sender);
    QSharedPointer<user> get_sender();
private:
    QSharedPointer<user> m_sender;

public:
    void set_notification_text(const QString& text);
    QString get_notification_text() const;
private:
    QString m_notification_text;

/*public:
    void set_entity_id(const QString& entity_id);
    QString get_entity_id() const;
private:
    QString m_entity_id;
*/

public:
    void set_entity(QSharedPointer<entity> the_entity);
    QSharedPointer<entity> get_entity();
private:
    QSharedPointer<entity> m_entity;

public:
    void set_targets_id(const QList<qint32>& targets_id);
    void add_target(qint32 target_id);
    bool remove_target(qint32 target_id);
    QList<qint32> get_targets_list() const;
private:
    QList<qint32> m_targets_list;

/*public:
    void set_targets(const QList<QSharedPointer<user> >& targets);
    void add_target(QSharedPointer<user> target);
    bool remove_target(QSharedPointer<user> target);
    QList<QSharedPointer<user> > get_targets_list() const;
private:
    QList<QSharedPointer<user> > m_targets_list;
*/

public:
    void set_notification_id(const QString& notification_id);
    QString get_notification_id() const;
private:
    QString m_notification_id;

public:
    void set_notification_type(notification_type type);
    notification_type get_notification_type() const;
private:
    notification_type m_notification_type;

public slots:
    void set_notification_status(notification_status status);
    void set_notification_as_read();
    void set_notification_as_unread();
    void delete_notification();
    notification_status get_notification_status() const;
private:
    notification_status m_notification_status;

public:
    void set_datetime(const QString& datetime);
    QString get_datetime() const;
private:
    QString m_datetime;

}; // class notification
} // core
} // kokain

#endif // NOTIFICATION_H
