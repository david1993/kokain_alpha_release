#ifndef RELATIONSHIP_H
#define RELATIONSHIP_H

namespace kokain
{

namespace core
{

class task;

/// @todo This should be a base class for all relationships
/// @todo Dicuss with the team
class relationship
{
public:
    relationship();

}; // class relationship

class task_relationship;

bool operator==(task_relationship& a, task_relationship& b);

/// @class task_relationship Represents relationship info on tasks
class task_relationship
{
public:
    enum relationship_type
    {
        DEPENDENCY = 1,
        CHILD,
        PARENT,
        UNKNOWN,
        relationship_type_max
    };

public:
    task_relationship();
    explicit task_relationship(relationship_type t);
    task_relationship(task* s, task* t, relationship_type rt = DEPENDENCY);

    /// @brief Source and target
public:
    void set_source(task* s);
    task* source();
    void set_target(task* t);
    task* target();

private:
    task* m_source;
    task* m_target;

    /// @brief Relationship type
public:
    void set_type(relationship_type e);
    relationship_type type() const;

private:
    relationship_type m_type;

private:
    task_relationship(const task_relationship&);
    task_relationship& operator=(const task_relationship&);
};

} // namespace core

} // namespace kokain

#endif // RELATIONSHIP_H
