#ifndef SHAREABLE_H
#define SHAREABLE_H

#include <map>

#include <QtGlobal>

namespace kokain
{
namespace core
{

class user;
class entity;

/// @struct permission_t - Pair of permission tpye and the author, who set
/// the corresponding permission
struct permission_t
{
    enum permission_type
    {
        VIEW = 1,
        EDIT = 2,
        SHARE = 4,
        REMOVE = 8,
        EXPORT = 16,
        IMPORT = 32,
        ARCHIVE = 64,
        LOCK = 128,
        COPY = 256,
        MOVE = 512,
        permission_type_max
    };

public:
    permission_type type;
    user* author;
};

/// @class shareable - Holds sharing info for an entity
/// inherited by entity (almost everything that could be shared)
class shareable
{
public:
    shareable();
    virtual ~shareable() = 0;

    /// @brief Permission management
public:
    virtual void set_permission(user* u, permission_t::permission_type pt);
    virtual void unset_permission(user* u, permission_t::permission_type tp);
    virtual void set_permission_object(user* u, const permission_t& po);
    virtual void remove_permission_object(user* u);
    virtual bool has_permission(user* u, const permission_t& p);
    virtual permission_t permission_object_for_user(user* u);

public:
    void set_permission_map(const std::map<qint32, permission_t>&);
    std::map<qint32, permission_t> permission_map() const;

private:
    std::map<qint32, permission_t> m_permissions_map;

    /// @brief Entity-related
public:
    virtual QString entity_id() const = 0;
    virtual entity* current_entity() = 0;

    /// @brief Author-related
private:
    virtual user* default_author() = 0;

private:
    shareable(const shareable&);
    shareable& operator=(const shareable&);

}; // class shareable

} // namespace core

} // namespace kokain

#endif // SHAREABLE_H
