#include "entity.h"

#include <QDebug>

#include "../main_controller.h"
#include "user.h"

namespace kokain
{
namespace core
{

qint64 entity::s_id_counter = Q_INT64_C(0);

bool operator==(const entity& e1, const entity& e2)
{
    return e1.id().split('_')[0].toInt() == e2.id().split('_')[0].toInt() &&
            e1.id().split('_')[1].toInt() == e2.id().split('_')[1].toInt();
}

bool operator<(const entity& e1, const entity& e2)
{
    return (e1.id().split('_')[0].toInt() < e2.id().split('_')[0].toInt() &&
            e1.id().split('_')[1].toInt() == e2.id().split('_')[1].toInt()) ||
            (e1.id().split('_')[1].toInt() < e2.id().split('_')[1].toInt());
}

// should be called from outside general intializer function
void entity::initialize_id_counter(qint32 id)
{
    s_id_counter = id;
}

qint32 entity::get_last_id()
{
    return s_id_counter;
}

entity::entity()
{
    QString tmp_id = "_" + QString::number(s_id_counter) + "_" + QString::number(main_controller::get_instance()->current_user_id()) + "_";
    set_id(tmp_id);
    ++s_id_counter;
    qDebug() << "An instance is created with id: '" << tmp_id << "'";
}

entity::~entity()
{
}

void entity::set_id(const QString& _id)
{
    m_id = _id;
}

QString entity::id() const
{
    return m_id;
}

QString entity::entity_id() const
{
    return id();
}

entity* entity::current_entity()
{
    return this;
}

user* entity::default_author()
{
    return NULL;
}

} // namespace core

} // namespace kokain
