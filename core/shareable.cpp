#include "shareable.h"

#include "user.h"

using namespace kokain::core;

shareable::shareable()
{
}

shareable::~shareable()
{
}

void shareable::set_permission(user *u, permission_t::permission_type pt)
{
    Q_ASSERT(0 != u);
    permission_t perm = m_permissions_map[u->id()];
    perm.type = static_cast<permission_t::permission_type>(perm.type | pt);
    perm.author = default_author();
    m_permissions_map[u->id()] = perm;

    u->set_permission_for_entity(this->current_entity(), pt);
}

void shareable::unset_permission(user *u, permission_t::permission_type pt)
{
    Q_ASSERT(0 != u);
    permission_t perm = m_permissions_map[u->id()];
    perm.type = static_cast<permission_t::permission_type>(perm.type & ~pt);
    perm.author = default_author();
    m_permissions_map[u->id()] = perm;

    u->unset_permission_for_entity(this->current_entity(), pt);
}

void shareable::set_permission_object(user *u, const permission_t &po)
{
    Q_ASSERT(0 != u);
    m_permissions_map[u->id()] = po;

    u->set_permission_object_for_entity(this->current_entity(), po);
}

void shareable::remove_permission_object(user *u)
{
    Q_ASSERT(0 != u);
    m_permissions_map.erase(u->id());

    u->remove_permission_object_for_entity(this->current_entity());
}

bool shareable::has_permission(user *u, const permission_t &p)
{
    Q_ASSERT(0 != u);
    permission_t perm = m_permissions_map[u->id()];
    return ((perm.type & p.type) != 0);
}

permission_t shareable::permission_object_for_user(user *u)
{
    Q_ASSERT(0 != u);
    return m_permissions_map[u->id()];
}

void shareable::set_permission_map(const std::map<qint32, permission_t>& mp)
{
    if (!mp.empty()) {
        m_permissions_map.clear();
        m_permissions_map = mp;
    }
}

std::map<qint32, permission_t> shareable::permission_map() const
{
    return m_permissions_map;
}
