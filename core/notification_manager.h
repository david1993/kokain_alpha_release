#ifndef NOTIFICATION_MANAGER_H
#define NOTIFICATION_MANAGER_H

#include <QObject>
#include <QSharedPointer>

#include "notification.h"
#include "db/db_manager.h"

class QTimer;

namespace kokain {

namespace core {

class notification_manager : public QObject
{
    Q_OBJECT

public:
    static notification_manager* get_notification_manager_instance();
    ~notification_manager();

private:
    notification_manager();
    notification_manager(notification_manager const&);
    void operator=(notification_manager const&);
    static notification_manager* m_notification_manager_instance;

public:
    kokain::db::db_manager* get_db_manager();
private:
    kokain::db::db_manager* m_db_manager;

public:
    void make_notification(QSharedPointer<notification> the_notification);
    void remove_notification(const QString& not_id);
    void set_as_read_notification(const QString& not_id);
    void set_as_unread_notification(const QString &not_id);
private:
    QString generate_notification_text(QSharedPointer<notification> the_notification);

public:
    void get_notifications_from_db(qint32 num_of_not_to_get, qint32 current_user_id = 0);
    QList<QSharedPointer<notification> > get_notifications_list();
    void change_notification_status(const QString& notification_id,
                                    notification::notification_status status);
private:
    QList<QSharedPointer<notification> > m_notifications_list;

public:
    qint32 get_num_of_unread_notifications();

private:
    QTimer* m_timer;

signals:
    void request_new_notifications();
}; // class notification_manager
} // core
} // kokain

#endif // NOTIFICATION_MANAGER_H
