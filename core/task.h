#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QVector>
#include <QMap>

#include "task_property_definitions.h"
#include "relationship.h"
#include "entity.h"

namespace kokain
{

namespace core
{
class project;
class user;
class estimation;
class team;
class comment;
class tag;
class attachment;
class plan;
class task;

bool operator==(const task& a, const task& b);

/// @class task - The core class for 'task' objects
class task : public entity
{
    // TODO: return to complete ctor list
public:
    task();
    explicit task(project* p);
    explicit task(const QString& n);
    task(project* p, const QString& n);

private:
    void initialize_();

    /// @brief Project
public:
    void set_owner_project(project* p);
    project* owner_project();

private:
    project* m_project;

    /// @brief Title and Description
public:
    void set_title(const QString& t);
    QString title() const;
    void set_description(const QString& );
    QString description() const;

private:
    QString m_title;
    QString m_description;

    /// @brief Properties (priority, status, area)
public:
    void set_priority(task_priority_t p);
    task_priority_t priority() const;
    void set_status(task_status_t s);
    task_status_t status() const;
    void set_area(task_area_t a);
    task_area_t area() const;

private:
    task_priority_t m_priority;
    task_status_t m_status;
    task_area_t m_area;

    /// @brief Relationships
public:
    void set_relationships(const QVector<task_relationship*>& v);
    void add_relationship(task_relationship* r);
    void remove_relationship(task_relationship* r);
    QVector<task_relationship*> relationships();
    bool is_related_to(task* t);

private:
    QVector<task_relationship*> m_relationships;

    /// @brief Seen/unseen
public:
    void add_seen_user(user *u);
    bool has_seen(user *u) const;

private:
    QMap<user*, bool> m_seen_map;

    /// @brief Estimation
public:
    void set_task_estimation(estimation* e);
    estimation* task_estimation();

private:
    estimation* m_estimation;

    /// @brief People-related (assignees, followers, teams)
public:
    void set_author(user *u);
    user *author();

    void set_assignees(QVector<user*>& as);
    void add_assignee(user* u);
    void remove_assignee(user* u);
    QVector<user*> assignees();

    void set_followers(QVector<user*>& fl);
    void add_follower(user* f);
    void remove_follower(user* f);
    QVector<user*> followers();

    void set_teams(const QVector<team*>& tm);
    void add_team(team* t);
    void remove_team(team* t);
    QVector<team*> teams();

private:
    user* m_author;
    QVector<user*> m_assignees;
    QVector<user*> m_followers;
    QVector<team*> m_teams;

    /// @brief Comments
public:
    void set_comments(const QVector<comment*>& cm);
    void add_comment(comment* c);
    void remove_comment(comment* c);
    QVector<comment*> comments();

private:
    QVector<comment*> m_comments;

    /// @brief Task Datetimes, Progress and Spent time
public:
    void set_work_periods(const QVector<work_period*>& wp);
    void add_work_period(work_period* wp);
    void remove_work_period(work_period* wp);
    QVector<work_period*> work_periods();

    qint64 spent_hours() const;
    qint64 spent_minutes() const;

    void set_progress(qint32 p);
    qint32 progress();

private:
    QVector<work_period*> m_work_periods;
    qint64 m_spent_hours;
    qint32 m_progress;

public:
    void set_datetimes(const QVector<task_datetime_operation_t*>& tdt);
    void add_datetime(task_datetime_operation_t* dt);
    void remove_datetime(task_datetime_operation_t* dt);
    QVector<task_datetime_operation_t*> datetimes();
    QDateTime datetime_for(task_datetime_operation_t::operation op) const;

    /// @todo implement 2 more functions - set_start_date, set_end_date

private:
    QVector<task_datetime_operation_t*> m_datetimes;

    /// @brief Attachments
public:
    void set_attachments(const QVector<attachment*>& at);
    void add_attachment(attachment* a);
    void remove_attachment(const attachment* a);
    bool has_attachment(const attachment* a) const;
    QVector<attachment*> attachments();

private:
    QVector<attachment*> m_attachments;

    /// @brief Plans, milestones, deadlines
public:
    void set_plans(const QVector<plan*>& pl);
    void add_plan(plan* p);
    void remove_plan(const plan* p);
    bool has_plan(const plan* p) const;
    QVector<plan*> plans();
    /// @todo After plan implementation, replan(), get_nearest_plan() etc
    /// member-functions should appear

private:
    QVector<plan*> m_plans;

    /// @brief Sharing and permissions
    /// Overrided from @ref shareable
protected:
    virtual user* default_author();

private:
    static const QString k_default_task_title;
    static const QString k_default_task_description;

    //virtual functions to be implemented
public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

private:
    task(const task&);
    task& operator=(const task&);

}; // class task


} // namespace core

} // namespace kokain

#endif // TASK_H
