#include "settings_manager.h"

#include "../db/db_manager.h"
#include "../main_controller.h"

//settings manager
using namespace kokain::settings;

setting settings_manager::m_setting;

settings_manager::settings_manager()
{
    qDebug() << "An instance of settings_manager is being created";
    db::db_manager* db_inst = db::db_manager::get_instance();
    db::query qr(db::query::SELECT, "settings");
    QSqlQuery qsqlqr = db_inst->execute(qr);

    if(!qsqlqr.next())
    {
        qDebug() << "Settings table is empty. There is no user registered!";
        //TODO show registration widget
        //>>>>>>>>>>>>>>>>>>>>has to be changed
        settings_manager::m_setting.last_entity_id = 0;
        settings_manager::m_setting.current_user_id = 25;
        //<<<<<<<<<<<<<<<<<<<<
        db::query new_query(db::query::INSERT, "settings");
        new_query.set_value(settings_manager::m_setting.serialize_to_json());
        db_inst->execute(new_query);
    }
    else
    {
        settings_manager::m_setting.deserialize_from_json(qsqlqr.value(0).toString());
        qDebug() << "SETTINGS";
        qDebug() << "   current_user_id: " << settings_manager::m_setting.current_user_id;
        qDebug() << "   last_entity_id: " << settings_manager::m_setting.last_entity_id;
    }

    core::entity::initialize_id_counter(settings_manager::m_setting.last_entity_id);
    main_controller::get_instance()->set_current_user_id(settings_manager::m_setting.current_user_id);
    qDebug() << "An instance of settings_manager is created";
}

settings_manager::~settings_manager()
{
}

settings_manager* settings_manager::s_instance;

settings_manager* settings_manager::get_instance()
{
    if(!s_instance)
    {
        settings_manager* tmp = new settings_manager();
        s_instance = tmp;
    }
    return s_instance;
}

void settings_manager::remove_instance()
{
    qDebug() << "removing settings_manager isntance";
    //needs discussion
    m_setting.last_entity_id = core::entity::get_last_id();
    db::query new_query(db::query::UPDATE, "settings");
    new_query.set_value(m_setting.serialize_to_json());
    db::db_manager::get_instance()->execute(new_query);
    //
    delete s_instance;
    s_instance = 0;
    qDebug() << "settings_manager removed";
}
//

//setting
QString setting::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("current_user_id", current_user_id);
    j_obj.insert("last_entity_id", last_entity_id);

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void setting::deserialize_from_json(const QString &serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    current_user_id = qint32(j_obj.value("current_user_id").toInt());
    last_entity_id = qint32(j_obj.value("last_entity_id").toInt());
    return;
}

QString setting::object_type() const
{
    return "setting";
}

//
