#ifndef SETTINGS_MANAGER_H
#define SETTINGS_MANAGER_H

#include <QtGlobal>

#include "../core/entity.h"

namespace kokain
{

namespace settings
{

struct setting : public core::entity
{
    qint32 current_user_id;
    qint32 last_entity_id;

    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;
};

class settings_manager
{
private:
    settings_manager();
    settings_manager(const settings_manager&);
    settings_manager& operator=(const settings_manager&);
    ~settings_manager();

    //singleton
public:
    static settings_manager* s_instance;
    static settings_manager* get_instance();
    static void remove_instance();

    //settings
private:
    static setting m_setting;
};

}//namespace settings
}//namespace kokain

#endif // SETTINGS_MANAGER_H
