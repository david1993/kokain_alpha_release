#include "main_window.h"

#include <QDebug>

#include "gui/main_window_central_widget.h"
#include "main_controller.h"

namespace kokain
{

main_window::main_window(QWidget *parent)
    : QMainWindow(parent)
{
    initialize_();
}

main_window::~main_window()
{
}

void main_window::closeEvent(QCloseEvent *e)
{
    qDebug() << "Program is going to be closed.";
    main_controller::remove_instance();
}

void main_window::initialize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();

    setPalette(QPalette(QColor("white")));
}

void main_window::create_widgets_()
{
    m_central_widget = new gui::main_window_central_widget(this);
    Q_ASSERT(0 != m_central_widget);
    setCentralWidget(m_central_widget);
}

void main_window::setup_layout_()
{

}

void main_window::make_connections_()
{
    connect(m_central_widget, SIGNAL(maximize_clicked()), this, SLOT(handle_maximize_clicks()));
    connect(m_central_widget, SIGNAL(minimize_clicked()), this, SLOT(showMinimized()));
}

void main_window::handle_maximize_clicks()
{
    isMaximized() ? showNormal() : showMaximized();
}

} // namespace kokain
