#include "year_month.h"

using namespace kokain::calendar_namespace;

year_month::year_month(QWidget *parent):QWidget(parent)
{
    initilize_();
}

void year_month::initilize_()
{
    create_widgets_();
    make_connections_();
    setup_layout_();
}

void year_month::create_widgets_()
{
    m_curr_day = new QLabel(QString::number(QDate::currentDate().day()));
    m_curr_day->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_curr_weekday = new QLabel(QDate::longDayName(QDate::currentDate().dayOfWeek()));
    m_curr_weekday->setStyleSheet("qproperty-alignment: AlignCenter;");
    m_curr_day->setStyleSheet("qproperty-alignment: AlignBottom;");

    QFont font = m_curr_day->font();
    font.setPointSize(20);
    m_curr_day->setFont(font);

    font = m_curr_weekday->font();
    font.setPointSize(20);
    m_curr_weekday->setFont(font);
    m_year = new year;



    m_month = new QComboBox(this);
    m_month->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    m_month->addItem("January");
    m_month->addItem("February");
    m_month->addItem("March");
    m_month->addItem("April");
    m_month->addItem("May");
    m_month->addItem("June");
    m_month->addItem("July");
    m_month->addItem("August");
    m_month->addItem("September");
    m_month->addItem("October");
    m_month->addItem("Novermber");
    m_month->addItem("December");

    m_month->setCurrentIndex(QDate::currentDate().month() - 1);

    m_next_year = new QPushButton(">");
    m_next_year->setFixedSize(20,20);
    m_prev_year = new QPushButton("<");
    m_prev_year->setFixedSize(20,20);
}

void year_month::setup_layout_()
{
    QHBoxLayout* main = new QHBoxLayout;
    QHBoxLayout* year = new QHBoxLayout;
    QHBoxLayout* month = new QHBoxLayout;
    QHBoxLayout* year_and_month = new QHBoxLayout;
    QVBoxLayout* week_and_year_month = new QVBoxLayout;

    year->addWidget(m_curr_day);
    year->addWidget(m_prev_year);
    year->addWidget(m_year);
    year->addWidget(m_next_year);


    month->addWidget(m_month);

    year_and_month->addLayout(year);
    year_and_month->addLayout(month);

    week_and_year_month->addWidget(m_curr_weekday);
    week_and_year_month->addLayout(year_and_month);

    setLayout(week_and_year_month);
}

void year_month::make_connections_()
{
    connect(m_prev_year, SIGNAL(clicked()), this, SLOT(prev_year()));
    connect(m_next_year, SIGNAL(clicked()), this, SLOT(next_year()));
    connect(m_year, SIGNAL(date_changed()), this, SLOT(on_date_changed()));
    connect(m_month, SIGNAL(currentIndexChanged(int)), this, SLOT(on_date_changed()));
}

void year_month::set_curr_day(int day)
{
    QString curr_day = QString::number(day);
    m_curr_day->setText(curr_day);
}

void year_month::set_curr_weekday(int weekday)
{
    if(weekday == 1)
        m_curr_weekday->setText("Monday");
    if(weekday == 2)
        m_curr_weekday->setText("Tuesday");
    if(weekday == 3)
        m_curr_weekday->setText("Wednesday");
    if(weekday == 4)
        m_curr_weekday->setText("Thursday");
    if(weekday == 5)
        m_curr_weekday->setText("Friday");
    if(weekday == 6)
        m_curr_weekday->setText("Saturday");
    if(weekday == 7)
        m_curr_weekday->setText("Sunday");
}

int year_month::get_month()
{
    return m_month->currentIndex() + 1;
}

int year_month::get_year()
{
    return m_year->get_year();
}

QString year_month::get_curr_day()
{
    return m_curr_day->text();
}

QString year_month::get_curr_weekday()
{
    return m_curr_weekday->text();
}

void year_month::prev_year()
{
    if(m_year->get_year() == 1800)
        m_year->set_year(1800);
    else
        m_year->set_year(m_year->get_year() - 1);
    emit date_changed();
}

void year_month::next_year()
{
    if(m_year->get_year() == 8000)
        m_year->set_year(8000);
    else
        m_year->set_year(m_year->get_year() + 1);
    emit date_changed();
}

void year_month::on_date_changed()
{
    emit date_changed();
}
