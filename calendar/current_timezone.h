#ifndef CURRENT_TIMEZONE_H
#define CURRENT_TIMEZONE_H

#include <QString>

namespace kokain
{
namespace calendar_namespace
{

class current_timezone
{
private:
    int m_from_all;
    int m_from_current;
    QString m_name;

public:
    current_timezone();
    void set_from_all(int);
    void set_from_current(int);
    void set_name(QString);
    int get_from_all();
    int get_from_current();
    QString get_name() const;
//    inline bool operator==(const current_timezone &s) const;
//    inline bool operator!=(const current_timezone &s) const;
//    inline bool operator>(const current_timezone &s) const;
//    inline bool operator<(const current_timezone &s) const;
//    inline bool operator>=(const current_timezone &s) const;
//    inline bool operator<=(const current_timezone &s) const;
};

}//namespace calendar_namespace
}//namespace kokain

#endif // CURRENT_TIMEZONE_H
