#include "label_edit.h"

using namespace kokain::calendar_namespace;

const qint32 label::close_pos = 13;

label::label(QWidget *parent):QLabel(parent)
{
    initilize_();
}

void label::initilize_()
{
    m_is_link = false;
    m_clicked_close = false;

    create_widgets_();
    make_connections_();

    setStyleSheet("border: 1px solid");
}

void label::create_widgets_()
{
    QFont font = this->font();
    font.setPointSize(10);
    setFont(font);
    setFixedSize(170,25);

    m_text_edit = new QLineEdit(this);
    m_text_edit->setFixedSize(170,25);
    m_link = new QLabel(this);
    m_link->close();

    m_delete = new QPixmap(QString("C:/Users/David/Desktop/uu/user3/images/close.png"));

    m_image = new QLabel(this);
    m_image->setPixmap(m_delete->scaled(12.5,12.5));
    m_image->setGeometry(width() - 13 ,0,13,13);
    m_link->setGeometry(0,0,170,25);
    m_image->close();
    m_first_enter = true;
}



void label::make_connections_()
{
    connect(m_text_edit, SIGNAL(returnPressed()), this, SLOT(clicked()));
}


void label::mouseDoubleClickEvent(QMouseEvent *event)
{
      if (event -> button() == Qt::LeftButton) {
          m_text_edit->show();
          m_image->close();
          m_link->close();
      }
}

void label::mouseReleaseEvent(QMouseEvent *event)
{
    if (event -> button() == Qt::LeftButton) {
        int x = event->x();
        int y = event->y();
        if(x >= width() - 13 && y <= 13 && m_clicked_close){
            this->hide();
            emit closed();
        }
    }
}

void label::mousePressEvent(QMouseEvent *event)
{
    if (event -> button() == Qt::LeftButton) {
        int x = event->x();
        int y = event->y();
        if(x >= width() - 13 && y <= 13){
            m_clicked_close = true;
        }
    }
}

void label::clicked()
{
    if(!m_is_link){
        setText(m_text_edit->text());
        m_text_edit->close();
    }
    else{
        m_link->setTextFormat(Qt::RichText);
        QString s = " \"" + m_text_edit->text()+ "\"";
        m_link->setText("<a href=" + s + ">" + m_text_edit->text() +  "</a>");
        m_link->setOpenExternalLinks(true);
        m_text_edit->close();
        m_link->show();
    }
    m_image->show();
    if(m_first_enter){
        emit enter_click();
        m_first_enter = false;
    }
    else{
        emit other_enter_clicked();
    }
}

void label::set_is_link(bool flag)
{
    m_is_link = flag;
}

QString label::get_label()
{
    return m_text_edit->text();
}

void label::set_label(QString line)
{
    m_text_edit->setText(line);
    m_link->setText(line);
}

void label::m_link_show()
{
    m_link->show();
    m_text_edit->close();
    m_image->show();
}




