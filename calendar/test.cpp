#include "test.h"

#include "user_data.h"
#include "../db/db_manager.h"
#include "../db/query.h"
#include "../db/condition.h"
#include "flowlayout.h"

using namespace kokain::calendar_namespace;

test::test(QWidget *parent)
    : QWidget(parent)
{
    initilize_();
}

void test::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void test::create_widgets_()
{


    m_drag = new DragWidget(this);
    m_drag_test = new DragWidget(this);
    m_add = new QPushButton("Add", this);

    m_main = new QVBoxLayout(this);
    m_set_params = new user_dialog_box();
    m_data = new QSignalMapper(this);


    m_curr = 0;
    resize(600,600);
}

void test::setup_layout_()
{

    QVBoxLayout* buttons = new QVBoxLayout;
    buttons->addWidget(m_add, 0 , Qt::AlignTop);
    QHBoxLayout* up = new QHBoxLayout;
    up->addWidget(m_drag);
    up->addLayout(buttons);
    m_main->addLayout(up);
    m_main->addWidget(m_drag_test);
    setLayout(m_main);
}

void test::make_connections_()
{
    connect (m_data, SIGNAL(mapped(int)), this, SLOT(data_showed(int)));
    connect(m_add,SIGNAL(clicked()), this, SLOT(add_clicked()));
    connect(m_set_params,SIGNAL(ok_clicked()), this, SLOT(set()));

    kokain::db::db_manager* manager = kokain::db::db_manager::get_instance();
    kokain::db::query* q = new kokain::db::query(kokain::db::query::SELECT, "users");
    QSqlQuery res = manager->execute(*q);
    while (res.next()) {
        kokain::core::user* user = new kokain::core::user();
        user->deserialize_from_json(res.value(0).toString());
        m_drag->set_user(user);
        m_user.insert(m_curr,user);

        user_data* data = new user_data();
        data = user->get_m_data();
        data->hide();
        m_user_data.insert(m_curr,data);
        connect(data, SIGNAL(data_back_clicked()), this, SLOT(data_back_clicked()));
        connect(m_user.at(m_curr), SIGNAL(data_showed()),m_data,SLOT(map()));
        m_data->setMapping(m_user.at(m_curr), m_curr);
        m_curr++;
        m_main->addWidget(data);
    }


}

void test::add_clicked()
{
    m_set_params->show();
}

void test::set()
{
        kokain::core::user* user = new kokain::core::user();
        user->set_name(m_set_params->get_user_name());
        QPixmap pic;
        pic.load(m_set_params->get_picture());
        pic.scaled(40,40);
        user->set_picture(pic);
        m_drag->set_user(user);
        m_user.insert(m_curr,user);

        QSqlQuery res1("SELECT * FROM users");
        res1.seek(-1);
        QVector<int> ids;
        while (res1.next()) {
            kokain::core::user* cur = new kokain::core::user;
            cur->deserialize_from_json(res1.value(0).toString());
            ids.push_back(cur->id());
            delete cur;
        }
        if(ids.empty())
            user->set_id(1);
        else{
            qSort(ids);
            user->set_id(ids.back() + 1);
        }

        kokain::db::db_manager* manager = kokain::db::db_manager::get_instance();
        kokain::db::query* q = new kokain::db::query(kokain::db::query::INSERT, "users");
        q->set_value(user->serialize_to_json());
        //QSqlQuery
        manager->execute(*q);
        user_data* data = new user_data;
        data = user->get_m_data();
        data->hide();
        m_user_data.insert(m_curr,data);
        connect(data, SIGNAL(data_back_clicked()), this, SLOT(data_back_clicked()));
        connect(m_user.at(m_curr), SIGNAL(data_showed()),m_data,SLOT(map()));
        m_data->setMapping(m_user.at(m_curr), m_curr);
        m_curr++;
        m_main->addWidget(data);
}



void test::data_showed(int i)
{
    m_drag->close();
    m_user_data.at(i)->show();
    m_add->hide();
    m_drag_test->hide();
}


void test::data_back_clicked()
{
    for(int i = 0; i < m_user_data.size(); i ++) {
        m_user_data.at(i)->hide();
    }
    m_drag->show();
    m_drag_test->show();
    m_add->show();
}

