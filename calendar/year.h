#ifndef YEAR_H
#define YEAR_H

#include <QtWidgets>

class QLabel;
class QLineEdit;

namespace kokain
{
namespace calendar_namespace
{

class year:public QLabel
{
    Q_OBJECT

public:
    explicit year(QWidget* parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void make_connections_();

private:
    const static qint32 close_pos;

private:
    QLabel* m_label;
    QLineEdit* m_text_edit;

public:
    void set_year(int);
    int get_year();

protected:
    void mouseDoubleClickEvent ( QMouseEvent * event );

public slots:
    void clicked();

signals:
    void date_changed();
};

}//namespace calendar_namespace
}//namespace kokain

#endif // YEAR_H
