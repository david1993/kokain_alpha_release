#ifndef EVENT_H
#define EVENT_H

#include <QtWidgets>

#include "../core/entity.h"
#include "dialog_box.h"
#include "event_manager.h"

using namespace kokain::core;

namespace kokain
{
namespace calendar_namespace
{
class event_manager;

class cal_event : public QWidget, public entity
{
    Q_OBJECT

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QLabel* m_name;
    QLabel* m_color;
    QString m_description;
    QDateTime* m_start_date;
    QDateTime* m_end_date;
    dialog_box* m_properties;
    QList<cal_event*> m_events_list;
    event_manager* m_event_manager;
    QList<current_timezone> m_all_timezones;
    QTimeZone m_event_timezone;


public:
    explicit cal_event(QWidget* parent = 0);
    //cal_event::cal_event(const cal_event &obj);
    ~cal_event();

public:
    void set_name(QString);
    void set_color(QString);
    void set_property_name(QString);
    void set_property_color(QString);
    void set_property_start_date(QDateTime);
    void set_property_end_date(QDateTime);
    void set_start_date(QDateTime);
    void set_end_date(QDateTime);
    QString get_name() const;
    QString get_color() const;
    QDateTime get_start_date();
    QDateTime get_end_date();
    QTimeZone get_timezone() const;
    void set_list(QList<cal_event *>);
    const QList<cal_event*>& get_events() const;
    void show_properties();
    void set_event_manager(event_manager*);
    void timezone(QList<current_timezone>);
    void set_timezone(QTimeZone);

public:
    void resizeEvent(QResizeEvent*);
    void mousePressEvent(QMouseEvent*);

public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;
    cal_event& operator=(const cal_event& right);


public slots:
    void discarded();
    void closed();
    void ok_clicked();

signals:
    void is_closed();
};

}//namespace calendar_namespace
}//namespace kokain

#endif // EVENT_H
