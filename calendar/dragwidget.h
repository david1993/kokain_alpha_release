#ifndef DRAGWIDGET_H
#define DRAGWIDGET_H

#include <QtWidgets>


#include "../core/user.h"
#include "flowlayout.h"


QT_BEGIN_NAMESPACE
class QDragEnterEvent;
class QDropEvent;
QT_END_NAMESPACE

namespace kokain
{
namespace calendar_namespace
{

class DragWidget : public QListWidget
{
    Q_OBJECT

public:
    explicit DragWidget(QWidget *parent = 0);

private:
   void initilize_();
   void setup_layout_();

private:
    FlowLayout *flowLayout;


public:
    void set_user(core::user* user);


protected:
    virtual void dropEvent(QDropEvent* event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);


signals:
     void dropped();
};

}//namespace calendar_namespace
}//namespace kokain

#endif // DRAGWIDGET_H
