#include "user_data.h"

using namespace kokain::calendar_namespace;

user_data::user_data(QWidget *parent)
    : QFrame(parent)
{
    initilize_();
}

void user_data::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void user_data::create_widgets_()
{
    m_calendar_show = new QPushButton("Calendar");
    m_back = new QPushButton("back");
    m_back->setFixedSize(50,30);
}

void user_data::setup_layout_()
{
    m_main = new QVBoxLayout;
    m_main->addWidget(m_back);
    m_main->addWidget(m_calendar_show);
    setLayout(m_main);
}

void user_data::make_connections_()
{
    connect(m_back, SIGNAL(clicked()), this, SLOT(back_clicked()));
    connect(m_calendar_show, SIGNAL(clicked()), this, SLOT(calendar_showed()));
}

void user_data::add_data(QString name, bool flag)
{
    m_curr_name = new QLabel(name);
    m_current = new multiple_labels;
    connect(m_current, SIGNAL(all_labels_changed()), this, SLOT(data_changed()));
    m_current->set_is_link(flag);
    m_curr = new QHBoxLayout;
    m_curr->addWidget(m_curr_name);
    m_curr->addWidget(m_current);
    m_main->addLayout(m_curr);
    m_all_options.push_back(m_current);
}

void user_data::back_clicked()
{
    emit data_back_clicked();
}

QVector<multiple_labels*>& user_data::get_all_options()
{
    return m_all_options;
}

void user_data::set_data(QVector<QVector<QString> > all_options)
{

    for(int i = 0; i < all_options.size(); ++i){
        m_all_options.at(i)->set_option(all_options.at(i));
    }
}

void user_data::data_changed()
{
    emit on_data_changed();
}

void user_data::calendar_showed()
{
    emit on_calendar_showed();
}

