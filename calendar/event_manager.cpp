#include "event_manager.h"

#include <QSqlQuery>

#include "event.h"
#include "../db/db_manager.h"
#include "../db/condition.h"
#include "../db/query.h"

using namespace kokain::calendar_namespace;

event_manager::event_manager()
{
    kokain::db::db_manager* manager = kokain::db::db_manager::get_instance();
    kokain::db::query q(kokain::db::query::SELECT, "calendar_events");
    QSqlQuery res = manager->execute(q);
    while (res.next()) {
        cal_event* cur = new cal_event();
        cur->deserialize_from_json(res.value(0).toString());
        qDebug() << cur->get_timezone();
        m_events.push_back(cur);
    }
}


QList<cal_event*> event_manager::get_events()
{
    return m_events;
}

void event_manager::remove_all()
{

    QList<cal_event*>::iterator i;
    for(i = m_events.begin(); i != m_events.end(); ++ i){
        delete (*i);
    }
    m_events.clear();
}

void event_manager::add_event(cal_event* event)
{
    kokain::db::query* q = new kokain::db::query(kokain::db::query::INSERT, "calendar_events");
    q->set_value(event->serialize_to_json());
    QSqlQuery res = kokain::db::db_manager::get_instance()->execute(*q);
    m_events.push_back(event);
}

QList<cal_event*> event_manager::get_events_for_day(QDateTime cur_time)
{
    QList<cal_event*> for_day;
    QList<cal_event*>::iterator i;
    int offset_from_utc ;
    int event_offset;
    for(i = m_events.begin(); i != m_events.end(); ++ i){
        if((*i)->get_start_date().addSecs((*i)->get_timezone().offsetFromUtc(QDateTime::currentDateTime()) - QDateTime::currentDateTime().timeZone().offsetFromUtc(QDateTime::currentDateTime())) <= QDateTime(cur_time.date(), QTime(23,59,59)) && QDateTime(cur_time.date(), QTime(0,0,0)) <= (*i)->get_end_date().addSecs((*i)->get_timezone().offsetFromUtc(QDateTime::currentDateTime()) - QDateTime::currentDateTime().timeZone().offsetFromUtc(QDateTime::currentDateTime()))){
            for_day.push_back((*i));
    }
    }
    return for_day;
}

void event_manager::remove_at(const QString& id)
{

    //qDebug() << "blabla1";
//    m_events.clear();
//    qDebug() << "blabla1";
//    QSqlQuery res;
//    res.exec("SELECT * FROM calendar_events");
//    while (res.next()) {
//        cal_event* cur = new cal_event;
//        cur->deserialize_from_json(res.value(0).toString());
//        qDebug() << "blabla1";
//        m_events.push_back(cur);
//        qDebug() << "blabla1";
//    }

    kokain::db::query* q = new kokain::db::query(kokain::db::query::REMOVE, "calendar_events");
    kokain::db::condition* cond = new kokain::db::condition("content", "MATCH", "\"id\": " + id);
    q->set_condition(*cond);
    QSqlQuery res2 = kokain::db::db_manager::get_instance()->execute(*q);

    qDebug() << m_events.size() << "bggggggggggggggg";


    QMutableListIterator<cal_event*> i(m_events);
    while (i.hasNext()) {
        if (i.next()->id() == id) {
            i.remove();
        }
    }
}

void event_manager::set_events(QList<cal_event *> events)
{
    m_events = events;
}

