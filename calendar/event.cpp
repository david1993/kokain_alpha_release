#include "event.h"

#include <QSqlQuery>
#include "../db/query.h"
#include "../db/db_manager.h"

using namespace kokain::calendar_namespace;

cal_event::cal_event(QWidget *parent)
    : QWidget(parent)
{
    initilize_();
}

void cal_event::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void cal_event::create_widgets_()
{
    m_properties = new dialog_box;
    m_name = new QLabel("blabla",this);
    resize(40,40);
    m_color = new QLabel(this);
    m_color->setStyleSheet("QLabel { background-color : red; color : blue; }");
    m_start_date = new QDateTime;
    m_end_date = new QDateTime;
    QFont font = m_name->font();
    font.setPointSize((qMin(m_name->width(), m_name->height())));
    m_name->setFont(font);
}

void cal_event::setup_layout_()
{
    QHBoxLayout* main = new QHBoxLayout;
    main->addWidget(m_color);
    main->addWidget(m_name);
    setLayout(main);
}

void cal_event::make_connections_()
{
   connect(m_properties, SIGNAL(discard_clicked()), this, SLOT(discarded()));
   connect(m_properties, SIGNAL(closed()), this, SLOT(closed()));
   connect(m_properties, SIGNAL(ok_clicked()), this, SLOT(ok_clicked()));
}

void cal_event::set_name(QString event_name)
{
    m_name->setText(event_name);
}

void cal_event::set_color(QString event_color)
{
    m_color->setText(event_color);
    m_color->setStyleSheet("QLabel { background-color :" +  event_color + "; color :" + event_color + "; }");
}

QString cal_event::get_color() const
{
    return m_color->text();
}

QString cal_event::get_name() const
{
    return m_name->text();
}


void cal_event::resizeEvent(QResizeEvent*)
{
    QFont font = m_name->font();
    font.setPointSize(this->height()/4);
    m_name->setFont(font);
    m_color->resize(m_color->width(), m_name->height());
}

void cal_event::discarded()
{

//    QSqlQuery res2;
//    db_manager* manager = db_manager::get_instance("db1.sqlite");
//    query* q = new query(query_type::REMOVE, "calendar_events");
//    condition* cond = new condition("content", "MATCH", "\"id\": " + QString::number(this->id()));
//    q->setCondition(*cond);
//    res2 = manager->execute(*q);
    m_event_manager->remove_at(id());
    emit is_closed();
    this->close();
}

void cal_event::closed()
{
}

void cal_event::mousePressEvent(QMouseEvent*)
{
    m_properties->set_event_name(m_name->text());
    m_properties->set_event_color(m_color->text());
    m_properties->set_start_date(get_start_date());
    m_properties->set_end_date(get_end_date());
    m_properties->show();
}

void cal_event::ok_clicked()
{
    this->set_name(m_properties->get_event_name());
    this->set_color(m_properties->get_color());
    this->set_start_date(m_properties->get_start_date());
    this->set_end_date(m_properties->get_end_date());
    this->set_timezone(m_properties->get_timezone());

    qDebug() << "hasanq";


    m_event_manager->remove_at(id());

    cal_event* eventik = new cal_event;
    eventik->set_id(id());
    eventik->set_name(m_properties->get_event_name());
    eventik->set_color(m_properties->get_color());
    eventik->set_start_date(m_properties->get_start_date());
    eventik->set_end_date(m_properties->get_end_date());
    eventik->timezone(m_all_timezones);
    eventik->set_timezone(m_properties->get_timezone());
    m_event_manager->add_event(eventik);
    eventik->set_event_manager(m_event_manager);


    emit is_closed();
}

void cal_event::set_list(QList<cal_event *> list)
{
     m_events_list = list;
}

void cal_event::show_properties()
{
    m_properties->show();
}

const QList<cal_event*>& cal_event::get_events() const
{
    return m_events_list;
}

void cal_event::set_property_name(QString name)
{
    m_properties->set_event_name(name);
}

void cal_event::set_property_color(QString color)
{
    m_properties->set_event_color(color);
}

void cal_event::set_property_start_date(QDateTime start)
{
    m_properties->set_start_date(start);
}

void cal_event::set_property_end_date(QDateTime end)
{
    m_properties->set_end_date(end);
}

void cal_event::set_start_date(QDateTime start)
{
    m_start_date = new QDateTime(start);
}

void cal_event::set_end_date(QDateTime end)
{
    m_end_date = new QDateTime(end);
}

QDateTime cal_event::get_start_date()
{
    return *m_start_date;
}

QDateTime cal_event::get_end_date()
{
    return *m_end_date;
}


QString cal_event::serialize_to_json() const
{
    QJsonObject j_obj;

    j_obj.insert("id", id());
    j_obj.insert("name", this->get_name());
    j_obj.insert("color", get_color());
    j_obj.insert("start_datetime", (*m_start_date).toString("dddd/ MMMM dd/ yyyy/  hh:mm"));
    j_obj.insert("end_datetime", (*m_end_date).toString("dddd/ MMMM dd/ yyyy/  hh:mm"));
    j_obj.insert("timezone", QString(get_timezone().id()));

    QJsonDocument j_doc(j_obj);
    return QString(j_doc.toJson());
}

void cal_event::deserialize_from_json(const QString& serialized)
{
    QJsonDocument j_doc = QJsonDocument::fromJson(serialized.toUtf8());
    QJsonObject j_obj = j_doc.object();

    set_id(j_obj.value("id").toString());
    set_name(j_obj.value("name").toString());
    set_color(j_obj.value("color").toString());
    set_start_date(QDateTime::fromString(j_obj.value("start_datetime").toString(), "dddd/ MMMM dd/ yyyy/  hh:mm"));
    set_end_date(QDateTime::fromString(j_obj.value("end_datetime").toString(), "dddd/ MMMM dd/ yyyy/  hh:mm"));
    set_timezone(QTimeZone(j_obj.value("timezone").toVariant().toByteArray()));
    return;
}

QString cal_event::object_type() const
{
    return "cale_event";
}

cal_event::~cal_event()
{
    delete m_name;
    m_name = NULL;
    delete m_color;
    m_color = NULL;
    delete m_start_date;
    m_start_date = NULL;
    delete m_end_date;
    m_end_date = NULL;
    delete m_properties;
    m_properties = NULL;

//    if(!m_events_list.empty()){
//        QList<cal_event*>::iterator i = m_events_list.begin();
//        qDebug() << "start list cleanup";
//        while (i != m_events_list.end()) {
//            qDebug() << "start element deletion";
//            if(this != (*i)){
//                delete (*i);
//                qDebug() << "xsxs";
//            }
//            ++i;
//        }
//    }

    //delete m_events_list;
}

void cal_event::set_event_manager(event_manager * manager)
{
    m_event_manager = manager;
}

//cal_event::cal_event(const cal_event &obj)
//{
//    m_name = new QLabel;
//    m_color = new QLabel;
//    m_description = obj.m_description;
//    m_start_date = new QDateTime;
//    m_end_date = new QDateTime;
//    m_properties = new dialog_box;
//    m_events_list = obj.m_events_list;
//    m_event_manager = new m_event_manager;
//    *m_name = *obj.m_name;
//    *m_color = *obj.m_color;
//    *m_start_date = *obj.m_start_date;
//    *m_end_date = *obj.m_end_date;
//    *m_properties = *obj.m_properties;
//    *m_event_manager = *obj.m_event_manager;
//}

 cal_event& cal_event::operator=(const cal_event& obj)
 {
     if (this == &obj) {
         return *this;
     }
     m_name = new QLabel;
     m_color = new QLabel;
     m_description = obj.m_description;
     m_start_date = new QDateTime;
     m_end_date = new QDateTime;
     m_properties = new dialog_box;
     m_events_list = obj.m_events_list;
     m_event_manager = new event_manager;
     set_name(obj.get_name());
     set_color(obj.get_color());
     *m_start_date = *obj.m_start_date;
     *m_end_date = *obj.m_end_date;
     m_properties->set_event_name(obj.m_properties->get_event_name());
     m_properties->set_event_color(obj.m_properties->get_color());
     m_properties->set_start_date(obj.m_properties->get_start_date());
     m_properties->set_end_date(obj.m_properties->get_end_date());
     m_event_manager->set_events(obj.m_event_manager->get_events());
     return *this;
 }

void cal_event::timezone(QList<current_timezone> list)
{
    m_all_timezones = list;
    m_properties->timezone(list);
}

void cal_event::set_timezone(QTimeZone timezone)
{
    m_event_timezone = timezone;
}

QTimeZone cal_event::get_timezone() const
{
    return m_event_timezone;
}
