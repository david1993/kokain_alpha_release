#ifndef DIALOG_BOX_H
#define DIALOG_BOX_H

#include <QtWidgets>

#include "event_start_end.h"
#include "timezone.h"

class QLabel;
class QLineEdit;
class QPushButton;

namespace kokain
{
namespace calendar_namespace
{
class year;

class dialog_box : public QDialog
{
    Q_OBJECT
public:
    dialog_box(QWidget* parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    event_timezone* m_event_timezone;
public:
    QTimeZone get_timezone();

private:
    QLabel* m_event_name;

public:
    QString get_event_name();
    void set_event_name(QString name);

private:
    QLabel* m_event_color;

public:
    QString get_color();
    void set_event_color(QString);
    void set_start_date(QDateTime start);
    void set_end_date(QDateTime end);
    QDateTime get_start_date();
    QDateTime get_end_date();
    event_start_end* get_event_start_end();
    void timezone(QList<current_timezone>);


private:
    QLineEdit* m_event_name_edit;
    QLineEdit* m_event_color_edit;
    event_start_end* m_event_start_end;
    QPushButton* m_ok;
    QPushButton *m_cancel;
    QPushButton *m_discard;

public slots:
    void my_accept();
    void discarded();

signals:
     void ok_clicked();
     void discard_clicked();
     void closed();
};

}//namespace calendar_namespace
}//namespace kokain

#endif // DIALOG_BOX_H
