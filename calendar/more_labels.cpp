#include "more_labels.h"

using namespace kokain::calendar_namespace;

more_labels::more_labels(QWidget *parent):QWidget(parent)
{
    initilize_();
}

void more_labels::initilize_()
{
    create_widgets_();
    setup_layout_();
}

void more_labels::create_widgets_()
{
    m_labels = new QQueue<label*>;
}

void more_labels::setup_layout_()
{
    m_main_layout = new QVBoxLayout;
    setLayout(m_main_layout);
}

void more_labels::add_label(label *curr_label)
{
    m_labels->enqueue(curr_label);
    m_main_layout->addWidget(curr_label);
}

label* more_labels::get_label()
{
    return m_labels->dequeue();
}

label* more_labels::curr_label(int i)
{
    return m_labels->at(i);
}

bool more_labels::is_empty()
{
    return m_labels->count() == 0;
}


int more_labels::count()
{
    return m_labels->count();
}
