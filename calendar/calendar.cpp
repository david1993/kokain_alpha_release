#include "calendar.h"

#include "../db/db_manager.h"
#include "../db/condition.h"
#include "../db/query.h"
#include "event_manager.h"
#include "test.h"
#include "calendar_piece.h"
#include "year_month.h"
#include "event.h"

using namespace kokain::calendar_namespace;

int** calendar_curr_page(int year, int month)
{
    int**arr = new int*[6];
    for(int i = 0; i < 6; ++ i){
        arr[i] = new int[7];
    }
    QDate curr(year, month, 1);
    QDate prev(year, (month != 1 ? month -1 : 12), 1);
    int offset = curr.dayOfWeek();
    int prev_days = prev.daysInMonth();
    if(offset == 7)
        offset = 0;

    for(int i = 0; i < offset; ++ i){
        arr[0][offset - i - 1] = prev_days - i;
    }

    int days = 1;

    for(int i = offset; i < 7; ++ i){
        arr[0][i] = days ++;
    }


    for(int i = 1; i < 6; ++ i){
        for(int j = 0; j < 7; ++ j){
            arr[i][j] = days ++;
            if(days == curr.daysInMonth() + 1)
                days = 1;
        }
    }
    return arr;
}

calendar::calendar()
{

        current_timezone cur;
        int t = 0;
        QString S;

        for(int i = -50400; i <= 50400; i += 1800){
            for(int j = 0; j < QTimeZone::availableTimeZoneIds(i).size(); ++ j){
                S = QLocale::countryToString(QTimeZone(QTimeZone::availableTimeZoneIds(i).at(j)).country()) + "      " + QTimeZone(QTimeZone::availableTimeZoneIds(i).at(j)).comment();
                if(QLocale::countryToString(QTimeZone(QTimeZone::availableTimeZoneIds(i).at(j)).country()) != "Default"){
                    cur.set_name(S);
                    cur.set_from_all(i);
                    cur.set_from_current(j);
                    for( t = 0; t < m_all_timezones.size(); ++ t ){
                        if(S == m_all_timezones[t].get_name())
                            break;
                    }
                    if(t == m_all_timezones.size())
                        m_all_timezones.push_back(cur);
                }
            }
        }

    qSort(m_all_timezones.begin(), m_all_timezones.end(), &calendar::should_remove);

    setFixedSize(624,740);
    m_event_manager = new event_manager;
    foreach (cal_event* cur, m_event_manager->get_events()) {
        qDebug() << cur->get_color();
    }

    m_calendar = new QWidget;
    m_calendar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    m_year_month = new year_month;
    m_year_month->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    sun = new QLabel("Sun");
    sun->setStyleSheet("qproperty-alignment: AlignCenter;");
    sun->setFixedHeight(20);
    mon = new QLabel("Mon");
    mon->setStyleSheet("qproperty-alignment: AlignCenter;");
    mon->setFixedHeight(20);
    tue = new QLabel("Tue");
    tue->setStyleSheet("qproperty-alignment: AlignCenter;");
    tue->setFixedHeight(20);
    wed = new QLabel("Wed");
    wed->setStyleSheet("qproperty-alignment: AlignCenter;");
    wed->setFixedHeight(20);
    thu = new QLabel("Thu");
    thu->setStyleSheet("qproperty-alignment: AlignCenter;");
    thu->setFixedHeight(20);
    fri = new QLabel("Fri");
    fri->setStyleSheet("qproperty-alignment: AlignCenter;");
    fri->setFixedHeight(20);
    sut = new QLabel("Sat");
    sut->setStyleSheet("qproperty-alignment: AlignCenter;");
    sut->setFixedHeight(20);



    int i = 0;

    QFont font;
    font.setPointSize(10);
    font.setBold(true);
    sun->setFont(font);
    mon->setFont(font);
    tue->setFont(font);
    wed->setFont(font);
    thu->setFont(font);
    fri->setFont(font);
    sut->setFont(font);

    int** A = new int*[6];
    for(int i = 0; i < 6; ++ i){
        A[i] = new int[7];
    }

    cal_pieces = new calendar_piece*[42];


    A = calendar_curr_page(m_year_month->get_year(), m_year_month->get_month());
    m_vertical = new QVBoxLayout;
    m_horizontal = new QHBoxLayout;
    m_horizontal0 = new QHBoxLayout;
    m_horizontal1 = new QHBoxLayout;
    m_horizontal2 = new QHBoxLayout;
    m_horizontal3 = new QHBoxLayout;
    m_horizontal4 = new QHBoxLayout;
    m_horizontal5 = new QHBoxLayout;
    m_horizontal6 = new QHBoxLayout;

    m_horizontal0->addWidget(sun);
    m_horizontal0->addWidget(mon);
    m_horizontal0->addWidget(tue);
    m_horizontal0->addWidget(wed);
    m_horizontal0->addWidget(thu);
    m_horizontal0->addWidget(fri);
    m_horizontal0->addWidget(sut);




    bool flag = true;
    bool flag1 = true;
    QDateTime curr = QDateTime::currentDateTime();

    int count = 0;
    for(int i = 0; i < 7; ++ i){
        qDebug() << i;
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[0][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count] = new calendar_piece;
        cal_pieces[count]->timezones(m_all_timezones);
        connect(cal_pieces[count], SIGNAL(on_closed()), this, SLOT(on_date_changed()));
        cal_pieces[count]->set_event_manager(m_event_manager);
        if(A[0][i] == 1)
                flag = false;
        if(flag){
            cal_pieces[count]->set_is_on_in_this_month(true);
            curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month() - 1, A[0][i]));
            curr.setTime(QTime(23,59,59));
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        else{
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        cal_pieces[count]->set_date_day(QString("%1").arg(A[0][i]));
        m_horizontal1->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 7; ++ i){
        qDebug() << i;
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[1][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count] = new calendar_piece;
        cal_pieces[count]->timezones(m_all_timezones);
        cal_pieces[count]->set_date_time(curr);
        connect(cal_pieces[count], SIGNAL(on_closed()), this, SLOT(on_date_changed()));
        cal_pieces[count]->set_event_manager(m_event_manager);
        cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        cal_pieces[count]->set_date_day(QString("%1").arg(A[1][i]));
        m_horizontal2->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 7; ++ i){
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[2][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count] = new calendar_piece;
        cal_pieces[count]->timezones(m_all_timezones);
        cal_pieces[count]->set_date_time(curr);
        cal_pieces[count]->set_event_manager(m_event_manager);
        cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        connect(cal_pieces[count], SIGNAL(on_closed()), this, SLOT(on_date_changed()));
        cal_pieces[count]->set_events_ids(m_events_ids);
        cal_pieces[count]->set_date_day(QString("%1").arg(A[2][i]));
        m_horizontal3->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 7; ++ i){
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[3][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count] = new calendar_piece;
        cal_pieces[count]->timezones(m_all_timezones);
        cal_pieces[count]->set_date_time(curr);
        connect(cal_pieces[count], SIGNAL(on_closed()), this, SLOT(on_date_changed()));
        cal_pieces[count]->set_events_ids(m_events_ids);
        cal_pieces[count]->set_event_manager(m_event_manager);
        cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        cal_pieces[count]->set_date_day(QString("%1").arg(A[3][i]));
        m_horizontal4->addWidget(cal_pieces[count]);
        ++count;
    }


    flag = false;
    flag1 = false;

    for(int i = 0; i < 7; ++ i){
        cal_pieces[count] = new calendar_piece;
        cal_pieces[count]->timezones(m_all_timezones);
        connect(cal_pieces[count], SIGNAL(on_closed()), this, SLOT(on_date_changed()));
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[4][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_event_manager(m_event_manager);
        if(A[4][i] == 1){
            flag = true;
            flag1 = true;
        }
        if(flag){
            cal_pieces[count]->set_is_on_in_this_month(true);
            curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month() + 1, A[4][i]));
            curr.setTime(QTime(23,59,59));
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        else{
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        cal_pieces[count]->set_date_day(QString("%1").arg(A[4][i]));
        m_horizontal5->addWidget(cal_pieces[count]);
        ++count;
    }


    for(int i = 0; i < 7; ++ i){
        qDebug() << count;
        cal_pieces[count] = new calendar_piece;
        cal_pieces[count]->timezones(m_all_timezones);
        connect(cal_pieces[count], SIGNAL(on_closed()), this, SLOT(on_date_changed()));
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[5][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_date_time(curr);
        cal_pieces[count]->set_events_ids(m_events_ids);
        cal_pieces[count]->set_event_manager(m_event_manager);
        if(A[5][i] == 1)
            flag = true;
        if(flag || flag1){
            cal_pieces[count]->set_is_on_in_this_month(true);
            curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month() + 1, A[5][i]));
            curr.setTime(QTime(23,59,59));
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
            cal_pieces[count]->set_date_time(curr);
        }
        else{
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
            cal_pieces[count]->set_date_time(curr);
        }
        cal_pieces[count]->set_date_day(QString("%1").arg(A[5][i]));
        m_horizontal->addWidget(cal_pieces[count]);
        ++count;
    }

    qDebug() << "bl";
    for(int i = 0; i < 6; ++ i)
        delete A[i];
    delete[] A;

    m_horizontal6->addWidget(m_year_month);

    m_vertical->addLayout(m_horizontal6);
    m_vertical->addLayout(m_horizontal0);
    m_vertical->addLayout(m_horizontal1);
    m_vertical->addLayout(m_horizontal2);
    m_vertical->addLayout(m_horizontal3);
    m_vertical->addLayout(m_horizontal4);
    m_vertical->addLayout(m_horizontal5);
    m_vertical->addLayout(m_horizontal);

    m_calendar->setLayout(m_vertical);
    main = new QVBoxLayout;
    main->addWidget(m_calendar);
    setLayout(main);

    for(int i = 0; i < 42; ++ i){
        emit cal_pieces[i]->ended();
        qDebug() << i << "rd@";
    }
    flag = 1;
    connect(m_year_month, SIGNAL(date_changed()), this, SLOT(on_date_changed()));
}

void calendar::resizeEvent(QResizeEvent *e)
{
}



void calendar::on_date_changed()
{
    m_calendar->close();
//    for(int i = 0; i < 42; ++i){
//        cal_pieces[i]->delete_events();
//    }
    int count = 0;
    int**A = new int*[6];
    for(int i = 0; i < 6; ++ i){
        A[i] = new int[7];
    }

    A = calendar_curr_page(m_year_month->get_year(), m_year_month->get_month());



    bool flag = true;
    bool flag1 = true;
    QDateTime curr = QDateTime::currentDateTime();

    for(int i = 0; i < 7; ++ i){
        qDebug() << i;
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[0][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_event_manager(m_event_manager);
        if(A[0][i] == 1)
                flag = false;
        if(flag){
            cal_pieces[count]->set_is_on_in_this_month(true);
            curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month() - 1, A[0][i]));
            curr.setTime(QTime(23,59,59));
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        else{
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        cal_pieces[count]->set_date_day(QString("%1").arg(A[0][i]));
        m_horizontal1->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 7; ++ i){
        qDebug() << i;
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[1][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_date_time(curr);
        cal_pieces[count]->set_event_manager(m_event_manager);
        cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        cal_pieces[count]->set_date_day(QString("%1").arg(A[1][i]));
        m_horizontal2->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 7; ++ i){
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[2][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_date_time(curr);
        cal_pieces[count]->set_event_manager(m_event_manager);
        cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        cal_pieces[count]->set_events_ids(m_events_ids);
        cal_pieces[count]->set_date_day(QString("%1").arg(A[2][i]));
        m_horizontal3->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 7; ++ i){
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[3][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_date_time(curr);
        cal_pieces[count]->set_events_ids(m_events_ids);
        cal_pieces[count]->set_event_manager(m_event_manager);
        cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        cal_pieces[count]->set_date_day(QString("%1").arg(A[3][i]));
        m_horizontal4->addWidget(cal_pieces[count]);
        ++count;
    }


    flag = false;
    flag1 = false;

    for(int i = 0; i < 7; ++ i){
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[4][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_event_manager(m_event_manager);
        if(A[4][i] == 1){
            flag = true;
            flag1 = true;
        }
        if(flag){
            cal_pieces[count]->set_is_on_in_this_month(true);
            curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month() + 1, A[4][i]));
            curr.setTime(QTime(23,59,59));
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        else{
            cal_pieces[count]->set_date_time(curr);
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
        }
        cal_pieces[count]->set_date_day(QString("%1").arg(A[4][i]));
        m_horizontal5->addWidget(cal_pieces[count]);
        ++count;
    }


    for(int i = 0; i < 7; ++ i){
        qDebug() << count;
        curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month(), A[5][i]));
        curr.setTime(QTime(23,59,59));
        cal_pieces[count]->set_date_time(curr);
        cal_pieces[count]->set_events_ids(m_events_ids);
        cal_pieces[count]->set_event_manager(m_event_manager);
        if(A[5][i] == 1)
            flag = true;
        if(flag || flag1){
            cal_pieces[count]->set_is_on_in_this_month(true);
            curr.setDate(QDate(m_year_month->get_year(), m_year_month->get_month() + 1, A[5][i]));
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
            cal_pieces[count]->set_date_time(curr);
        }
        else{
            cal_pieces[count]->set_list(m_event_manager->get_events_for_day(curr));
            cal_pieces[count]->set_date_time(curr);
        }
        cal_pieces[count]->set_date_day(QString("%1").arg(A[5][i]));
        m_horizontal->addWidget(cal_pieces[count]);
        ++count;
    }

    for(int i = 0; i < 42; ++ i){
        qDebug() << i << "rrd@";
        emit cal_pieces[i]->ended();
    }


    for(int i = 0; i < 6; ++ i)
        delete[] A[i];
    delete[] A;
    m_calendar->show();

}

void calendar::delete_events()
{
    QSqlQuery res1("DELETE FROM calendar_events");
}
