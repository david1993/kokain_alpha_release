#include "user_dialog_box.h"

using namespace kokain::calendar_namespace;

user_dialog_box::user_dialog_box()
{
    initilize_();
}

void user_dialog_box::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
    setFixedSize(220,220);
}

void user_dialog_box::create_widgets_()
{
    m_user_name = new QLabel("User name",this);
    m_user_picture = new QLabel("Picture", this);

    m_user_name_edit = new QLineEdit(this);
    m_user_picture_edit = new QLineEdit(this);

    m_ok = new QPushButton("Ok",this);
    m_cancel = new QPushButton("Cancel", this);
    m_browse = new QPushButton("Browse", this);
}

void user_dialog_box::setup_layout_()
{
    QHBoxLayout* buttons = new QHBoxLayout;

    buttons->addWidget(m_ok);
    buttons->addWidget(m_cancel);

    QVBoxLayout* task_parameters = new QVBoxLayout;

    task_parameters->addWidget(m_user_name);
    task_parameters->addWidget(m_user_picture);

    QVBoxLayout* task_parameters_edit = new QVBoxLayout;

    QHBoxLayout* picture = new QHBoxLayout;
    picture->addWidget(m_user_picture_edit);
    picture->addWidget(m_browse);

    task_parameters_edit->addWidget(m_user_name_edit);
    task_parameters_edit->addLayout(picture);

    QHBoxLayout* all = new QHBoxLayout;

    all->addLayout(task_parameters);
    all->addLayout(task_parameters_edit);

    QVBoxLayout* main = new QVBoxLayout;
    main->addLayout(all);
    main->addLayout(buttons);

    setLayout(main);
}

void user_dialog_box::make_connections_()
{
    connect(m_ok,SIGNAL(clicked()),this,SLOT(my_accept()));
    connect(m_cancel,SIGNAL(clicked()),this,SLOT(close()));
    connect(m_browse, SIGNAL(clicked()),this,SLOT(browse_picture()));
}

QString user_dialog_box::get_user_name()
{
    return m_user_name_edit->text();
}

QString user_dialog_box::get_picture()
{
    return m_user_picture_edit->text();
}

void user_dialog_box::my_accept()
{
    emit ok_clicked();
    this->close();
}

void user_dialog_box::browse_picture()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if ( !fileName.isEmpty() ){
        m_user_picture_edit->setText(fileName);
    }
}
