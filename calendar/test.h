#ifndef TEST_H
#define TEST_H

#include <QtWidgets>

#include "dragwidget.h"
#include "../core/user.h"
#include "user_dialog_box.h"

class QPushButton;
class QVBoxLayout;
class QSignalMapper;

namespace kokain
{
namespace calendar_namespace
{
class user_dialog_box;
class DragWidget;

class test : public QWidget
{
  Q_OBJECT

public:
   explicit test(QWidget *parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QPushButton* m_add;
    user_dialog_box* m_set_params;
    DragWidget* m_drag;
    DragWidget* m_drag_test;
    QVBoxLayout* m_main;
    FlowLayout* m_teams;
    QSignalMapper* m_data;
    QSignalMapper* m_signalMapper;
    QVector<user_data*> m_user_data;
    QVector<core::user*> m_user;
    int m_curr;
    int m_team_num;


public slots:
    void add_clicked();
    void data_back_clicked();
    void set();
    void data_showed(int i);
};

}//namespace calendar_namespace
}//namespace kokain

#endif // TEST_H
