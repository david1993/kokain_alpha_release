#include "timezone.h"

using namespace kokain::calendar_namespace;

event_timezone::event_timezone(QWidget *w) : QWidget(w)
{
    initilize_();
}

void event_timezone::initilize_()
{
    create_widgets_();
    make_connections_();
    setup_layouts_();
}

void event_timezone::create_widgets_()
{

}

void event_timezone::setup_layouts_()
{
    QHBoxLayout* main = new QHBoxLayout;
    main->addWidget(&m_all_timezones);
    setLayout(main);
}

void event_timezone::make_connections_()
{

}

QTimeZone event_timezone::get_time_zone()
{
    QString curr_timezone = m_all_timezones.currentText();
    QTimeZone current;
    foreach (current_timezone cur, m_timezones_list) {
        if(cur.get_name() == curr_timezone)
            current = QTimeZone(QTimeZone::availableTimeZoneIds(cur.get_from_all()).at(cur.get_from_current()));
    }
    return current;
}

void event_timezone::timezones(QList<current_timezone> list)
{
    m_timezones_list = list;
    foreach (current_timezone str, m_timezones_list) {
        m_all_timezones.addItem(str.get_name());
    }
}
