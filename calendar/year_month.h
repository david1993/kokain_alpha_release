#ifndef YEAR_MONTH_H
#define YEAR_MONTH_H

#include <QtWidgets>

#include "year.h"

class QLabel;

namespace kokain
{
namespace calendar_namespace
{

class year_month:public QWidget
{
    Q_OBJECT

public:
    explicit year_month(QWidget* parent = 0);

private:
    void create_widgets_();
    void make_connections_();
    void setup_layout_();
    void initilize_();

private:
    QLabel* m_curr_day;
    QLabel* m_curr_weekday;
    year* m_year;
    QComboBox* m_month;
    QPushButton* m_next_year;
    QPushButton* m_prev_year;

public:
    void set_curr_day(int);
    void set_curr_weekday(int);
    QString get_curr_day();
    QString get_curr_weekday();
    int get_year();
    int get_month();

public slots:
    void prev_year();
    void next_year();
    void on_date_changed();

signals:
    void date_changed();
};

}//namesoace calendar
}//namespace kokain

#endif // YEAR_MONTH_H
