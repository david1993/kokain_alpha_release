#include "more_events.h"

#include <QSqlQuery>

using namespace kokain::calendar_namespace;

more_events::more_events(QWidget *parent):QWidget(parent)
{
    initilize_();
}

void more_events::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void more_events::create_widgets_()
{
    m_cur_event = new cal_event;
}

void more_events::setup_layout_()
{
    m_main_layout = new QVBoxLayout;
    setLayout(m_main_layout);
}

void more_events::make_connections_()
{
    connect(this, SIGNAL(event_created()), this, SLOT(closed()));
    connect(this, SIGNAL(fiinsh_signal()), this, SLOT(closed_other()));
}

void more_events::event_created_signal()
{
    emit event_created();
}

void more_events::add_event(cal_event *curr_event)
{
    m_events_list.push_back(curr_event);
    m_main_layout->addWidget(curr_event);
}

cal_event* more_events::get_event()
{
    return m_events_list.back();
}

cal_event* more_events::curr_event(int i)
{
    return m_events_list.at(i);
}

bool more_events::is_empty()
{
    return m_events_list.count() == 0;
}


int more_events::count()
{
    return m_events_list.count();
}


void more_events::closed()
{
    if(!m_deleteable.empty()){
        foreach (cal_event* cur, m_deleteable) {
            cur->close();
        }
    }
    m_deleteable.clear();
        int count = 0;
        QList<cal_event*>::iterator i;
        for(i = m_events_list.begin(); i != m_events_list.end(); ++ i){
            count ++;
            if(count >= 4){
                connect((*i),SIGNAL(is_closed()), this, SLOT(closed()));
                (*i)->timezone(m_all_timezones);
                m_main_layout->addWidget((*i));
                m_deleteable.push_back(*i);
                (*i)->show();
            }
        }
    emit on_closed();
}

void more_events::closed_other()
{
    if(!m_deleteable.empty()){
        foreach (cal_event* cur, m_deleteable) {
            cur->close();
        }
    }
    m_deleteable.clear();
    int count = 0;
    QList<cal_event*>::iterator i;
    for(i = m_events_list.begin(); i != m_events_list.end(); ++ i){
        count ++;
        if(count >= 4){
            connect((*i),SIGNAL(is_closed()), this, SLOT(closed()));
            m_main_layout->addWidget((*i));
            m_deleteable.push_back(*i);
            (*i)->show();
        }
    }
}

void more_events::set_list(QList<cal_event*> list)
{
    m_events_list = list;
}

QList<cal_event*> more_events::get_list()
{
    return m_events_list;
}

void more_events::set_date_time(QDateTime day)
{
    m_date_time = day;
}

void more_events::timezone(QList<current_timezone> list)
{
    m_all_timezones = list;
}
