#include "calendar_piece.h"

using namespace kokain::calendar_namespace;

calendar_piece::calendar_piece(QWidget *parent):QLabel(parent)
{
    initilize_();
    setMouseTracking(true);
    m_mouse_move = false;
    flag = true;
}

void calendar_piece::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
}

void calendar_piece::create_widgets_()
{
    m_event = new dialog_box(this);
    m_more_events = new more_events();
    //m_more_events->set_date_time(m_date_time);
    m_not_in_this_month = false;
    q = new kokain::db::query(kokain::db::query::INSERT, "calendar_events");
    qDebug() << "1";
}

void calendar_piece::setup_layout_()
{

}

void calendar_piece::make_connections_()
{
    connect(m_event, SIGNAL(ok_clicked()), this, SLOT(event_created()));
    connect(this, SIGNAL(ended()), this, SLOT(more_closed()));
    connect(m_more_events, SIGNAL(on_closed()), this, SLOT(closed()));
}

void calendar_piece::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    QFont font(painter.font());
    font.setBold(true);
    font.setPointSize((width() + height())/6);
    painter.setFont(font);
    if(m_mouse_move)
        painter.fillRect(rect(),QBrush("#E8E8E8"));
    else
        painter.fillRect(rect(), Qt::white);
    if(m_not_in_this_month){
        QPen penHText(QColor("gray"));
        painter.setPen(penHText);
        painter.drawText(QRect(rect().left(), rect().top() + 5, rect().width(), rect().height() + 5),Qt::AlignCenter, m_date_day);
    }
    else
        painter.drawText(QRect(rect().left(), rect().top() + 5, rect().width(), rect().height() + 5),Qt::AlignCenter, m_date_day);


        int j = 1;
        QMutableListIterator<cal_event*> i(m_events_list);
        while (i.hasNext()) {
            painter.setBrush(QBrush(QColor(QString(i.next()->get_color()))));
            painter.drawRect(0, height() / 15 * (j - 1) + 3*(j - 1) + 2, width()/2, height() / 15 + 1);
            j++;
            if(j == 4)
                break;
        }

        j = 1;
        painter.setBrush(QBrush("black"));
        font.setPointSize(7);
        painter.setFont(font);
        QString S;
        QString S2 = "...";
        QString S1;

        QMutableListIterator<cal_event*> k(m_events_list);
        QFontMetrics fm(font);

        while (k.hasNext()) {
            S = k.next()->get_name();

            S1 = fm.elidedText(S, Qt::ElideRight, width() / 2);

            painter.drawText(QRect(width()/2 + 1, height() / 15 * (j - 1) + 3*(j - 1) +  1, width()/2 , height() / 15 + 4),Qt::AlignLeft, S1);

            j++;
            if(j == 4)
                break;
        }
        if(m_events_list.size() > 3){
            S1 = "More";
            font.setBold(false);
            font.setPixelSize(8.5);
            painter.setFont(font);
            painter.drawText(QRect(0, height() / 15 * 3 + 11, width()/2, height() / 15 + 1), Qt::AlignLeft, S1);
        }


    /*painter.setBrush(QBrush("yellow"));
    painter.drawEllipse(width() - height() / 10, height() / 60, height() / 15, height() / 15);*/
}



void calendar_piece::enterEvent(QEvent *e)
{
    m_mouse_move = true;
    repaint();
}

void calendar_piece::leaveEvent(QEvent *e)
{
    m_mouse_move = false;
    repaint();
}

void calendar_piece::mousePressEvent(QMouseEvent *e)
{
    m_event->move(this->pos().x() + this->width()/2 + m_event->width(),this->pos().y());

    if(m_events_list.size() == 0){
        m_event->set_event_color(QString(""));
        m_event->set_event_name(QString(""));
        m_event->set_start_date(QDateTime::currentDateTime());
        m_event->set_end_date(QDateTime::currentDateTime());
        m_event->show();
        return;
    }
    else{
        if(m_events_list.size() == 1){
            if(e -> y() <= height()/15 + 3 && e -> y() >= 2){
                (m_events_list)[0]->timezone(m_all_timezones);
                (m_events_list)[0]->set_property_name((m_events_list)[0]->get_name());
                (m_events_list)[0]->set_property_color((m_events_list)[0]->get_color());
                (m_events_list)[0]->set_property_start_date((m_events_list)[0]->get_start_date());
                (m_events_list)[0]->set_property_end_date((m_events_list)[0]->get_end_date());
                (m_events_list)[0]->show_properties();
                connect((m_events_list)[0], SIGNAL(is_closed()), this, SLOT(closed()));
                return;
            }
            else{
                m_event->set_event_color(QString(""));
                m_event->set_event_name(QString(""));
                m_event->set_start_date(QDateTime::currentDateTime());
                m_event->set_end_date(QDateTime::currentDateTime());
                m_event->show();
                return;
            }
        }
        if(m_events_list.size() == 2){
            if(e -> y() <= height()/15 + 3 && e -> y() >= 2){
                (m_events_list)[0]->timezone(m_all_timezones);
                (m_events_list)[0]->set_property_name((m_events_list)[0]->get_name());
                (m_events_list)[0]->set_property_color((m_events_list)[0]->get_color());
                (m_events_list)[0]->set_property_start_date((m_events_list)[0]->get_start_date());
                (m_events_list)[0]->set_property_end_date((m_events_list)[0]->get_end_date());
                (m_events_list)[0]->show_properties();
                connect((m_events_list)[0], SIGNAL(is_closed()), this, SLOT(closed()));
                return;
            }
            if(e -> y() >= height()/15 + 5 && e -> y() <= 2*height()/15 + 6){
                (m_events_list)[1]->timezone(m_all_timezones);
                (m_events_list)[1]->set_property_name((m_events_list)[1]->get_name());
                (m_events_list)[1]->set_property_color((m_events_list)[1]->get_color());
                (m_events_list)[1]->set_property_start_date((m_events_list)[1]->get_start_date());
                (m_events_list)[1]->set_property_end_date((m_events_list)[1]->get_end_date());
                qDebug() << (m_events_list)[1]->get_timezone();
                (m_events_list)[1]->show_properties();
                connect((m_events_list)[1], SIGNAL(is_closed()), this, SLOT(closed()));
                return;
            }
            else{
                m_event->set_event_color(QString(""));
                m_event->set_event_name(QString(""));
                m_event->set_start_date(QDateTime::currentDateTime());
                m_event->set_end_date(QDateTime::currentDateTime());
                m_event->show();
                return;
            }
        }
        if(m_events_list.size() >= 3){
            if(e -> y() <= height()/15 + 3 && e -> y() >= 2){
                (m_events_list)[0]->timezone(m_all_timezones);
                (m_events_list)[0]->set_property_name((m_events_list)[0]->get_name());
                (m_events_list)[0]->set_property_color((m_events_list)[0]->get_color());
                (m_events_list)[0]->set_property_start_date((m_events_list)[0]->get_start_date());
                (m_events_list)[0]->set_property_end_date((m_events_list)[0]->get_end_date());
                (m_events_list)[0]->show_properties();
                connect((m_events_list)[0], SIGNAL(is_closed()), this, SLOT(closed()));
                return;
            }
            if(e -> y() >= height()/15 + 5 && e -> y() <= 2 * height()/15 + 6){
                (m_events_list)[1]->timezone(m_all_timezones);
                (m_events_list)[1]->set_property_name((m_events_list)[1]->get_name());
                (m_events_list)[1]->set_property_color((m_events_list)[1]->get_color());
                (m_events_list)[1]->set_property_start_date((m_events_list)[1]->get_start_date());
                (m_events_list)[1]->set_property_end_date((m_events_list)[1]->get_end_date());
                (m_events_list)[1]->show_properties();
                connect((m_events_list)[1], SIGNAL(is_closed()), this, SLOT(closed()));
                return;
            }
            if(e -> y() >= 2 * height()/15 + 8 && e -> y() <= 3*height()/15 + 9){
                (m_events_list)[2]->timezone(m_all_timezones);
                (m_events_list)[2]->set_property_name((m_events_list)[2]->get_name());
                (m_events_list)[2]->set_property_color((m_events_list)[2]->get_color());
                (m_events_list)[2]->set_property_start_date((m_events_list)[2]->get_start_date());
                (m_events_list)[2]->set_property_end_date((m_events_list)[2]->get_end_date());
                (m_events_list)[2]->show_properties();
                connect((m_events_list)[2], SIGNAL(is_closed()), this, SLOT(closed()));
                return;
            }
            if(e -> y() >= 3 * height()/15 + 11 && e -> y() <= 4 * height()/15 + 11){
                m_more_events->set_list(m_events_list);
                m_more_events->event_created_signal();
                m_more_events->show();
                return;
            }
            else{
                m_event->set_event_color(QString(""));
                m_event->set_event_name(QString(""));
                m_event->set_start_date(QDateTime::currentDateTime());
                m_event->set_end_date(QDateTime::currentDateTime());
                m_event->show();
                return;
            }
        }
    }
}

void calendar_piece::event_created()
{
    cur_event = new cal_event();
    cur_event->timezone(m_all_timezones);
    cur_event->set_name(m_event->get_event_name());
    cur_event->set_color(m_event->get_color());
    cur_event->set_timezone(m_event->get_timezone());
    cur_event->set_start_date(m_event->get_start_date());
    cur_event->set_end_date(m_event->get_end_date());
    //connect(cur_event, SIGNAL(is_closed()), this, SLOT(closed()));
//    int size = cur_event->get_start_date().date().daysTo(cur_event->get_end_date().date()) + 1;
//        q = new query(query_type::INSERT, "calendar_events");
//        q->setId(cur_event->id());
//        q->setValue(cur_event->serialize_to_json());
//        res = manager1->execute(*q);
    connect(cur_event, SIGNAL(is_closed()), this, SLOT(closed()));
    m_event_manager->add_event(cur_event);
    cur_event->set_event_manager(m_event_manager);
//        if(flag){
//        m_more_events->set_date_time(m_date_time);
//        connect(m_more_events, SIGNAL(on_closed()), this, SLOT(closed()));
//    }
    flag = false;
    emit on_closed();
    repaint();
}

//void calendar_piece::add_event(cal_event *eventik)
//{
//    eventik->set_list(m_events_list);
//    m_events_list->push_back(eventik);
//    if(flag){
//        m_more_events = new more_events(m_events_list);
//        m_more_events->set_date_time(m_date_time);
//        connect(m_more_events, SIGNAL(on_closed()), this, SLOT(closed()));
//    }
//    flag = false;

//    QMutableListIterator<cal_event*> i(*m_events_list);
//       while (i.hasNext()) {
//           i.next()->set_list(m_events_list);
//       }
//    repaint();
//}

void calendar_piece::closed()
{
    //m_more_events->set_list(m_events_list);
    //m_more_events->event_created_signal();

    emit on_closed();
    m_more_events->set_list(m_events_list);
    emit m_more_events->fiinsh_signal();
    repaint();
}

void calendar_piece::more_closed()
{
   // m_more_events->set_list(m_events_list);
    //m_more_events->event_created_signal();
    repaint();
}

void calendar_piece::set_list(QList<cal_event*> list)
{
    QList<cal_event*>::iterator i;
//    for(i = m_events_list.begin(); i != m_events_list.end(); ++ i){
//        delete (*i);
//    }

    m_events_list = list;
    for(i = m_events_list.begin(); i != m_events_list.end(); ++ i){
        (*i)->set_event_manager(m_event_manager);
    }
}

void calendar_piece::set_date_day(const QString & str)
{
    m_date_day = str;
}

void calendar_piece::set_is_on_in_this_month(bool flag)
{
   m_not_in_this_month = flag;
}

void calendar_piece::set_events_ids(qint64& ids)
{
    m_events_ids = &ids;
}

void calendar_piece::delete_events()
{
    QList<cal_event*>::iterator i;
    for(i = m_events_list.begin(); i != m_events_list.end(); ++ i){
        delete (*i);
    }
    m_events_list.clear();
    //m_more_events->set_list(m_events_list);
}

void calendar_piece::set_date_time(QDateTime date_time)
{
    m_date_time = date_time;
}

void calendar_piece::set_event_manager(event_manager * manager)
{
    m_event_manager = manager;
}

void calendar_piece::timezones(QList<current_timezone> list)
{
    m_all_timezones = list;
    m_event->timezone(list);
    m_more_events->timezone(list);
}
