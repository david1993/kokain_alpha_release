#ifndef USER_DIALOG_BOX_H
#define USER_DIALOG_BOX_H

#include <QtWidgets>

class QLabel;
class QLineEdit;
class QPushButton;

namespace kokain
{
namespace calendar_namespace
{

class user_dialog_box : public QDialog
{
    Q_OBJECT
public:
    user_dialog_box();

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QLabel* m_user_name;

public:
    QString get_user_name();

private:
    QLabel* m_user_picture;

public:
    QString get_picture();

private:
    QLineEdit* m_user_name_edit;
    QLineEdit* m_user_picture_edit;
    QPushButton* m_ok;
    QPushButton *m_cancel;
    QPushButton *m_browse;

public slots:
    void my_accept();
    void browse_picture();

signals:
     void ok_clicked();
};

}//namespace calendar_namespace
}//namespace kokain

#endif // USER_DIALOG_BOX_H
