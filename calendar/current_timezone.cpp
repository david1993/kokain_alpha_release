#include "current_timezone.h"

using namespace kokain::calendar_namespace;

current_timezone::current_timezone()
{
    m_name = "";
    m_from_all = 0;
    m_from_current = 0;
}

void current_timezone::set_from_all(int from_all){
    m_from_all = from_all;
}

void current_timezone::set_from_current(int from_current){
    m_from_current = from_current;
}

void current_timezone::set_name(QString name){
    m_name = name;
}

int current_timezone::get_from_all()
{
    return m_from_all;
}

int current_timezone::get_from_current()
{
    return m_from_current;
}

QString current_timezone::get_name() const
{
   return m_name;
}

//bool current_timezone::operator ==(const current_timezone &s) const
//{
//    return m_name == s.get_name();
//}

//bool current_timezone::operator >=(const current_timezone &s) const
//{
//    return m_name >= s.get_name();
//}

//bool current_timezone::operator >(const current_timezone &s) const
//{
//    return m_name > s.get_name();
//}

//bool current_timezone::operator <(const current_timezone &s) const
//{
//    return m_name < s.get_name();
//}

//bool current_timezone::operator <=(const current_timezone &s) const
//{
//    return get_name() <= s.get_name();
//}

//bool current_timezone::operator !=(const current_timezone &s) const
//{
//    return m_name != s.get_name();
//}
