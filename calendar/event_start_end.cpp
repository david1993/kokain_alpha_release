#include "event_start_end.h"

using namespace kokain::calendar_namespace;

event_start_end::event_start_end(QWidget *parent):QWidget(parent)
{
    initilize_();
    //setFixedSize(430,40);
}

void event_start_end::initilize_()
{
    create_widgets_();
    setup_layouts_();
    make_connections_();
}

void event_start_end::create_widgets_()
{
   m_start_date = new QDateTimeEdit;
   m_end_date = new QDateTimeEdit;
   m_start_date->setDateRange(QDate(1800, 1, 1), QDate(8000, 12, 31));
   m_start_date->setDisplayFormat("dddd/ MMMM dd/ yyyy/  hh:mm");
   m_end_date->setDisplayFormat("dddd/ MMMM dd/ yyyy/  hh:mm");
   m_end_date->setDateRange(QDate(1800, 1, 1), QDate(8000, 12, 31));
   setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

}

void event_start_end::setup_layouts_()
{
    QHBoxLayout* main = new QHBoxLayout;
    main->addWidget(m_start_date);
    QLabel* to = new QLabel("to");
    main->addWidget(to);
    main->addWidget(m_end_date);

    setLayout(main);
}

void event_start_end::make_connections_()
{

}

int event_start_end::get_start_year()
{
    return m_start_date->date().year();
}

int event_start_end::get_start_month()
{
    return m_start_date->date().month();
}

QTime event_start_end::get_start_time()
{
    return m_start_date->dateTime().time();
}

int event_start_end::get_end_year()
{
    return m_end_date->date().year();
}

int event_start_end::get_end_month()
{
    return m_end_date->date().month();
}

QTime event_start_end::get_end_time()
{
    return m_end_date->dateTime().time();
}

void event_start_end::set_start_date(QDateTime start)
{
    m_start_date->setDateTime(start);
}

void event_start_end::set_end_date(QDateTime end)
{
    m_end_date->setDateTime(end);
}

QDateTime event_start_end::get_end_date()
{
    return m_end_date->dateTime();
}

QDateTime event_start_end::get_start_date()
{
    return m_start_date->dateTime();
}
