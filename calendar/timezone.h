#ifndef TIMEZONE_H
#define TIMEZONE_H

#include <QtWidgets>

#include "current_timezone.h"

namespace kokain
{
namespace calendar_namespace
{

class event_timezone : public QWidget
{
    Q_OBJECT

private:
    void initilize_();
    void create_widgets_();
    void make_connections_();
    void setup_layouts_();

public:
   explicit event_timezone(QWidget* w = 0);

private:
    QComboBox m_all_timezones;
    QList<current_timezone> m_timezones_list;
    //timezon@ drvuma tvyal eventi hamar aysinqn ira jamketi koxxq@ grvuma te eti @st vor timezonia grvac, aysinqn ete
    //draca americayi timezonov u ok em sxmum im mot petqa calendari vra mi or tarberutyab cuyc ta, nuyn@
    //myus userneri hamara arvum

public:
    QTimeZone get_time_zone();
    void timezones(QList<current_timezone>);
};

}//namespace calendar_namespace
}//namespace kokain

#endif // TIMEZONE_H
