#include "multiple_labels.h"

#include "label_edit.h"

using namespace kokain::calendar_namespace;

multiple_labels::multiple_labels(QWidget *parent):QWidget(parent)
{
    initilize_();
}


void multiple_labels::initilize_()
{
     create_widgets_();
     setup_layout_();
     make_connections_();
}

void multiple_labels::create_widgets_()
{
    m_add = new QPushButton("+", this);
    m_add->setFixedSize(20,20);
    m_more = new QPushButton("More", this);
    m_more->setFixedSize(40,20);
    m_more->close();
    m_more_labels = new more_labels;
    m_this_count = 0;
    m_is_link = false;
    m_opened = false;
    m_timer = new QTimer(this);
    setFixedSize(620,37);
}

void multiple_labels::make_connections_()
{
    connect(m_add, SIGNAL(clicked()), this, SLOT(add_clicked()));
    connect(m_more, SIGNAL(clicked()), this, SLOT(more_labels_show()));
    connect(m_timer, SIGNAL(timeout()), this, SLOT(count_changed()));
    m_timer->start(10);
}

void multiple_labels::setup_layout_()
{
    m_labels = new QHBoxLayout;

    QHBoxLayout* buttons = new QHBoxLayout;

    buttons->addWidget(m_more,0,Qt::AlignLeft);
    buttons->addWidget(m_add,0,Qt::AlignLeft);


    QHBoxLayout* main = new QHBoxLayout;
    main->addLayout(m_labels);
    main->addLayout(buttons);

    setLayout(main);
}

void multiple_labels::add_clicked()
{
    m_curr_label = new label();
    if(m_is_link)
        m_curr_label->set_is_link(true);
    if(m_this_count < 2){
        m_this_count ++;
        m_labels->addWidget(m_curr_label);
        connect(m_curr_label, SIGNAL(closed()), this, SLOT(closed()));
        connect(m_curr_label, SIGNAL(enter_click()), this, SLOT(any_enter_clicked()));
        connect(m_curr_label, SIGNAL(other_enter_clicked()),this, SLOT(other_enter()));
    }
    else{
        m_labels->addWidget(m_curr_label);
        connect(m_curr_label, SIGNAL(enter_click()), this, SLOT(enter_clicked()));
        connect(m_curr_label, SIGNAL(enter_click()), this, SLOT(any_enter_clicked()));
        connect(m_curr_label, SIGNAL(other_enter_clicked()),this, SLOT(other_enter()));
        m_add->disconnect();
    }
}

void multiple_labels::closed()
{
    int i;
    for(i = 0; i < m_more_labels->count();  i ++){
        if(!m_more_labels->curr_label(i)->isHidden())
            break;
    }

    if(!m_more_labels->is_empty() && i != m_more_labels->count()){
        label* curr = new label;
        curr = m_more_labels->get_label();
        if(curr->isHidden()){
            while (curr->isHidden() && !m_more_labels->is_empty()) {
                curr = m_more_labels->get_label();
                if(!curr->isHidden()){
                    connect(curr , SIGNAL(closed()), this, SLOT(closed()));
                    m_all_labels.push_back(curr);
                    m_labels->addWidget(curr);
                                      break;
                }
            }
        }
        else{
            connect(curr , SIGNAL(closed()), this, SLOT(closed()));
            m_all_labels.push_back(curr);
            m_labels->addWidget(curr);
        }
    }
    else{
        m_this_count --;
        m_more->close();
    }

    for(int i = 0; i < m_all_labels.size(); ++i){
        if(m_all_labels.at(i)->isHidden()){
            m_all_labels.removeAt(i);
            qDebug() << i;
        }
    }
    emit all_labels_changed();
}

void multiple_labels::deleted()
{
    qDebug() << "blablabla";
    for(int i = 0; i < m_all_labels.size(); ++i){
        if(m_all_labels.at(i)->isHidden()){
            m_all_labels.removeAt(i);
            qDebug() << i;
        }
    }
    emit all_labels_changed();
}

void multiple_labels::count_changed()
{
    if(m_this_count < 2)
        m_more->close();
    if(m_more_labels->is_empty())
        m_more->close();
    else{
        int i;
        for(i = 0; i < m_more_labels->count();  i ++){
            if(!m_more_labels->curr_label(i)->isHidden())
                break;
        }
        if(i == m_more_labels->count())
            m_more->close();
    }
}

void multiple_labels::enter_clicked()
{
    m_more_labels->add_label(m_curr_label);
    connect(m_curr_label, SIGNAL(closed()), this, SLOT(deleted()));
    m_more->show();
    connect(m_add, SIGNAL(clicked()), this, SLOT(add_clicked()));
}

void multiple_labels::any_enter_clicked()
{
    m_all_labels.push_back(m_curr_label);
    for(int i = 0; i < m_all_labels.size(); ++i){
        if(m_all_labels.at(i)->isHidden()){
            delete m_all_labels.at(i);
            m_all_labels.removeAt(i);
        }
    }
    emit all_labels_changed();
}

void multiple_labels::other_enter()
{
    emit all_labels_changed();
}

void multiple_labels::more_labels_show()
{
    if(!m_opened || !m_more_labels->isVisible()){
        m_more_labels->show();
        m_opened = true;
    }
    else{
        m_more_labels->close();
        m_opened = false;
    }
}

void multiple_labels::set_is_link(bool flag)
{
    m_is_link = flag;
}

QVector<label *> multiple_labels::get_all_labels()
{
    return m_all_labels;
}

void multiple_labels::set_option(QVector<QString> option)
{

    for(int i = 0; i < option.size(); ++i){
        m_curr_label = new label();
        m_curr_label->set_label(option.at(i));
        m_curr_label->m_link_show();
        m_all_labels.push_back(m_curr_label);


        if(m_is_link)
            m_curr_label->set_is_link(true);
        if(m_this_count < 2){
            m_this_count ++;
            m_labels->addWidget(m_curr_label);
            connect(m_curr_label, SIGNAL(closed()), this, SLOT(closed()));
            connect(m_curr_label, SIGNAL(other_enter_clicked()),this, SLOT(other_enter()));
            emit m_curr_label->enter_click();
        }
        else{
            m_labels->addWidget(m_curr_label);
            connect(m_curr_label, SIGNAL(enter_click()), this, SLOT(enter_clicked()));
            connect(m_curr_label, SIGNAL(other_enter_clicked()),this, SLOT(other_enter()));
            emit m_curr_label->enter_click();
        }
    }
}

