#ifndef MULTIPLE_LABELS_H
#define MULTIPLE_LABELS_H

#include <QtWidgets>

#include "more_labels.h"

class QHBoxLayout;
class QPushButton;

namespace kokain
{
namespace calendar_namespace
{

class multiple_labels:public QWidget
{
    Q_OBJECT

public:
    explicit  multiple_labels(QWidget* parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QHBoxLayout* m_labels;
    QPushButton* m_add;
    QPushButton* m_more;
    more_labels* m_more_labels;
    label* m_curr_label;
    QTimer *m_timer;
    QVector<label*> m_all_labels;
    int m_count;
    int m_this_count;
    bool m_opened;
    bool m_is_link;

public:
    QVector<label *> get_all_labels();
    void set_option(QVector<QString>);

public slots:
    void add_clicked();
    void closed();
    void count_changed();
    void enter_clicked();
    void any_enter_clicked();
    void more_labels_show();
    void set_is_link(bool flag);
    void other_enter();
    void deleted();

signals:
    void all_labels_changed();
};

}//namespace calendar_namespace
}//namespace kokain

#endif // MULTIPLE_LABELS_H
